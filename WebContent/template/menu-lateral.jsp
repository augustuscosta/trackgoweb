<ul id="menu-lateral">
	<li><a href="${ctx}/mapa" class="menu-lateral"><img src="${ctx}/images/ocorrencia.png" class="icon"><div class="menu-text">Mapa</div></a></li>
	<li><a href="${ctx}/mapa_pesquisa" class="menu-lateral"><img src="${ctx}/images/globe.png" class="icon"><div class="menu-text">Mapa de pesquisa</div></a>
	<li>
		<a href="" class="menu-lateral"><img src="${ctx}/images/escala.png" class="icon"><div class="menu-text">Relat�rios</div></a>
	<ul id="sub-menu-lateral">
		<li><a href="${ctx}/relatorio_eventos" class="menu-lateral"><img src="${ctx}/images/relatoriodeeventos.png" class="icon"><div class="menu-text">Relat�rio eventos</div></a></li> 
		<li><a href="${ctx}/relatorio_path" class="menu-lateral"><img src="${ctx}/images/relatoriodeposicao.png" class="icon"><div class="menu-text">Relat�rio posi��o</div></a></li> 
		<li><a href="${ctx}/relatorio_permanecia_cerca" class="menu-lateral"><img src="${ctx}/images/nacerca.png" class="icon"><div class="menu-text">Tempo em cerca</div></a></li> 
		<li><a href="${ctx}/relatorio_permanecia_fora_cerca" class="menu-lateral"><img src="${ctx}/images/foradacerca.png" class="icon"><div class="menu-text">Tempo fora da cerca</div></a></li> 
		<li><a href="${ctx}/relatorio_permanecia_rota" class="menu-lateral"><img src="${ctx}/images/narota.png" class="icon"><div class="menu-text">Tempo em rota</div></a></li> 
		<li><a href="${ctx}/relatiorios_relatorio_permanecia_fora_rota"" class="menu-lateral"><img src="${ctx}/images/foraderota.png" class="icon"><div class="menu-text">Tempo fora da rota</div></a></li> 
		<li><a href="${ctx}/relatorio_passagem_ponto" class="menu-lateral"><img src="${ctx}/images/relatoriodeposicao.png" class="icon"><div class="menu-text">Relat�rio de pontos</div></a></li>
		<li><a href="${ctx}/relatorio_sem_posicionar" class="menu-lateral"><img src="${ctx}/images/relatoriodepontos.png" class="icon"><div class="menu-text">Sem comunica��o</div></a></li> 
		<li><a href="${ctx}/relatorio_tempo_inicao" class="menu-lateral"><img src="${ctx}/images/relatoriodeingnicao.png" class="icon"><div class="menu-text">Tempo Ligado</div></a></li> 
		<li><a href="${ctx}/relatorio_marcha_lenta" class="menu-lateral"><img src="${ctx}/images/relatoriodeparado.png" class="icon"><div class="menu-text">Marcha Lenta</div></a></li> 
	</ul>
	<li>
</ul>
	<div id="logout"><a href="<c:url value="/j_logout"/>" class="menu-lateral"><img src="${ctx}/images/quit.png" class="icon2"><div class="menu-text">Sair</div></a></div>

<script>
$( "#menu-lateral" ).menu();
</script>