
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/template/jstl.jsp"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
<link rel="stylesheet" href="${ctx}/css/index.css"></link>
<script src="https://cdn.socket.io/socket.io-1.0.6.js"></script> 
<link rel="stylesheet" href="${ctx}/css/jquery-ui-1.9.2.custom.css"></link>
<link rel="stylesheet" href="${ctx}/css/jquery.dataTables.css"></link>
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script type="text/javascript" src="${ctx}/js/viewQualifyer.js"></script>
    <script type="text/javascript">
<c:set var="ctx">${pageContext.request.contextPath}</c:set>
</script>
     <link rel="icon"  type="image/png"  href="../images/favicon.ico"></link>   
     <script type="text/javascript" charset="utf-8">
     ctx = "${pageContext.request.contextPath}";
     setUserId("${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.id}");
     deviceView = "${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.deviceAddEdit}";
 	cercaView = "${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.cercaAddEdit}";
 	rotaView = "${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.rotaAddEdit}";
 	eventosView = "${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.eventosAddEdit}";
 	pontoView = "${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.pontoDeInteresseAddEdit}";
 	veiculoView = "${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.veiculoAddEdit}";
 	usuariosView = "${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.usuariosAddEdit}";
 	gruposView = "${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.gruposAddEdit}";
	var socket = io.connect('http://54.84.198.243:9090');
	var some = "";
	jQuery.emitToNode = jQuery.ajax({
			url:jQuery("#ctx").text() + "/trackgoweb/devices/json",
	        dataType: "json"
	}).done(function(data) {
			var ids = data;
			 some = ids;
			socket.emit("events/devices", ids);			
	});
	socket.on('events/event_history', function(data) {
		event = data.result;
		console.log(event);
		var eventName = event['eventName'];
		if (getEventMacth(eventName) == "nico"){
			playPanicSoud();
		}
		var eventDesc =  event['eventDescription'] + " - " + event['deviceName']; 	
		for (var int = 0; some.devices.length; int++) {
			if(event['deviceId'] == some.devices[int] ){
	 		jQuery('<div  onclick="clean(event["eventHistoryDetailId"])"></div>').text(eventDesc).addClass("event_item").attr("id", event['eventHistoryDetailId']).appendTo("#alertWindow");
	 		jQuery('#alertWindow').show();
	 		jQuery('#'+event['eventHistoryDetailId']).slideUp(10000, function() {
	 			$(this).remove(); 
	 		});
	 		break;
			}
		}
	});
	
	function getEventMacth(name){
		if(name && name.length > 2){
			return name.substring(2, name.length);
		}
		return name;
	}
	
	
	function playPanicSoud(){
		sound_file_url = ctx + '/sounds/siren.wav';
		play_sound(sound_file_url);
	}
	
	function html5_audio(){
		var a = document.createElement('audio');
		return !!(a.canPlayType && a.canPlayType('audio/mpeg;').replace(/no/, ''));
	}
	 
	var play_html5_audio = false;
	if(html5_audio()) play_html5_audio = true;
	 
	function play_sound(url){
		if(play_html5_audio){
			var snd = new Audio(url);
			snd.load();
			snd.play();
		}else{
			jQuery("#sound").remove();
			var sound = $("<embed id='sound' type='audio/mpeg' />");
			sound.attr('src', url);
			sound.attr('loop', false);
			sound.attr('hidden', true);
			sound.attr('autostart', true);
			jQuery('body').append(sound);
		}
	}
	
	function clean(id){
		jQuery('#'+id).remove();
	}
	
	jQuery('.event_item').click(function(e) {
			$(this).remove();	 			
		});
	
	jQuery('#excluir_button').click(function() {
        jQuery('#alertWindow').find('.event_item').remove();
	});
	
</script>

	<title>Track Go<sitemesh:write property='title' /></title>
</head>
<body id="corpo" lefitmargin="0px" topmargin="0px">
	<div id="geral">
		<nav>
		<div class="menu"><%@ include file="/template/menu.jsp"%></div>
		</nav>
		
		<div id="conteudo">
		<div id="menu-l" class="menu-lateral"><%@ include file="/template/menu-lateral.jsp"%></div>
			<div id="conteudo-usuario">
				<sitemesh:write property='body'/>
			</div>
		</div>

		
	</div>
	<div id="alertWindow">
	</div>
</body>
<%@ include file="/template/main-javascripts.html"%>
<sitemesh:write property='head' />
</html>