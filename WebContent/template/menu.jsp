<div id="menu" >
<ul id="menu-links">
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
	<li><a href="${ctx}/mapa" class="menu"><img src="${ctx}/images/logo_ok.png" id="logo" border="0"></a> <img src="${ctx}/images/separador.jpg" id="separador"></li>
	<li id="separadordispositivo"><a href="#">Dispositivos</a><img src="${ctx}/images/separador.jpg" id="separador">
		<ul class="sub">
		<li class="sub"><a href="${ctx}/devices" class="menu"> <img src="${ctx}/images/lista.png" alt="Listar" title="Listar"><div class="menu-text-sub">Listar</div></a></li>
		<li class="sub"><a class="plus" href="<c:url value="/device/add"/>" ><img src="${ctx}/images/adicionar.png" alt="Cadastrar" title="Cadastrar"><div class="menu-text-sub"> Cadastrar</div></a></li>
		</ul>
	</li>
	<li id="separadorcerca"><a href="#">Cercas</a> <img src="${ctx}/images/separador.jpg" id="separador">
		<ul class="sub">
		<li class="sub"><a href="${ctx}/cercas" class="menu"><img src="${ctx}/images/lista.png" alt="Listar" title="Listar"><div class="menu-text-sub">Listar</div></a></li>
		<li class="sub"><a class="plus" href="<c:url value="/cerca/add"/>" ><img src="${ctx}/images/adicionar.png" alt="Cadastrar" title="Cadastrar"><div class="menu-text-sub">Cadastrar</div></a></li>
		</ul>
	</li>
	<li id="separadorrota"><a href="#">Rotas</a> <img src="${ctx}/images/separador.jpg" id="separador">
		<ul class="sub">
		<li class="sub"><a href="${ctx}/rotas" class="menu"><img src="${ctx}/images/lista.png" alt="Listar" title="Listar"><div class="menu-text-sub">Listar</div></a></li>
		<li class="sub"><a class="plus" href="<c:url value="/rota/add"/>" ><img src="${ctx}/images/adicionar.png" alt="Cadastrar" title="Cadastrar"><div class="menu-text-sub">Cadastrar</div></a></li>
		</ul>
	</li>
	<li id="separadorevento"><a href="#">Eventos</a> <img src="${ctx}/images/separador.jpg" id="separador">
		<ul class="sub">
		<li class="sub"><a href="${ctx}/events" class="menu"><img src="${ctx}/images/lista.png" alt="Listar" title="Listar"><div class="menu-text-sub">Listar</div></a></li>
		<li class="sub"><a class="plus" href="<c:url value="/event/add"/>" ><img src="${ctx}/images/adicionar.png" alt="Cadastrar" title="Cadastrar"><div class="menu-text-sub">Cadastrar</div></a></li>
		</ul>
	</li>
	<li id="separadorponto"><a href="#">Pontos de interesse</a><img src="${ctx}/images/separador.jpg" id="separador">
		<ul class="sub">
		<li class="sub"><a href="${ctx}/pontoDeInteresses" class="menu"><img src="${ctx}/images/lista.png" alt="Listar" title="Listar"><div class="menu-text-sub">Listar</div></a></li>
		<li class="sub"><a class="plus" href="<c:url value="/pontoDeInteresse/add"/>" ><img src="${ctx}/images/adicionar.png" alt="Cadastrar" title="Cadastrar"><div class="menu-text-sub">Cadastrar</div></a></li>
		</ul>
	</li>
	<li id="separadorveiculo"><a href="#">Ve�culos</a><img src="${ctx}/images/separador.jpg" id="separador">
		<ul class="sub">
		<li class="sub"><a href="${ctx}/veiculos" class="menu"><img src="${ctx}/images/lista.png" alt="Listar" title="Listar"><div class="menu-text-sub">Listar</div></a></li>
		<li class="sub"><a class="plus" href="<c:url value="/veiculo/add"/>" ><img src="${ctx}/images/adicionar.png" alt="Cadastrar" title="Cadastrar"><div class="menu-text-sub">Cadastrar</div></a></li>
		</ul>
	</li>
	<li id="separadorgrupo"><a href="#">Grupos</a><img src="${ctx}/images/separador.jpg" id="separador">
		<ul class="sub">
		<li class="sub"><a href="${ctx}/grupos" class="menu"><img src="${ctx}/images/lista.png" alt="Listar" title="Listar"><div class="menu-text-sub">Listar</div></a></li>
		<li class="sub"><a class="plus" href="<c:url value="/grupo/add"/>" ><img src="${ctx}/images/adicionar.png" alt="Cadastrar" title="Cadastrar"><div class="menu-text-sub">Cadastrar</div></a></li>
		</ul>
	</li>
	<li id="separadorusuario"><a href="#">Usu�rios</a><img src="${ctx}/images/separador.jpg" id="separador">
		<ul class="sub">
		<li class="sub"><a href="${ctx}/usuarios"><img src="${ctx}/images/lista.png" alt="Listar" title="Listar"><div class="menu-text-sub">Listar</div></a></li>
		<li class="sub"><a class="plus" href="<c:url value="/usuario/add"/>" ><img src="${ctx}/images/adicionar.png" alt="Cadastrar" title="Cadastrar"><div class="menu-text-sub">Cadastrar</div></a></li>
		</ul>
	</li>
	
</ul>
</div>
<div id="menu-select">
<div style="float:left;padding:10px;margin-top:10px;"><a href="${ctx}/mapa" class="menu"><img src="${ctx}/images/logo_ok.png" id="logo" border="0"></a></div>
	<select style="float:left; margin-left:10px;margin-top:10px;">
		<option value="" selected="selected"> Selecione uma op��o</option>
		<option name="dispositivosSelect" value="${ctx}/devices"><div class="menu-text-sub">Dispositivos - Listar</div></option>
		<option name="dispositivosSelect" value="${ctx}/device/add" ><div class="menu-text-sub">Dispositivos - Cadastrar</div></option>
	
		<option name="cercasSelect" value="${ctx}/cercas"><div class="menu-text-sub"> Cercas - Listar</div></option>
		<option name="cercasSelect"value="${ctx}/cerca/add"/><div class="menu-text-sub"> Cercas - Cadastrar</div></option>
	
		<option name="rotasSelect" value="${ctx}/rotas"><div class="menu-text-sub"> Rotas - Listar</div></option>
		<option name="rotasSelect" value="${ctx}/rota/add"/><div class="menu-text-sub"> Rotas - Cadastrar</div></option>
		
		<option name="eventosSelect" value="${ctx}/events"><div class="menu-text-sub">Eventos - Listar</div></option>
		<option name="eventosSelect"value="${ctx}/event/add" ><div class="menu-text-sub">Eventos - Cadastrar</div></option>
		
		<option name="pontosSelect" value="${ctx}/pontoDeInteresses"><div class="menu-text-sub">Pontos de Intersses - Listar</div></option>
		<option name="pontosSelect" value="${ctx}/pontoDeInteresse/add" ><div class="menu-text-sub">Pontos de Intersses - Cadastrar</div></option>
		
		<option name="veiculosSelect" value="${ctx}/veiculos"><div class="menu-text-sub">Ve�culos - Listar</div></option>
		<option name="veiculosSelect" value="${ctx}/veiculo/add" ><div class="menu-text-sub">Ve�culos - Cadastrar</div></option>
		
		<option name="gruposSelect" value="${ctx}/grupos"><div class="menu-text-sub">Grupos - Listar</div></option>
		<option name="gruposSelect" value="${ctx}/grupo/add" ><div class="menu-text-sub">Grupos - Cadastrar</div></option>
		
		<option name="usuariosSelect" value="${ctx}/usuarios"><div class="menu-text-sub">Usu�rios - Listar</div></option>
		<option name="usuariosSelect" value="${ctx}/usuario/add" ><div class="menu-text-sub">Usu�rios - Cadastrar</div></option>
		</select>
</div>
<script>
jQuery( "#menu-links" ).menu({position: {at: "left-10 top+60"} });
jQuery("nav select").change(function() {
	  window.location = jQuery(this).find("option:selected").val();
	});
</script>