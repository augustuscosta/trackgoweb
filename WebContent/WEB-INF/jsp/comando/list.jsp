<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jPaginate.js"></script>
<script type="text/javascript" src="${ctx}/js/comando/comando.js"></script>
</head>
<body>
	<div class="titulo">Dispositivo: ${device.name}</div>
	
	<table class="comando">
		<thead>
			<tr>
				<th width="60%" align="left">
		<a href="<c:url value="/comando/add/${device.id}"/>"><input
			type="submit" id="update_button4" value="" title="Adicionar" /></a>Comando</th>
				<th width="20%" align="left">Enviado</th>
				<th width="20%" align="left">Estado</th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${empty comandos}">
                <tr>
					<td colspan="3" align="center">                             
						 Nenhum comando enviado</td>

				</tr>
            </c:if>
		</tbody>
		<tbody id="tableholder" class="classholder">
			<c:forEach var="comando" items="${comandos}">
				<tr>
					<td>${comando.comando}</td>
					<td>${comando.enviado}</td>
					<td>${comando.estado}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
</body>
</html>