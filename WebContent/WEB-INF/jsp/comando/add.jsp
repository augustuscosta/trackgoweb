<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<link rel="stylesheet" href="${ctx}/css/jquery.checkbox.css" />
<link rel="stylesheet" href="${ctx}/css/jquery.safari-checkbox.css" />
<script type="text/javascript" src="${ctx}/js/jquery.checkbox.js"></script>
<script type="text/javascript" src="${ctx}/js/comando/comando.js"></script>
</head>
<body>
<div class="titulo">Adicione Comando</div>
<table class="menucomando">
<tr>
<td>
<select id="comandoType" name="condition.comandoType">
						<option value="0">Pedir Posi��o</option>
						<option value="1">Desarmar P�nico</option>
						<option value="2">Utilizar Anti-furto</option>
						<option value="3">Desarmar Anti-furto</option>
						<option value="4">Fuso Hor�rio</option>
						<option value="5">Reset</option>
						<option value="6">Sa�das</option>
						<option value="7">APN</option>
						<option value="8">Server</option>
</select>
</td>

	<td class="position">
	<%@ include file="request_position.jsp"%>
	</td>
	<td class="panic">
	<%@ include file="disable_panic.jsp"%>
	</td>
	<td class="anti-furto">
	<%@ include file="anti_theft.jsp"%>
	</td>
	<td class="desarm-anti-furto">
	<%@ include file="disable_anti_theft.jsp"%>
	</td>
	<td class="timezone">
	<%@ include file="time_zone.jsp"%>
	</td>
	<td class="reset">
	<%@ include file="reset.jsp"%>
	</td>
	<td class="output" >
	<%@ include file="output.jsp"%>
	</td>
	<td class="apn" >
	<%@ include file="apn.jsp"%>
	</td>
	<td class="server" >
	<%@ include file="server.jsp"%>
	</td>
	</tr>
	</table>
</body>
</html>