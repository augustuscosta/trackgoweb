<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<link rel="stylesheet" href="${ctx}/css/jquery.checkbox.css" />
<link rel="stylesheet" href="${ctx}/css/jquery.safari-checkbox.css" />
<script type="text/javascript" src="${ctx}/js/jquery.checkbox.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.validation.js"></script>
<script type="text/javascript" src="${ctx}/js/usuario/usuario.js"></script>
</head>
<body>
<div class="titulo">Editar os Usu�rios</div>
	<form action="<c:url value="/usuario/update"/>" method="POST"
		class="form">
		<input type="hidden" id="usuario_id" name="usuario.id"
			value="${usuario.id}" />
		<table class="deviceForm">
			<tr class="linha">
				<td>Nome:</td>
				<td class="required-field"><input  id="name" name="usuario.name"
					value="${usuario.name}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Usu�rio:</td>
				<td class="required-field"><input  id="username" name="usuario.username"
					value="${usuario.username}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Email:</td>
				<td class="required-field"><input  id="email" name="usuario.email"
					value="${usuario.email}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Telefone Celular:</td>
				<td class="required-field"><input  id="mobilephoneNumber" name="usuario.mobilephoneNumber"
					value="${usuario.mobilephoneNumber}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Nova senha:</td>
				<td><input  id="password" name="usuario.password"
					value="${usuario.password}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Ativo:</td>
				<td class="required-field"><div id="enabled"><input type="checkbox" id="enabled" name="usuario.enabled" <c:if test="${usuario.enabled}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Receber Notifica��es via email:</td>
				<td class="required-field"><div id="mailNotifications"><input type="checkbox" id="mailNotifications" name="usuario.mailNotifications" <c:if test="${usuario.mailNotifications}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Receber Notifica��es via SMS:</td>
				<td class="required-field"><div id="smsNotifications"><input type="checkbox" id="smsNotifications" name="usuario.smsNotifications" <c:if test="${usuario.smsNotifications}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Grupos:</td>
				<td class="required-field"><select id="gruposusuarios" name="usuario.grupos.id" multiple="multiple">
						<c:forEach var="grupo" items="${grupos}">
							<option value="${grupo.id}" <c:if test="${fn:contains(usuario.grupos, grupo)}">selected="selected"</c:if>>${grupo.name}</option>
						</c:forEach>
				</select></td>

			</tr>

			<tr>
				<td></td>
				<td align="right"><input type="submit" id="update_button" value="" title="Salvar" /></td>
			</tr>
		</table>
	</form>
</body>
</html>