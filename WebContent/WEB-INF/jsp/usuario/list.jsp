<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="${ctx}/js/usuario/usuario.js"></script>
</head>
<body>
<div class="titulo">Lista de usu�rios</div>
	<table id="table_id" class="usuarios">
		<thead>
			<tr>
				<th width="20%" align="left">Nome</th>
				<th width="40%" align="left">Email</th>
				<th width="20%" align="left">Ativo</th>
				<th colspan="2" width="20%" align="left">Excluir</th>
			</tr>
		</thead>
		<tbody id="tableholder" class="classholder">
		<c:forEach var="usuario" items="${usuarios}">
			<tr>
				<td ><a href="<c:url value="/usuario/update/${usuario.id}"/>" >${usuario.name}</a></td>
				<td ><a href="<c:url value="/usuario/update/${usuario.id}"/>" >${usuario.email}</a></td>
				<td ><a href="<c:url value="/usuario/update/${usuario.id}"/>" >${usuario.enabled}</a></td>
				<td ><a id="${usuario.id}" class="delete" href="#"><img
							src="${ctx}/images/lixo.png" alt="Remover" title="Remover"></a></td>
			</tr>
		</c:forEach>
		</tbody>
		</table>
</body>
</html>