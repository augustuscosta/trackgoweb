<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<link rel="stylesheet" href="${ctx}/css/jquery.checkbox.css" />
<link rel="stylesheet" href="${ctx}/css/jquery.safari-checkbox.css" />
<script type="text/javascript" src="${ctx}/js/jquery/jquery.validation.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.checkbox.js"></script>
<script type="text/javascript" src="${ctx}/js/usuario/usuario.js"></script>
</head>
<body>
<div class="titulo">Cadastro de Novos Usu�rios</div>
	<form action="<c:url value="/usuario/create"/>" method="POST"
		class="form">
		<table class="deviceForm">
			<tr class="linha">
				<td>Nome:</td>
				<td><input  id="name" name="usuario.name"
					value="${usuario.name}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Usu�rio:</td>
				<td class="required-field"><input  id="username" name="usuario.username"
					value="${usuario.username}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Email:</td>
				<td class="required-field"><input  id="email" name="usuario.email"
					value="${usuario.email}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Telefone Celular:</td>
				<td class="required-field"><input  id="mobilephoneNumber" name="usuario.mobilephoneNumber"
					value="${usuario.mobilephoneNumber}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Senha:</td>
				<td class="required-field"><input  id="password" name="usuario.password"
					value="${usuario.password}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Ativo:</td>
				<td><div id="enabled"><input type="checkbox" id="enabled" name="usuario.enabled" <c:if test="${usuario.enabled}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Receber Notifica��es via email:</td>
				<td class="required-field"><div id="mailNotifications"><input type="checkbox" id="mailNotifications" name="usuario.mailNotifications" <c:if test="${usuario.mailNotifications}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Receber Notifica��es via SMS:</td>
				<td class="required-field"><div id="smsNotifications"><input type="checkbox" id="smsNotifications" name="usuario.smsNotifications" <c:if test="${usuario.smsNotifications}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Grupos:</td>
				<td class="required-field"><select id="gruposusuarios" name="usuario.grupos.id" multiple="multiple">
						<c:forEach var="grupo" items="${grupos}">
							<option value="${grupo.id}">${grupo.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			
			<tr>
				<td></td>
				<td align="right"><input type="submit" id="update_button" value="" title="Salvar" /></td>
			</tr>
		</table>
	</form>
</body>
</html>