<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="${ctx}/js/cerca/cerca.js"></script>
</head>
<body>
<div class="titulo">Lista de cercas</div>
	<table id="table_id" class="cerca">
		<thead>
			<tr>
				<th width="70%" align="left">Nome</th>
				<th>Excluir</th>
			</tr>
		</thead>
		<tbody id="tableholder" class="classholder">
		<c:forEach var="cerca" items="${cercas}">
			<tr>
				<td ><a href="<c:url value="/cerca/update/${cerca.id}"/>" >${cerca.name}</a></td>
				<td ><a id="${cerca.id}" class="delete" href="#"><input type="submit" id="excluir_button" value="" title="Excluir" /></a></td>
			</tr>
		</c:forEach>
		</tbody>
		
		</table>
</body>
</html>