<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>

<link type="text/css" href="${ctx}/css/jquery-autocomplete.css" rel="stylesheet" />
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>
<script type="text/javascript" src="${ctx}/js/gmap3.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery-autocomplete.js"></script> 
<script type="text/javascript" src="${ctx}/js/cerca/cercaEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript">
	<c:if test="${not empty cerca.geoPonto}">
	<c:forEach var="ponto" items="${cerca.geoPonto}">
		var elem = Object();
		var lat = ${ponto.latitude};
		var lng = ${ponto.longitude};
		elem.latLng = new google.maps.LatLng(lat, lng);
		addPoint(elem);
	</c:forEach>
	addPontos();
	</c:if>
</script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.validation.js"></script>
</head>
<body>
<div class="titulo">Editar cercas</div>
		<form action="<c:url value="/cerca/update"/>" method="POST" class="form">
		<input type="hidden" id="cerca_id" name="cerca.id"value="${cerca.id}"/>
			<table class="deviceForm">
				<tr>
					<td>Nome<b style="color: #FF0000; font-size: 14px;"> * </b>:</td>
							
						<td class="input" style="margin-bottom: 10px;">
							<input name="cerca.name" type="text" value="${cerca.name}" id="autocomplete" class="ui-autocomplete-input" />	
						</td>
						<td>Grupos<b style="color: #FF0000; font-size: 14px;"> * </b>:</td>
						<td class="required-field">
							<select id="gruposcercas" name="cerca.grupos.id" multiple="multiple">
									<c:forEach var="grupo" items="${grupos}">
										<option value="${grupo.id}"<c:if test="${fn:contains(cerca.grupos, grupo)}">selected="selected"</c:if>>${grupo.name}</option>
									</c:forEach>
							</select>
						</td>
						<td>
						<input id="update_button" type="submit" value="" title="Salvar" /> <input id="limparButton" type="reset" value="" title="Limpar"/>
					</td>
					</tr>
					
					<tr>
					<td colspan="5">
					<div id="mapaAddCerca" style="float:left;"></div>
					<div id="cerca"></div>
					</td>
					</tr>
			</table>
		</form>
</body>
</html>

