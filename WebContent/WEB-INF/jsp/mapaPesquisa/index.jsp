<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<head>
<c:set var="ctx">${pageContext.request.contextPath}</c:set>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAcEzpDs5fpjWQPAOSqaWsR2mhn6Svu_S0&sensor=true"></script>
<script type="text/javascript" src="${ctx}/js/mapa/infoBooble.js" ></script>

<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>

<script type="text/javascript"
	src="${ctx}/js/jquery/ui/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript" src="${ctx}/js/mapa/searchMap.js"></script>

<link type="text/css" href="${ctx}/css/black-tie/jquery-ui-1.8.19.custom.css" rel="stylesheet" />

</head>
<body>
<div class="titulo">Mapa de pesquisa</div>
		<table class="deviceForm">
			<tr>
				<td>Dispositivo:</td>
				<td><select id="spinner" class="ui-spinner-input focus" name="criteria.device.id">
						<c:forEach var="device" items="${devices}">
							<option value="${device.id}">${device.name}</option>
						</c:forEach>
				</select></td>
				
			</tr>
			<tr>
				<td>Inicio:</td>
				<td >
					<div class="input">
						<input type="text" name="criteria.start" id="criteriastart" />
					</div>
				</td>
				<td>Fim:</td>
				<td >
					<div class="input" >
						<input type="text" name="criteria.end" id="criteriaend" />
					</div>
				</td>
				<td align="right"><input type="submit" id="search_button" value="" title="Pesquisa" /></td>
			</tr>
			<tr>
		</table>

	<span id="ctx" style="display: none">${ctx}</span>
	<div id='loader2' style="display: none"><img src="${ctx}/images/spinner.gif"/></div>
	<div id="map_canvas" style=""></div>
</body>
</html>