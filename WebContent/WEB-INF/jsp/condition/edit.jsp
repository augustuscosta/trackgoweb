x
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>

<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script type="text/javascript"
	src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${ctx}/js/condition/condition.js"></script>
<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<link rel="stylesheet"
	href="${ctx}/css/black-tie/jquery-ui-1.8.19.custom.css" />
<link rel="stylesheet" href="${ctx}/css/jquery.checkbox.css" />
<link rel="stylesheet" href="${ctx}/css/jquery.safari-checkbox.css" />
<script type="text/javascript"
	src="${ctx}/js/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.checkbox.js"></script>

</head>
<body>
	<div class="titulo">Editar Evento</div>
	<form action="<c:url value="/condition/update"/>" method="POST"
		class="form">
		<input type="hidden" id="event_id" name="condition.event.id"
			value="${event.id}" /> <input type="hidden" id="condition_id"
			name="condition.id" value="${condition.id}" />
		<table class="deviceForm">
			<tr>
				<td>Nome:</td>
				<td><input type="text" id="name" name="condition.name"
					value="${condition.name}" /></td>
				<td>Descri��o:</td>
				<td><input type="text" id="description"
					name="condition.description" value="${condition.description}" /></td>
			</tr>
			<tr>
				<td>Tipo de condi��o:</td>
				<td><select id="eventType" name="condition.eventType">
						<option value="0"
							${condition.eventType == 'OGNL_EVENT' ? 'selected="selected"' : ''}>Evento_Personalizado</option>
						<option value="1"
							${condition.eventType == 'ENTERED_CERCA' ? 'selected="selected"' : ''}>Entrando
							na_Cerca</option>
						<option value="2"
							${condition.eventType == 'GOT_OUT_OF_CERCA' ? 'selected="selected"' : ''}>Saindo_da_Cerca</option>
						<option value="3"
							${condition.eventType == 'OUT_OF_ROUTE' ? 'selected="selected"' : ''}>Sa�da
							de rota</option>
						<option value="4"
							${condition.eventType == 'ENTERED_ROUTE' ? 'selected="selected"' : ''}>Entrada
							na rota</option>
				</select></td>
				<td>Ativo:</td>
				<td><input type="checkbox" id="enabled" name="condition.enable"
					<c:if test="${condition.enable}">checked</c:if> /></td>

			</tr>
			<tr>
				<td id="cercas2">Cercas:</td>
				<td><select
					style="display: block; width: 200px; margin-right: 10px;"
					id="cercas" name="condition.cercas.id" multiple="multiple">
						<c:forEach var="cerca" items="${cercas}">
							<option value="${cerca.id}"
								<c:if test="${fn:contains(condition.cercas, cerca)}">selected="selected"</c:if>>${cerca.name}</option>
						</c:forEach>
				</select></td>

			</tr>
			<tr>
				<td id="rotas2">Rotas:</td>
				<td><select id="rotas" name="condition.rotas.id"
					multiple="multiple">
						<c:forEach var="rota" items="${rotas}">
							<option value="${rota.id}"
								<c:if test="${fn:contains(condition.rotas, rota)}">selected="selected"</c:if>>${rota.name}</option>
						</c:forEach>
				</select></td>

			</tr>

			<tr>
				<td id="script2">Script:</td>
				<td id="script3">

					<div id="cart">

						<div class="ui-widget-content">
							<ol id="items">
								<li class="placeholder">Adicione aqui seus param�tros</li>
							</ol>
						</div>
					</div>
				</td>


				<td colspan="2">
					<div id="script4">
						<div id="catalog">
							<ul id="sortable2" class="ui-widget-content connectedSortable ">
								<div id="colunaEsquerda">
									<li class="ui-state-highlight" id="velocidade">Velocidade
										<select id="selectVelocidade"> �
											<option value=">">Maior</option> �
											<option value="<">Menor</option> �
											<option value="==">Igual</option> �
											<option value="<=">Menor Igual</option> �
											<option value=">=">Maior Igual</option>
									</select> <input id="campoVelocidade" type="text"
										name="eventMessage.getSpeed()">
									</li>
									<li class="ui-state-highlight" id="rpm">RPM <select
										id="selectRpm"> �
											<option value=">">Maior</option> �
											<option value="<">Menor</option> �
											<option value="==">Igual</option> �
											<option value="<=">Menor Igual</option> �
											<option value=">=">Maior Igual</option>
									</select> <input id="campoRpm" type="text" name="eventMessage.getRpm()"></li>
									<li class="ui-state-highlight" id="altitude">Altitude <select
										id="selectAltitude"> �
											<option value=">">Maior</option> �
											<option value="<">Menor</option> �
											<option value="==">Igual</option> �
											<option value="<=">Menor Igual</option> �
											<option value=">=">Maior Igual</option>
									</select> <input id="campoAltitude" type="text"
										name="eventMessage.getAltitude()"></li>
									<li class="ui-state-highlight" id="direcao">Dire��o <select
										id="selectDirecao"> �
											<option value=">">Maior</option> �
											<option value="<">Menor</option> �
											<option value="==">Igual</option> �
											<option value="<=">Menor Igual</option> �
											<option value=">=">Maior Igual</option>
									</select> <input id="campoDirecao" type="text"
										name="eventMessage.getCourse()"></li>
								</div>
								<div id="colunaDireita">
									<li class="ui-state-highlight" id="data">Data <select
										id="selectData"> �
											<option value=">">Maior</option> �
											<option value="<">Menor</option> �
											<option value="==">Igual</option> �
											<option value="<=">Menor Igual</option> �
											<option value=">=">Maior Igual</option>
									</select> <input id="campoData" type="text"
										name="eventMessage.getDateInMills()"></li>
									<li class="ui-state-highlight" id="portaAberta">Porta
										aberta
										<div>
											<input id="campoPortaAberta" type="checkbox"
												name="eventMessage.getOpenedDoor()">
										</div>
									</li>
									<li class="ui-state-highlight" id="bloqueio">Ve�culo
										bloqueado
										<div>
											<input id="campoBloqueio" type="checkbox" id="campo4"
												name="eventMessage.getEngineBlock()">
										</div>
									</li>
									<li class="ui-state-highlight" id="panico">P�nico
										<div>
											<input id="campoPanico" type="checkbox"
												name="eventMessage.getPanic()">
										</div>
									</li>
									<li class="ui-state-highlight" id="ignicao">Igni��o
										<div>
											<input id="campoIgnicao" type="checkbox"
												name="eventMessage.getIgnition()">
										</div>
									</li>

								</div>
							</ul>
						</div>
					</div>
				</td>
			</tr>



			<tr>
				<td></td>
				<td align="right" colspan="3"><input type="submit"
					id="update_button" value="" title="Adicionar"
					style="margin-right: 10px;" /></td>
				<input type="hidden" id="campofinal"
					name="condition.scriptCondition"
					value="${condition.scriptCondition}" style="width: 300px;">

			</tr>
		</table>
	</form>
</body>
</html>