<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<head>
<c:set var="ctx">${pageContext.request.contextPath}</c:set>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="${ctx}/css/black-tie/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAcEzpDs5fpjWQPAOSqaWsR2mhn6Svu_S0&sensor=true"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="${ctx}/js/relatorio/marchaLenta.js"></script>
<style type="text/css">
  #tabs-container{
  	float:left;
  	padding-left:10px;
  	padding-top:10px;
  	width:80%;
  }
  
</style>
<script type="text/javascript">
	google.load('visualization', '1', {packages: ['table']});
</script>
</head>
<body>
	<div id="map_dialog" title="Localização">
		<div id="position_map_canvas" style="width: 500px; height: 350px;"></div>
	</div>
	<div class="titulo">Relatório de Marcha Lenta</div>
		<table class="deviceForm">
			<tr>
				<td>Dispositivo:</td>
				<td><select id="spinner" class="ui-spinner-input focus" name="device.id">
						<c:forEach var="device" items="${devices}">
							<option value="${device.id}">${device.name}</option>
						</c:forEach>
				</select></td>
				<td>Minutos:</td>
				<td><select id="minutoSpinner" class="ui-spinner-input focus" name="horasSenPosicionar">
						<option value="1">01</option>
						<option value="2">02</option>
						<option value="3">03</option>
						<option value="4">04</option>
						<option value="5">05</option>
						<option value="6">06</option>
						<option value="7">07</option>
						<option value="8">08</option>
						<option value="9">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						<option value="36">36</option>
						<option value="37">37</option>
						<option value="38">38</option>
						<option value="39">39</option>
						<option value="40">40</option>
						<option value="41">41</option>
						<option value="42">42</option>
						<option value="43">43</option>
						<option value="44">44</option>
						<option value="45">45</option>
						<option value="46">46</option>
						<option value="47">47</option>
						<option value="48">48</option>
						<option value="49">49</option>
						<option value="50">50</option>
						<option value="51">51</option>
						<option value="52">52</option>
						<option value="53">53</option>
						<option value="54">54</option>
						<option value="55">55</option>
						<option value="56">56</option>
						<option value="57">57</option>
						<option value="58">58</option>
						<option value="59">59</option>
						<option value="60">60</option>
				</select></td>
			</tr>
			<tr>
				<td>Inicio:</td>
				<td>
					<div class="input">
						<input type="text" name="criteria.start" id="criteriastart" />
					</div>
				</td>
				<td>Fim:</td>
				<td>
					<div class="input">
						<input type="text" name="criteria.end" id="criteriaend" />
					</div>
				</td>
				<td align="right"><input type="submit" id="search_button" value="" title="Pesquisa" /></td>
			</tr>
			<tr>
				<td>
					<button id="print_button" onclick="print()">Imprimir</button>
				</td>
			</tr>
		</table>

	<span id="ctx" style="display: none">${ctx}</span>
	<div id='loader_relatorio_marcha_lenta' style="display: none"><img src="${ctx}/images/spinner.gif"/></div>
	<div id="tabs-container">
		<div id="tabs">
			<ul>
				<li><a href="#table-chart">Marcha lenta</a></li>
			</ul>
			<div id="table_div"></div>
		</div>
	</div>
</body>
</html>