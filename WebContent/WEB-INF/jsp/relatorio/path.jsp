<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<head>
<c:set var="ctx">${pageContext.request.contextPath}</c:set>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="${ctx}/css/black-tie/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="${ctx}/js/relatorio/relatorio_path.js"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=true" type="text/javascript"></script>
<script type="text/javascript" src="${ctx}/js/gmap3.js"></script>
<style type="text/css">
  #tabs-container{
  	float:left;
  	padding-left:10px;
  	padding-top:10px;
  	width:80%;
  }
  
</style>
<script type="text/javascript">
	google.load('visualization', '1.0', {'packages':['controls']});
</script>
</head>
<body>
	<div id="map_dialog" title="Localiza��o">
		<div id="position_map_canvas" style="width: 500px; height: 350px;"></div>
	</div>
	<div class="titulo">Relat�rio das posi��es</div>
		<table class="deviceForm">
			<tr>
				<td>Dispositivo:</td>
				<td><select id="spinner" class="ui-spinner-input focus" name="device.id">
						<c:forEach var="device" items="${devices}">
							<option value="${device.id}">${device.name}</option>
						</c:forEach>
				</select></td>
				
			</tr>
			<tr>
				<td>Inicio:</td>
				<td>
					<div class="input">
						<input type="text" name="criteria.start" id="criteriastart" />
					</div>
				</td>
				<td>Fim:</td>
				<td>
					<div class="input">
						<input type="text" name="criteria.end" id="criteriaend" />
					</div>
				</td>
				<td align="right"><input type="submit" id="search_button" value="" title="Pesquisa" /></td>
			</tr>
			<tr>
		</table>

	<span id="ctx" style="display: none">${ctx}</span>
	<div id='loader_relatorio_path' style="display: none"><img src="${ctx}/images/spinner.gif"/></div>
	<div id="tabs-container">
		<div id="tabs">
			<ul>
				<li><a href="#table-chart">Tabela de posi��es</a></li>
			</ul>
			<div id="table-chart">
				<div id="dashboard_table">
					<div id="table_filters_div">
						<div id="velocidade_table_filter_div"></div>
					</div>
					<div id="table_div"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>