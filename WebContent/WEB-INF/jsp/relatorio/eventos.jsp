<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<head>
<c:set var="ctx">${pageContext.request.contextPath}</c:set>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAcEzpDs5fpjWQPAOSqaWsR2mhn6Svu_S0&sensor=true"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="${ctx}/js/relatorio/relatorio_eventos.js"></script>
<link type="text/css" href="${ctx}/css/black-tie/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
<style type="text/css">
  #tabs-container{
  	float:left;
  	padding-left:10px;
  	padding-top:10px;
  	width:80%;
  }
  
</style>
<script type="text/javascript">
	google.load('visualization', '1.0', {'packages':['controls']});
</script>
</head>
<body>
	<div id="map_dialog" title="Localização">
		<div id="position_map_canvas" style="width: 500px; height: 350px;"></div>
	</div>
	<span id="ctx" style="display: none">${ctx}</span>
	<div id='loader_relatorio_eventos' style="display: none">
		<img src="${ctx}/images/spinner.gif" />
	</div>
	<div>
		<div class="titulo">Relatório dos eventos</div>
		<table class="deviceForm">
			<tr>
				<td>Inicio:</td>
				<td>
					<div class="input">
						<input type="text" name="criteria.start" id="criteriastart" />
					</div>
				</td>
				<td>Fim:</td>
				<td>
					<div class="input">
						<input type="text" name="criteria.end" id="criteriaend" />
					</div>
				</td>
				<td align="right"><input type="submit" id="search_button"
					value="" title="Pesquisa" /></td>
			</tr>
			<tr>
				<td>
					<button id="print_button" onclick="print()">Imprimir</button>
				</td>
			</tr>
		</table>

		<div id="tabs-container">
			<div id="tabs">
				<ul>
					<li><a href="#pie-chart">Quantidade dos eventos</a></li>
					<li><a href="#table-chart">Tabela dos eventos</a></li>
				</ul>

				<div id="pie-chart">
					<div id="dashboard_pie">
						<div id="pie_filters_div">
							<div id="quantidade_pie_filter_div"></div>
							<div id="evento_pie_filter_div"></div>
							<div id="pie_div"></div>
						</div>
					</div>
				</div>

				<div id="table-chart">
					<div id="dashboard_table">
						<div id="table_filters_div">
							<div id="quantidade_table_filter_div"></div>
							<div id="evento_table_filter_div"></div>
							<div id="device_table_filter_div"></div>
						</div>
						<div id="table_div"></div>
					</div>
				</div>

			</div>
		</div>

	</div>
</body>
</html>