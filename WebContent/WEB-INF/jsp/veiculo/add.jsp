<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.validation.js"></script>
<script type="text/javascript" src="${ctx}/js/veiculo/veiculo.js"></script>
</head>
<body>
<div class="titulo">Cadastrar novos ve�culos</div>
	<form action="<c:url value="/veiculo/create"/>" method="POST"
		class="form">
		<table class="deviceForm">
			<tr class="linha">
				<td>Titulo:</td>
				<td class="required-field"><input  id="titulo" name="veiculo.titulo"
					value="${veiculo.titulo}" /></td>
			</tr>
			<tr class="linha">
				<td>Descricao:</td>
				<td class="required-field"><input  id="descricao" name="veiculo.descricao"
					value="${veiculo.descricao}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Placa:</td>
				<td class="required-field"><input  id="placa" name="veiculo.placa"
					value="${veiculo.placa}" /></td>
			</tr>
			
			<tr class="linha">
				<td>C�digo:</td>
				<td class="required-field"><input  id="codigo" name="veiculo.codigo"
					value="${veiculo.codigo}" /></td>
			</tr>
			
			<tr class="linha">
				<td >Cor:</td>
				<td><input  id="cor" name="veiculo.cor"
					value="${veiculo.cor}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Chassi:</td>
				<td class="required-field"class="required-field"><input  id="chassi" name="veiculo.chassi"
					value="${veiculo.chassi}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Modelo:</td>
				<td class="required-field"><input  id="modelo" name="veiculo.modelo"
					value="${veiculo.modelo}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Grupos:</td>
				<td class="required-field"><select id="gruposveiculos" name="veiculo.grupos.id" multiple="multiple">
						<c:forEach var="grupo" items="${grupos}">
							<option value="${grupo.id}">${grupo.name}</option>
						</c:forEach>
				</select></td>

			</tr>
			
			<tr>
				<td></td>
				<td align="right"><input type="submit" id="update_button" value="" title="Salvar" /></td>
			</tr>
		</table>
	</form>
</body>
</html>