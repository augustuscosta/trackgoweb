<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="${ctx}/js/veiculo/veiculo.js"></script>
</head>
<body>
	<div class="titulo">Lista de Ve�culos</div>
	<table id="table_id" class="veiculo">
		<thead>
			<tr>
				<th width="40%" align="left">Nome</th>
				<th width="40%" align="left">Placa</th>
				<th width="20%" align="left">Excluir</th>

			</tr>
		</thead>
		<tbody id="tableholder" class="classholder">
			<c:forEach var="veiculo" items="${veiculos}">
				<tr>
					<td><a href="<c:url value="/veiculo/update/${veiculo.id}"/>">${veiculo.titulo}</a></td>
					<td><a href="<c:url value="/veiculo/update/${veiculo.id}"/>">${veiculo.placa}</a></td>
					<td><a id="${veiculo.id}" class="delete" href="#"><img
							src="${ctx}/images/lixo.png" alt="Remover" title="Remover"></a></td>
				</tr>
			</c:forEach>
		</tbody>

	</table>

</body>
</html>