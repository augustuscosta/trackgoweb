<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="${ctx}/js/eventhistorydetail/eventhistorydetail.js"></script>
</head>
<body>
	<div class="titulo">Histórico de Eventos</div>
	<table class="device">
		<thead>
			<tr>
				<th >Nome</th>
				<th>Hora</th>
				<th>Dispositivo</th>
			</tr>
		</thead>
		<tbody id="tableholder" class="classholder" >
			<c:forEach var="event" items="${events}">
				<tr>
					<td style='padding:5px'>${event.event.name}</td>
					<td style='padding:5px'>${event.metricDate}</td>
					<td style='padding:5px'>${event.device.name}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>