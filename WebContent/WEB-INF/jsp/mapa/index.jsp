<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
	<head>
	<script type="text/javascript" src="${ctx}/js/mapa/infoBooble.js" ></script>
	
	
	<style type="text/css">
	.labels {
		color: red;
		background-color: white;
		font-family: "Lucida Grande", "Arial", sans-serif;
		font-size: 10px;
		font-weight: bold;
		text-align: center;
		white-space: nowrap;
	}
	#styles, #add-tab {
        float: left;
        margin-top: 10px;
        width: 400px;
      }

      #styles label,
      #add-tab label {
        display: inline-block;
        width: 130px;
      }

      .phoney {
        background: -webkit-gradient(linear,left top,left bottom,color-stop(0, rgb(112,112,112)),color-stop(0.51, rgb(94,94,94)),color-stop(0.52, rgb(57,57,57)));
        background: -moz-linear-gradient(center top,rgb(112,112,112) 0%,rgb(94,94,94) 51%,rgb(57,57,57) 52%);
      }

      .phoneytext {
        text-shadow: 0 -1px 0 #0;
        color: #fff;
        font-family: Helvetica Neue, Helvetica, arial;
        font-size: 18px;
        line-height: 25px;
        padding: 4px 45px 4px 15px;
        font-weight: bold;
        background: url(../images/arrow.png) 95% 50% no-repeat;
      }

      .phoneytab {
        text-shadow: 0 -1px 0 #000;
        color: #fff;
        font-family: Helvetica Neue, Helvetica, arial;
        font-size: 18px;
        background: rgb(112,112,112) !important;
      }
	</style>
	
<c:set var="ctx">${pageContext.request.contextPath}</c:set>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAcEzpDs5fpjWQPAOSqaWsR2mhn6Svu_S0&sensor=true"></script>
		<script type="text/javascript" src="${ctx}/js/markerwithlabel.js"></script>
		<script type="text/javascript" src="${ctx}/js/mapa/deviceMap.js" charset="utf-8"></script>
		
		
	</head>
	<body>
		<span id="ctx" style="display: none">${ctx}</span>
		<div id='loader' style="display: none"><img src="${ctx}/images/spinner.gif"/></div>
		<div id="lupa"></div>
		<div id="pesquisa"><input id="pesquisatext" type="text" value=""/></div>
		<div id="map_canvas" style=""></div>
	</body>
</html>