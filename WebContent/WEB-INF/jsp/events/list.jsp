<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="${ctx}/js/event/event.js"></script>
</head>
<body>
<div class="titulo">Lista de  eventos</div>
	<table class="eventos">
		<thead>
			<tr>
				<th >Nome</th>
				<th >Descri��o</th>
				<th >Ativo</th>
				<th >A��es</th>
				<th >Excluir</th>
			</tr>
		</thead>
		<tbody id="tableholder" class="classholder">
		<c:forEach var="event" items="${events}">
			<tr>
				<td ><a href="<c:url value="/event/update/${event.id}"/>" >${event.name}</a></td>
				<td ><a href="<c:url value="/event/update/${event.id}"/>" >${event.description}</a></td>
				<td ><a href="<c:url value="/event/update/${event.id}"/>" >${event.enable}</a></td>
				<td ><a href="<c:url value="/conditions/${event.id}"/>" ><img src="${ctx}/images/condition.png" alt="Comandos" title="Comandos"></a></td>
				<td ><a id="${event.id}" class="delete" href="#"><input type="submit" id="excluir_button" value="" title="Excluir" /></a></td>
			</tr>
		</c:forEach>
		</tbody>
		</table>

</body>
</html>