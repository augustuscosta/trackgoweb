<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<link rel="stylesheet" href="${ctx}/css/jquery.checkbox.css" />
<link rel="stylesheet" href="${ctx}/css/jquery.safari-checkbox.css" />
<script type="text/javascript" src="${ctx}/js/jquery.checkbox.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.validation.js"></script>
<script type="text/javascript" src="${ctx}/js/event/event.js"></script>
</head>
<body>
<div class="titulo">Cadastrar novos eventos</div>
	<form action="<c:url value="/event/create"/>" method="POST"
		class="form">
		<table class="deviceForm">
			<tr class="linha">
				<td>Nome:</td>
				<td class="required-field"><input type="text" id="name" name="event.name" value="${event.name}" /></td>
			</tr>

			<tr class="linha">
				<td>Ativo:</td>
				<td><div id="enabled"><input type="checkbox" id="enable" name="event.enable"
					<c:if test="${event.enable}">checked</c:if> /></div></td>
			</tr>

			<tr class="linha">
				<td>Descri��o:</td>
				<td><input type="text" id="description" name="event.description" value="${event.description}" /></td>
			</tr>

			<tr class="linha">
				<td>Dispositivos:</td>
				<td class="required-field"><select id="devicesevents" name="event.devices.id" multiple="multiple">
						<c:forEach var="device" items="${devices}">
							<option value="${device.id}">${device.name}</option>
						</c:forEach>
				</select></td>

			</tr >
			
			<tr class="linha">
				<td>Grupos:</td>
				<td class="required-field"><select id="gruposevents" name="event.grupos.id" multiple="multiple">
						<c:forEach var="grupo" items="${grupos}">
							<option value="${grupo.id}">${grupo.name}</option>
						</c:forEach>
				</select></td>

			</tr>

			<tr>
				<td></td>
				<td align="right"><input type="submit" id="update_button" value="" title="Salvar" /></td>
			</tr>
		</table>
	</form>
</body>
</html>