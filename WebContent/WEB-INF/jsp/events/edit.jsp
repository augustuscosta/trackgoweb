<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<link rel="stylesheet" href="${ctx}/css/jquery.checkbox.css" />
<link rel="stylesheet" href="${ctx}/css/jquery.safari-checkbox.css" />
<script type="text/javascript" src="${ctx}/js/jquery.checkbox.js"></script>
<script type="text/javascript" src="${ctx}/js/event/event.js"></script>
</head>
<body>
<div class="titulo">Editar eventos</div>
	<form action="<c:url value="/event/update"/>" method="POST"
		class="form">
		<input type="hidden" id="event_id" name="event.id" value="${event.id}" />
		<table class="deviceForm">
			<tr class="linha">
				<td>Nome:</td>
				<td class="required-field"><input type="text" id="name" name="event.name" value="${event.name}" /></td>
			</tr>

			<tr class="linha">
				<td>Ativo:</td>
				<td class="required-field"><div id="enabled"><input type="checkbox" id="enable" name="event.enable"
					<c:if test="${event.enable}">checked</c:if> /></div></td>
			</tr>

			<tr class="linha">
				<td>Descri��o:</td>
				<td class="required-field"><input type="text" id="description" name="event.description" value="${event.description}" /></td>
			</tr>

			<tr>
				<td>Dispositivos:</td>
				<td class="required-field"><select id="devicesevents" name="event.devices.id" multiple="multiple">
						<c:forEach var="device" items="${devices}">
							<option value="${device.id}" <c:if test="${fn:contains(event.devices, device)}">selected="selected"</c:if>>${device.name}</option>
						</c:forEach>
				</select></td>

			</tr>
			
			<tr class="linha">
				<td>Grupos:</td>
				<td class="required-field"><select id="gruposevents" name="event.grupos.id" multiple="multiple">
						<c:forEach var="grupo" items="${grupos}">
							<option value="${grupo.id}" <c:if test="${fn:contains(event.grupos, grupo)}">selected="selected"</c:if>>${grupo.name}</option>
						</c:forEach>
				</select></td>

			</tr>

			<tr>
				<td></td>
				<td align="right"><input type="submit" id="update_button" value="" title="Salvar" /></td>
			</tr>
		</table>
	</form>
</body>
</html>