<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<link rel="stylesheet" href="${ctx}/css/jquery.checkbox.css" />
<link rel="stylesheet" href="${ctx}/css/jquery.safari-checkbox.css" />
<script type="text/javascript" src="${ctx}/js/jquery.checkbox.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.validation.js"></script>
<script type="text/javascript" src="${ctx}/js/device/device.js"></script>
</head>
<body>
<div class="titulo">Editar Dispositivos</div>
	<form action="<c:url value="/device/update"/>" method="POST"
		class="form">
		<input type="hidden" id="device_id" name="device.id"
			value="${device.id}" />
		<table class="deviceForm">
			<tr class="linha">
				 <td>Nome:</td>
				<td class="required-field"><input  type="text" id="name" name="device.name" value="${device.name}" /></td>
			</tr>
			<tr class="linha">
				<td>C�digo:</td>
				<td  class="required-field"><input type="text" id="code" name="device.code" value="${device.code}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Chip:</td>
				<td class="required-field"><input  type="text" id="chip" name="device.chip"
					value="${device.chip}" /></td>
			</tr>

			<tr class="linha">
				<td>Ativo:</td>
				<td><div id="enabled"><input type="checkbox" name="device.enabled"
					<c:if test="${device.enabled}">checked</c:if> /></div></td>
			</tr>

			<tr class="linha">
				<td>Tipo de dispositivo:</td>
				<td class="required-field">
				
				<select id="deviceType" name="device.deviceType">
						<option value="0" ${device.deviceType == 'TK10X' ? 'selected="selected"' : ''} >Tk10x</option>
						<option value="1" ${device.deviceType == 'ASPICORE' ? 'selected="selected"' : ''} >Aspicore</option>
						<option value="2" ${device.deviceType == 'MAXTRACK' ? 'selected="selected"' : ''} >Maxtrack</option>
						<option value="3" ${device.deviceType == 'SKYPATROL' ? 'selected="selected"' : ''} >Skypatrol</option>
						<option value="4" ${device.deviceType == 'NIAGARA2' ? 'selected="selected"' : ''} >Niagara2</option>
						<option value="5" ${device.deviceType == 'NIAGARAHW6' ? 'selected="selected"' : ''} >NiagaraHw6</option>
				</select>
				
				<select id="protocol" name="device.protocol">
						<option value="">Selecione...</option>
						<option value="MTC400_ID_MENOR_QUE_65535" ${device.protocol == 'MTC400_ID_MENOR_QUE_65535' ? 'selected="selected"' : ''}>MTC400_ID_MENOR_QUE_65535</option>
						<option value="MTC400_ID_MAIOR_QUE_65535" ${device.protocol == 'MTC400_ID_MAIOR_QUE_65535' ? 'selected="selected"' : ''}>MTC400_ID_MAIOR_QUE_65535</option>
						<option value="MTC500" ${device.protocol == 'MTC500' ? 'selected="selected"' : ''}>MTC500</option>
						<option value="MTC600" ${device.protocol == 'MTC600' ? 'selected="selected"' : ''}>MTC600</option>
						<option value="MXT100" ${device.protocol == 'MXT100' ? 'selected="selected"' : ''}>MXT100</option>
						<option value="MXT101" ${device.protocol == 'MXT101' ? 'selected="selected"' : ''}>MXT101</option>
						<option value="MXT140" ${device.protocol == 'MXT140' ? 'selected="selected"' : ''}>MXT140</option>
						<option value="MXT150" ${device.protocol == 'MXT150' ? 'selected="selected"' : ''}>MXT150</option>
						<option value="MXT151" ${device.protocol == 'MXT151' ? 'selected="selected"' : ''}>MXT151</option>
						<option value="MXT120" ${device.protocol == 'MXT120' ? 'selected="selected"' : ''}>MXT120</option>
				</select>
				
				</td>
			</tr>
			
			<tr class="linha">
				<td>Ve�culo</td>
				<td>
					<select id="veiculos" name="device.veiculo.id">  
                    	<option value=""> Selecione...</option>  
                    	<c:forEach var="veiculo" items="${veiculos}">
                        	<option value="${veiculo.id}" 
                        		<c:if test="${device.veiculo.id eq veiculo.id}">selected</c:if>>
                        	${veiculo.titulo}
                        </option>  
                    </c:forEach>  
                	</select>
				</td>
				
			</tr>
			
			<tr class="linha">
				<td>Grupos:</td>
				<td class="required-field"><select id="gruposdevices" name="device.grupos.id" multiple="multiple">
						<c:forEach var="grupo" items="${grupos}">
							<option value="${grupo.id}" <c:if test="${fn:contains(device.grupos, grupo)}">selected="selected"</c:if>>${grupo.name}</option>
						</c:forEach>
				</select></td>

			</tr>
			
			<tr class="linha">
				<td>Descri��o:</td>
				<td><input type="text" id="description" name="device.description"
					value="${device.description}" /></td>
			</tr>
			

			<tr>
				<td></td>
				<td align="right"><input type="submit" id="update_button" value="" title="Salvar" /></td>
			</tr>
		</table>
	</form>
</body>
</html>