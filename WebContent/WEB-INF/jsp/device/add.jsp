<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<link rel="stylesheet" href="${ctx}/css/jquery.checkbox.css" />
<link rel="stylesheet" href="${ctx}/css/jquery.safari-checkbox.css" />
<script type="text/javascript" src="${ctx}/js/jquery.checkbox.js"></script>
<script type="text/javascript" src="${ctx}/js/device/device.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.validation.js"></script>
</head>
<body>
<div class="titulo">Cadastrar novos dispositivos</div>
	<form action="<c:url value="/device/create"/>" method="POST"
		class="form">
		<table class="deviceForm" id="deviceForm">
			<tr class="linha" >
				<td>Nome:</td>
				<td class="required-field"><input  id="name" name="device.name"
					value="${device.name}" /></td>
			</tr>
			<tr class="linha">
				<td>C�digo:</td>
				<td class="required-field"><input  type="text" id="code" name="device.code"
					value="${device.code}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Chip:</td>
				<td class="required-field"><input  type="text" id="chip" name="device.chip"
					value="${device.chip}" /></td>
			</tr>

			<tr class="linha">
				<td>Ativo:</td>
				<td><div id="enabled"><input type="checkbox" class="top5" name="device.enabled" <c:if test="${device.enabled}">checked</c:if>/></div></td>
			</tr>

			<tr class="linha">
				<td>Tipo de dispositivo:</td>
				<td class="required-field">
				
				<select id="deviceType" name="device.deviceType">
						<option value="0">Tk10x</option>
						<option value="1">Aspicore</option>
						<option value="2">Maxtrack</option>
						<option value="3">Skypatrol</option>
						<option value="4">Niagara2</option>
						<option value="5">NiagaraHw6</option>
				</select>
				
				<select id="protocol" name="device.protocol">
						<option value="" >Selecione...</option>
						<option value="MTC400_ID_MENOR_QUE_65535">MTC400_ID_MENOR_QUE_65535</option>
						<option value="MTC400_ID_MAIOR_QUE_65535">MTC400_ID_MAIOR_QUE_65535</option>
						<option value="MTC500">MTC500</option>
						<option value="MTC600">MTC600</option>
						<option value="MXT100">MXT100</option>
						<option value="MXT101">MXT101</option>
						<option value="MXT140">MXT140</option>
						<option value="MXT150">MXT150</option>
						<option value="MXT151">MXT151</option>
						<option value="MXT120">MXT120</option>
				</select>
				
				</td>
			</tr>
			
			<tr class="linha">
				<td>Ve�culo</td>
				<td>
					<select id="veiculo" name="device.veiculo.id">  
                   		<option value=""> Selecione...</option>  
                   		<c:forEach var="veiculo" items="${veiculos}">  
                        	<option value="${veiculo.id}"> ${veiculo.titulo} </option>  
                    	</c:forEach>  
                	</select>
				</td>
				
			</tr>
			
			<tr class="linha">
				<td>Grupos:</td>
				<td class="required-field"><select id="gruposdevices" name="device.grupos.id" multiple="multiple">
						<c:forEach var="grupo" items="${grupos}">
							<option value="${grupo.id}">${grupo.name}</option>
						</c:forEach>
				</select></td>

			</tr>
			
			<tr class="linha">
				<td>Descri��o:</td>
				<td class="required-field"><input type="text" id="description" name="device.description"
					value="${device.description}"/></td>
			</tr>
			

			<tr>
				<td></td>
				<td align="right"><input type="submit" id="update_button" value="" title="Salvar" /></td>
			</tr>
		</table>
	</form>
</body>
</html>