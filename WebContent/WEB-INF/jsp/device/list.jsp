<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="${ctx}/js/device/device.js"></script>

</head>
<body>

<div class="titulo">Lista de Dispositivos</div>
	<table class="device" id="table_id">
		<thead>
			<tr>
				<th >Nome</th>
				<th >C�digo</th>
				<th >A��es</th>
				<th >Excluir</th>

			</tr>
		</thead>
		<tbody id="tableholder" class="classholder">
		<c:forEach var="device" items="${devices}">
			<tr>
				<td><a href="<c:url value="/device/update/${device.id}"/>">${device.name}</a></td>
				<td><a href="<c:url value="/device/update/${device.id}"/>">${device.code}</a></td>
				<td><a href="<c:url value="/comandos/${device.id}"/>"><img src="${ctx}/images/comandos.png" alt="Comandos" title="Comandos"></a></td>
				<td><a id="${device.id}" class="delete" href="#"><input type="submit" id="excluir_button" value="" title="Excluir" /></a></td>
			</tr>
		</c:forEach>
		</tbody>
	</table>

</body>
</html>