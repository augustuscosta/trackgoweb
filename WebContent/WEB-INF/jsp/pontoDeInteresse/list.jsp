<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="${ctx}/js/pontoDeInteresse/pontoDeInteresse.js"></script>
</head>
<body>
	<div class="titulo">Lista de pontos de interesse</div>
	<table id="table_id"class="interest">
		<thead>
			<tr>
				<th width="80%" align="left">Titulo</th>
				<th width="20%" align="left">Excluir</th>
			</tr>
		</thead>
		<tbody id="tableholder" class="classholder">
			<c:forEach var="pontoDeInteresse" items="${pontoDeInteresses}">
				<tr>
					<td><a
						href="<c:url value="/pontoDeInteresse/update/${pontoDeInteresse.id}"/>">${pontoDeInteresse.titulo}</a></td>
					<td><a id="${pontoDeInteresse.id}" class="delete" href="#"><img src="${ctx}/images/lixo.png" alt="Remover" title="Remover"></a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>