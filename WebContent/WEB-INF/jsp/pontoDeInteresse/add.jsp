<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.validation.js"></script>
<script type="text/javascript" src="${ctx}/js/gmap3.js"></script>
<script type="text/javascript" src="${ctx}/js/pontoDeInteresse/pontoDeInteresse.js"></script>
</head>
<body>
<div class="titulo">Cadastrar novos pontos de interesse</div>
	<div class="box box-inline">
		<form action="<c:url value="/pontoDeInteresse/create"/>" method="POST" class="form">
			<table class="poiForm">
				<tr class="linha">
				<td>Titulo<b style="color: #FF0000; font-size: 14px;"> * </b>:</td>
				<td colspan="2" class="required-field"><input name="pontoDeInteresse.titulo" type="text" value="${pontoDeInteresse.titulo}"
								style="background-color: #FFF;" id="name" /></td>
				</tr>
				<tr class="linha">
					<td>Descri��o<b style="color: #FF0000; font-size: 14px;"> * </b>:</td>
					<td colspan="2"><input name="pontoDeInteresse.descricao" type="text" value="${pontoDeInteresse.descricao}"
								style="background-color: #FFF;" id="description"  /></td>
				</tr>
				<tr class="linha">
					<td>Endere�o<b style="color: #FF0000; font-size: 14px;"> * </b>:</td>
					<td class="required-field"><input name="pontoDeInteresse.endereco" type="text" value="${pontoDeInteresse.endereco}"
								style="background-color: #FFF;" id="endereco" /></td>
					<td><input id="geocodingButton" type="button" value="" title="Geolocalizar"/></td>
						
				</tr>

						
					
					
						<tr class="linha">
							<td >Latitude<b style="color: #FF0000; font-size: 14px;"> * </b>:</td>
							<td  colspan="2" class="required-field"><input name="pontoDeInteresse.latitude" type="text" value="${pontoDeInteresse.latitude}"
								style="background-color: #FFF;" id="latitude" /></td>
						</tr>

						
					
					
						<tr class="linha">
							<td >Longitude<b
								style="color: #FF0000; font-size: 14px;"> * </b>:
							</td>
							<td colspan="2" class="required-field"><input  name="pontoDeInteresse.longitude" type="text" value="${pontoDeInteresse.longitude}"
								style="background-color: #FFF;" id="longitude"  /></td>
						</tr>

					<tr class="linha">
							<td>Grupos<b
								style="color: #FF0000; font-size: 14px;"> * </b>:
							</td>
							<td  colspan="2" class="required-field"><select id="grupospois" name="pontoDeInteresse.grupos.id" multiple="multiple">
									<c:forEach var="grupo" items="${grupos}">
										<option value="${grupo.id}">${grupo.name}</option>
									</c:forEach>
							</select></td>
						</tr>
						
						
					<tr >
						<td colspan="3"><input type="submit" value="" title="Salvar" id="update_button" /></td>
					</tr>
					</table>
					<div id="map_canvas" style="height:410px;float:left;"></div>
			</form>
		
	</div>
</body>
</html>

