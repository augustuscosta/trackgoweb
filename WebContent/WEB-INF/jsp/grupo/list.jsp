<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="${ctx}/js/grupo/grupo.js"></script>
</head>
<body>
<div class="titulo">Lista de grupos</div>
<table id="table_id" class="grupos">
		<thead>
			<tr>
				<th width="80%" align="left">Nome</th>
				<th colspan="2" width="20%" align="left">Excluir</th>
			</tr>
		</thead>
		<tbody id="tableholder" class="classholder">
		<c:forEach var="grupo" items="${grupos}">
			<tr>
				<td ><a href="<c:url value="/grupo/update/${grupo.id}"/>" >${grupo.name}</a></td>
				<td ><a id="${grupo.id}" class="delete" href="#"><img
							src="${ctx}/images/lixo.png" alt="Remover" title="Remover"></a></td>
			</tr>
		</c:forEach>
		</tbody>
		
		</table>
<div class="adicionar">		
</div>
</body>
</html>