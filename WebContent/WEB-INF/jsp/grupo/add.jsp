<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<link rel="stylesheet" href="${ctx}/css/jquery.checkbox.css" />
<link rel="stylesheet" href="${ctx}/css/jquery.safari-checkbox.css" />
<script type="text/javascript" src="${ctx}/js/jquery.checkbox.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.validation.js"></script>
<script type="text/javascript" src="${ctx}/js/grupo/grupo.js"></script>
<script type="text/javascript" src="${ctx}/js/grupo/add.js"></script>
</head>
<body>
<div class="titulo">Cadastrar novos grupos</div>
	<form action="<c:url value="/grupo/create"/>" method="POST"
		class="form">
		<table class="deviceForm">
			<tr class="linha">
				<td>Nome:</td>
				<td class="required-field"><input type="text" id="name" name="grupo.name" value="${grupo.name}" /></td>
			</tr>

			<tr class="linha">
				<td>Descri��o:</td>
				<td><input type="text" id="description" name="grupo.description" value="${grupo.description}" /></td>
			</tr>
			
			<tr class="linha">
				<td>Ativo:</td>
				<td><div id="enabled"><input type="checkbox" id="enabled" name="grupo.enabled" <c:if test="${grupo.enabled}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Permiss�o:</td>
				<td>
				
				
				<select id="permissao" name="grupo.permissao">
						<option value="ROLE_ADMINISTRATOR">ROLE_ADMINISTRATOR</option>
						<option value="ROLE_USER">ROLE_USER</option>
				</select>
				
				</td>
			</tr>
			
			<tr class="linha">
				<td>Listar e cadastrar dispositivos:</td>
				<td><div id="deviceAddEdit"><input type="checkbox" id="deviceAddEdit" name="grupo.deviceAddEdit" <c:if test="${grupo.deviceAddEdit}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Listar e cadastrar cercas:</td>
				<td><div id="cercaAddEdit"><input type="checkbox" id="cercaAddEdit" name="grupo.cercaAddEdit" <c:if test="${grupo.cercaAddEdit}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Listar e cadastrar rotas:</td>
				<td><div id="rotaAddEdit"><input type="checkbox" id="rotaAddEdit" name="grupo.rotaAddEdit" <c:if test="${grupo.rotaAddEdit}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Listar e cadastrar Eventos:</td>
				<td><div id="eventosAddEdit"><input type="checkbox" id="eventosAddEdit" name="grupo.eventosAddEdit" <c:if test="${grupo.eventosAddEdit}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Listar e cadastrar Pontos de Interesse:</td>
				<td><div id="pontoDeInteresseAddEdit"><input type="checkbox" id="pontoDeInteresseAddEdit" name="grupo.pontoDeInteresseAddEdit" <c:if test="${grupo.pontoDeInteresseAddEdit}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Listar e cadastrar ve�culos:</td>
				<td><div id="pontoDeInteresseAddEdit"><input type="checkbox" id="veiculoAddEdit" name="grupo.veiculoAddEdit" <c:if test="${grupo.veiculoAddEdit}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Listar e cadastrar usu�rios:</td>
				<td><div id="usuariosAddEdit"><input type="checkbox" id="usuariosAddEdit" name="grupo.usuariosAddEdit" <c:if test="${grupo.usuariosAddEdit}">checked</c:if>/></div></td>
			</tr>
			
			<tr class="linha">
				<td>Listar e cadastrar grupos:</td>
				<td><div id="gruposAddEdit"><input type="checkbox" id="gruposAddEdit" name="grupo.gruposAddEdit" <c:if test="${grupo.gruposAddEdit}">checked</c:if>/></div></td>
			</tr>
			
			<tr>
				<td></td>
				<td align="right"><input type="submit" id="update_button" value="" title="Salvar" /></td>
			</tr>
		</table>
	</form>
</body>
</html>