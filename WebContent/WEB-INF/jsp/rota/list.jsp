<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="${ctx}/js/rota/rotaList.js"></script>
</head>
<body>
<div class="titulo">Lista de Rotas</div>
	<table id="table_id" class="cerca">
		<thead>
			<tr>
				<th width="70%" align="left">Nome</th>
				<th>Excluir</th>
			</tr>
		</thead>
		<tbody id="tableholder" class="classholder">
		<c:forEach var="rota" items="${rotas}">
			<tr>
				<td ><a href="<c:url value="/rota/update/${rota.id}"/>" >${rota.name}</a></td>
				<td ><a id="${rota.id}" class="delete" href="#"><input type="submit" id="excluir_button" value="" title="Excluir" /></a></td>
			</tr>
		</c:forEach>
		</tbody>
		
		</table>
</body>
</html>