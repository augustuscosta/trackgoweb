<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/template/jstl.jsp"%>
<html>
<title></title>
<head>

<link type="text/css" href="${ctx}/css/jquery-autocomplete.css" rel="stylesheet" />
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDXrcnyCQDpOd8ZaF4GiK486pLz0N6T9GE&sensor=false" type="text/javascript"></script>
<script type="text/javascript" src="${ctx}/js/gmap3.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery-autocomplete.js"></script> 
<script type="text/javascript" src="${ctx}/js/rota/rotaForm.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/ui/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery/jquery.validation.js"></script>
<script type="text/javascript">
	<c:if test="${not empty rota.geoPonto}">
	<c:forEach var="ponto" items="${rota.geoPonto}">
		var elem = Object();
		var lat = ${ponto.latitude};
		var lng = ${ponto.longitude};
		elem.latLng = new google.maps.LatLng(lat, lng);
		addElement(elem);
	</c:forEach>
	<c:forEach var="ponto" items="${rota.wayPoints}">
		var elem = Object();
		var lat = ${ponto.latitude};
		var lng = ${ponto.longitude};
		elem.latLng = new google.maps.LatLng(lat, lng);
		addWaypoint(elem);
	</c:forEach>
	var inicio = Object();
	var lat = ${rota.pontoInicial.latitude};
	var lng = ${rota.pontoInicial.longitude};
	inicio.latLng = new google.maps.LatLng(lat, lng);
	setBegining(inicio);
	var fim = Object();
	var lat = ${rota.pontoFinal.latitude};
	var lng = ${rota.pontoFinal.longitude};
	fim.latLng = new google.maps.LatLng(lat, lng);
	setEnding(fim);
</c:if>
</script>
	
</head>
<body>
<div class="titulo">Editar rota</div>
		<form action="<c:url value="/rota/update"/>" method="POST" class="form">
		<input type="hidden" id="rota_id" name="rota.id"value="${rota.id}"/>
			<table class="deviceForm">
				<tr>
					<td>Nome<b style="color: #FF0000; font-size: 14px;"> * </b>:</td>
							
						<td class="input" style="margin-bottom: 10px;">
							<input name="rota.name" type="text" value="${rota.name}" id="autocomplete" class="ui-autocomplete-input" />	
						</td>
						<td>Grupos<b style="color: #FF0000; font-size: 14px;"> * </b>:</td>
						<td class="required-field">
							<select id="gruposrotas" name="rota.grupos.id" multiple="multiple">
									<c:forEach var="grupo" items="${grupos}">
										<option value="${grupo.id}"<c:if test="${fn:contains(rota.grupos, grupo)}">selected="selected"</c:if>>${grupo.name}</option>
									</c:forEach>
							</select>
						</td>
						<td>
						<input id="update_button" type="submit" value="" title="Salvar" /> <input id="limparButton" type="reset" value="" title="Limpar"/>
					</td>
					</tr>
					
					<tr>
					<td colspan="5">
					<div id="mapaAddRota" style="float:left;"></div>
					<div id="rota"></div>
					<div id="wayPoint"></div>
					<div id="pontoInicial"></div>
					<div id="pontoFinal"></div>
					</td>
					</tr>
			</table>
		</form>
</body>
</html>

