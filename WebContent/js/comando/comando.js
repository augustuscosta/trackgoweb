jQuery(document).ready(function () {
	if(jQuery("#tableholder").length > 0){
		if(jQuery("#tableholder").find ("tr").length > 7){
			jQuery("#tableholder").jPaginate({items: 8,next: "Seguinte",previous: "Anterior"});		
		}
	}	
	checkEventType();
	fullSize();
	jQuery('input:checkbox:not([safari])').checkbox();
	jQuery('input[safari]:checkbox').checkbox({cls:'jquery-safari-checkbox'});
	jQuery('input:radio').checkbox();
});

var alturaConteudo = jQuery(window).height()+16;
var altura = jQuery(window).height() -43;



function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(altura);
}

jQuery(window).resize(function() {
	alturaConteudo = jQuery(window).height()+10;
	altura = jQuery(window).height() -50;
	fullSize();
});

displayForm = function (elementId)
{
	var content = [];
	jQuery('#' + elementId + ' input').each(function(){
		var el = jQuery(this);
		if ( (el.attr('type').toLowerCase() == 'radio'))
		{
			if ( this.checked )
				content.push([
					'"', el.attr('name'), '": ',
					'value="', ( this.value ), '"',
					( this.disabled ? ', disabled' : '' )
				].join(''));
		}
		else
			content.push([
				'"', el.attr('name'), '": ',
				( this.checked ? 'checked' : 'not checked' ), 
				( this.disabled ? ', disabled' : '' )
			].join(''));
	});
	alert(content.join('\n'));
}

changeStyle = function(skin)
{
	jQuery('#myform :checkbox').checkbox((skin ? {cls: skin} : {}));
}

jQuery("#comandoType").change(function() {
	checkEventType();
});

function checkEventType(){
	var option = jQuery("#comandoType").val();
	
	switch (option){
	case '0':
		showPosition();
		break;
	case '1' :
		showDesarmPanic();
		break;
	case '2' :
		showAntiFurto();
		break;	
	case '3' :
		showDesarmAntiFurto();
		break;
	case '4' :
		showTimeZone();
		break;
	case '5' :
		showReset();
		break;
	case '6' :
		showOutput();
		break;
	case '7' :
		showApn();
		break;
	case '8' :
		showServer();
		break;
	}
}

function showPosition(){
	jQuery(".position").show();
	jQuery(".panic").hide();
	jQuery(".anti-furto").hide();
	jQuery(".desarm-anti-furto").hide();
	jQuery(".timezone").hide();
	jQuery(".reset").hide();
	jQuery(".output").hide();
	jQuery(".apn").hide();
	jQuery(".server").hide();

}

function showDesarmPanic(){
	jQuery(".position").hide();
	jQuery(".panic").show();
	jQuery(".anti-furto").hide();
	jQuery(".desarm-anti-furto").hide();
	jQuery(".timezone").hide();
	jQuery(".reset").hide();
	jQuery(".output").hide();
	jQuery(".apn").hide();
	jQuery(".server").hide();
}

function showAntiFurto(){
	jQuery(".position").hide();
	jQuery(".panic").hide();
	jQuery(".anti-furto").show();
	jQuery(".desarm-anti-furto").hide();
	jQuery(".timezone").hide();
	jQuery(".reset").hide();
	jQuery(".output").hide();
	jQuery(".apn").hide();
	jQuery(".server").hide();
}

function showDesarmAntiFurto(){
	jQuery(".position").hide();
	jQuery(".panic").hide();
	jQuery(".anti-furto").hide();
	jQuery(".desarm-anti-furto").show();
	jQuery(".timezone").hide();
	jQuery(".reset").hide();
	jQuery(".output").hide();
	jQuery(".apn").hide();
	jQuery(".server").hide();
}

function showTimeZone(){
	jQuery(".position").hide();
	jQuery(".panic").hide();
	jQuery(".anti-furto").hide();
	jQuery(".desarm-anti-furto").hide();
	jQuery(".timezone").show();
	jQuery(".reset").hide();
	jQuery(".output").hide();
	jQuery(".apn").hide();
	jQuery(".server").hide();
}

function showReset(){
	jQuery(".position").hide();
	jQuery(".panic").hide();
	jQuery(".anti-furto").hide();
	jQuery(".desarm-anti-furto").hide();
	jQuery(".timezone").hide();
	jQuery(".reset").show();
	jQuery(".output").hide();
	jQuery(".apn").hide();
	jQuery(".server").hide();
}

function showOutput(){
	jQuery(".position").hide();
	jQuery(".panic").hide();
	jQuery(".anti-furto").hide();
	jQuery(".desarm-anti-furto").hide();
	jQuery(".timezone").hide();
	jQuery(".reset").hide();
	jQuery(".output").show();
	jQuery(".apn").hide();
	jQuery(".server").hide();
}

function showApn(){
	jQuery(".position").hide();
	jQuery(".panic").hide();
	jQuery(".anti-furto").hide();
	jQuery(".desarm-anti-furto").hide();
	jQuery(".timezone").hide();
	jQuery(".reset").hide();
	jQuery(".output").hide();
	jQuery(".apn").show();
	jQuery(".server").hide();
}

function showServer(){
	jQuery(".position").hide();
	jQuery(".panic").hide();
	jQuery(".anti-furto").hide();
	jQuery(".desarm-anti-furto").hide();
	jQuery(".timezone").hide();
	jQuery(".reset").hide();
	jQuery(".output").hide();
	jQuery(".apn").hide();
	jQuery(".server").show();
}