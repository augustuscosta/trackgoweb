jQuery(document).ready(function () {
	applyDataTable();
	fullSize();
});

function applyDataTable(){
	if(jQuery("#table_id").length > 0){
		jQuery("#table_id").dataTable({
			"oLanguage": {
				"sInfo": " _TOTAL_ Rotas  exibindo de _START_ a _END_",
				"sEmptyTable": "Não existem rotas cadastradas",
				"sZeroRecords": "Não existem rotas cadastradas com  filtro pesquisado",
				"sInfoEmpty": "",
				"sInfoFiltered": "(Filtrados _END_ de _MAX_ rotas)",
				"sSearch": "Pesquisar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Próximo"
				},
				"sLengthMenu": "Mostrar _MENU_ rotas"
			}, 
			aoColumnDefs: [
				                { "aTargets": [ 0 ], "bSortable": true },
				                { "aTargets": [ 1 ], "bSortable": false }]
			
		});
	}
}

var alturaConteudo = jQuery(window).height()-60;

function fullSize() {
	jQuery('#menu-l').height(alturaConteudo);
	jQuery('#conteudo').height(alturaConteudo);
}

jQuery(window).resize(function() {
	alturaConteudo = jQuery(window).height()-60;
	fullSize();
}); 

jQuery(".delete").click(function() {
	id = jQuery(this).attr("id");
	jQuery.ajax({
		url: ctx + "/trackgoweb/rota/" + id,
		type: "DELETE",
		success: function() {
			location.reload();
		}
	});
});