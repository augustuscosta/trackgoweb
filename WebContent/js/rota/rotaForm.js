//map variables

var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();
var latlngMapa = new google.maps.LatLng(-3.745314, -38.469307);
var mapAddRota;
var elems = new Array();
var rotaPontoA = "";
var rotaPontoB = "";
var wayPoints ="";
var wayPointsLocations =[];
var rota;
var comRota = false;
var rotaOk = true;
var pontoA = new google.maps.MarkerImage(
		ctx+"images/marcadores_rota/green_MarkerA.png",
		new google.maps.Size(20, 34), new google.maps.Point(0, 0),
		new google.maps.Point(10, 32));

var pontoB = new google.maps.MarkerImage(
		ctx+"images/marcadores_rota/green_MarkerB.png",
		new google.maps.Size(20, 34), new google.maps.Point(0, 0),
		new google.maps.Point(10, 32));

var path = new google.maps.MVCArray;
var poly = new google.maps.Marker({
    fillColor: '#FF0000',
    strokeOpacity: 0.3,
    strokeColor: "#FF0000",
    strokeWeight: 2,
    fillOpacity: 0.05
});

//Resize Values
var alturaConteudo = jQuery(window).height() - 60;
var larguraMapaAddRota = jQuery(window).width() - 350;
var alturaMapaAddRota = jQuery(window).height() - 230;

jQuery(document).ready(function() {
	setSize();
});

function setSize() {
	setSizeValues();
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(alturaConteudo);
	jQuery('#mapaAddRota').width(larguraMapaAddRota);
	jQuery('#mapaAddRota').height(alturaMapaAddRota);
}

jQuery(window).resize(function() {
	setSize();
});

function setSizeValues() {
	alturaConteudo = jQuery(window).height() - 60;
	larguraMapaAddRota = jQuery(window).width() - 350;
	alturaMapaAddRota = jQuery(window).height() - 230;
}

//Map Initialization

jQuery("#mapaAddRota").ready(function() {
	jQuery("#mapaAddRota").gmap3({
		action : 'init',
		options : {
			center : [ latlngMapa.lat(), latlngMapa.lng() ],
			zoom : 14
		}
	});
	jQuery('#limparButton').click(limparRota);

	mapAddRota = jQuery("#mapaAddRota").gmap3('get');
	directionsDisplay.setOptions({
		draggable : true,
		preserveViewport : true
	});
	
	if(elems.length >0){
		addOldRota();
	}

	google.maps.event.addListener(mapAddRota, "click", function(event) {

		if (rotaPontoA == "") {

			rotaPontoA = event.latLng;
			rota = {
					origin : rotaPontoA,
					destination : rotaPontoA,
					travelMode : google.maps.DirectionsTravelMode.DRIVING
			};

		} else if (rotaPontoB == "") {

			rotaPontoB = event.latLng;
			rota = {
					origin : rotaPontoA,
					destination : rotaPontoB,
					travelMode : google.maps.DirectionsTravelMode.DRIVING
			};
		} 

		directionsService.route(rota, function(result, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(result);
				addPontos();
				if (directionsDisplay.getMap() == null) {
					directionsDisplay.setMap(mapAddRota);
				}
			}
		});
	});
	
	google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {  
		
    	rotaOk = true;
    	
		if (directionsDisplay.getDirections() != null) {
			
			rota = directionsDisplay.directions.routes[0].overview_path;
			wayPoints = directionsDisplay.directions.routes[0].legs[0].via_waypoint;
				addPontos();
		} else {
			rotaOk = false;
		}
		
    });
});

function addPontos(){
	rotaForm ="";
	wayPoint="";
	pontoInicial=directionsDisplay.directions.routes[0].legs[0].start_location;
	pontoFinal=directionsDisplay.directions.routes[0].legs[0].end_location;
	pontoInicialForm="";
	pontoFinalForm="";
	for (var i = 0; i < directionsDisplay.directions.routes[0].overview_path.length; ++i) {
		posicao = directionsDisplay.directions.routes[0].overview_path[i];
		rotaForm = rotaForm + '<input type="hidden" name="rota.geoPonto[' + i + '].latitude" value="' + posicao.lat() + '"/>';
		rotaForm = rotaForm + '<input type="hidden" name="rota.geoPonto[' + i + '].longitude" value="' + posicao.lng() + '"/>';
		rotaForm = rotaForm + '<input type="hidden" name="rota.geoPonto[' + i + '].rota.id" value="${rota.id}"/>';
	}
	for (var i = 0; i < wayPoints.length; ++i) {
		posicao = wayPoints[i].location;
		wayPoint = wayPoint + '<input type="hidden" name="rota.wayPoints[' + i + '].latitude" value="' + posicao.lat() + '"/>';
		wayPoint = wayPoint + '<input type="hidden" name="rota.wayPoints[' + i + '].longitude" value="' + posicao.lng() + '"/>';
		wayPoint = wayPoint + '<input type="hidden" name="rota.wayPoints[' + i + '].rota.id" value="${rota.id}"/>';
	}
	pontoInicialForm = pontoInicialForm + '<input type="hidden" name="rota.pontoInicial.latitude" value="' + pontoInicial.lat() + '"/>';
	pontoInicialForm = pontoInicialForm + '<input type="hidden" name="rota.pontoInicial.longitude" value="' + pontoInicial.lng() + '"/>';
	pontoInicialForm = pontoInicialForm + '<input type="hidden" name="rota.pontoInicial.rota.id" value="${rota.id}"/>';
	pontoFinalForm = pontoFinalForm + '<input type="hidden" name="rota.pontoFinal.latitude" value="' + pontoFinal.lat() + '"/>';
	pontoFinalForm = pontoFinalForm + '<input type="hidden" name="rota.pontoFinal.longitude" value="' + pontoFinal.lng() + '"/>';
	pontoFinalForm = pontoFinalForm + '<input type="hidden" name="rota.pontoFinal.rota.id" value="${rota.id}"/>';
	jQuery('#pontoInicial').html(pontoInicialForm);
	jQuery('#pontoFinal').html(pontoFinalForm);
	jQuery('#wayPoint').html(wayPoint);
	jQuery('#rota').html(rotaForm);
}

function limparRota(){
	directionsDisplay.setMap(null);
	rotaPontoA = "";
	rotaPontoB="";
	jQuery('#rota').html("");
}

function addElement(elem){
	elems.push(elem);
}

function addWaypoint(elem){
	wayPointsLocations.push(elem);
}

function setBegining(begining){
	pontoA = begining;
}

function setEnding(ending){
	pontoB = ending;
}

function addOldRota(){
	var waypts =[];
	for ( var int = 0; int < wayPointsLocations.length; int++) {
			waypts.push({
				location:new google.maps.LatLng(wayPointsLocations[int].latLng.lat(), wayPointsLocations[int].latLng.lng()),
				stopover:false
				});
	}
	
	   var request = {
		      origin: pontoA.latLng,
		      destination: pontoB.latLng,
		      waypoints: waypts,
		      optimizeWaypoints: true,
		      travelMode: google.maps.TravelMode.DRIVING
		  };

	   directionsService.route(request, function(result, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(result);
				addPontos();
				if (directionsDisplay.getMap() == null) {
					directionsDisplay.setMap(mapAddRota);
				}
			}
		});
}


function validate (){
	return jQuery('form').validation();
}

$(function () { 
	$('#update_button').click(function(e) {
		return validate();
	}); 
});
