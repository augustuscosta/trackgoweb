jQuery(document).ready(function () {
	if(jQuery(".eventos").length > 0){
		jQuery(".eventos").dataTable({
			"oLanguage": {
				"sInfo": " _TOTAL_ Eventos  exibindo de _START_ a _END_",
				"sEmptyTable": "Não existem eventos Cadastrados",
				"sZeroRecords": "Não existem eventos com  filtro pesquisado",
				"sInfoEmpty": "",
				"sInfoFiltered": "(Filtrados _END_ de _MAX_ eventos)",
				"sSearch": "Pesquisar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Próximo"
				},
				"sLengthMenu": "Mostrar _MENU_ eventos"
			}
		}); 
	}
	fullSize();
	jQuery('input:checkbox:not([safari])').checkbox();
	jQuery('input[safari]:checkbox').checkbox({cls:'jquery-safari-checkbox'});
	jQuery('input:radio').checkbox();
});

function validate (){
	return jQuery('form').validation();
}

$(function () { 
	  $('#update_button').click(function(e) {
		 return validate();
	  }); 
	});

jQuery(".delete").click(function() {
	id = jQuery(this).attr("id");
	jQuery.ajax({
		url: ctx + "/trackgoweb/event/" + id,
		type: "DELETE",
		success: function() {
			location.reload();
		}
	});
});

var alturaConteudo = jQuery(window).height()-60;


function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(alturaConteudo);
	
	}

jQuery(window).resize(function() {
	alturaConteudo = jQuery(window).height()-60;
	fullSize();
}); 

displayForm = function (elementId)
{
	var content = [];
	jQuery('#' + elementId + ' input').each(function(){
		var el = jQuery(this);
		if ( (el.attr('type').toLowerCase() == 'radio'))
		{
			if ( this.checked )
				content.push([
					'"', el.attr('name'), '": ',
					'value="', ( this.value ), '"',
					( this.disabled ? ', disabled' : '' )
				].join(''));
		}
		else
			content.push([
				'"', el.attr('name'), '": ',
				( this.checked ? 'checked' : 'not checked' ), 
				( this.disabled ? ', disabled' : '' )
			].join(''));
	});
	alert(content.join('\n'));
}

changeStyle = function(skin)
{
	jQuery('#myform :checkbox').checkbox((skin ? {cls: skin} : {}));
}

function on() {
	var btnOn =  "<img id='btnOn' src='../trackgoweb/images/on.png' >";
	return btnOn;
};

function off() {
	var btnOff =  "<img id='btnOn' src='../trackgoweb/images/off.png' >";
	return btnOff;
};
function btnState(event){
if(event.enable == true){ 
	return on(); 
	
}else{ 

	return off(); 
	} ;
};