var alturaConteudo = jQuery(window).height();
var altura = jQuery(window).height() -60;
var map;
var marker;
var geocoder;
var positions;

jQuery(document).ready(function() {
	fullSize();
	jQuery.init();
});

function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(altura);
}

jQuery.ajaxSetup({
	beforeSend : function() {
		jQuery('#loader_relatorio_path').show();
	},
	complete : function() {
		jQuery('#loader_relatorio_path').hide();
	},
	success : function() {
	}
});

jQuery.init = function() {
	
	jQuery('#map_dialog').dialog({ 
		autoOpen: false, 
		height: 400, 
		width: 530 });
	
	jQuery.datepicker.regional['ru'] = {
			prevText : 'Anterior',
			nextText : 'Próximo',
			monthNames : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
					'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
			monthNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			dayNames : [ 'Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta',
					'Sábado', 'Domingo' ],
			dayNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			dayNamesMin : [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D' ],
			dateFormat : 'dd/mm/yy'
		};

		jQuery.datepicker.setDefaults(jQuery.datepicker.regional['ru']);

		jQuery('#criteriastart').datetimepicker({
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm:ss',
			timeText : 'Horário',
			hourText : 'Hora',
			minuteText : 'Minuto',
			secondText : 'Segundo',
			currentText : 'Agora',
			closeText : 'Ok'
		});

		jQuery('#criteriaend').datetimepicker({
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm:ss',
			timeText : 'Horário',
			hourText : 'Hora',
			minuteText : 'Minuto',
			secondText : 'Segundo',
			currentText : 'Agora',
			closeText : 'Ok'
		});
		jQuery('#search_button').click(searchPath);
		
		jQuery("#tabs").tabs({
			select : changeTab
		});
		
		geocoder = new google.maps.Geocoder();
		
};

function searchPath(){
	/*if(!verifyParams()){
		var alertString = new String("A data de início é superior a data final corrija os parâmetros");
		alert(alertString);
		return;
	}*/
	jQuery.ajax({
		url: ctx + "/relatorios/path",
		data : getParamsData(),
		type : 'GET'
	}).done(function(dataReturned) {
		if(!dataReturned.device || !dataReturned.device.positions  || dataReturned.device.positions.length < 1){
			var alertString = new String("não foram encontrados Dados para o veículo nesta data");
			alert(alertString);
			return;
		}else{
			data = dataReturned.device;
			positions = data.positions;
		    geocodePositions();
		}
	});
}



function verifyParams(){
	var start = jQuery('#criteriastart').val();
	var end  = jQuery('#criteriaend').val();
	if(start == null || start.lenght < 1){
		return false;
	}
	
	if(end == null || end.lenght < 1){
		return false;
	}
	
	return getDateOrder(start,end);
}

function getDateOrder(start,end){
	if(start > end){
		return false;
	}else{
		return true;
	}
}

function parseDateFromPickerString(dateText){
	var stringDate = dateText.slice(0,10);
	var stringTime = dateText.slice(11,19);
	var dateParts  = stringDate.split("/");
	var timeParts  = stringTime.split(":");
	return new Date(dateParts[2], (dateParts[1] - 1), dateParts[1],timeParts[0],timeParts[1],timeParts[2]);
}

function getParamsData(){
	params = Object();
	params.deviceId = jQuery('#spinner').val();
	params.start = jQuery('#criteriastart').val();
	params.end = jQuery('#criteriaend').val();
	return params;
}

function changeTab(event, ui){
	if(data != null){
		drawCharts();
	}
}

function initMap(){
	var myOptions = {
			zoom : 12,
			mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("position_map_canvas"), myOptions);
}

function showDeviceLocation(device,lat,lng){
	jQuery('#map_dialog').dialog('open');
	initMap();
	cleanMap();
	var myLatlng = new google.maps.LatLng(lat, lng);
	marker = new google.maps.Marker({
	      position: myLatlng,
	      map: map,
	      title: device
	 });
	marker.setMap(map);
	map.setCenter(myLatlng);
}

function cleanMap(){
	if(marker != null)
		marker.setMap(null);
}

function drawCharts(){
	drawPositionsTable();
	fullSize();
}

function getPositionsTableData(){
	var toReturn3 = new google.visualization.DataTable();
    toReturn3.addColumn('string', 'Dispositivo');
    toReturn3.addColumn('datetime', 'Data');
    toReturn3.addColumn('boolean', 'Pânico');
    toReturn3.addColumn('boolean', 'Em movimento');
    toReturn3.addColumn('boolean', 'Ligado');
    toReturn3.addColumn('number', 'Velocidade');
    toReturn3.addColumn('string', 'Localização');
    
    for(var i in positions){
    	position = positions[i];
    	htmlString = "";
    	if(position.adrress){
    		htmlString = position.adrress;
    	}else{
    		htmlString = "<button id='"+ position.latitude + position.longitude +"' onclick=\"showDeviceLocation('" + data.name + "'," + position.latitude + "," + position.longitude + ")\">" + position.latitude + "," + position.longitude + "</button>";
    	}
		positionDate = new Date(getTimestamp(position.date));
    	toReturn3.addRows([[data.name, positionDate, position.panic, position.moving, position.ignition, position.speed, htmlString]]);
    }
    return toReturn3;
}

function geocodePositions(){
	if(positions && positions.length > 0){
		geocodePosition(0);
	}
}

function geocodePosition(index) {
	if(index < positions.length){
		position = data.positions[index];
		jQuery('#loader_relatorio_path').show();
		geocoder.geocode({
			latLng: new google.maps.LatLng(position.latitude, position.longitude)
		}, function(responses, status) {
			jQuery('#loader_relatorio_path').hide();
			if (status == google.maps.GeocoderStatus.OK) {
				if (responses && responses.length > 0) {
					position.adrress = responses[0].formatted_address;
					setTimeout( geocodePosition, 1000, index + 1 );
				}else{
					setTimeout( geocodePosition, 1000, index + 1 );
				}
			}else{
				setTimeout( geocodePosition, 1000, index + 1 );
			}
		});
	}else{
		drawCharts();
	}
}

function getTimestamp(str) {
	var d = str.match(/\d+/g); // extract date parts
	return +new Date(d[0], d[1] - 1, d[2], d[3], d[4], d[5]); // build Date
																// object
}


function drawPositionsTable(){
	
	var dashboard = new google.visualization.Dashboard(
            document.getElementById('dashboard_table'));
	
	var slider = new google.visualization.ControlWrapper({
	    'controlType': 'NumberRangeFilter',
	    'containerId': 'velocidade_table_filter_div',
	    'options': {
	      'filterColumnLabel': 'Velocidade'
	    }
	  });

	
	var chart = new google.visualization.ChartWrapper({
		'chartType' : 'Table',
		'containerId' : 'table_div',
		'options' : {
			'alternatingRowStyle':true,
			'showRowNumber':true,
			'width': "100%",
	        'height': 300,
	        'allowHtml':true
			
		}
	});

    dashboard.bind(slider, chart);
    dashboard.draw(getPositionsTableData());
}

