var data;
var deviceData;
var alturaConteudo = jQuery(window).height();
var altura = jQuery(window).height() -60;
var map;
var marker;

jQuery(document).ready(function() {
	fullSize();
	jQuery.init();
});

jQuery.ajaxSetup({
	beforeSend : function() {
		jQuery('#loader_relatorio_eventos').show();
	},
	complete : function() {
		jQuery('#loader_relatorio_eventos').hide();
	},
	success : function() {
	}
});

function initMap(){
	var myOptions = {
			zoom : 12,
			mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("position_map_canvas"), myOptions);
}

function showEventLocation(event,lat,lng){
	jQuery('#map_dialog').dialog('open');
	initMap();
	cleanMap();
	var myLatlng = new google.maps.LatLng(lat, lng);
	marker = new google.maps.Marker({
	      position: myLatlng,
	      map: map,
	      title: event
	 });
	marker.setMap(map);
	map.setCenter(myLatlng);
}

function cleanMap(){
	if(marker != null)
		marker.setMap(null);
}

jQuery.init = function() {
	
	jQuery('#map_dialog').dialog({ 
		autoOpen: false, 
		height: 400, 
		width: 530 });
	
	jQuery.datepicker.regional['ru'] = {
			prevText : 'Anterior',
			nextText : 'Próximo',
			monthNames : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
					'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
			monthNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			dayNames : [ 'Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta',
					'Sábado', 'Domingo' ],
			dayNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			dayNamesMin : [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D' ],
			dateFormat : 'dd/mm/yy'
		};

		jQuery.datepicker.setDefaults(jQuery.datepicker.regional['ru']);

		jQuery('#criteriastart').datetimepicker({
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm:ss',
			timeText : 'Horário',
			hourText : 'Hora',
			minuteText : 'Minuto',
			secondText : 'Segundo',
			currentText : 'Agora',
			closeText : 'Ok'
		});

		jQuery('#criteriaend').datetimepicker({
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm:ss',
			timeText : 'Horário',
			hourText : 'Hora',
			minuteText : 'Minuto',
			secondText : 'Segundo',
			currentText : 'Agora',
			closeText : 'Ok'
		});
		jQuery('#search_button').click(searchEvents);
		jQuery("#tabs").tabs({
			select : changeTab
		});
		
};

function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(altura);
}

function searchEvents() {
	/*if(!verifyParams()){
		var alertString = new String("A data de início é superior a data final corrija os parâmetros");
		alert(alertString);
		return;
	}*/
	clear();
	requestEvents();
}

function requestEvents(){
	jQuery.ajax({
		url: ctx + "/relatorios/eventos",
		data : getParamsData(),
		type : 'GET'
	}).done(function(returnedData) {
		data = returnedData['events'];
		drawCharts();
	});
}

function verifyParams(){
	var start = jQuery('#criteriastart').val();
	var end  = jQuery('#criteriaend').val();
	if(start == null || start.lenght < 1){
		return false;
	}
	
	if(end == null || end.lenght < 1){
		return false;
	}
	
	return getDateOrder(start,end);
}

function getDateOrder(start,end){
	if(start > end){
		return false;
	}else{
		return true;
	}
}

function clear(){
	
}

function getParamsData(){
	params = Object();
	params.start = jQuery('#criteriastart').val();
	params.end = jQuery('#criteriaend').val();
	return params;
}


// DRAW CHARTS


function drawCharts(){
	drawEventsPie();
	drawEventsTable();
	fullSize();
}



function getEventsPieData(){
	var toReturn = new google.visualization.DataTable();
	toReturn.addColumn('string', 'Evento');
    toReturn.addColumn('number', 'Eventos disparados');
    for(var i in data){
    	event = data[i];
    	size = 0;
    	if(event.eventHistoryDetails != null){
    		size = event.eventHistoryDetails.length;
    	}
    	toReturn.addRows([[event.name, size]]);
    }
    return toReturn;
}

function drawEventsPie(){
	
	var dashboard = new google.visualization.Dashboard(
            document.getElementById('dashboard_pie'));
	
	var eventosDisparadosSlider = new google.visualization.ControlWrapper({
        'controlType': 'NumberRangeFilter',
        'containerId': 'quantidade_pie_filter_div',
        'options': {
          'filterColumnLabel': 'Eventos disparados'
        }
      });
	
	var eventosDisparadosPicker = new google.visualization.ControlWrapper({
		'controlType' : 'CategoryFilter',
		'containerId' : 'evento_pie_filter_div',
		'options' : {
			'filterColumnLabel' : 'Evento',
			'ui' : {
				'allowTyping' : false,
				'allowMultiple' : true,
				'selectedValuesLayout' : 'belowStacked',
				'caption' : 'Selecione...'
			}
		}
	});
	
      // Create a pie chart, passing some options
      var pieChart = new google.visualization.ChartWrapper({
        'chartType': 'PieChart',
        'containerId': 'pie_div',
        'options': {
          'width': "100%",
          'height': 350,
          'pieSliceText': 'value',
          'legend': 'right',
          'is3D' : true
        }
      });

      dashboard.bind(eventosDisparadosSlider, pieChart);
      dashboard.bind(eventosDisparadosPicker, pieChart);
      dashboard.draw(getEventsPieData());
}

function getEventsTableData(){
	var toReturn3 = new google.visualization.DataTable();
    toReturn3.addColumn('string', 'Evento');
    toReturn3.addColumn('string', 'Dispositivo');
    toReturn3.addColumn('datetime', 'Data');
    toReturn3.addColumn('string', 'Localização');
    toReturn3.addColumn('number', 'Eventos disparados');
    
    for(var i in data){
    	event = data[i];
    	size = 0;
    	if(event.eventHistoryDetails != null){
    		size = event.eventHistoryDetails.length;
    	}
    	for(var x in event.eventHistoryDetails){
    		device = "";
    		if(event.eventHistoryDetails[x].device != null){
    			device = event.eventHistoryDetails[x].device.name;
    		}
    		metricDate = new Date(getTimestamp(event.eventHistoryDetails[x].metricDate));
    		htmlString = "<button onclick=\"showEventLocation('" + event.name + "'," + event.eventHistoryDetails[x].latitude + "," + event.eventHistoryDetails[x].longitude + ")\">" + event.eventHistoryDetails[x].latitude + "," + event.eventHistoryDetails[x].longitude + "</button>";	
    		toReturn3.addRows([[event.name, device, metricDate, htmlString, size]]);
    	}
    	
    }
    return toReturn3;
}

function getTimestamp(str) {
	var d = str.match(/\d+/g); // extract date parts
	return +new Date(d[0], d[1] - 1, d[2], d[3], d[4], d[5]); // build Date
																// object
}


function drawEventsTable(dashboard, eventosDisparadosSlider , eventosDisparadosPicker, devicesPicker){
	
	var dashboard = new google.visualization.Dashboard(
            document.getElementById('dashboard_table'));
	
	var eventosDisparadosSlider = new google.visualization.ControlWrapper({
        'controlType': 'NumberRangeFilter',
        'containerId': 'quantidade_table_filter_div',
        'options': {
          'filterColumnLabel': 'Eventos disparados'
        }
      });
	
	var eventosDisparadosPicker = new google.visualization.ControlWrapper({
		'controlType' : 'CategoryFilter',
		'containerId' : 'quantidade_table_filter_div',
		'options' : {
			'filterColumnLabel' : 'Evento',
			'ui' : {
				'allowTyping' : false,
				'allowMultiple' : true,
				'selectedValuesLayout' : 'belowStacked',
				'caption' : 'Selecione...'
			}
		}
	});
	
	var devicesPicker = new google.visualization.ControlWrapper({
		'controlType' : 'CategoryFilter',
		'containerId' : 'device_table_filter_div',
		'options' : {
			'filterColumnLabel' : 'Dispositivo',
			'ui' : {
				'allowTyping' : false,
				'allowMultiple' : true,
				'selectedValuesLayout' : 'belowStacked',
				'caption' : 'Selecione...'
			}
		}
	});
	
	var chart = new google.visualization.ChartWrapper({
		'chartType' : 'Table',
		'containerId' : 'table_div',
		'options' : {
			'alternatingRowStyle':true,
			'showRowNumber':true,
			'width': "100%",
	        'height': "100%",
	        'allowHtml':true
			
		}
	});

	dashboard.bind(eventosDisparadosSlider, chart);
    dashboard.bind(eventosDisparadosPicker, chart);
    dashboard.bind(devicesPicker, chart);
    dashboard.draw(getEventsTableData());
}

function changeTab(event, ui){
	if(data != null){
		drawCharts();
	}
}

function print(){
	var recipe =  window.open('','RecipeWindow','width=600,height=600');
    var html = '<html><head><title>Relatório de marcha lenta</title></head><body><div id="myprintrecipe">' 
    	+ jQuery('<div />').append(jQuery('#table_div').clone()).html() 
    + '</div></body></html>';
    recipe.document.open();
    recipe.document.write(html);
    recipe.document.close();
}


