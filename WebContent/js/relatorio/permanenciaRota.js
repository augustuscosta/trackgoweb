var data;
var alturaConteudo = jQuery(window).height();
var altura = jQuery(window).height() -60;
var map;
var marker;

jQuery(document).ready(function() {
	fullSize();
	jQuery.init();
});

function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(altura);
}

jQuery.ajaxSetup({
	beforeSend : function() {
		jQuery('#loader_relatorio_permanencia_rota').show();
	},
	complete : function() {
		jQuery('#loader_relatorio_permanencia_rota').hide();
	},
	success : function() {
	}
});

jQuery.init = function() {
	
	jQuery('#map_dialog').dialog({ 
		autoOpen: false, 
		height: 400, 
		width: 530 });
	
	jQuery.datepicker.regional['ru'] = {
			prevText : 'Anterior',
			nextText : 'Próximo',
			monthNames : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
					'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
			monthNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			dayNames : [ 'Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta',
					'Sábado', 'Domingo' ],
			dayNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			dayNamesMin : [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D' ],
			dateFormat : 'dd/mm/yy'
		};

		jQuery.datepicker.setDefaults(jQuery.datepicker.regional['ru']);

		jQuery('#criteriastart').datetimepicker({
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm:ss',
			timeText : 'Horário',
			hourText : 'Hora',
			minuteText : 'Minuto',
			secondText : 'Segundo',
			currentText : 'Agora',
			closeText : 'Ok'
		});

		jQuery('#criteriaend').datetimepicker({
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm:ss',
			timeText : 'Horário',
			hourText : 'Hora',
			minuteText : 'Minuto',
			secondText : 'Segundo',
			currentText : 'Agora',
			closeText : 'Ok'
		});
		jQuery('#search_button').click(searchPath);
		
		jQuery("#tabs").tabs({
			select : changeTab
		});
		
};

function searchPath(){
	jQuery.ajax({
		url: ctx + "/relatorios/permanecia_rota",
		data : getParamsData(),
		type : 'GET'
	}).done(function(dataReturned) {
		if(!dataReturned.permanencias){
			var alertString = new String("não foram encontrados Dados para o veículo nesta data");
			alert(alertString);
			return;
		}else{
			data = dataReturned;
			drawCharts();
		}
	});
}

function verifyParams(){
	var start = jQuery('#criteriastart').val();
	var end  = jQuery('#criteriaend').val();
	if(start == null || start.lenght < 1){
		return false;
	}
	
	if(end == null || end.lenght < 1){
		return false;
	}
	
	return getDateOrder(start,end);
}

function getDateOrder(start,end){
	if(start > end){
		return false;
	}else{
		return true;
	}
}

function parseDateFromPickerString(dateText){
	var stringDate = dateText.slice(0,10);
	var stringTime = dateText.slice(11,19);
	var dateParts  = stringDate.split("/");
	var timeParts  = stringTime.split(":");
	return new Date(dateParts[2], (dateParts[1] - 1), dateParts[1],timeParts[0],timeParts[1],timeParts[2]);
}

function getParamsData(){
	params = Object();
	params.deviceId = jQuery('#spinner').val();
	params.start = jQuery('#criteriastart').val();
	params.end = jQuery('#criteriaend').val();
	params.rotaId=jQuery('#rotaSpinner').val();
	return params;
}

function changeTab(event, ui){
	if(data != null){
		drawCharts();
	}
}

function initMap(){
	var myOptions = {
			zoom : 12,
			mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("position_map_canvas"), myOptions);
}

function showDeviceLocation(device,lat,lng){
	jQuery('#map_dialog').dialog('open');
	initMap();
	cleanMap();
	var myLatlng = new google.maps.LatLng(lat, lng);
	marker = new google.maps.Marker({
	      position: myLatlng,
	      map: map,
	      title: device
	 });
	marker.setMap(map);
	map.setCenter(myLatlng);
}

function cleanMap(){
	if(marker != null)
		marker.setMap(null);
}

function drawCharts(){
	drawTable();
	fullSize();
}

function getPositionsTableData(){
	var toReturn3 = new google.visualization.DataTable();
    toReturn3.addColumn('string', 'Dispositivo');
    toReturn3.addColumn('string', 'Rota');
    toReturn3.addColumn('datetime', 'Entrada');
    toReturn3.addColumn('datetime', 'Saída');
    toReturn3.addColumn('string', 'Tempo de permanência');
    
    var total = 0;
    for(var i in data.permanencias){
    	permanencia = data.permanencias[i];
    	firstPosition = new Date(getTimestamp(permanencia.firstPosition.date));
    	lastPosition = new Date(getTimestamp(permanencia.lastPosition.date));
    	tempo = getPermanencia(firstPosition, lastPosition);
    	total += tempo;
    	tempoString = tempo.toFixed(2) + " minutos";
    	toReturn3.addRows([[permanencia.device.name, permanencia.rota.name, firstPosition, lastPosition, tempoString]]);
    }
    totalString = total.toFixed(2) + " minutos";
    toReturn3.addRows([["Total: ", null, null, null, totalString]]);
    
    return toReturn3;
}

function getPermanencia(firstPosition, lastPosition){
	milliseconds = lastPosition - firstPosition;
	seconds =  milliseconds/1000;
	minute = seconds/60;
	return minute;
}

function getTimestamp(str) {
	var d = str.match(/\d+/g); // extract date parts
	return +new Date(d[0], d[1] - 1, d[2], d[3], d[4], d[5]); // build Date
																// object
}


function drawTable(){
	var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(getPositionsTableData(), {showRowNumber: true, alternatingRowStyle: true, allowHtml:true, width: "100%", height: "100%"});
}

function print(){
	var recipe =  window.open('','RecipeWindow','width=600,height=600');
    var html = '<html><head><title>Relatório de em rota</title></head><body><div id="myprintrecipe">' + jQuery('<div />').append(jQuery('#table_div').clone()).html() + '</div></body></html>';
    recipe.document.open();
    recipe.document.write(html);
    recipe.document.close();
}
