jQuery(document).ready(function () {
	applyDataTable();
	fullSize();
});

function applyDataTable(){
	if(jQuery("#table_id").length > 0){
		jQuery("#table_id").dataTable({
			"oLanguage": {
				"sInfo": " _TOTAL_ Veículos  exibindo de _START_ a _END_",
				"sEmptyTable": "Não existem veículos cadastrados",
				"sZeroRecords": "Não existem veículos com  filtro pesquisado",
				"sInfoEmpty": "",
				"sInfoFiltered": "(Filtrados _END_ de _MAX_ veículos",
				"sSearch": "Pesquisar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Próximo"
				},
				"sLengthMenu": "Mostrar _MENU_ veículos"
			},
			 aoColumnDefs: [
			                { "aTargets": [ 0 ], "bSortable": true },
			                { "aTargets": [ 1 ], "bSortable": true },
			                { "aTargets": [ 2 ], "bSortable": false }]
		
		});
	}
}

jQuery(".delete").click(function() {
	id = jQuery(this).attr("id");
	jQuery.ajax({
		url: ctx + "/trackgoweb/veiculo/" + id,
		type: "DELETE",
		success: function() {
			location.reload();
		}
	});
});

function validate (){
	return jQuery('form').validation();
}

$(function () { 
	  $('#update_button').click(function(e) {
		 return validate();
	  }); 
	});


var alturaConteudo = jQuery(window).height();
var altura = jQuery(window).height() -60;


function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(altura);

}

jQuery(window).resize(function() {
	alturaConteudo = jQuery(window).height();
	altura = jQuery(window).height() -60;
	fullSize();
});