jQuery(document).ready(function () {
	if(jQuery("#table_id").length > 0){
		jQuery("#table_id").dataTable({
			"oLanguage": {
				"sInfo": " _TOTAL_ Condições  exibindo de _START_ a _END_",
				"sEmptyTable": "Não existem condições cadastradas",
				"sZeroRecords": "Não existem condições com  filtro pesquisado",
				"sInfoEmpty": "",
				"sInfoFiltered": "(Filtrados _END_ de _MAX_ condições)",
				"sSearch": "Pesquisar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Próximo"
				},
				"sLengthMenu": "Mostrar _MENU_ condições"
			}
		}); 	
	}
	fullSize();
	checkEventType();
	populateArray();
	jQuery('input:checkbox:not([safari])').checkbox();
	jQuery('input[safari]:checkbox').checkbox({cls:'jquery-safari-checkbox'});
	jQuery('input:radio').checkbox();
	jQuery.datepicker.regional['ru'] = {
			prevText : 'Anterior',
			nextText : 'Próximo',
			monthNames : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
			               'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
			               monthNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			               dayNames : [ 'Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta',
			                            'S��bado', 'Domingo' ],
			                            dayNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			                            dayNamesMin : [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D' ],
			                            dateFormat : 'dd/mm/yy'
	};

	jQuery.datepicker.setDefaults(jQuery.datepicker.regional['ru']);

	jQuery('#campoData').datetimepicker({
		showSecond : false,
		showMillisec : false,
		timeFormat : 'hh:mm:ss',
		timeText : 'Horário',
		hourText : 'Hora',
		minuteText : 'Minuto',
		secondText : 'Segundo',
		currentText : 'Agora',
		closeText : 'Ok'
	});

});

jQuery(".delete").click(function() {
	id = jQuery(this).attr("id");
	jQuery.ajax({
		url: ctx + "/trackgoweb/condition/" + id,
		type: "DELETE",
		success: function() {
			location.reload();
		}
	});
});

jQuery("#eventType").change(function() {
	checkEventType();
});

function checkEventType(){
	if(jQuery("#eventType") == undefined) return;
	switch (jQuery("#eventType").val()) {
	case "0":
		showScript();
		break;
	case "1":
		showCercas();
		break;
	case "2":
		showCercas();
		break;
	case "3":
		showRotas();
		break;
	case "4":
		showRotas();
		break;

	default:
		break;
	}
}

function populateArray(){

	var readScript = id('campofinal').value;
	if(readScript.length > 4 && readScript.indexOf('&&') > 0){
		conditionArray = readScript.split(" && ");
		arrayCounter = conditionArray.length -1;
	}else if(readScript.length > 4){
		conditionArray[arrayCounter] = readScript;
		arrayCounter+=1;
	}

	for ( var int = 0; int < conditionArray.length; int++) {

		jQuery("#cart ol").find(".placeholder").remove();
		jQuery('<li></li>').text(conditionArray[int]).addClass("cart-item")
		.appendTo("#cart ol");
	}

}


function showScript(){
	jQuery("#script4").show();
	jQuery("#script2").show();
	jQuery("#script3").show();
	jQuery("#cercas").hide();
	jQuery("#cercas2").hide();
	jQuery("#rotas2").hide();
	jQuery("#rotas").hide();
}

function showCercas(){
	jQuery("#script4").hide();
	jQuery("#script2").hide();
	jQuery("#script3").hide();
	jQuery("#rotas2").hide();
	jQuery("#rotas").hide();
	jQuery("#cercas").show();
	jQuery("#cercas2").show();
}

function showRotas(){
	jQuery("#script4").hide();
	jQuery("#script2").hide();
	jQuery("#script3").hide();
	jQuery("#cercas").hide();
	jQuery("#cercas2").hide();
	jQuery("#rotas").show();
	jQuery("#rotas2").show();
}

function id( el ){
	return document.getElementById( el );
}

//Event drag code and logic

var conditionArray = new Array();
var arrayCounter = 0;

jQuery(function() {
	jQuery( "#sortable1, sortable3" ).sortable({
		connectWith: ".connectedSortable",
		opacity: 0.75,
		placeholder: "ui-state-highlight"

	}).disableSelection();

	jQuery( "#sortable2 li" ).draggable({
		connectToSortable: ".connectedSortable",
		helper: "clone",
		opacity: 0.75,

	}) ;

});

jQuery(function() {

	jQuery("#catalog li").draggable({
		appendTo: "body",
		helper: "clone"
	});
	jQuery("#cart ol").droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		accept: ":not(.ui-sortable-helper)",

		drop: function(event, ui) {

			var campo = ui.draggable.attr("id");
			var finalField = '';
			var booleano = '';

			switch (campo) {
			case 'velocidade':
				conditionArray[arrayCounter] = id('campoVelocidade').name+' '+id('selectVelocidade').value +' '+id('campoVelocidade').value;
				arrayCounter+=1;
				jQuery(this).find(".placeholder").remove();
				jQuery("<li></li>").text( id('campoVelocidade').name+' '+id('selectVelocidade').value +' '+id('campoVelocidade').value)
				.addClass("cart-item")
				.appendTo(this);

				break;

			case 'rpm':				
				conditionArray[arrayCounter] = id('campoRpm').name+' '+id('selectRpm').value +' '+id('campoRpm').value;
				arrayCounter+=1;
				jQuery(this).find(".placeholder").remove();
				jQuery("<li></li>").text( id('campoRpm').name+' '+id('selectRpm').value +' '+id('campoRpm').value)
				.addClass("cart-item")
				.appendTo(this);
				break;

			case 'altitude':				
				conditionArray[arrayCounter] = id('campoAltitude').name+' '+id('selectAltitude').value +' '+id('campoAltitude').value;
				arrayCounter+=1;
				jQuery(this).find(".placeholder").remove();
				jQuery("<li></li>").text( id('campoAltitude').name+' '+id('selectAltitude').value +' '+id('campoAltitude').value)
				.addClass("cart-item")
				.appendTo(this);
				break;

			case 'direcao':				
				conditionArray[arrayCounter] = id('campoDirecao').name+' '+id('selectDirecao').value +' '+id('campoDirecao').value;
				arrayCounter+=1;
				jQuery(this).find(".placeholder").remove();
				jQuery("<li></li>").text( id('campoDirecao').name+' '+id('selectDirecao').value +' '+id('campoDirecao').value)
				.addClass("cart-item")
				.appendTo(this);
				break;

			case 'data':				
				conditionArray[arrayCounter] = id('campoData').name+' '+id('selectData').value +' '+parseDateFromPickerString(id('campoData').value);
				arrayCounter+=1;
				jQuery(this).find(".placeholder").remove();
				jQuery("<li></li>").text( id('campoData').name+' '+id('selectData').value +' '+parseDateFromPickerString(id('campoData').value))
				.addClass("cart-item")
				.appendTo(this);
				break;

			case 'portaAberta':
				if(id('campoPortaAberta').checked){
					booleano = 'true';
				}else{
					booleano = 'false';
				}

				conditionArray[arrayCounter] = id('campoPortaAberta').name+' == '+ booleano;
				arrayCounter+=1;
				jQuery(this).find(".placeholder").remove();
				jQuery("<li></li>").text(id('campoPortaAberta').name+' == '+ booleano)
				.addClass("cart-item")
				.appendTo(this);	
				break;

			case 'bloqueio':
				if(id('campoBloqueio').checked){
					booleano = 'true';
				}else{
					booleano = 'false';
				}

				conditionArray[arrayCounter] = id('campoBloqueio').name+' == '+booleano;
				arrayCounter+=1;
				jQuery(this).find(".placeholder").remove();
				jQuery("<li></li>").text(id('campoBloqueio').name+' == '+booleano)
				.addClass("cart-item")
				.appendTo(this);
				break;

			case 'panico':
				if(id('campoPanico').checked){
					booleano = 'true';
				}else{
					booleano = 'false';
				}

				conditionArray[arrayCounter] = id('campoPanico').name+' == '+ booleano;
				arrayCounter+=1;
				jQuery(this).find(".placeholder").remove();
				jQuery("<li></li>").text(id('campoPanico').name+' == '+ booleano)
				.addClass("cart-item")
				.appendTo(this);

				break;

			case 'movimento':				
				if(id('campoMovimento').checked){
					booleano = 'true';
				}else{
					booleano = 'false';
				}

				conditionArray[arrayCounter] = id('campoMovimento').name+' == '+ booleano;
				arrayCounter+=1;
				jQuery(this).find(".placeholder").remove();
				jQuery("<li></li>").text(id('campoMovimento').name+' == '+ booleano)
				.addClass("cart-item")
				.appendTo(this);
				break;
				
			case 'ignicao':				
				if(id('campoIgnicao').checked){
					booleano = 'true';
				}else{
					booleano = 'false';
				}
				
				conditionArray[arrayCounter] = id('campoIgnicao').name+' == '+ booleano;
				arrayCounter+=1;
				jQuery(this).find(".placeholder").remove();
				jQuery("<li></li>").text(id('campoIgnicao').name+' == '+ booleano)
				.addClass("cart-item")
				.appendTo(this);
				break;

			default:
				alert('nao validou o case statement ' + 'valor do id é ' + campo);
			break;
			}

			for ( var int = 0; int < conditionArray.length; int++) {
				if(int == 0){
					finalField += conditionArray[int];
				}else{
					finalField += ' && ' + conditionArray[int];
				}
			}

			id('campofinal').value = finalField; 

		}

	}).sortable({
		items: "li:not(.placeholder)",
		sort: function() {
			jQuery(this).removeClass("ui-state-default");

		}

	});
	jQuery("#catalog ul").droppable({
		hoverClass: "ui-state-hover",
		accept: '.cart-item',
		drop: function(event, ui) {
			var removedField  = ui.draggable.context.childNodes[0].data;
			var finalField ='';
			jQuery(ui.draggable).remove();
			for ( var int = 0; int < conditionArray.length; int++) {
				if(conditionArray[int] == removedField){
					conditionArray.splice(int,1);
					arrayCounter-=1;
					break;
				}
			}

			for ( var int = 0; int < conditionArray.length; int++) {
				if(int == 0){
					finalField += conditionArray[int];
				}else{
					finalField += ' && ' + conditionArray[int];
				}
			}

			id('campofinal').value = finalField; 
		}
	});
});

var alturaConteudo = jQuery(window).height();
var altura = jQuery(window).height() -60;


function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(altura);

}

jQuery(window).resize(function() {
	alturaConteudo = jQuery(window).height();
	altura = jQuery(window).height() -60;
	fullSize();
});  

displayForm = function (elementId){
	var content = [];
	jQuery('#' + elementId + ' input').each(function(){
		var el = jQuery(this);
		if ( (el.attr('type').toLowerCase() == 'radio'))
		{
			if ( this.checked )
				content.push([
				              '"', el.attr('name'), '": ',
				              'value="', ( this.value ), '"',
				              ( this.disabled ? ', disabled' : '' )
				              ].join(''));
		}
		else
			content.push([
			              '"', el.attr('name'), '": ',
			              ( this.checked ? 'checked' : 'not checked' ), 
			              ( this.disabled ? ', disabled' : '' )
			              ].join(''));
	});
	alert(content.join('\n'));
}

changeStyle = function(skin){
	jQuery('#myform :checkbox').checkbox((skin ? {cls: skin} : {}));
}

function parseDateFromPickerString(dateText){
	var stringDate = dateText.slice(0,10);
	var stringTime = dateText.slice(11,19);
	var dateParts  = stringDate.split("/");
	var timeParts  = stringTime.split(":");
	return new Date(dateParts[2], (dateParts[1] - 1), dateParts[1],timeParts[0],timeParts[1],timeParts[2]).getTime();
}
