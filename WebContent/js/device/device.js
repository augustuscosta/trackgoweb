
jQuery(document).ready(function () {
	checkDeviceTypeToShowProtocol();
	applyDataTable();
	fullSize();
	jQuery('input:checkbox:not([safari])').checkbox();
	jQuery('input[safari]:checkbox').checkbox({cls:'jquery-safari-checkbox'});
	jQuery('input:radio').checkbox();

});

function applyDataTable(){
	if(jQuery("#table_id").length > 0){
		jQuery("#table_id").dataTable({
			"oLanguage": {
				"sInfo": " _TOTAL_ Dispositivos  exibindo de _START_ a _END_",
				"sEmptyTable": "Não existem dispositivos cadastrados",
				"sZeroRecords": "Não existem dispositivos com  filtro pesquisado",
				"sInfoEmpty": "",
				"sInfoFiltered": "(Filtrados _END_ de _MAX_ dispositivos)",
				"sSearch": "Pesquisar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Próximo"
				},
				"sLengthMenu": "Mostrar _MENU_ dispositivos"
			},aoColumnDefs: [
				                { "aTargets": [ 0 ], "bSortable": true },
				                { "aTargets": [ 1 ], "bSortable": true },
				                { "aTargets": [ 2 ], "bSortable": false },
				                { "aTargets": [ 3 ], "bSortable": false }
				                ]
			
		});
		}
}

function validate (){
	return jQuery('form').validation();
}

$(function () { 
	$('#update_button').click(function(e) {
		return validate();
	}); 
});

jQuery(".delete").click(function() {
	id = jQuery(this).attr("id");
	jQuery.ajax({
		url: ctx + "/trackgoweb/device/" + id,
		type: "DELETE",
		success: function() {
			location.reload();
		}
	});
});

jQuery("#deviceType").change(function() {
	checkDeviceTypeToShowProtocol();
});


function checkDeviceTypeToShowProtocol(){
	if(jQuery("#deviceType").val() == 2){
		showProtocol();
	}else{
		hideProtocol();
	}
}

function showProtocol(){
	jQuery("#protocol").show();
}

function hideProtocol(){
	jQuery("#protocol").val('');
	jQuery("#protocol").hide();
}


var alturaConteudo = jQuery(window).height();
var altura = jQuery(window).height() -60;


function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(altura);
}

jQuery(window).resize(function() {
	alturaConteudo = jQuery(window).height();
	fullSize();
});  

displayForm = function (elementId){
	var content = [];
	jQuery('#' + elementId + ' input').each(function(){
		var el = jQuery(this);
		if ( (el.attr('type').toLowerCase() == 'radio'))
		{
			if ( this.checked )
				content.push([
				              '"', el.attr('name'), '": ',
				              'value="', ( this.value ), '"',
				              ( this.disabled ? ', disabled' : '' )
				              ].join(''));
		}
		else
			content.push([
			              '"', el.attr('name'), '": ',
			              ( this.checked ? 'checked' : 'not checked' ), 
			              ( this.disabled ? ', disabled' : '' )
			              ].join(''));
	});
	alert(content.join('\n'));
};


changeStyle = function(skin){
	jQuery('#myform :checkbox').checkbox((skin ? {cls: skin} : {}));
};
