var inCall = false;

jQuery(document).ready(function () {
	jQuery.init();
	fullSize();
	reload();
});


function reload(){
	var tid = setInterval(reload,60000);
	if(inCall == false){
		inCall = true;
		jQuery.carregaVeiculos();
	}
		
}


var data2;
var markersArray = new Array();

jQuery.ajaxSetup({
	beforeSend: function() {
		jQuery('#loader').show();
	},
	complete: function(){
		jQuery('#loader').hide();
	},
	success: function() {}
});

function clearOverlays() {
	for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	}
}
var stringSearch ="";

jQuery('#pesquisatext').bind('input', function(e) {
	clearOverlays();
	var counter = 0;
	stringSearch = this.value;
	for(var i in data2.devices){
		var device = data2.devices[i];
		if(device.lastPosition != null &&
				(device.name.toUpperCase().indexOf(this.value.toUpperCase()) != -1 || device.description.toUpperCase().indexOf(this.value.toUpperCase()) != -1  
						|| device.code.toUpperCase().indexOf(this.value.toUpperCase()) != -1)){
			createMarker(device);
			counter +=1;
		}
	}
	if(counter < 1){
		var alertString = new String("Não existem veículos com o filtro digitado","utf-8");
		alert(alertString);
	}
});

function showFiltered(){
	clearOverlays();
	for(var i in data2.devices){
		var device = data2.devices[i];
		if(device.lastPosition != null &&
				(device.name.toUpperCase().indexOf(stringSearch.toUpperCase()) != -1 
						|| device.description.toUpperCase().indexOf(stringSearch.toUpperCase()) != -1  
						|| device.code.toUpperCase().indexOf(stringSearch.toUpperCase()) != -1)){
			createMarker(device);
		}
	}
}

var altura = jQuery(window).height() -60;
var alturaConteudo = jQuery(window).height();


function fullSize() {
	jQuery('#map_canvas').height(altura);														
	jQuery('#menu-l').height(altura);
	jQuery('#conteudo').height(alturaConteudo);
	google.maps.event.trigger(map, 'resize');					
				
}

jQuery(window).resize(function() {
	altura = jQuery(window).height()- 60;
	alturaConteudo = jQuery(window).height();
	
	fullSize();
});  

jQuery.ajaxSetup({
	  beforeSend: function() {
	     jQuery('#loader').show();
	  },
	  complete: function(){
	     $('#loader').hide();
	  },
	  success: function() {}
	});


jQuery.init = function() {
	var latlng = new google.maps.LatLng(-3.745314, -38.469307);
	var myOptions = {
			zoom : 12,
			center : latlng,
			mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

}

jQuery.carregaVeiculos = function() {
	jQuery.ajax({
		url: ctx + "/mapa/devices"
	}).done(function(data) {
		inCall = false;
		clearOverlays();
		data2 = data;
			showFiltered();			
	});
}

function createMarker(device){
	var marker = getMarker(device);
	createInfoWindow(device, marker);
	marker.setMap(map);
	markersArray.push(marker);
}

function getMarkerImage(device){
	var imageUrl = "images/icones_mapa/"+getDeviceState(device)+".png";
	return new google.maps.MarkerImage(
			imageUrl,
			new google.maps.Size(78,80),
			new google.maps.Point(0, 0),
			new google.maps.Point(32,74)
	);		
}

function getDeviceState(device){
	var deviceState = "device_map";
	var delayDate   = 86400000;
	var stringDate = device.lastPosition.date;
	var stringDate2 = stringDate.slice(0,10);
	var dateParts = stringDate2.split("-");
	var stringTime =stringDate.slice(11,19);
	var timeParts = stringTime.split(":");
	var positionDate =new Date(dateParts[0], (dateParts[1] - 1), dateParts[2],timeParts[0],timeParts[1],timeParts[2]);
	var booleanOld = positionDate.getTime() < (new Date().getTime() - delayDate);

	if( booleanOld){
		deviceState += "_old_position";
	}

	deviceState+= returnDeviceDirection(device.lastPosition.course);

	if (device.lastPosition.ignition == true && device.lastPosition.panic == false  && !booleanOld){
		deviceState+="_ignition";
	}

	if (device.lastPosition.openedDoor == true && device.lastPosition.engineBlock == false){
		deviceState+="_opened_door";
	}

	if (device.lastPosition.panic == true && !booleanOld){
		deviceState+="_panic";
	}

	if (device.lastPosition.engineBlock == true){
		deviceState+="_engine_block";
	}

	return deviceState;

}

function returnDeviceDirection(direction){

	switch (direction) {
	case 0:

		return "_norte";
	case 1:

		return "_nordeste";
	case 2:

		return "_leste";
	case 3:

		return "_sudeste";
	case 4:

		return "_sul";
	case 5:

		return "_sudoeste";
	case 6:

		return "_oeste";
	case 7:

		return "_noroeste";
	default:

		return "_indefinida";

	}

}


function getMarker(device){
	var image = getMarkerImage(device);
	return new MarkerWithLabel({
		position: new google.maps.LatLng(device.lastPosition.latitude,device.lastPosition.longitude),
		map: map,
		icon:image,
		title: String(device.id),
		draggable: false,
		labelContent: device.name,
		labelAnchor: new google.maps.Point(-10, 42),
		labelClass: "labels", // the CSS class for the label
		labelStyle: {opacity: 0.75}
	});
	
}

function createInfoWindow(device, marker){
	google.maps.event.addListener(marker, 'click', function() {
		 if (infoBubble2 != null && infoBubble2.isOpen()) {
			 infoBubble2.close();
			 }
		 infoBubble2 = getInfoBubbleForDevice(device, marker);
		 infoBubble2.open(map,marker);
	});
}

var infoBubble2;
function getInfoWindowContentString(device){

	var toReturn = "<div id=\"window\" style=\"width:400px; height:350px\">" +
	"<div><h1 style='float:left;'>" + getStringForValue(device.name) + "</h1>" + "<div style='float:left;'><a href='../trackgoweb/comando/add/" + device.id+"'> <img src='../trackgoweb/images/comandos.png' ></a></div>"+
	"<br/><br/><br/><div class=\"bobble-left\">Tipo de rastreador: </div><div class=\"bobble-right\">Data:</div>" +  
	"<div class=\"sub-bobble-left\">" +getStringForValue(device.deviceType) +" </div><div class=\"sub-bobble-right\">"+ getStringForValue(device.lastPosition.date) + "</div>"+
	"<div class=\"bobble-left\">Velocidade: </div><div  class=\"bobble-right\">Altitude:</div>"+
	"<div class=\"sub-bobble-left\">"+ getStringForValue(device.lastPosition.speed) +  "</div><div class=\"sub-bobble-right\">"  +  getStringForValue(device.lastPosition.altitude) + "</div>"+
	"<div class=\"bobble-left\">Em movimento:</div><div class=\"bobble-right\">Ligado:</div>" + 
	"<div class=\"sub-bobble-left\">"+ movingBtn(device) +  "</div><div class=\"sub-bobble-right\">" + ignitionBtn(device) +"</div>" +
	"<div class=\"bobble-left\">Pânico:</div><div class=\"bobble-right\"  style=\"height:17px;\"></div>" + 
	"<div class=\"sub-bobble-left\">"+panicBtn(device) +  "</div><div class=\"sub-bobble-right\" style=\"height:25px;\"></div>" +
	"<div class=\"bobble-left\">Latitude:</div><div class=\"bobble-right\">Longitude:</div>" + 
	"<div class=\"sub-bobble-left\">"+ getStringForValue(device.lastPosition.latitude) +  " </div><div class=\"sub-bobble-right\">" + getStringForValue(device.lastPosition.longitude) + "</div>" +
	"</div>";
	return toReturn;
}

function getInfoWindowContentStringComando(device){

	var toReturn = "<a href='../trackgoweb/comando/add/" + device.id+"'> Comandos</a>";
	return toReturn;
}


function getInfoBubbleForDevice(device, marker){
	var infoBubble;
	infoBubble = new InfoBubble({
		maxWidht:300
	  });
	infoBubble.addTab('Parâmetros', getInfoWindowContentString(device));
	
	return infoBubble;

}

function on() {
	var btnOn =  "<img id='btnOn' src='../trackgoweb/images/on.png' >";
	return btnOn;
};

function off() {
	var btnOff =  "<img id='btnOn' src='../trackgoweb/images/off.png' >";
	return btnOff;
};
function ignitionBtn(device){
if(device.lastPosition.ignition == true){ 
	return on(); 
	
}else{ 

	return off(); 
	} ;
};

function panicBtn(device){
	if(device.lastPosition.panic == true){ 
		return on(); 
		
	}else{ 

		return off(); 
		} ;
	};
function movingBtn(device){
	if(device.lastPosition.moving == true){ 
		return on(); 
			
	}else{ 

		return off(); 
		} ;
	};

