var markersArray = [];

jQuery(document).ready(function() {
	jQuery.init();
	fullSize();
});

jQuery.ajaxSetup({
	  beforeSend: function() {
	     jQuery('#loader2').show();
	  },
	  complete: function(){
	     jQuery('#loader2').hide();
	  },
	  success: function() {}
	});
var alturaMenu = jQuery(window).height() -60;
var altura = jQuery(window).height() -251;
var largura = jQuery(window).width() -222;
var alturaConteudo = jQuery(window).height();
var alturaLoader = jQuery(window).height()-300;
var larguraLoader = jQuery(window).width()-505;

function fullSize() {
	jQuery('#map_canvas').height(altura);
	jQuery('#map_canvas').width(largura);
	jQuery('#menu-l').height(alturaMenu);
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#loader').height(alturaLoader);
	jQuery('#loader').width(larguraLoader);
	google.maps.event.trigger(map, 'resize');					
				
}

jQuery(window).resize(function() {
	alturaMenu = jQuery(window).height() -60;
	altura = jQuery(window).height()- 251;
	largura = jQuery(window).width() -222;
	alturaConteudo = jQuery(window).height();
	alturaLoader = jQuery(window).height()-300;
	larguraLoader = jQuery(window).width()-505;
	fullSize();
});  

jQuery.init = function() {
	var latlng = new google.maps.LatLng(-3.745314, -38.469307);
	var myOptions = {
		zoom : 12,
		center : latlng,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
	
	jQuery.datepicker.regional['ru'] = {
			prevText : 'Anterior',
			nextText : 'Próximo',
			monthNames : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
					'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
			monthNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			dayNames : [ 'Domingo', 'Segunda', 'Ter��a', 'Quarta', 'Quinta', 'Sexta',
					'Sábado', 'Domingo' ],
			dayNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom' ],
			dayNamesMin : [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D' ],
			dateFormat : 'dd/mm/yy'
		};

		jQuery.datepicker.setDefaults(jQuery.datepicker.regional['ru']);

		jQuery('#criteriastart').datetimepicker({
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm:ss',
			timeText : 'Horário',
			hourText : 'Hora',
			minuteText : 'Minuto',
			secondText : 'Segundo',
			currentText : 'Agora',
			closeText : 'Ok'
		});

		jQuery('#criteriaend').datetimepicker({
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm:ss',
			timeText : 'Horário',
			hourText : 'Hora',
			minuteText : 'Minuto',
			secondText : 'Segundo',
			currentText : 'Agora',
			closeText : 'Ok'
		});
		jQuery('#search_button').click(searchPath);
}

function searchPath() {
	/*if(!verifyParams()){
		var alertString = new String("A data de início é superior a data final corrija os parâmetros");
		alert(alertString);
		return;
	}*/
	clearOverlays();
	jQuery.ajax({
		url: ctx + "/mapa_pesquisa/path",
		data : getParamsData(),
		type : 'GET'
	}).done(function(data) {
		if(!data.device || !data.device.positions  || data.device.positions.length < 1){
			var alertString = new String("não foram encontrados Dados para o veículo nesta data");
			alert(alertString);
			return;
		}
		for (var i in data.device.positions) {
			var position = data.device.positions[i];
			createMarker(data.device, position);
		}
	});
}

function verifyParams(){
	var start = jQuery('#criteriastart').val();
	var end  = jQuery('#criteriaend').val();
	if(start == null || start.lenght < 1){
		return false;
	}
	
	if(end == null || end.lenght < 1){
		return false;
	}
	
	return getDateOrder(start,end);
}

function getDateOrder(start,end){
	if(start > end){
		return false;
	}else{
		return true;
	}
}

function parseDateFromPickerString(dateText){
	var stringDate = dateText.slice(0,10);
	var stringTime = dateText.slice(11,19);
	var dateParts  = stringDate.split("/");
	var timeParts  = stringTime.split(":");
	return new Date(dateParts[2], (dateParts[1] - 1), dateParts[1],timeParts[0],timeParts[1],timeParts[2]);
}

function clearOverlays() {
	  if (markersArray) {
	    for (var i = 0; i < markersArray.length; i++ ) {
	      markersArray[i].setMap(null);
	    }
	  }
	}

function getParamsData(){
	params = Object();
	params.deviceId = jQuery('#spinner').val();
	params.start = jQuery('#criteriastart').val();
	params.end = jQuery('#criteriaend').val();
	return params;
}


function createMarker(device,position) {
	var marker = getMarker(device,position);
	createInfoWindow(device, marker,position);
	marker.setMap(map);
	markersArray.push(marker);
}

function getMarker(device,position){
	var image = getMarkerImage(device,position);
	return new google.maps.Marker({
		position: new google.maps.LatLng(position.latitude,position.longitude),
		map: map,
		icon:image,
		title: String(device.id),
		draggable: false,
		labelContent: device.name,
		labelAnchor: new google.maps.Point(-10, 42),
		labelClass: "labels", // the CSS class for the label
		labelStyle: {opacity: 0.75}
	});
}


function getMarkerImage(device,position){
	var imageUrl = "images/icones_mapa/"+getDeviceState(device,position)+".png";
	return new google.maps.MarkerImage(
			imageUrl,
			new google.maps.Size(78,80),
			new google.maps.Point(0, 0),
			new google.maps.Point(32,74)
	);		
}

function getDeviceState(device,position){
	var deviceState = "device_map";
	var delayDate   = 86400000;
	var stringDate = position.date;
	var stringDate2 = stringDate.slice(0,10);
	var dateParts = stringDate2.split("-");
	var stringTime =stringDate.slice(11,19);
	var timeParts = stringTime.split(":");
	var positionDate =new Date(dateParts[0], (dateParts[1] - 1), dateParts[2],timeParts[0],timeParts[1],timeParts[2]);
	var booleanOld = positionDate.getTime() < (new Date().getTime() - delayDate);

	if( booleanOld){
		deviceState += "_old_position";
	}

	deviceState+= returnDeviceDirection(position.course);

	if (position.ignition == true && position.panic == false  && !booleanOld){
		deviceState+="_ignition";
	}

	if (position.openedDoor == true && position.engineBlock == false){
		deviceState+="_opened_door";
	}

	if (position.panic == true && !booleanOld){
		deviceState+="_panic";
	}

	if (position.engineBlock == true){
		deviceState+="_engine_block";
	}

	return deviceState;

}


function returnDeviceDirection(direction){

	switch (direction) {
	case 0:

		return "_norte";
	case 1:

		return "_nordeste";
	case 2:

		return "_leste";
	case 3:

		return "_sudeste";
	case 4:

		return "_sul";
	case 5:

		return "_sudoeste";
	case 6:

		return "_oeste";
	case 7:

		return "_noroeste";
	default:

		return "_indefinida";

	}

}

var infoBubble2;

function createInfoWindow(device, marker,position){
	google.maps.event.addListener(marker, 'click', function() {
		 if (infoBubble2 != null && infoBubble2.isOpen()) {
			 infoBubble2.close();
			 }
		 infoBubble2 = getInfoBubbleForDevice(device, marker,position);
		 infoBubble2.open(map,marker);
	});
}


function getInfoBubbleForDevice(device, marker,position){
	var infoBubble;
	infoBubble = new InfoBubble({
		maxWidht:300
	  });
	infoBubble.addTab('Param&#234tros', getInfoWindowContentString(device,position));
	
	return infoBubble;

}


function getInfoWindowContentString(device, position){

	var toReturn = "<div id=\"window\" style=\"width:400px; height:350px\">" +
	"<div><h1 style='float:left;'>" + getStringForValue(device.name) + "</h1>" + "<div style='float:left;'><a href='../trackgoweb/comando/add/" + device.id+"'> <img src='../trackgoweb/images/comandos.png' ></a></div>"+
	"<br/><br/><br/><div class=\"bobble-left\">Tipo de rastreador: </div><div class=\"bobble-right\">Data:</div>" +  
	"<div class=\"sub-bobble-left\">" +getStringForValue(device.deviceType) +" </div><div class=\"sub-bobble-right\">"+ getStringForValue(position.date) + "</div>"+
	"<div class=\"bobble-left\">Velocidade: </div><div  class=\"bobble-right\">Altitude:</div>"+
	"<div class=\"sub-bobble-left\">"+ getStringForValue(position.speed) +  "</div><div class=\"sub-bobble-right\">"  + getStringForValue(position.altitude) + "</div>"+
	"<div class=\"bobble-left\">Em movimento:</div><div class=\"bobble-right\">Ligado:</div>" + 
	"<div class=\"sub-bobble-left\">"+ movingBtn(position) +  "</div><div class=\"sub-bobble-right\">" + ignitionBtn(position) +"</div>" +
	"<div class=\"bobble-left\">Pânico:</div><div class=\"bobble-right\"  style=\"height:17px;\"></div>" + 
	"<div class=\"sub-bobble-left\">"+panicBtn(position) +  "</div><div class=\"sub-bobble-right\" style=\"height:25px;\"></div>" +
	"<div class=\"bobble-left\">Latitude:</div><div class=\"bobble-right\">Longitude:</div>" + 
	"<div class=\"sub-bobble-left\">"+ getStringForValue(position.latitude) +  " </div><div class=\"sub-bobble-right\">" + getStringForValue(position.longitude) + "</div>" +
	"</div>"
	;
	return toReturn;
}
function ignitionBtn(position){
	if(position.ignition == true){ 
		return on(); 
		
	}else{ 

		return off(); 
		} ;
};
	
function panicBtn(position){
	if(position.panic == true){ 
		return on(); 
		
	}else{ 

		return off(); 
		} ;
	};
function movingBtn(position){
	if(position.moving == true){ 
		return on(); 
			
	}else{ 

		return off(); 
		} ;
	};
	
	function on() {
		var btnOn =  "<img id='btnOn' src='../trackgoweb/images/on.png' >";
		return btnOn;
	};

	function off() {
		var btnOff =  "<img id='btnOn' src='../trackgoweb/images/off.png' >";
		return btnOff;
	};	
