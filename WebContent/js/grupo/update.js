jQuery(document).ready(function () {
	if(jQuery('#rotaAddEdit')!=undefined){
		updatesEnableAndDisableCheckboxes();		
	}
});

function updatesEnableAndDisableCheckboxes(){
if(jQuery("#permissao") == undefined) return;
	
	if(jQuery("#permissao").val() == "ROLE_ADMINISTRATOR"){
		markAndDisable();
	}else{
		updatesEnable;
	}
}


function updatesEnable(){
	document.getElementsByName("grupo.deviceAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.cercaAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.rotaAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.eventosAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.pontoDeInteresseAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.veiculoAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.usuariosAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.gruposAddEdit")[0].disabled=false;
}
