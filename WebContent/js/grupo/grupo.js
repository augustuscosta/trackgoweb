jQuery(document).ready(function () {
	applyDataTable();
	fullSize();
	jQuery('input:checkbox:not([safari])').checkbox();
	jQuery('input[safari]:checkbox').checkbox({cls:'jquery-safari-checkbox'});
	jQuery('input:radio').checkbox();
});

function applyDataTable(){
	if(jQuery("#table_id").length > 0){
		jQuery("#table_id").dataTable({
			"oLanguage": {
				"sInfo": " _TOTAL_ grupos exibindo de _START_ a _END_",
				"sEmptyTable": "Não existem grupos cadastradas",
				"sZeroRecords": "Não existem grupos com  filtro pesquisado",
				"sInfoEmpty": "",
				"sInfoFiltered": "(Filtrados _END_ de _MAX_ grupos)",
				"sSearch": "Pesquisar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Próximo"
				},
				"sLengthMenu": "Mostrar _MENU_ grupos"
			}, 
			aoColumnDefs: [
				                { "aTargets": [ 0 ], "bSortable": true },
				                { "aTargets": [ 1 ], "bSortable": false }]
			
		});
	}
}

function validate (){
	return jQuery('form').validation();
}

$(function () { 
	  $('#update_button').click(function(e) {
		 return validate();
	  }); 
	});

jQuery(".delete").click(function() {
	id = jQuery(this).attr("id");
	jQuery.ajax({
		url: ctx + "/trackgoweb/grupo/" + id,
		type: "DELETE",
		success: function() {
			location.reload();
		}
	});
});

var alturaConteudo = jQuery(window).height();
var altura = jQuery(window).height() -60;


function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(altura);
	
}

jQuery(window).resize(function() {

	alturaConteudo = jQuery(window).height();
	altura = jQuery(window).height() -60;
	fullSize();
}); 

displayForm = function (elementId)
{
	var content = [];
	jQuery('#' + elementId + ' input').each(function(){
		var el = jQuery(this);
		if ( (el.attr('type').toLowerCase() == 'radio'))
		{
			if ( this.checked )
				content.push([
					'"', el.attr('name'), '": ',
					'value="', ( this.value ), '"',
					( this.disabled ? ', disabled' : '' )
				].join(''));
		}
		else
			content.push([
				'"', el.attr('name'), '": ',
				( this.checked ? 'checked' : 'not checked' ), 
				( this.disabled ? ', disabled' : '' )
			].join(''));
	});
	alert(content.join('\n'));
}

jQuery("#permissao").change(function() {
	enableAndDisableCheckboxes();
});

function enableAndDisableCheckboxes(){
	if(jQuery("#permissao") == undefined) return;
	
	if(jQuery("#permissao").val() == "ROLE_ADMINISTRATOR"){
		markAndDisable();
	}else{
		enable();
	}
}

function markAndDisable(){
	document.getElementsByName("grupo.deviceAddEdit")[0].checked = true;
	document.getElementsByName("grupo.deviceAddEdit")[0].disabled=true;
	document.getElementsByName("grupo.cercaAddEdit")[0].checked = true;
	document.getElementsByName("grupo.cercaAddEdit")[0].disabled=true;
	document.getElementsByName("grupo.rotaAddEdit")[0].checked = true;
	document.getElementsByName("grupo.rotaAddEdit")[0].disabled=true;
	document.getElementsByName("grupo.eventosAddEdit")[0].checked = true;
	document.getElementsByName("grupo.eventosAddEdit")[0].disabled=true;
	document.getElementsByName("grupo.pontoDeInteresseAddEdit")[0].checked = true;
	document.getElementsByName("grupo.pontoDeInteresseAddEdit")[0].disabled=true;
	document.getElementsByName("grupo.veiculoAddEdit")[0].checked = true;
	document.getElementsByName("grupo.veiculoAddEdit")[0].disabled=true;
	document.getElementsByName("grupo.usuariosAddEdit")[0].checked = true;
	document.getElementsByName("grupo.usuariosAddEdit")[0].disabled=true;
	document.getElementsByName("grupo.gruposAddEdit")[0].checked = true;
	document.getElementsByName("grupo.gruposAddEdit")[0].disabled=true;
}




function enable(){
	document.getElementsByName("grupo.deviceAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.cercaAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.rotaAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.eventosAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.pontoDeInteresseAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.veiculoAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.usuariosAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.gruposAddEdit")[0].disabled=false;
	document.getElementsByName("grupo.deviceAddEdit")[0].checked = false;
	document.getElementsByName("grupo.cercaAddEdit")[0].checked = false;
	document.getElementsByName("grupo.rotaAddEdit")[0].checked = false;
	document.getElementsByName("grupo.eventosAddEdit")[0].checked = false;
	document.getElementsByName("grupo.pontoDeInteresseAddEdit")[0].checked = false;
	document.getElementsByName("grupo.veiculoAddEdit")[0].checked = false;
	document.getElementsByName("grupo.usuariosAddEdit")[0].checked = false;
	document.getElementsByName("grupo.gruposAddEdit")[0].checked = false;
}


changeStyle = function(skin){
	jQuery('#myform :checkbox').checkbox((skin ? {cls: skin} : {}));
}
