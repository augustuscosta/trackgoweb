var acessos;

var deviceView = false;

var cercaView = false;

var rotaView = false;

var eventosView = false;

var pontoView = false;

var veiculoView = false;

var usuariosView = false;

var gruposView = false;

var sessionUser;

var userId;

jQuery(document).ready(function () {
	setOrHideViews();
});

function setUserId(usuarioId){
	userId = usuarioId;
}

function carregaUsuario(usuarioId)  {
	jQuery.ajax({
		url: ctx +"/usuarios/find/"+usuarioId
	}).done(function(data) {
		sessionUser = data;
		setValues();
	});
}


function setValues(){
	if(sessionUser == null) return;

	acessos = sessionUser.grupos;

	for ( var int = 0; int < acessos.length; int++) {
		verifyGrupo(acessos[int]);
	}
	
	setOrHideViews();
}

function verifyGrupo(acesso){
	if(acesso.permissao == "ROLE_ADMINISTRATOR"){
		deviceView= true;
		cercaView= true;
		rotaView= true;
		eventosView= true;
		pontoView = true;
		veiculoView = true;
		gruposView = true;
		usuariosView = true;
		return;
	}
	
	if(acesso.deviceAddEdit!= undefined && acesso.deviceAddEdit){
		deviceView= true;
	}

	if(acesso.cercaAddEdi!= undefined && acesso.cercaAddEdit){
		cercaView= true;
	}

	if(acesso.rotaAddEdit!= undefined && acesso.rotaAddEdit){
		rotaView= true;
	}

	if(acesso.eventosAddEdit!= undefined && acesso.eventosAddEdit){
		eventosView= true;
	}

	if(acesso.pontoDeInteresseAddEdit!= undefined && acesso.pontoDeInteresseAddEdit){
		pontoView = true;
	}

	if(acesso.veiculoAddEdit!= undefined && acesso.veiculoAddEdit){
		veiculoView = true;
	}

	if(acesso.gruposAddEdit!=undefined && acesso.gruposAddEdit){
		gruposView = true;
	}

	if(acesso.usuariosAddEdit!=undefined && acesso.usuariosAddEdit){
		usuariosView = true;
	}
}

jQuery(window).resize(function() {
	setOrHideViews();
}); 

function setOrHideViews(){
	if(deviceView != "true"){
		for ( var int = 0; int < document.getElementsByName('dispositivosSelect').length; int++) {
			document.getElementsByName('dispositivosSelect')[int].hidden=true;
		}
		jQuery('#separadordispositivo').hide(this);		
	}
	if(cercaView != "true"){
		for ( var int = 0; int < document.getElementsByName('cercasSelect').length; int++) {
			document.getElementsByName('cercasSelect')[int].hidden=true;
		}
		jQuery('#separadorcerca').hide(this);		
	}
	if(rotaView != "true"){
		for ( var int = 0; int < document.getElementsByName('rotasSelect').length; int++) {
			document.getElementsByName('rotasSelect')[int].hidden=true;
		}
		jQuery('#separadorrota').hide(this);		
	}

	if(pontoView != "true"){
		for ( var int = 0; int < document.getElementsByName('pontosSelect').length; int++) {
			document.getElementsByName('pontosSelect')[int].hidden=true;
		}
		jQuery('#separadorponto').hide(this);		
	}
	if(eventosView != "true"){
		for ( var int = 0; int < document.getElementsByName('eventosSelect').length; int++) {
			document.getElementsByName('eventosSelect')[int].hidden=true;
		}
		jQuery('#separadorevento').hide(this);		
	}
		
	if(veiculoView != "true"){
		for ( var int = 0; int < document.getElementsByName('veiculosSelect').length; int++) {
			document.getElementsByName('veiculosSelect')[int].hidden=true;
		}
		jQuery('#separadorveiculo').hide(this);		
	}
	if(usuariosView != "true"){
		for ( var int = 0; int < document.getElementsByName('usuariosSelect').length; int++) {
			document.getElementsByName('usuariosSelect')[int].hidden=true;
		}
		jQuery('#separadorusuario').hide(this);		
	}
	if(gruposView != "true"){
		for ( var int = 0; int < document.getElementsByName('gruposSelect').length; int++) {
			document.getElementsByName('gruposSelect')[int].hidden=true;
		}
		jQuery('#separadorgrupo').hide(this);
	}
}




