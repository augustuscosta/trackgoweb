jQuery(document).ready(function () {
	applyDataTable();
	fullSize();
});

function applyDataTable(){
	if(jQuery("#table_id").length > 0){
		jQuery("#table_id").dataTable({
			"oLanguage": {
				"sInfo": " _TOTAL_ Pontos de interesse  exibindo de _START_ a _END_",
				"sEmptyTable": "Não existem pontos de interesse",
				"sZeroRecords": "Não existem pontos de interesse com  filtro pesquisado",
				"sInfoEmpty": "",
				"sInfoFiltered": "(Filtrados _END_ de _MAX_ pontos de interesse)",
				"sSearch": "Pesquisar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Próximo"
				},
				"sLengthMenu": "Mostrar _MENU_ pontos de interesse"
			}, 
			aoColumnDefs: [
			                { "aTargets": [ 0 ], "bSortable": true },
			                { "aTargets": [ 1 ], "bSortable": false }]
		});
	}
}

function validate (){
	return jQuery('form').validation();
}

$(function () { 
	$('#update_button').click(function(e) {
		return validate();
	}); 
});

var alturaConteudo = jQuery(window).height();
var altura = jQuery(window).height() -60;
var larguraMapa = jQuery(window).width()- 705;

function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(altura);
	jQuery('#map_canvas').width(larguraMapa);
}

jQuery(window).resize(function() {
	larguraMapa = jQuery(window).width()- 705;
	alturaConteudo = jQuery(window).height();
	altura = jQuery(window).height() -60;
	fullSize();
});  

jQuery(".delete").click(function() {
	id = jQuery(this).attr("id");
	jQuery.ajax({
		url: ctx + "/trackgoweb/pontoDeInteresse/" + id,
		type: "DELETE",
		success: function() {
			location.reload();
		}
	});
});

var map;
var geocoder;
var marker;
var initialLocation;
var browserSupportFlag =  new Boolean();

jQuery(document).ready(function(){
	initialize();
	jQuery('#geocodingButton').click(function(event){
		geocode();
	});
});

function initialize() {

	var latlng = new google.maps.LatLng(-3.745314, -38.469307);
	var myOptions = {
			zoom : 12,
			center : latlng,
			mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	geocoder = new google.maps.Geocoder();
	google.maps.event.addListener(map, 'dblclick', function(event) {
		addMarker(event.latLng);
		jQuery("#latitude").val(event.latLng.lat());
		jQuery("#longitude").val(event.latLng.lng());
		geocodePosition(event.latLng);
	});

	findInitialMapPosition();

}

function findInitialMapPosition(){
	var latitude = jQuery("#latitude").val();
	var longitude = jQuery("#longitude").val();
	if((latitude != null && latitude != 0 && latitude != "") && (longitude != null && longitude != 0 && longitude != "")){
		var latlng = new google.maps.LatLng(latitude,longitude);
		addMarker(latlng);
	}
}

function resizeMap(){
	google.maps.event.trigger(map, 'resize');
}

function geocode() {
	var address = jQuery("#endereco").val();
	geocoder.geocode({
		'address': address,
		'partialmatch': true}, geocodeResult);
}

function geocodeResult(results, status) {
	if (status == 'OK' && results.length > 0) {
		var latLng = results[0].geometry.location;
		addMarker(latLng);
		jQuery("#latitude").val(latLng.lat());
		jQuery("#longitude").val(latLng.lng());
		map.fitBounds(results[0].geometry.viewport);
	} else {
		alert("Endereço não encontrado");
	}
}

function addMarker(latLng){
	if(marker){
		marker.setMap(null);
	}
	marker = new google.maps.Marker({
		position: latLng,
		map: map,
		draggable: true
	});
	map.center = latLng;
	google.maps.event.addListener(marker, 'dragend', function() {
		jQuery("#latitude").val(marker.getPosition().lat());
		jQuery("#longitude").val(marker.getPosition().lng());
		geocodePosition(marker.getPosition());
	});
	geocodePosition(latLng);
}

function geocodePosition(pos) {
	geocoder.geocode({
		latLng: pos
	}, function(responses) {
		if (responses && responses.length > 0) {
			updateMarkerAddress(responses[0].formatted_address);
		} else {
			updateMarkerAddress('');
		}
	});
}

function updateMarkerAddress(str) {
	jQuery('#endereco').val(str);
}

displayForm = function (elementId)
{
	var content = [];
	jQuery('#' + elementId + ' input').each(function(){
		var el = jQuery(this);
		if ( (el.attr('type').toLowerCase() == 'radio'))
		{
			if ( this.checked )
				content.push([
				              '"', el.attr('name'), '": ',
				              'value="', ( this.value ), '"',
				              ( this.disabled ? ', disabled' : '' )
				              ].join(''));
		}
		else
			content.push([
			              '"', el.attr('name'), '": ',
			              ( this.checked ? 'checked' : 'not checked' ), 
			              ( this.disabled ? ', disabled' : '' )
			              ].join(''));
	});
	alert(content.join('\n'));
}

changeStyle = function(skin)
{
	jQuery('#myform :checkbox').checkbox((skin ? {cls: skin} : {}));
}