jQuery(document).ready(function () {
	jQuery(".device").dataTable({
		"oLanguage": {
			"sInfo": " _TOTAL_ Eventos  exibindo de _START_ a _END_",
			"sEmptyTable": "Não existem eventos disparados",
			"sZeroRecords": "Não existem eventos disparados com  filtro pesquisado",
			"sInfoEmpty": "",
			"sInfoFiltered": "(Filtrados _END_ de _MAX_ eventos disparados)",
			"sSearch": "Pesquisar:",
			"oPaginate": {
				"sPrevious": "Anterior",
				"sNext": "Próximo"
			},
			"sLengthMenu": "Mostrar _MENU_ eventos disparados"
		}
	}); 	
	fullSize();
});

var alturaConteudo = jQuery(window).height();
var altura = jQuery(window).height() -60;


function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(altura);
	
}

jQuery(window).resize(function() {

	alturaConteudo = jQuery(window).height();
	altura = jQuery(window).height() -60;
	fullSize();
});  
