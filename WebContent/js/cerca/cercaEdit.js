var latlngMapa = new google.maps.LatLng(-3.745314, -38.469307);

var mapAddCerca;
var path = new google.maps.MVCArray;
var markers = [];

var poly = new google.maps.Polygon({
	fillColor: '#FF0000',
	strokeOpacity: 0.3,
	strokeColor: "#FF0000",
	strokeWeight: 2,
	fillOpacity: 0.05
});

var imagePontoCerca = new google.maps.MarkerImage('images/square_transparent.png',
		new google.maps.Size(11, 11),
		new google.maps.Point(0, 0),
		new google.maps.Point(6, 6)
);
jQuery(document).ready(function () {
	fullSize();
});	

jQuery("#mapaAddCerca").ready( function() { 

	jQuery('#limparButton').click(limparCerca);

	jQuery("#mapaAddCerca").gmap3({ 
		action:'init',
		options:{
			center:[latlngMapa.lat(),latlngMapa.lng()],
			zoom: 14
		}	
	});

	mapAddCerca = jQuery('#mapaAddCerca').gmap3('get');
	mapRef = mapAddCerca;

	//jQuery('#mapaAddCerca').append('');

	poly.setMap(mapAddCerca);
	poly.setPaths(new google.maps.MVCArray([path]));

	google.maps.event.addListener(mapAddCerca, "click", function(event) {

		addPoint(event);
		addPontos();
	});

//	jQuery('#enderecoCerca').autocomplete({
//	source: function() {
//	jQuery("#mapaAddCerca").gmap3({
//	action:'getAddress',
//	address: jQuery(this).val(),
//	callback:function(results){
//	if (!results) return;
//	jQuery('#enderecoCerca').autocomplete('display', results, false);
//	}
//	});
//	},
//	cb:{
//	cast: function(item){
//	return item.formatted_address;
//	},
//	select: function(item) {				
//	mapAddCerca.setCenter(item.geometry.location);				
//	}
//	}
//	});
}); 

function addPoint(event) {

	path.insertAt(path.length, event.latLng);

	var marker = new google.maps.Marker({
		position: event.latLng,
		map: mapAddCerca,
		draggable: true,
		icon: imagePontoCerca
	});
	markers.push(marker);
	marker.setTitle("#" + path.length);

	google.maps.event.addListener(marker, 'click', function() {

		marker.setMap(null);
		for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
		markers.splice(i, 1);
		path.removeAt(i);
		addPontos();

	});

	google.maps.event.addListener(marker, 'dragend', function() {

		for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
		path.setAt(i, marker.getPosition());    
		addPontos();

	});
}

function addPontos() {

	cerca = "";

	for (var i = 0; i < markers.length; ++i) {
		posicao = path.getAt(i);
		cerca = cerca + '<input type="hidden" name="cerca.geoPonto[' + i + '].latitude" value="' + posicao.lat() + '"/>';
		cerca = cerca + '<input type="hidden" name="cerca.geoPonto[' + i + '].longitude" value="' + posicao.lng() + '"/>';
		cerca = cerca + '<input type="hidden" name="cerca.geoPonto[' + i + '].cerca.id" value="${cerca.id}"/>';

	}

	if (markers.length > 0) {
		posicao = path.getAt(0);
	}	  

	jQuery('#cerca').html(cerca);

}

function limparCerca() {

	path = new google.maps.MVCArray;
	poly.setPaths(new google.maps.MVCArray([path]));

	for(var i=0; i<markers.length; i++) {

		markers[i].setMap(null);
	}

	markers = [];
	jQuery('#cerca').html("");

}
var alturaConteudo = jQuery(window).height()-60;
var larguraMapaAddCerca = jQuery(window).width()-350;
var alturaMapaAddCerca = jQuery(window).height()-230;

function fullSize() {
	jQuery('#conteudo').height(alturaConteudo);
	jQuery('#menu-l').height(alturaConteudo);
	jQuery('#mapaAddCerca').width(larguraMapaAddCerca);
	jQuery('#mapaAddCerca').height(alturaMapaAddCerca);
}

jQuery(window).resize(function() {
	alturaConteudo = jQuery(window).height()-60;
	larguraMapaAddCerca = jQuery(window).width()-350;
	alturaMapaAddCerca = jQuery(window).height()-230;
	fullSize();
});  

function validate (){
	return jQuery('form').validation();
}

$(function () { 
	$('#update_button').click(function(e) {
		return validate();
	}); 
});
