jQuery(document).ready(function () {
	applyDataTable();
	fullSize();
});

function applyDataTable(){
	if(jQuery("#table_id").length > 0){
		jQuery("#table_id").dataTable({
			"oLanguage": {
				"sInfo": " _TOTAL_ Cercas  exibindo de _START_ a _END_",
				"sEmptyTable": "Não existem cercas cadastradas",
				"sZeroRecords": "Não existem cercas cadastradas com  filtro pesquisado",
				"sInfoEmpty": "",
				"sInfoFiltered": "(Filtrados _END_ de _MAX_ cercas)",
				"sSearch": "Pesquisar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Próximo"
				},
				"sLengthMenu": "Mostrar _MENU_ cercas"
			}, 
			aoColumnDefs: [
				                { "aTargets": [ 0 ], "bSortable": true },
				                { "aTargets": [ 1 ], "bSortable": false }]
			
		});
	}
}

function validate (){
	return jQuery('form').validation();
}

$(function () { 
	  $('#update_button').click(function(e) {
		 return validate();
	  }); 
	});

jQuery(".delete").click(function() {
	id = jQuery(this).attr("id");
	jQuery.ajax({
		url: ctx + "/trackgoweb/cerca/" + id,
		type: "DELETE",
		success: function() {
			location.reload();
		}
	});
});

var alturaConteudo = jQuery(window).height()-60;


function fullSize() {
	jQuery('#menu-l').height(alturaConteudo);
	jQuery('#conteudo').height(alturaConteudo);
	
}

jQuery(window).resize(function() {
	alturaConteudo = jQuery(window).height()-60;
	fullSize();
});  