package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.events.EventCondition;

@Component
public class EventConditionDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public EventCondition find(EventCondition event){
		EventCondition toReturn = (EventCondition) createCriteria(entityManager, EventCondition.class).add(Restrictions.eq("id", event.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventCondition> getAllEventConditions(){
		List<EventCondition> toReturn = (List<EventCondition>) createCriteria(entityManager, EventCondition.class).list();
		return toReturn;
	}
	
	public void add(EventCondition event){
		entityManager.persist(event);
	}
	
	public void update(EventCondition event){
		entityManager.merge(event);
	}
	
	public void delete(EventCondition event){
		entityManager.remove(event);
	}
}
