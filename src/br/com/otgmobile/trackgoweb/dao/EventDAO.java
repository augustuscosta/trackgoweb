package br.com.otgmobile.trackgoweb.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.model.events.Event;
import br.com.otgmobile.trackgoweb.model.events.EventHistoryDetail;

@Component
public class EventDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Event find(Event event){
		Event toReturn = (Event) createCriteria(entityManager, Event.class).add(Restrictions.eq("id", event.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Event> getAllEvents(){
		List<Event> toReturn = (List<Event>) createCriteria(entityManager, Event.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Event> getAllEvents(Usuario usuario){
		List<Event> toReturn = (List<Event>) addGroupsRestrictionToCriteria(createCriteria(entityManager, Event.class), usuario).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	public void add(Event event){
		entityManager.persist(event);
	}
	
	public void update(Event event){
		entityManager.merge(event);
	}
	
	public void delete(Event event){
		entityManager.remove(event);
	}

	@SuppressWarnings("unchecked")
	public List<EventHistoryDetail> getEventHistoryDetaiils(Event event, Date start, Date end) {
		return createCriteria(entityManager, EventHistoryDetail.class).add(Restrictions.between("metricDate", start, end)).add(Restrictions.eq("event.id", event.getId())).list();
	}
}
