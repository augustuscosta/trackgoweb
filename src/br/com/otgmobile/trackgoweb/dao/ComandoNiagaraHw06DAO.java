package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DevicePathCriteria;
import br.com.otgmobile.trackgoweb.model.niagarahw6.ComandoNiagaraHw06;

@Component
public class ComandoNiagaraHw06DAO  extends AbstractDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public ComandoNiagaraHw06 find(ComandoNiagaraHw06 comando) {
		ComandoNiagaraHw06 toReturn = (ComandoNiagaraHw06) createCriteria(entityManager, ComandoNiagaraHw06.class).add(Restrictions.eq("id", comando.getId())).uniqueResult();
		return toReturn;
	}
	
	public ComandoNiagaraHw06 findLast() {
		ComandoNiagaraHw06 toReturn = (ComandoNiagaraHw06) createCriteria(entityManager, ComandoNiagaraHw06.class).addOrder(Order.desc("id")).setMaxResults(1).uniqueResult();;
		return toReturn;
	}
	
	public ComandoNiagaraHw06 findNotSent(int squenceControl) {
		ComandoNiagaraHw06 toReturn = (ComandoNiagaraHw06) createCriteria(entityManager, ComandoNiagaraHw06.class)
				.add(Restrictions.eq("sequencecontrolnumber", squenceControl))
				.add(Restrictions.eq("sent", false))
				.uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<ComandoNiagaraHw06> fetchAll(){
		List<ComandoNiagaraHw06> toReturn = (List<ComandoNiagaraHw06>) createCriteria(entityManager, ComandoNiagaraHw06.class).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<ComandoNiagaraHw06> fetchAll(Device device){
		List<ComandoNiagaraHw06> toReturn = (List<ComandoNiagaraHw06>) createCriteria(entityManager, ComandoNiagaraHw06.class).add(Restrictions.eq("productserialnumber", device.getCode())).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<ComandoNiagaraHw06> getComandosNotSent(DevicePathCriteria devicePath){
		List<ComandoNiagaraHw06> toReturn = (List<ComandoNiagaraHw06>) createCriteria(entityManager, ComandoNiagaraHw06.class)
				.add(Restrictions.eq("sent", false))
				.add(Restrictions.eq("productserialnumber", devicePath.getDevice().getCode())).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<ComandoNiagaraHw06> getComandosSent(DevicePathCriteria devicePath){
		List<ComandoNiagaraHw06> toReturn = (List<ComandoNiagaraHw06>) createCriteria(entityManager, ComandoNiagaraHw06.class)
				.add(Restrictions.eq("sent", true))
				.add(Restrictions.eq("productserialnumber", devicePath.getDevice().getCode())).list();
		return toReturn;
	}
	
	public void add(ComandoNiagaraHw06 commando){
		ComandoNiagaraHw06 lastComando = findLast();
		if(lastComando != null && lastComando.getSequenceControlNumber() < 255 ){
			commando.setSequenceControlNumber(lastComando.getSequenceControlNumber() + 1);
		}
		else{
			commando.setSequenceControlNumber(0);
		}
		entityManager.persist(commando);
	}
	
	public void update(ComandoNiagaraHw06 commando){
		entityManager.merge(commando);
	}
	
	
	public void deleteAll(ComandoNiagaraHw06 commando){
		Query query = entityManager.createQuery("delete from niagarahw06_sever");
		query.executeUpdate();
	}

}
