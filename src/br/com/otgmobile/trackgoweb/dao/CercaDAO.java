package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Cerca;
import br.com.otgmobile.trackgoweb.model.Usuario;

@Component
public class CercaDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Cerca find(Cerca cerca){
		Cerca toReturn = (Cerca) createCriteria(entityManager, Cerca.class).add(Restrictions.eq("id", cerca.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Cerca> getAllCercas(){
		List<Cerca> toReturn = (List<Cerca>) createCriteria(entityManager, Cerca.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Cerca> getAllCercas(Usuario usuario){
		List<Cerca> toReturn = (List<Cerca>) addGroupsRestrictionToCriteria(createCriteria(entityManager, Cerca.class), usuario).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	public void add(Cerca cerca){
		entityManager.persist(cerca);
	}
	
	public void update(Cerca cerca){
		entityManager.merge(cerca);
	}
	
	public void delete(Cerca cerca){
		entityManager.remove(cerca);
	}
}
