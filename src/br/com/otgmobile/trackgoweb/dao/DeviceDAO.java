package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.Usuario;

@Component
public class DeviceDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Device find(Device device){
		Device toReturn = (Device) createCriteria(entityManager, Device.class)
				.add(Restrictions.eq("id", device.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Device> getAllDevices(){
		List<Device> toReturn = (List<Device>) createCriteria(entityManager, Device.class).addOrder(Order.asc("name")).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Device> getAllDevices(Usuario usuario){
		List<Device> toReturn = (List<Device>) addGroupsRestrictionToCriteria(createCriteria(entityManager, Device.class), usuario).addOrder(Order.asc("name")).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Device> getAllEnabledDevices(){
		List<Device> toReturn = (List<Device>) createCriteria(entityManager, Device.class)
				.add(Restrictions.eq("enabled", true))
				.addOrder(Order.asc("name"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Device> getAllEnabledDevices(Usuario usuario){
		List<Device> toReturn = (List<Device>) addGroupsRestrictionToCriteria(createCriteria(entityManager, Device.class), usuario)
				.add(Restrictions.eq("enabled", true))
				.addOrder(Order.asc("name"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		return toReturn;
	}
	
	public void add(Device device){
		entityManager.persist(device);
	}
	
	public void update(Device device){
		entityManager.merge(device);
	}
	
	public void delete(Device device){
		entityManager.remove(device);
	}
}
