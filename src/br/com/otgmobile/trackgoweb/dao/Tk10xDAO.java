package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DevicePathCriteria;
import br.com.otgmobile.trackgoweb.model.tk10x.TK10XMessage;

@Component
public class Tk10xDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public TK10XMessage getLastPosition(Device device) {
		TK10XMessage toReturn = (TK10XMessage) createCriteria(entityManager, TK10XMessage.class)
				.add(Restrictions.eq("imei", device.getCode()))
				.addOrder(Order.desc("date")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<TK10XMessage> getPositions(DevicePathCriteria devicePath) {
		List<TK10XMessage> toReturn = createCriteria(entityManager, TK10XMessage.class)
				.add(Restrictions.between("date", devicePath.getStart(), devicePath.getEnd()))
				.add(Restrictions.eq("imei", devicePath.getDevice().getCode())).addOrder(Order.asc("date")).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public TK10XMessage getOldPosition(DevicePathCriteria devicePath) {
		List<TK10XMessage> toReturn = createCriteria(entityManager, TK10XMessage.class)
				.add(Restrictions.gt("date", devicePath.getEnd()))
				.add(Restrictions.eq("imei", devicePath.getDevice().getCode())).list();
		if(toReturn == null || toReturn.isEmpty()){
			return getLastOldPosition(devicePath);				
		}
		
		return null;
	}
	
	private TK10XMessage getLastOldPosition(DevicePathCriteria devicePath) {
		TK10XMessage toReturn = (TK10XMessage) createCriteria(entityManager, TK10XMessage.class)
				.add(Restrictions.lt("date", devicePath.getEnd()))
				.add(Restrictions.eq("imei", devicePath.getDevice().getCode()))
				.addOrder(Order.desc("date")).setMaxResults(1).uniqueResult();
		return toReturn;
	}

	public void insertPosition(TK10XMessage position){
		entityManager.persist(position);
	}
	
	public void deleteAllPositions(){
		Query query = entityManager.createQuery("delete from TK10XMessage");
		query.executeUpdate();
	}
	
}
