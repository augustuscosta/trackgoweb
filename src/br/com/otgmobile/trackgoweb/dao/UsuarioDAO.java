package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Usuario;

@Component
public class UsuarioDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Usuario find(Usuario device){
		Usuario toReturn = (Usuario) createCriteria(entityManager, Usuario.class)
				.add(Restrictions.eq("id", device.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> getAll(){
		List<Usuario> toReturn = (List<Usuario>) createCriteria(entityManager, Usuario.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> getAll(Usuario usuario){
		List<Usuario> toReturn = (List<Usuario>) addGroupsRestrictionToCriteria(createCriteria(entityManager, Usuario.class),usuario).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	public void add(Usuario device){
		entityManager.persist(device);
	}
	
	public void update(Usuario device){
		entityManager.merge(device);
	}
	
	public void delete(Usuario device){
		entityManager.remove(device);
	}
	
	public Usuario loadByUsername(String username){
		 return (Usuario) createCriteria(entityManager, Usuario.class).add(Restrictions.eq("username", username)).uniqueResult();
	}
	
	public Usuario loadByToken(String token){
		return (Usuario) createCriteria(entityManager, Usuario.class).add(Restrictions.eq("mobileSessionId", token)).uniqueResult();
	}
}
