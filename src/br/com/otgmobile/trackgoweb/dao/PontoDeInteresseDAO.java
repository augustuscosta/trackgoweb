package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.PontoDeInteresse;
import br.com.otgmobile.trackgoweb.model.Usuario;

@Component
public class PontoDeInteresseDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public PontoDeInteresse find(PontoDeInteresse pontoDeInteresse){
		PontoDeInteresse toReturn = (PontoDeInteresse) createCriteria(entityManager, PontoDeInteresse.class).add(Restrictions.eq("id", pontoDeInteresse.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<PontoDeInteresse> getAllPontoDeInteresses(){
		List<PontoDeInteresse> toReturn = (List<PontoDeInteresse>) createCriteria(entityManager, PontoDeInteresse.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<PontoDeInteresse> getAllPontoDeInteresses(Usuario usuario){
		List<PontoDeInteresse> toReturn = (List<PontoDeInteresse>) addGroupsRestrictionToCriteria(createCriteria(entityManager, PontoDeInteresse.class), usuario).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	public void add(PontoDeInteresse pontoDeInteresse){
		entityManager.persist(pontoDeInteresse);
	}
	
	public void update(PontoDeInteresse pontoDeInteresse){
		entityManager.merge(pontoDeInteresse);
	}
	
	public void delete(PontoDeInteresse pontoDeInteresse){
		entityManager.remove(pontoDeInteresse);
	}
}
