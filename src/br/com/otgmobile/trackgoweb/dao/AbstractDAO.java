package br.com.otgmobile.trackgoweb.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Usuario;

public abstract class AbstractDAO {
	
	public Criteria createCriteria(EntityManager entityManager, Class<?> clazz) {
		return ((Session) entityManager.getDelegate()).createCriteria(clazz);
	}
	
	public Criteria addGroupsRestrictionToCriteria(Criteria criteria, Usuario usuario) {
		List<Long> list = new ArrayList<Long>();
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled())) {
				list.add(grupo.getId());
			}
		}
		return criteria.createCriteria("grupos").add(
				Restrictions.in("id", list));
	}
	
}
