package br.com.otgmobile.trackgoweb.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.model.events.EventHistoryDetail;

@Component
public class EventHistoryDetailDAO extends AbstractDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public EventHistoryDetail find(EventHistoryDetail event){
		EventHistoryDetail toReturn = 
				(EventHistoryDetail) createCriteria(entityManager, EventHistoryDetail.class)
				.add(Restrictions.eq("id", event.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventHistoryDetail> fetchAllnotSent() {
		List<EventHistoryDetail> toReturn = createCriteria(entityManager, EventHistoryDetail.class)
				.add(Restrictions.eq("sent", false)).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventHistoryDetail> fetchAll(Usuario usuario) {
		List<EventHistoryDetail> toReturn = addGroupsRestrictionToCriteria(createCriteria(entityManager, EventHistoryDetail.class),usuario).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventHistoryDetail> fetchAllForMobile(Usuario usuario) {
		List<EventHistoryDetail> toReturn = addGroupsRestrictionToCriteria(createCriteria(entityManager, EventHistoryDetail.class),usuario).setMaxResults(50).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventHistoryDetail> fetchAllSent() {
		List<EventHistoryDetail> toReturn = createCriteria(entityManager, EventHistoryDetail.class)
				.add(Restrictions.eq("sent", false)).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventHistoryDetail> fetchAll() {
		List<EventHistoryDetail> toReturn = createCriteria(entityManager, EventHistoryDetail.class)
				.addOrder(Order.desc("metricDate")).setMaxResults(100).list();
		return toReturn;
	}
	
	public void add(EventHistoryDetail eventHistoryDetail){
		entityManager.persist(eventHistoryDetail);
	}
	
	public void add(List<EventHistoryDetail> eventHistoryDetails){
		if(eventHistoryDetails != null){
			for(EventHistoryDetail detail : eventHistoryDetails)
				add(detail);
		}
	}
	
	public void merge(EventHistoryDetail eventHistoryDetail){
		entityManager.merge(eventHistoryDetail);
	}
	
	public void delete(EventHistoryDetail eventHistoryDetail){
		entityManager.detach(eventHistoryDetail);
	}
	
	
	@Override
	public Criteria addGroupsRestrictionToCriteria(Criteria criteria, Usuario usuario) {
		List<Long> list = new ArrayList<Long>();
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled())) {
				list.add(grupo.getId());
			}
		}
		return criteria.createAlias("device", "d").createCriteria("d.grupos").add(
				Restrictions.in("id", list));
	}

}
