package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DevicePathCriteria;
import br.com.otgmobile.trackgoweb.model.aspicore.AspicoreMessage;

@Component
public class AspicoreDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public AspicoreMessage getLastPosition(Device device) {
		AspicoreMessage toReturn = (AspicoreMessage) createCriteria(entityManager, AspicoreMessage.class)
				.add(Restrictions.eq("imei", device.getCode()))
				.addOrder(Order.desc("postime")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<AspicoreMessage> getPositions(DevicePathCriteria devicePath) {
		List<AspicoreMessage> toReturn = createCriteria(entityManager, AspicoreMessage.class)
				.add(Restrictions.between("postime", devicePath.getStart(), devicePath.getEnd()))
				.add(Restrictions.eq("imei", devicePath.getDevice().getCode())).addOrder(Order.asc("postime")).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public AspicoreMessage getOldPositions(DevicePathCriteria devicePath) {
		List<AspicoreMessage> hasNewPositon = createCriteria(entityManager, AspicoreMessage.class)
				.add(Restrictions.gt("postime", devicePath.getEnd()))
				.add(Restrictions.eq("imei", devicePath.getDevice().getCode())).list();
		if(hasNewPositon == null || hasNewPositon.isEmpty()){
			AspicoreMessage toReturn = getOldLAstPosition(devicePath);
			return toReturn;
		}
		return null;
	}

	private AspicoreMessage getOldLAstPosition(DevicePathCriteria devicePath) {
		return (AspicoreMessage) createCriteria(entityManager, AspicoreMessage.class)
				.add(Restrictions.lt("postime", devicePath.getEnd()))
				.add(Restrictions.eq("imei", devicePath.getDevice().getCode()))
				.addOrder(Order.desc("postime")).setMaxResults(1).uniqueResult();
	}
	
	public void insertPosition(AspicoreMessage position){
		entityManager.persist(position);
	}
	
	public void deleteAllPositions(){
		Query query = entityManager.createQuery("delete from AspicoreMessage");
		query.executeUpdate();
	}
	
}
