package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DevicePathCriteria;
import br.com.otgmobile.trackgoweb.model.maxtrack.Position;

@Component
public class MaxtrackDAO extends AbstractDAO{

	@PersistenceContext
	private EntityManager entityManager;
	
	public Position getLastPosition(Device device) {
		Position toReturn = (Position) createCriteria(entityManager, Position.class)
				.createAlias("gps", "gps")
				.add(Restrictions.eq("serial", device.getCode()))
				.addOrder(Order.desc("gps.date")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Position> getPositions(DevicePathCriteria devicePath) {
		List<Position> toReturn = createCriteria(entityManager, Position.class)
				.createAlias("gps", "gps")
				.add(Restrictions.between("gps.date", devicePath.getStart(), devicePath.getEnd()))
				.add(Restrictions.eq("serial", devicePath.getDevice().getCode())).addOrder(Order.asc("gps.date")).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public Position getOldPosition(DevicePathCriteria devicePath) {
		List<Position> hasNotTrasnmited = createCriteria(entityManager, Position.class)
				.createAlias("gps", "gps")
				.add(Restrictions.gt("gps.date", devicePath.getEnd()))
				.add(Restrictions.eq("serial", devicePath.getDevice().getCode())).list();
		if(hasNotTrasnmited == null || hasNotTrasnmited.isEmpty()){
			return getLastOldPosition(devicePath);
		}
		
		return null;
	}
	
	private Position getLastOldPosition(DevicePathCriteria devicePath) {
		Position toReturn = (Position)createCriteria(entityManager, Position.class)
				.createAlias("gps", "gps")
				.add(Restrictions.lt("gps.date", devicePath.getEnd()))
				.add(Restrictions.eq("serial", devicePath.getDevice().getCode()))
				.addOrder(Order.desc("gps.date")).setMaxResults(1).uniqueResult();
		return toReturn;
	}

	public void insertPosition(Position position){
		entityManager.persist(position);
	}
	
	public void deleteAllPositions(){
		Query query = entityManager.createQuery("delete from Position");
		query.executeUpdate();
	}
	
}
