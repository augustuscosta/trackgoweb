package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.PontoFinal;

@Component
public class PontoFinalDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public PontoFinal find(PontoFinal pontoFinal){
		PontoFinal toReturn = (PontoFinal) createCriteria(entityManager, PontoFinal.class).add(Restrictions.eq("id", pontoFinal.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<PontoFinal> getAllPontoFinals(){
		List<PontoFinal> toReturn = (List<PontoFinal>) createCriteria(entityManager, PontoFinal.class).list();
		return toReturn;
	}
	
	public void add(PontoFinal pontoFinal){
		entityManager.persist(pontoFinal);
	}
	
	public void update(PontoFinal pontoFinal){
		entityManager.merge(pontoFinal);
	}
	
	public void delete(PontoFinal pontoFinal){
		entityManager.remove(pontoFinal);
	}
	
	
}
