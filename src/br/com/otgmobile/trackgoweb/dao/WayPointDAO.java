package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.WayPoint;

@Component
public class WayPointDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public WayPoint find(WayPoint waypoint){
		WayPoint toReturn = (WayPoint) createCriteria(entityManager, WayPoint.class).add(Restrictions.eq("id", waypoint.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<WayPoint> getAllWayPoints(){
		List<WayPoint> toReturn = (List<WayPoint>) createCriteria(entityManager, WayPoint.class).list();
		return toReturn;
	}
	
	public void add(WayPoint wayPoint){
		entityManager.persist(wayPoint);
	}
	
	public void update(WayPoint waypoint){
		entityManager.merge(waypoint);
	}
	
	public void delete(WayPoint wayPoint){
		entityManager.remove(wayPoint);
	}
	
	
}
