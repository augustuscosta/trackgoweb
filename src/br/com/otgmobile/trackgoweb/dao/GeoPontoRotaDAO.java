package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.GeoPontoRota;

@Component
public class GeoPontoRotaDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public GeoPontoRota find(GeoPontoRota geoPontoRota){
		GeoPontoRota toReturn = (GeoPontoRota) createCriteria(entityManager, GeoPontoRota.class).add(Restrictions.eq("id", geoPontoRota.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeoPontoRota> getAllGeoPontoRotas(){
		List<GeoPontoRota> toReturn = (List<GeoPontoRota>) createCriteria(entityManager, GeoPontoRota.class).list();
		return toReturn;
	}
	
	public void add(GeoPontoRota geoPontoRota){
		entityManager.persist(geoPontoRota);
	}
	
	public void update(GeoPontoRota geoPontoRota){
		entityManager.merge(geoPontoRota);
	}
	
	public void delete(GeoPontoRota geoPontoRota){
		entityManager.remove(geoPontoRota);
	}
	
}