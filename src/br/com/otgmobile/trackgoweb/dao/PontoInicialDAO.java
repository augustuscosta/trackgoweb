package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.PontoInicial;

@Component
public class PontoInicialDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public PontoInicial find(PontoInicial pontoInicial){
		PontoInicial toReturn = (PontoInicial) createCriteria(entityManager, PontoInicial.class).add(Restrictions.eq("id", pontoInicial.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<PontoInicial> getAllPontoInicials(){
		List<PontoInicial> toReturn = (List<PontoInicial>) createCriteria(entityManager, PontoInicial.class).list();
		return toReturn;
	}
	
	public void add(PontoInicial pontoInicial){
		entityManager.persist(pontoInicial);
	}
	
	public void update(PontoInicial pontoInicial){
		entityManager.merge(pontoInicial);
	}
	
	public void delete(PontoInicial pontoInicial){
		entityManager.remove(pontoInicial);
	}
	
	
}
