package br.com.otgmobile.trackgoweb.dao;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DeviceType;
import br.com.otgmobile.trackgoweb.model.maxtrack.Comando;

@Component
public class ComandoMaxtrackDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Comando find(Comando comando){
		Comando toReturn = (Comando) createCriteria(entityManager, Comando.class)
				.add(Restrictions.eq("idCommand", comando.getIdCommand())).uniqueResult();
		return toReturn;
	}

	
	@SuppressWarnings("unchecked")
	public List<Comando> getAllMaxtrackCommands(Device device){
		if(device.getDeviceType() != DeviceType.MAXTRACK)
			return Collections.EMPTY_LIST;
		
		List<Comando> toReturn = (List<Comando>) createCriteria(entityManager, Comando.class)
				.add(Restrictions.eq("serial", device.getCode()))
				.list();
		return toReturn;
	}
	
	
	public void add(Comando comando){
		entityManager.persist(comando);
	}
	
	
	public void update(Comando comando){
		entityManager.merge(comando);
	}
	
}
