package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.GeoPontoCerca;

@Component
public class GeoPontoCercaDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public GeoPontoCerca find(GeoPontoCerca geoPontoCercaca){
		GeoPontoCerca toReturn = (GeoPontoCerca) createCriteria(entityManager, GeoPontoCerca.class).add(Restrictions.eq("id", geoPontoCercaca.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeoPontoCerca> getAllGeoPontoCercas(){
		List<GeoPontoCerca> toReturn = (List<GeoPontoCerca>) createCriteria(entityManager, GeoPontoCerca.class).list();
		return toReturn;
	}
	
	public void add(GeoPontoCerca geoPontoCercaca){
		entityManager.persist(geoPontoCercaca);
	}
	
	public void update(GeoPontoCerca geoPontoCercaca){
		entityManager.merge(geoPontoCercaca);
	}
	
	public void delete(GeoPontoCerca geoPontoCercaca){
		entityManager.remove(geoPontoCercaca);
	}
}
