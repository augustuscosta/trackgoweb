package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Rota;
import br.com.otgmobile.trackgoweb.model.Usuario;

@Component
public class RotaDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Rota find(Rota rota){
		Rota toReturn = (Rota) createCriteria(entityManager, Rota.class).add(Restrictions.eq("id", rota.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Rota> getAllRotas(){
		List<Rota> toReturn = (List<Rota>) createCriteria(entityManager, Rota.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Rota> getAllRotas(Usuario usuario){
		List<Rota> toReturn = (List<Rota>) addGroupsRestrictionToCriteria(createCriteria(entityManager, Rota.class), usuario).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	public void add(Rota rota){
		entityManager.persist(rota);
	}
	
	public void update(Rota rota){
		entityManager.merge(rota);
	}
	
	public void delete(Rota rota){
		entityManager.remove(rota);
	}
}

