package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DevicePathCriteria;
import br.com.otgmobile.trackgoweb.model.niagarahw6.NiagaraHW06message;

@Component
public class NiagaraHw6DAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public NiagaraHW06message getLastPosition(Device device) {
		NiagaraHW06message toReturn = (NiagaraHW06message) createCriteria(entityManager, NiagaraHW06message.class)
				.add(Restrictions.eq("productserialnumber", device.getCode()))
				.addOrder(Order.desc("messageDate")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<NiagaraHW06message> getPositions(DevicePathCriteria devicePath) {
		List<NiagaraHW06message> toReturn = createCriteria(entityManager, NiagaraHW06message.class)
				.add(Restrictions.between("messageDate", devicePath.getStart(), devicePath.getEnd()))
				.add(Restrictions.eq("productserialnumber", devicePath.getDevice().getCode())).addOrder(Order.asc("messageDate")).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public NiagaraHW06message getOldPositions(DevicePathCriteria devicePath) {
		List<NiagaraHW06message> hasNotTransmited = createCriteria(entityManager, NiagaraHW06message.class)
				.add(Restrictions.gt("messageDate", devicePath.getEnd()))
				.add(Restrictions.eq("productserialnumber", devicePath.getDevice().getCode())).list();
		if(hasNotTransmited == null || hasNotTransmited.isEmpty()){
			return getLastOldPosition(devicePath);
		}
		return null;
	}
	
	private NiagaraHW06message getLastOldPosition(DevicePathCriteria devicePath) {
		NiagaraHW06message toReturn = (NiagaraHW06message) createCriteria(entityManager, NiagaraHW06message.class)
				.add(Restrictions.gt("messageDate", devicePath.getEnd()))
				.add(Restrictions.eq("productserialnumber", devicePath.getDevice().getCode()))
				.addOrder(Order.desc("messageDate")).setMaxResults(1).uniqueResult();
		return toReturn;
	}

	public void insertPosition(NiagaraHW06message position){
		entityManager.persist(position);
	}
	
	public void deleteAllPositions(){
		Query query = entityManager.createQuery("delete from NiagaraHW06message");
		query.executeUpdate();
	}
	
}
