package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DevicePathCriteria;
import br.com.otgmobile.trackgoweb.model.skypatrol.SkypatrolMessage;

@Component
public class SkypatrolDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public SkypatrolMessage getLastPosition(Device device) {
		SkypatrolMessage toReturn = (SkypatrolMessage) createCriteria(entityManager, SkypatrolMessage.class)
				.add(Restrictions.eq("uniqueID", device.getCode()))
				.addOrder(Order.desc("gpsUtcTime")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<SkypatrolMessage> getPositions(DevicePathCriteria devicePath) {
		List<SkypatrolMessage> toReturn = createCriteria(entityManager, SkypatrolMessage.class)
				.add(Restrictions.between("gpsUtcTime", devicePath.getStart(), devicePath.getEnd()))
				.add(Restrictions.eq("uniqueID", devicePath.getDevice().getCode())).addOrder(Order.asc("gpsUtcTime")).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public SkypatrolMessage getOldPosition(DevicePathCriteria devicePath) {
		List<SkypatrolMessage> toReturn = createCriteria(entityManager, SkypatrolMessage.class)
				.add(Restrictions.gt("gpsUtcTime", devicePath.getEnd()))
				.add(Restrictions.eq("uniqueID", devicePath.getDevice().getCode())).list();
		if(toReturn == null || toReturn.isEmpty()){
			return getLastOlPosition(devicePath);
		}
		
		return null;
	}
	
	private SkypatrolMessage getLastOlPosition(	DevicePathCriteria devicePath) {
		SkypatrolMessage toReturn =  (SkypatrolMessage) createCriteria(entityManager, SkypatrolMessage.class)
				.add(Restrictions.lt("gpsUtcTime", devicePath.getEnd()))
				.add(Restrictions.eq("uniqueID", devicePath.getDevice().getCode()))
				.addOrder(Order.desc("gpsUtcTime")).setMaxResults(1).uniqueResult();
		return toReturn;
	}

	public void insertPosition(SkypatrolMessage position){
		entityManager.persist(position);
	}
	
	public void deleteAllPositions(){
		Query query = entityManager.createQuery("delete from SkypatrolMessage");
		query.executeUpdate();
	}
	
}
