package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.model.Veiculo;

@Component
public class VeiculoDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Veiculo find(Veiculo veiculo){
		Veiculo toReturn = (Veiculo) createCriteria(entityManager, Veiculo.class)
				.add(Restrictions.eq("id", veiculo.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Veiculo> getAllVeiculos(){
		List<Veiculo> toReturn = (List<Veiculo>) createCriteria(entityManager, Veiculo.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Veiculo> getAllVeiculos(Usuario usuario){
		List<Veiculo> toReturn = (List<Veiculo>) addGroupsRestrictionToCriteria(createCriteria(entityManager, Veiculo.class), usuario).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	public void add(Veiculo veiculo){
		entityManager.persist(veiculo);
	}
	
	public void update(Veiculo veiculo){
		entityManager.merge(veiculo);
	}
	
	public void delete(Veiculo veiculo){
		entityManager.remove(veiculo);
	}
}
