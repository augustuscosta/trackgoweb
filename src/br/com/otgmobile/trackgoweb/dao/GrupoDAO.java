package br.com.otgmobile.trackgoweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.trackgoweb.model.Grupo;

@Component
public class GrupoDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Grupo find(Grupo grupo){
		Grupo toReturn = (Grupo) createCriteria(entityManager, Grupo.class).add(Restrictions.eq("id", grupo.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Grupo> getAllGrupos(){
		List<Grupo> toReturn = (List<Grupo>) createCriteria(entityManager, Grupo.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return toReturn;
	}
	
	public void add(Grupo grupo){
		entityManager.persist(grupo);
	}
	
	public void update(Grupo grupo){
		entityManager.merge(grupo);
	}
	
	public void delete(Grupo grupo){
		entityManager.remove(grupo);
	}
	
}
