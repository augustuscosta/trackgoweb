package br.com.otgmobile.trackgoweb.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.AspicoreDAO;
import br.com.otgmobile.trackgoweb.dao.DeviceDAO;
import br.com.otgmobile.trackgoweb.dao.MaxtrackDAO;
import br.com.otgmobile.trackgoweb.dao.NiagaraHw6DAO;
import br.com.otgmobile.trackgoweb.dao.SkypatrolDAO;
import br.com.otgmobile.trackgoweb.dao.Tk10xDAO;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DeviceType;
import br.com.otgmobile.trackgoweb.model.TrackerDataBuilder;
import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.model.aspicore.AspicoreMessage;
import br.com.otgmobile.trackgoweb.model.maxtrack.Position;
import br.com.otgmobile.trackgoweb.model.niagarahw6.NiagaraHW06message;
import br.com.otgmobile.trackgoweb.model.skypatrol.SkypatrolMessage;
import br.com.otgmobile.trackgoweb.model.tk10x.TK10XMessage;

@Service
public class MapaService  extends AbstractService{

	@Autowired
	private DeviceDAO deviceDAO;
	@Autowired
	private MaxtrackDAO maxtrackDAO;
	@Autowired
	private Tk10xDAO tk10xDAO;
	@Autowired
	private AspicoreDAO aspicoreDAO;
	@Autowired
	private SkypatrolDAO skypatrolDAO;
	@Autowired
	private NiagaraHw6DAO niagaraHw6DAO;
	
	public List<Device> getAllEnabledDevicesWithLastPosition(){
		Usuario usuario = getCurentUser();
		List<Device> devices = null;			
		if(isAdministrator(usuario)){
			devices = deviceDAO.getAllEnabledDevices();			
		}else{
			 devices = deviceDAO.getAllEnabledDevices(usuario);						
		}
		if(devices == null)
			return Collections.emptyList();
		for(Device device : devices){
			getLastPosition(device);
		}
		return devices;
	}
	public List<Device> getAllEnabledDevicesWithLastPosition(Usuario usuario){
		List<Device> devices = null;			
		if(isAdministrator(usuario)){
			devices = deviceDAO.getAllEnabledDevices();			
		}else{
			devices = deviceDAO.getAllEnabledDevices(usuario);						
		}
		if(devices == null)
			return Collections.emptyList();
		for(Device device : devices){
			getLastPosition(device);
		}
		return devices;
	}
	

	private void getLastPosition(Device device) {
		if(device.getDeviceType() == DeviceType.ASPICORE)
			getAspicoreData(device);
		if(device.getDeviceType() == DeviceType.MAXTRACK)
			getMaxtrackData(device);
		if(device.getDeviceType() == DeviceType.TK10X)
			getTk10xData(device);
		if(device.getDeviceType() == DeviceType.SKYPATROL)
			getSkypatrolData(device);
		if(device.getDeviceType() == DeviceType.NIAGARAHW6)
			getNiagarHw6Data(device);
	}
	


	private void getNiagarHw6Data(Device device) {
		NiagaraHW06message message = niagaraHw6DAO.getLastPosition(device);
		if(message == null)
			return;
		device.setLastPosition(TrackerDataBuilder.parseNiagaraHW6Data(message));
	}


	private void getSkypatrolData(Device device) {
		SkypatrolMessage message = skypatrolDAO.getLastPosition(device);
		if(message == null)
			return;
		device.setLastPosition(TrackerDataBuilder.parseSkypatrolData(message));
	}


	private void getTk10xData(Device device) {
		TK10XMessage message = tk10xDAO.getLastPosition(device);
		if(message == null)
			return;
		device.setLastPosition(TrackerDataBuilder.parseTk10xkData(message));
	}


	private void getMaxtrackData(Device device) {
		Position position = maxtrackDAO.getLastPosition(device);
		if(position == null)
			return;
		device.setLastPosition(TrackerDataBuilder.parseMaxtrackData(position));
	}


	private void getAspicoreData(Device device) {
		AspicoreMessage message = aspicoreDAO.getLastPosition(device);
		if(message == null)
			return;
		device.setLastPosition(TrackerDataBuilder.parseAspicoreData(message));
	}
}
