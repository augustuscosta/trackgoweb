package br.com.otgmobile.trackgoweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.EventConditionDAO;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;
import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.model.events.EventCondition;

@Service
public class EventConditionService extends AbstractService{

	@Autowired
	private EventConditionDAO eventDAO;
	
	public EventCondition find(EventCondition event){
		return eventDAO.find(event);
	}
	
	public List<EventCondition> getAllEventConditions(){
		return eventDAO.getAllEventConditions();
	}
	
	public Boolean allowed(){
		Usuario usuario = getCurentUser();
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && 
					(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR) ||(grupo.getEventosAddEdit()!= null && grupo.getEventosAddEdit().equals(Boolean.TRUE)))) {
				return true;
			}
		}
		return false;
	}
	
	public void add(EventCondition event){
		eventDAO.add(event);
	}
	
	public void update(EventCondition event){
		eventDAO.update(event);
	}
	
	public void delete(EventCondition event){
		eventDAO.delete(event);
	}
}
