package br.com.otgmobile.trackgoweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.GeoPontoRotaDAO;
import br.com.otgmobile.trackgoweb.dao.PontoFinalDAO;
import br.com.otgmobile.trackgoweb.dao.PontoInicialDAO;
import br.com.otgmobile.trackgoweb.dao.RotaDAO;
import br.com.otgmobile.trackgoweb.dao.WayPointDAO;
import br.com.otgmobile.trackgoweb.model.GeoPontoRota;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;
import br.com.otgmobile.trackgoweb.model.PontoFinal;
import br.com.otgmobile.trackgoweb.model.PontoInicial;
import br.com.otgmobile.trackgoweb.model.Rota;
import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.model.WayPoint;

@Service
public class RotaService extends AbstractService{

	@Autowired
	private RotaDAO rotaDAO;
	
	@Autowired
	private GeoPontoRotaDAO geoPontoRotaDAO;
	
	@Autowired
	private WayPointDAO wayPointDAO;
	
	@Autowired
	private PontoInicialDAO pontoInicialDAO;
	
	@Autowired
	private PontoFinalDAO pontoFinalDAO;
	
	
	public Rota find(Rota rota){
		return rotaDAO.find(rota);
	}
	
	public List<Rota> getAllRotas(){
		List<Grupo> grupos = getCurentUser().getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR)) {
				return rotaDAO.getAllRotas();
			}
		}
		return rotaDAO.getAllRotas(getCurentUser());
	}
	
	public Boolean allowed(){
		Usuario usuario = getCurentUser();
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && 
					(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR) ||(grupo.getRotaAddEdit())!= null && grupo.getRotaAddEdit().equals(Boolean.TRUE))) {
				return true;
			}
		}
		return false;
	}
	
	public void add(Rota rota){
		List<GeoPontoRota> geoPonto = rota.getGeoPonto();
		List<WayPoint> wayPoints = rota.getWayPoints();
		PontoInicial pontoInicial = rota.getPontoInicial();
		PontoFinal pontoFinal = rota.getPontoFinal();
		rota.setPontoFinal(null);
		rota.setPontoInicial(null);
		rota.setGeoPonto(null);
		rota.setWayPoints(null);
		rotaDAO.add(rota);
		createGeoPonto(rota, geoPonto);
		createWayPoint(rota, wayPoints);
		createPontoInicialAndFinal(pontoInicial,pontoFinal,rota);
	}
	
	private void createPontoInicialAndFinal(PontoInicial pontoInicial,PontoFinal pontoFinal, Rota rota) {
		pontoInicial.setRota(rota);
		pontoInicialDAO.add(pontoInicial);
		pontoFinal.setRota(rota);
		pontoFinalDAO.add(pontoFinal);
		
	}

	public void update(Rota rota){
		List<GeoPontoRota> geoPonto = rota.getGeoPonto();
		List<WayPoint> wayPoints = rota.getWayPoints();
		PontoInicial pontoInicial = rota.getPontoInicial();
		PontoFinal pontoFinal = rota.getPontoFinal();
		Rota toUpdate = find(rota);
		toUpdate.setName(rota.getName());
		if(toUpdate.getGeoPonto() != null){
			for(GeoPontoRota ponto : toUpdate.getGeoPonto()){
				geoPontoRotaDAO.delete(ponto);
			}
		}
		deleteWayPointsAndGeoPoints(toUpdate);		
		toUpdate.setGeoPonto(null);
		toUpdate.setWayPoints(null);
		toUpdate.setPontoFinal(null);
		toUpdate.setPontoInicial(null);
		toUpdate.setGrupos(rota.getGrupos());
		rotaDAO.update(toUpdate);
		createGeoPonto(toUpdate, geoPonto);
		createWayPoint(toUpdate, wayPoints);
		createPontoInicialAndFinal(pontoInicial,pontoFinal,rota);
	}
	
	private void deleteWayPointsAndGeoPoints(Rota toUpdate) {
		deleteGeoPoints(toUpdate);
		deleteWayPoints(toUpdate);
		pontoInicialDAO.delete(toUpdate.getPontoInicial());
		pontoFinalDAO.delete(toUpdate.getPontoFinal());
	}

	private void deleteGeoPoints(Rota toUpdate) {
		if(toUpdate.getGeoPonto()!= null && !toUpdate.getGeoPonto().isEmpty()){
			for (GeoPontoRota toDelete : toUpdate.getGeoPonto()) {
				toDelete.setRota(null);
				geoPontoRotaDAO.delete(toDelete);
			}			
		}
	}

	private void deleteWayPoints(Rota toUpdate) {
		if(toUpdate.getWayPoints() != null && !toUpdate.getWayPoints().isEmpty()){
			for(WayPoint wayPoint:toUpdate.getWayPoints()){
				wayPoint.setRota(null);
				wayPointDAO.delete(wayPoint);
			}
		}
	}

	private void createGeoPonto(Rota rota, List<GeoPontoRota> geoPonto) {
		if(geoPonto != null){
			for(GeoPontoRota ponto : geoPonto){
				ponto.setRota(rota);
				geoPontoRotaDAO.add(ponto);
			}
		}
	}
	
	private void createWayPoint(Rota rota, List<WayPoint> wayPoints) {
		if(wayPoints != null){
			for(WayPoint ponto : wayPoints){
				ponto.setRota(rota);
				wayPointDAO.add(ponto);
			}
		}
	}
	
	public void delete(Rota rota){
		rotaDAO.delete(rota);
	}
}
