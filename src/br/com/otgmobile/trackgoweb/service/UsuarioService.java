package br.com.otgmobile.trackgoweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.UsuarioDAO;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;
import br.com.otgmobile.trackgoweb.model.Usuario;

@Service
public class UsuarioService extends AbstractService{

	@Autowired
	private UsuarioDAO usuarioDAO;

	
	public Usuario find(Usuario usuario){
		return usuarioDAO.find(usuario);
	}
	
	public List<Usuario> getAllUsuarios(){
		return usuarioDAO.getAll();
	}
	
	public List<Usuario> getAllUsuarios(Usuario usuario){
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR)) {
				return getAllUsuarios();
			}
		}
		return usuarioDAO.getAll(usuario);
	}
	
	public Boolean allowed(){
		Usuario usuario = getCurentUser();
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && 
					(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR) ||(grupo.getUsuariosAddEdit()!= null && grupo.getUsuariosAddEdit().equals(Boolean.TRUE)))) {
				return true;
			}
		}
		return false;
	}
	
	public Usuario loadByUserName(String username){
		return usuarioDAO.loadByUsername(username);
	}
	
	
	public Usuario loadByToken(String token){
		return usuarioDAO.loadByToken(token);
	}
	
	public void add(Usuario usuario){
		usuarioDAO.add(usuario);
	}
	
	public void update(Usuario usuario){
		usuarioDAO.update(usuario);
	}
	
	
	public void delete(Usuario usuario){
		usuarioDAO.delete(usuario);
	}

}
