package br.com.otgmobile.trackgoweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.DeviceDAO;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;
import br.com.otgmobile.trackgoweb.model.Usuario;

@Service
public class DeviceService extends AbstractService {

	@Autowired
	private DeviceDAO deviceDAO;
	
	public Device find(Device device){
		return deviceDAO.find(device);
	}
	
	public List<Device> getAllDevices(){
		return deviceDAO.getAllDevices();
	}
	
	public List<Device> getAllDevices(Usuario usuario){
		if(isAdministrator(usuario)){
			return getAllDevices();
		}
		return deviceDAO.getAllDevices(usuario);			
	}
	
	public List<Device> getAllEnabledDevices(){
		return deviceDAO.getAllEnabledDevices();
	}
	
	public List<Device> getAllEnabledDevices(Usuario usuario){
		if(isAdministrator(usuario)){
			return getAllDevices();
		}
		return deviceDAO.getAllEnabledDevices(usuario);
	}
	
	public Boolean allowed(){
		Usuario usuario = getCurentUser();
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && 
					(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR) ||(grupo.getDeviceAddEdit()!= null && grupo.getDeviceAddEdit().equals(Boolean.TRUE)))) {
				return true;
			}
		}
		return false;
	}
	
	public void add(Device device){
		deviceDAO.add(device);
	}
	
	public void update(Device device){
		deviceDAO.update(device);
	}
	
	public void delete(Device device){
		deviceDAO.delete(device);
	}
}
