package br.com.otgmobile.trackgoweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.VeiculoDAO;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;
import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.model.Veiculo;

@Service
public class VeiculoService extends AbstractService{

	@Autowired
	private VeiculoDAO veiculoDAO;
	
	public Veiculo find(Veiculo veiculo){
		return veiculoDAO.find(veiculo);
	}
	
	public List<Veiculo> getAllVeiculos(){
		return veiculoDAO.getAllVeiculos();
	}
	
	public List<Veiculo> getAllVeiculos(Usuario usuario){
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR)) {
				return getAllVeiculos();
			}
		}
		return veiculoDAO.getAllVeiculos(usuario);
	}
	
	public Boolean allowed(){
		Usuario usuario = getCurentUser();
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && 
					(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR) ||(grupo.getVeiculoAddEdit())!= null && grupo.getVeiculoAddEdit().equals(Boolean.TRUE))) {
				return true;
			}
		}
		return false;
	}
	
	public void add(Veiculo veiculo){
		veiculoDAO.add(veiculo);
	}
	
	public void update(Veiculo veiculo){
		veiculoDAO.update(veiculo);
	}
	
	public void delete(Veiculo veiculo){
		veiculoDAO.delete(veiculo);
	}
}
