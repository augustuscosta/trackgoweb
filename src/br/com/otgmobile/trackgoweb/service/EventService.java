package br.com.otgmobile.trackgoweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.EventDAO;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;
import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.model.events.Event;

@Service
public class EventService extends AbstractService{

	@Autowired
	private EventDAO eventDAO;
	
	public Event find(Event event){
		return eventDAO.find(event);
	}
	
	public List<Event> getAllEvents(){
		return eventDAO.getAllEvents();
	}
	
	public List<Event> getAllEvents(Usuario usuario){
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR)) {
				return getAllEvents();
			}
		}
		return eventDAO.getAllEvents(usuario);
	}
	
	public Boolean allowed(){
		Usuario usuario = getCurentUser();
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && 
					(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR) ||(grupo.getEventosAddEdit()!= null && grupo.getEventosAddEdit().equals(Boolean.TRUE)))) {
				return true;
			}
		}
		return false;
	}
	
	public void add(Event event){
		eventDAO.add(event);
	}
	
	public void update(Event event){
		eventDAO.update(event);
	}
	
	public void delete(Event event){
		eventDAO.delete(event);
	}
}
