package br.com.otgmobile.trackgoweb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.otgmobile.trackgoweb.dao.UsuarioDAO;
import br.com.otgmobile.trackgoweb.model.Usuario;

@Service("securityServiceTrack")
public class SecurityService implements UserDetailsService {

	@Autowired
	private UsuarioDAO usuarioDAO;

	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioDAO.loadByUsername(username);
		
		if(usuario == null)
			throw new UsernameNotFoundException("Usuario não encontrado");
		
		usuario.setDeviceAddEdit(usuario.getDeviceAddEditFromGroups());
		usuario.setCercaAddEdit(usuario.getCercaAddEditFromGroups());
		usuario.setRotaAddEdit(usuario.getRotaAddEditFromGroups());
		usuario.setEventosAddEdit(usuario.getEventosAddEditFromGroups());
		usuario.setEventosAddEdit(usuario.getEventosAddEditFromGroups());
		usuario.setPontoDeInteresseAddEdit(usuario.getPontoDeInteresseAddEditFromGroups());
		usuario.setVeiculoAddEdit(usuario.getVeiculoAddEditFromGroups());
		usuario.setGruposAddEdit(usuario.getGruposAddEditFromGroups());
		usuario.setUsuariosAddEdit(usuario.getUsuariosAddEditFromGroups());
		
		
		return usuario;
	}
	
}
