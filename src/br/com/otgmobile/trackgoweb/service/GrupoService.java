package br.com.otgmobile.trackgoweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.GrupoDAO;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;
import br.com.otgmobile.trackgoweb.model.Usuario;

@Service
public class GrupoService extends AbstractService{

	@Autowired
	private GrupoDAO grupoDAO;
	
	public Grupo find(Grupo grupo){
		return grupoDAO.find(grupo);
	}
	
	public List<Grupo> getAllGrupos(){
		return grupoDAO.getAllGrupos();
	}
	
	public List<Grupo> getAllGrupos(Usuario usuario){
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR)) {
				return getAllGrupos();
			}
		}
		return usuario.getGrupos();
	}
	
	public Boolean allowed(){
		Usuario usuario = getCurentUser();
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && 
					(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR) ||(grupo.getGruposAddEdit()!= null && grupo.getGruposAddEdit().equals(Boolean.TRUE)))) {
				return true;
			}
		}
		return false;
	}
	
	public void add(Grupo grupo){
		grupoDAO.add(grupo);
	}
	
	public void update(Grupo grupo){
		grupoDAO.update(grupo);
	}
	
	
	public void delete(Grupo grupo){
		grupoDAO.delete(grupo);
	}
}
