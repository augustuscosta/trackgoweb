package br.com.otgmobile.trackgoweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.PontoDeInteresseDAO;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;
import br.com.otgmobile.trackgoweb.model.PontoDeInteresse;
import br.com.otgmobile.trackgoweb.model.Usuario;

@Service
public class PontoDeInteresseService  extends AbstractService{

	@Autowired
	private PontoDeInteresseDAO pontoDeInteresseDAO;
	
	
	public PontoDeInteresse find(PontoDeInteresse pontoDeInteresse){
		return pontoDeInteresseDAO.find(pontoDeInteresse);
	}
	
	public List<PontoDeInteresse> getAllPontoDeInteresses(){
		return pontoDeInteresseDAO.getAllPontoDeInteresses();
	}
	
	public List<PontoDeInteresse> getAllPontoDeInteresses(Usuario usuario){
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR)) {
				return getAllPontoDeInteresses();
			}
		}
		return pontoDeInteresseDAO.getAllPontoDeInteresses(usuario);
	}
	
	public Boolean allowed(){
		Usuario usuario = getCurentUser();
		List<Grupo> grupos = usuario.getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && 
					(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR) ||(grupo.getPontoDeInteresseAddEdit()!= null && grupo.getPontoDeInteresseAddEdit().equals(Boolean.TRUE)))) {
				return true;
			}
		}
		return false;
	}
	
	public void add(PontoDeInteresse pontoDeInteresse){
		pontoDeInteresseDAO.add(pontoDeInteresse);
	}
	
	public void update(PontoDeInteresse pontoDeInteresse){
		PontoDeInteresse toUpdate = find(pontoDeInteresse);
		toUpdate.setTitulo(pontoDeInteresse.getTitulo());
		toUpdate.setDescricao(pontoDeInteresse.getDescricao());
		toUpdate.setEndereco(pontoDeInteresse.getEndereco());
		toUpdate.setLatitude(pontoDeInteresse.getLatitude());
		toUpdate.setLongitude(pontoDeInteresse.getLongitude());
		toUpdate.setGrupos(pontoDeInteresse.getGrupos());
		
		pontoDeInteresseDAO.update(toUpdate);
	}
	
	public void delete(PontoDeInteresse pontoDeInteresse){
		pontoDeInteresseDAO.delete(pontoDeInteresse);
	}
}
