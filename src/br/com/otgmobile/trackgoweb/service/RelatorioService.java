package br.com.otgmobile.trackgoweb.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.EventDAO;
import br.com.otgmobile.trackgoweb.model.events.Event;

@Service
public class RelatorioService extends AbstractService{
	
	@Autowired
	private EventDAO eventDAO;

	public List<Event> getEvents(Date start, Date end) {
		List<Event> events;
		if(isAdministrator(getCurentUser())){
			events = eventDAO.getAllEvents();
		}else{
			events = eventDAO.getAllEvents(getCurentUser());
		}
		for(Event event: events){
			event.setEventHistoryDetails(eventDAO.getEventHistoryDetaiils(event, start, end));
		}
		return events;
	}

}
