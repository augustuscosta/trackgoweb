package br.com.otgmobile.trackgoweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.EventHistoryDetailDAO;
import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.model.events.EventHistoryDetail;

@Service
public class EventHistoryDetailService extends AbstractService{

	@Autowired
	private EventHistoryDetailDAO eventDAO;
	
	public EventHistoryDetail find(EventHistoryDetail event){
		return eventDAO.find(event);
	}
	
	public List<EventHistoryDetail> getAllEventHistoryDetails(){
		if(isAllowed()){
			return eventDAO.fetchAll();
		}
		return eventDAO.fetchAll(getCurentUser());
	}
	
	public List<EventHistoryDetail> getAllEventHistoryDetails(Usuario usuario){
		
		return eventDAO.fetchAll(usuario);
	}
	
	public List<EventHistoryDetail> getAllEventHistoryDetailsForMobile(Usuario usuario){
		
		return eventDAO.fetchAllForMobile(usuario);
	}
	
	public List<EventHistoryDetail> getAllEventHistoryDetailsMobile(){
		return eventDAO.fetchAll();
	}
	
	
	public void add(EventHistoryDetail event){
		eventDAO.add(event);
	}
	
	public void update(EventHistoryDetail event){
		eventDAO.merge(event);
	}
	
	public void delete(EventHistoryDetail event){
		eventDAO.delete(event);
	}
}
