package br.com.otgmobile.trackgoweb.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.AspicoreDAO;
import br.com.otgmobile.trackgoweb.dao.DeviceDAO;
import br.com.otgmobile.trackgoweb.dao.MaxtrackDAO;
import br.com.otgmobile.trackgoweb.dao.NiagaraHw6DAO;
import br.com.otgmobile.trackgoweb.dao.SkypatrolDAO;
import br.com.otgmobile.trackgoweb.dao.Tk10xDAO;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DevicePathCriteria;
import br.com.otgmobile.trackgoweb.model.DeviceType;
import br.com.otgmobile.trackgoweb.model.TrackerDataBuilder;
import br.com.otgmobile.trackgoweb.model.aspicore.AspicoreMessage;
import br.com.otgmobile.trackgoweb.model.maxtrack.Position;
import br.com.otgmobile.trackgoweb.model.niagarahw6.NiagaraHW06message;
import br.com.otgmobile.trackgoweb.model.skypatrol.SkypatrolMessage;
import br.com.otgmobile.trackgoweb.model.tk10x.TK10XMessage;

@Service
public class MapaPesquisaService extends AbstractService{

	@Autowired
	private DeviceDAO deviceDAO;
	@Autowired
	private MaxtrackDAO maxtrackDAO;
	@Autowired
	private Tk10xDAO tk10xDAO;
	@Autowired
	private AspicoreDAO aspicoreDAO;
	@Autowired
	private SkypatrolDAO skypatrolDAO;
	@Autowired
	private NiagaraHw6DAO niagaraHw6DAO;
	
	
	public List<Device> getDevicesNotTransmiting(Date lastDate){
		List<Device> devices = deviceDAO.getAllEnabledDevices(getCurentUser());
		List<Device> devicesToReturn = new ArrayList<Device>();
		for(Device device : devices){
			DevicePathCriteria pathCriteria = new DevicePathCriteria();
			pathCriteria.setDevice(device);
			pathCriteria.setEnd(lastDate);
			Device toAdd = getLastPositionIfOffline(pathCriteria);
			if(toAdd != null){
				devicesToReturn.add(toAdd);
			}else{
				devicesToReturn.add(device);
			}
			
		}
		
		return devicesToReturn;
	}
	
	public Device getDevicePath(DevicePathCriteria criteria){
		criteria.setDevice(deviceDAO.find(criteria.getDevice()));
		return getPath(criteria);
	}
	
	private Device getLastPosition(Device device) {
		if(device.getDeviceType() == DeviceType.ASPICORE)
			getAspicoreData(device);
		if(device.getDeviceType() == DeviceType.MAXTRACK)
			getMaxtrackData(device);
		if(device.getDeviceType() == DeviceType.TK10X)
			getTk10xData(device);
		if(device.getDeviceType() == DeviceType.SKYPATROL)
			getSkypatrolData(device);
		if(device.getDeviceType() == DeviceType.NIAGARAHW6)
			getNiagarHw6Data(device);
		
		return device;
	}
	
	private Device getPath(DevicePathCriteria criteria) {
		if(criteria.getDevice().getDeviceType() == DeviceType.ASPICORE)
			getAspicoreData(criteria);
		if(criteria.getDevice().getDeviceType() == DeviceType.MAXTRACK)
			getMaxtrackData(criteria);
		if(criteria.getDevice().getDeviceType() == DeviceType.TK10X)
			getTk10xData(criteria);
		if(criteria.getDevice().getDeviceType() == DeviceType.SKYPATROL)
			getSkypatrolData(criteria);
		if(criteria.getDevice().getDeviceType() == DeviceType.NIAGARAHW6)
			getNiagarHw6Data(criteria);
		
		return criteria.getDevice();
	}
	
	private Device getLastPositionIfOffline(DevicePathCriteria criteria) {
		if(criteria.getDevice().getDeviceType() == DeviceType.ASPICORE)
			getAspicoreOldData(criteria);
		if(criteria.getDevice().getDeviceType() == DeviceType.MAXTRACK)
			getMaxtrackOldData(criteria);
		if(criteria.getDevice().getDeviceType() == DeviceType.TK10X)
			getTk10xOldData(criteria);
		if(criteria.getDevice().getDeviceType() == DeviceType.SKYPATROL)
			getSkypatrolOldData(criteria);
		if(criteria.getDevice().getDeviceType() == DeviceType.NIAGARAHW6)
			 getNiagarHw6OldData(criteria);
		
		return criteria.getDevice();
	}
	

	private void getNiagarHw6Data(DevicePathCriteria criteria) {
		List<NiagaraHW06message> path = niagaraHw6DAO.getPositions(criteria);
		if(path == null)
			return;
		criteria.getDevice().setPositions(TrackerDataBuilder.parseNiagaraHW6Data(path));
	}
	
	private void getSkypatrolData(DevicePathCriteria criteria) {
		List<SkypatrolMessage> path = skypatrolDAO.getPositions(criteria);
		if(path == null)
			return;
		criteria.getDevice().setPositions(TrackerDataBuilder.parseSkypatrolData(path));
	}
	
	private void getTk10xData(DevicePathCriteria criteria) {
		List<TK10XMessage> path = tk10xDAO.getPositions(criteria);
		if(path == null)
			return;
		criteria.getDevice().setPositions(TrackerDataBuilder.parseTk10xkData(path));
	}
	
	
	private void getMaxtrackData(DevicePathCriteria criteria) {
		List<Position> path = maxtrackDAO.getPositions(criteria);
		if(path == null)
			return;
		criteria.getDevice().setPositions(TrackerDataBuilder.parseMaxtrackData(path));
	}
	
	
	private void getAspicoreData(DevicePathCriteria criteria) {
		List<AspicoreMessage> path = aspicoreDAO.getPositions(criteria);
		if(path == null)
			return;
		criteria.getDevice().setPositions(TrackerDataBuilder.parseAspicoreData(path));
	}
	private void getNiagarHw6OldData(DevicePathCriteria criteria) {
		NiagaraHW06message path = niagaraHw6DAO.getOldPositions(criteria);
		if(path == null)
			return;
		criteria.getDevice().setLastPosition(TrackerDataBuilder.parseNiagaraHW6Data(path));
	}

	private void getSkypatrolOldData(DevicePathCriteria criteria) {
		SkypatrolMessage path = skypatrolDAO.getOldPosition(criteria);
		if(path == null)
			return;
		criteria.getDevice().setLastPosition(TrackerDataBuilder.parseSkypatrolData(path));
	}

	private void getTk10xOldData(DevicePathCriteria criteria) {
		TK10XMessage path = tk10xDAO.getOldPosition(criteria);
		if(path == null)
			return;
		criteria.getDevice().setLastPosition(TrackerDataBuilder.parseTk10xkData(path));
	}


	private void getMaxtrackOldData(DevicePathCriteria criteria) {
		Position path = maxtrackDAO.getOldPosition(criteria);
		if(path == null)
			return;
		criteria.getDevice().setLastPosition(TrackerDataBuilder.parseMaxtrackData(path));
	}


	private void getAspicoreOldData(DevicePathCriteria criteria) {
		AspicoreMessage path = aspicoreDAO.getOldPositions(criteria);
		if(path == null)
			return;
		criteria.getDevice().setLastPosition(TrackerDataBuilder.parseAspicoreData(path));
	}
	
	private void getNiagarHw6Data(Device device) {
		NiagaraHW06message message = niagaraHw6DAO.getLastPosition(device);
		if(message == null)
			return;
		device.setLastPosition(TrackerDataBuilder.parseNiagaraHW6Data(message));
	}


	private void getSkypatrolData(Device device) {
		SkypatrolMessage message = skypatrolDAO.getLastPosition(device);
		if(message == null)
			return;
		device.setLastPosition(TrackerDataBuilder.parseSkypatrolData(message));
	}


	private void getTk10xData(Device device) {
		TK10XMessage message = tk10xDAO.getLastPosition(device);
		if(message == null)
			return;
		device.setLastPosition(TrackerDataBuilder.parseTk10xkData(message));
	}


	private void getMaxtrackData(Device device) {
		Position position = maxtrackDAO.getLastPosition(device);
		if(position == null)
			return;
		device.setLastPosition(TrackerDataBuilder.parseMaxtrackData(position));
	}


	private void getAspicoreData(Device device) {
		AspicoreMessage message = aspicoreDAO.getLastPosition(device);
		if(message == null)
			return;
		device.setLastPosition(TrackerDataBuilder.parseAspicoreData(message));
	}
}
