package br.com.otgmobile.trackgoweb.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.ComandoMaxtrackDAO;
import br.com.otgmobile.trackgoweb.dao.ComandoNiagaraHw06DAO;
import br.com.otgmobile.trackgoweb.model.ComandoWrapper;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DeviceType;
import br.com.otgmobile.trackgoweb.model.maxtrack.Comando;
import br.com.otgmobile.trackgoweb.model.niagarahw6.ComandoNiagaraHw06;
import br.com.otgmobile.trackgoweb.util.ComandoUtil;

@Service
public class ComandoService {

	@Autowired
	private ComandoMaxtrackDAO comandoDAO;

	@Autowired
	private ComandoNiagaraHw06DAO comandoNiagaraHw06DAO;

	public List<ComandoWrapper> getCommands(Device device) {
		if (device.getDeviceType() == DeviceType.MAXTRACK)
			return ComandoUtil
					.getCommandsWrapperFromMaxtrackCommands(comandoDAO
							.getAllMaxtrackCommands(device));
		if (device.getDeviceType() == DeviceType.NIAGARAHW6)
			return ComandoUtil
					.getCommandsWrapperFromNiagarahw6Commands(comandoNiagaraHw06DAO
							.fetchAll(device));
		return Collections.emptyList();
	}

	public void requestPosition(Device device) {
		if (device.getDeviceType() == DeviceType.NIAGARAHW6) {
			ComandoNiagaraHw06 comando = ComandoUtil
					.createRequestPositionCommandNiagaraHw6(device);
			comandoNiagaraHw06DAO.add(comando);
		} else if (device.getDeviceType() == DeviceType.MAXTRACK) {
			Comando comando = ComandoUtil
					.createRequestPositionCommandMaxtrack(device);
			comandoDAO.add(comando);
		}
	}
	
	public void requestDisablePanic(Device device) {
		if (device.getDeviceType() != DeviceType.MAXTRACK)
			return;
		Comando comando = ComandoUtil.createDisablePanicCommand(device);
		comandoDAO.add(comando);
	}

	public void requestAntiTheft(Device device, Boolean enabled) {
		if (device.getDeviceType() != DeviceType.MAXTRACK)
			return;
		Comando comando = ComandoUtil.createAntiTheftCommand(device, enabled);
		comandoDAO.add(comando);
	}

	public void requestDisableAntiTheft(Device device) {
		if (device.getDeviceType() != DeviceType.MAXTRACK)
			return;
		Comando comando = ComandoUtil.createDisableAntiTheftCommand(device);
		comandoDAO.add(comando);
	}

	public void changeTimezone(Device device, Integer timeZone) {
		if (device.getDeviceType() != DeviceType.MAXTRACK)
			return;
		Comando comando = ComandoUtil.createTimeZoneCommand(device, timeZone);
		comandoDAO.add(comando);
	}

	public void changeOutput(Device device, Integer output1, Integer output2,
			Integer output3) {
		if (device.getDeviceType() == DeviceType.MAXTRACK) {
			Comando comando = ComandoUtil.createChangeOutputCommand(device,
					output1, output2, output3);
			comandoDAO.add(comando);
		}else if (device.getDeviceType() == DeviceType.NIAGARAHW6) {
			ComandoNiagaraHw06 comando = ComandoUtil.createActuatorCommand(device, output1);
			comandoNiagaraHw06DAO.add(comando);
		}
	}

	public void changeApn(Device device, String url, String username,
			String password) {
		if (device.getDeviceType() == DeviceType.MAXTRACK) {
			Comando comando = ComandoUtil.createChangeApnCommand(device, url,
					username, password);
			comandoDAO.add(comando);
		} else if (device.getDeviceType() == DeviceType.NIAGARAHW6) {
			ComandoNiagaraHw06 comando = ComandoUtil
					.createChangeApnNiagarahw6Command(device, url, username,
							password);
			comandoNiagaraHw06DAO.add(comando);
		}

	}

	public void changeServer(Device device, String primaryIp,
			String primaryPort, String seconderyIp, String seconderyPort) {
		if (device.getDeviceType() == DeviceType.MAXTRACK) {
			Comando comando = ComandoUtil.createChangeServerCommand(device, primaryIp, primaryPort, seconderyIp, seconderyPort);
			comandoDAO.add(comando);
		} else if (device.getDeviceType() == DeviceType.NIAGARAHW6) {
			ComandoNiagaraHw06 comando = ComandoUtil.createChangeServerNiagarahw6Command(device, primaryIp, primaryPort, seconderyIp, seconderyPort);
			comandoNiagaraHw06DAO.add(comando);
		}
	}

	public void requestReset(Device device) {
		if (device.getDeviceType() != DeviceType.MAXTRACK)
			return;
		Comando comando = ComandoUtil.createResetCommand(device);
		comandoDAO.add(comando);
	}

}
