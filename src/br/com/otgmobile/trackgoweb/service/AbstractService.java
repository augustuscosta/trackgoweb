package br.com.otgmobile.trackgoweb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.UsuarioDAO;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;
import br.com.otgmobile.trackgoweb.model.Usuario;


@Service
public class AbstractService {
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	public Usuario getCurentUser(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = (Usuario) auth.getPrincipal();
		Usuario usuario2 = usuarioDAO.find(usuario);
		return usuario2; 
	}
	
	public boolean isAdministrator(Usuario usuario){
		if(usuario.getGrupos() == null || usuario.getGrupos().isEmpty()){
			return false;
		}
		
		for(Grupo grupo :usuario.getGrupos()){
			if(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR)){
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isAllowed(){
		if(isAdministrator(getCurentUser())){
			return true;
		}
		
		return false;
		
	}

}
