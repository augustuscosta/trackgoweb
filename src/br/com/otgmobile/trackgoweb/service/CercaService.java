package br.com.otgmobile.trackgoweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.trackgoweb.dao.CercaDAO;
import br.com.otgmobile.trackgoweb.dao.GeoPontoCercaDAO;
import br.com.otgmobile.trackgoweb.model.Cerca;
import br.com.otgmobile.trackgoweb.model.GeoPontoCerca;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;

@Service
public class CercaService extends AbstractService{

	@Autowired
	private CercaDAO cercaDAO;
	
	@Autowired
	private GeoPontoCercaDAO geoPontoCercaDAO;
	
	public Cerca find(Cerca cerca){
		return cercaDAO.find(cerca);
	}
	
	public List<Cerca> getAllCercas(){
		List<Grupo> grupos = getCurentUser().getGrupos();
		for (Grupo grupo : grupos) {
			if (Boolean.TRUE.equals(grupo.getEnabled()) && grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR)) {
				return cercaDAO.getAllCercas();
			}
		}
		return cercaDAO.getAllCercas(getCurentUser());
	}
	
	public void add(Cerca cerca){
		List<GeoPontoCerca> geoPonto = cerca.getGeoPonto();
		cerca.setGeoPonto(null);
		cercaDAO.add(cerca);
		createGeoPonto(cerca, geoPonto);
	}
	
	public void update(Cerca cerca){
		List<GeoPontoCerca> geoPonto = cerca.getGeoPonto();
		Cerca toUpdate = find(cerca);
		toUpdate.setName(cerca.getName());
		if(toUpdate.getGeoPonto() != null){
			for(GeoPontoCerca ponto : toUpdate.getGeoPonto()){
				geoPontoCercaDAO.delete(ponto);
			}
		}
		toUpdate.setGeoPonto(null);
		toUpdate.setGrupos(cerca.getGrupos());
		cercaDAO.update(toUpdate);
		createGeoPonto(cerca, geoPonto);
	}
	
	private void createGeoPonto(Cerca cerca, List<GeoPontoCerca> geoPonto) {
		if(geoPonto != null){
			for(GeoPontoCerca ponto : geoPonto){
				ponto.setCerca(cerca);
				geoPontoCercaDAO.add(ponto);
			}
		}
	}
	
	public void delete(Cerca cerca){
		cercaDAO.delete(cerca);
	}
}
