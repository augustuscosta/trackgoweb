package br.com.otgmobile.trackgoweb.controller;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackgoweb.model.events.EventHistoryDetail;
import br.com.otgmobile.trackgoweb.service.EventHistoryDetailService;

@Resource
public class EventHistoryDetailController {

	private static final Logger logger = Logger.getLogger(EventHistoryDetailController.class);
	
	private final Result result;
	private final EventHistoryDetailService eventService;
	
	public EventHistoryDetailController(Result result, EventHistoryDetailService eventService){
		this.result = result;
		this.eventService = eventService;
	}
	
	@Get
	@Path("/event_history_details")
	public void list() {
		try {
			List<EventHistoryDetail> events = eventService.getAllEventHistoryDetails();
			result.include("events", events).include("event").include("device");
		} catch (Exception e) {
			logger.error("Erro ao listar o historico de eventos", e);
			result.include("erros", e.getMessage());
		}
	}

}
