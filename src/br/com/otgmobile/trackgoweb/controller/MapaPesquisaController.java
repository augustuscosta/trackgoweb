package br.com.otgmobile.trackgoweb.controller;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DevicePathCriteria;
import br.com.otgmobile.trackgoweb.service.DeviceService;
import br.com.otgmobile.trackgoweb.service.MapaPesquisaService;
import br.com.otgmobile.trackgoweb.util.DateUtil;

@Resource
public class MapaPesquisaController {
	
	private static final Logger logger = Logger.getLogger(MapaPesquisaController.class);

	private final Result result;
	private final MapaPesquisaService mapaPesquisaService;
	private final DeviceService deviceService;

	public MapaPesquisaController(Result result, MapaPesquisaService mapaPesquisaService, DeviceService deviceService) {
		this.result = result;
		this.mapaPesquisaService = mapaPesquisaService;
		this.deviceService = deviceService;
	}

	@Get
	@Path("/mapa_pesquisa")
	public void index() {
		try {
			List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
			result.include("devices", devices);
		} catch (Exception e) {
			logger.error("Erro ao listar os dispositivos", e);
			result.include("erros", e.getMessage());
		}
	} 
	
	@Get
	@Path("/mapa_pesquisa/path")
	public void getAllEnabledDevices(Integer deviceId, String start, String end) {
		Date startDate = DateUtil.parseDateFromString(start);
		Date endDate = DateUtil.parseDateFromString(end);
		DevicePathCriteria criteria = getCriteria(deviceId, startDate, endDate);
		Device toReturn = mapaPesquisaService.getDevicePath(criteria);
		result.use(Results.json()).from(toReturn, "device").include("positions").serialize();
	}

	private DevicePathCriteria getCriteria(Integer deviceId, Date start,
			Date end) {
		DevicePathCriteria criteria = new DevicePathCriteria();
		Device device = new Device();
		device.setId(deviceId.longValue());
		criteria.setDevice(device);
		criteria.setStart(start);
		criteria.setEnd(end);
		return criteria;
	}

}
