package br.com.otgmobile.trackgoweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackgoweb.model.Rota;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.service.RotaService;
import br.com.otgmobile.trackgoweb.service.GrupoService;

@Resource
public class RotaController {

	private static final Logger logger = Logger.getLogger(RotaController.class);
	
	private final Result result;
	private final RotaService rotaService;
	private final GrupoService grupoService;
	
	public RotaController(Result result, RotaService rotaService, GrupoService grupoService){
		this.result = result;
		this.rotaService = rotaService;
		this.grupoService = grupoService;
	}
	
	@Get("/rota/add")
	public void add() {
		if(!rotaService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}
	
	@Post("/rota/create")
	public void create(Rota rota) {
		if(!rotaService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(rota.getGrupos() != null){
				for(Grupo idGrupo : rota.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getRotas() == null)
						grupo.setRotas(new ArrayList<Rota>());
					grupos.add(grupo);
				}
			}
			rota.setGrupos(grupos);
			rotaService.add(rota);
			result.redirectTo(RotaController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao criar uma nova rota", e);
			result.include("erros", e.getMessage());
			result.redirectTo(RotaController.class).list();
		}
	}
	
	@Get
	@Path("/rota/update/{rota.id}")
	public void edit(Rota rota) {
		if(!rotaService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		rota = rotaService.find(rota);
		result.include("rota", rota).include("grupos");
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}

	@Post("/rota/update")
	public void update(Rota rota) {
		if(!rotaService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(rota.getGrupos() != null){
				for(Grupo idGrupo : rota.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getRotas() == null)
						grupo.setRotas(new ArrayList<Rota>());
					grupos.add(grupo);
				}
			}
			rota.setGrupos(grupos);
			rotaService.update(rota);
			result.redirectTo(RotaController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao atualizar uma rota", e);
			result.include("erros", e.getMessage());
			result.forwardTo(RotaController.class).list();
		}
	}
	
	@Get
	@Path("/rotas")
	public void list() {
		if(!rotaService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Rota> rotas = rotaService.getAllRotas();
			result.include("rotas", rotas);
		} catch (Exception e) {
			logger.error("Erro ao listar as rotas", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Delete("/rota/{rota.id}")
	public void remove(Rota rota) {
		if(!rotaService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			Rota toDelete = rotaService.find(rota);
			rotaService.delete(rotaService.find(toDelete));
			result.forwardTo(RotaController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao excluir uma rota", e);
			result.include("erros", e.getMessage());
			result.forwardTo(RotaController.class).list();
		}
	}
	
}
