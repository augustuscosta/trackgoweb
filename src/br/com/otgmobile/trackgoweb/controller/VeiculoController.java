package br.com.otgmobile.trackgoweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Veiculo;
import br.com.otgmobile.trackgoweb.service.GrupoService;
import br.com.otgmobile.trackgoweb.service.VeiculoService;

@Resource
public class VeiculoController {

	private static final Logger logger = Logger.getLogger(VeiculoController.class);
	
	private final Result result;
	private final VeiculoService veiculoService;
	private final GrupoService grupoService;
	
	public VeiculoController(Result result, VeiculoService veiculoService, GrupoService grupoService){
		this.result = result;
		this.veiculoService = veiculoService;
		this.grupoService = grupoService;
	}
	
	@Get("/veiculo/add")
	public void add() {
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}
	
	@Post("/veiculo/create")
	public void create(Veiculo veiculo) {
		try {
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(veiculo.getGrupos() != null){
				for(Grupo idGrupo : veiculo.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getVeiculos() == null)
						grupo.setVeiculos(new ArrayList<Veiculo>());
					grupos.add(grupo);
				}
			}
			veiculo.setGrupos(grupos);
			veiculoService.add(veiculo);
			result.forwardTo(VeiculoController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao criar um novo veiculo", e);
			result.include("erros", e.getMessage());
			result.redirectTo(VeiculoController.class).list();
		}
	}
	
	@Get
	@Path("/veiculo/update/{veiculo.id}")
	public void edit(Veiculo veiculo) {
		veiculo = veiculoService.find(veiculo);
		result.include("veiculo", veiculo).include("grupos");
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}

	@Post("/veiculo/update")
	public void update(Veiculo veiculo) {
		try {
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(veiculo.getGrupos() != null){
				for(Grupo idGrupo : veiculo.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getDevices() == null)
						grupo.setDevices(new ArrayList<Device>());
					grupos.add(grupo);
				}
			}
			veiculo.setGrupos(grupos);
			veiculoService.update(veiculo);
			result.redirectTo(VeiculoController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao atualizar um veiculo", e);
			result.include("erros", e.getMessage());
			result.forwardTo(VeiculoController.class).list();
		}
	}
	
	@Get
	@Path("/veiculos")
	public void list() {
		try {
			List<Veiculo> veiculos = veiculoService.getAllVeiculos(veiculoService.getCurentUser());
			result.include("veiculos", veiculos);
		} catch (Exception e) {
			logger.error("Erro ao listar os veiculos", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Delete("/veiculo/{veiculo.id}")
	public void remove(Veiculo veiculo) {
		try {
			Veiculo toDelete = veiculoService.find(veiculo);
			veiculoService.delete(veiculoService.find(toDelete));
			result.forwardTo(VeiculoController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao excluir um veiculo", e);
			result.include("erros", e.getMessage());
			result.forwardTo(VeiculoController.class).list();
		}
	}
	
}
