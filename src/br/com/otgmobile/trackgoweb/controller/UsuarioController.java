package br.com.otgmobile.trackgoweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.service.GrupoService;
import br.com.otgmobile.trackgoweb.service.UsuarioService;
import br.com.otgmobile.trackgoweb.util.EncriptarMD5;

@Resource
public class UsuarioController {

	private static final Logger logger = Logger.getLogger(UsuarioController.class);
	
	
	
	private final Result result;
	private final UsuarioService usuarioService;
	private final GrupoService grupoService;
	
	public UsuarioController(Result result, UsuarioService usuarioService, GrupoService grupoService){
		this.result = result;
		this.usuarioService = usuarioService;
		this.grupoService = grupoService;
	}
	
	@Get("/usuario/add")
	public void add() {
		if(!usuarioService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}
	
	@Post("/usuario/create")
	public void create(Usuario usuario) {
		if(!usuarioService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			if(usuario.getEnabled() == null)
				usuario.setEnabled(false);
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(usuario.getGrupos() != null){
				for(Grupo idGrupo : usuario.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getUsuarios() == null)
						grupo.setUsuarios(new ArrayList<Usuario>());
					grupos.add(grupo);
				}
			}
			usuario.setPassword(EncriptarMD5.encriptar(usuario.getPassword()));
			usuario.setGrupos(grupos);
			usuarioService.add(usuario);
			result.redirectTo(UsuarioController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao criar uma nova usuario", e);
			result.include("erros", e.getMessage());
			result.redirectTo(UsuarioController.class).list();
		}
	}
	
	@Get
	@Path("/usuario/update/{usuario.id}")
	public void edit(Usuario usuario) {
		if(!usuarioService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		usuario = usuarioService.find(usuario);
		usuario.setPassword("");
		result.include("usuario", usuario).include("grupos");
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}

	@Post("/usuario/update")
	public void update(Usuario usuario) {
		if(!usuarioService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(usuario.getGrupos() != null){
				for(Grupo idGrupo : usuario.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getUsuarios() == null)
						grupo.setUsuarios(new ArrayList<Usuario>());
					grupos.add(grupo);
				}
			}
			Usuario toUpdate = usuarioService.find(usuario);
			toUpdate.setGrupos(grupos);
			toUpdate.setName(usuario.getName());
			toUpdate.setEmail(usuario.getEmail());
			toUpdate.setEnabled(usuario.getEnabled());
			toUpdate.setUsername(usuario.getUsername());
			toUpdate.setMailNotifications(usuario.getMailNotifications());
			toUpdate.setSmsNotifications(usuario.getSmsNotifications());
			toUpdate.setMobilephoneNumber(usuario.getMobilephoneNumber());
			
			if(usuario.getPassword() != null && usuario.getPassword().length() > 0){
				toUpdate.setPassword(EncriptarMD5.encriptar(usuario.getPassword()));				
			}
			
			usuarioService.update(toUpdate);
			result.redirectTo(UsuarioController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao atualizar uma usuario", e);
			result.include("erros", e.getMessage());
			result.forwardTo(UsuarioController.class).list();
		}
	}
	
	@Get
	@Path("/usuarios")
	public void list() {
		if(!usuarioService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Usuario> usuarios = usuarioService.getAllUsuarios(usuarioService.getCurentUser());
			result.include("usuarios", usuarios);
		} catch (Exception e) {
			logger.error("Erro ao listar as usuarios", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Get
	@Path("/usuarios/find/{usuario.id}")
	public void find(Usuario usuario) {
		try {
			Usuario usuario2 = usuarioService.find(usuario);
			List<Grupo> grupos = usuario2.getGrupos();
			result.use(Results.json()).from(grupos,"grupos").serialize();
		} catch (Exception e) {
			logger.error("Erro ao listar as usuarios", e);
			result.include("erros", e.getMessage());
		}
	}
	
	
	
	@Delete("/usuario/{usuario.id}")
	public void remove(Usuario usuario) {
		if(!usuarioService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			Usuario toDelete = usuarioService.find(usuario);
			usuarioService.delete(usuarioService.find(toDelete));
			result.forwardTo(UsuarioController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao excluir uma usuario", e);
			result.include("erros", e.getMessage());
			result.forwardTo(UsuarioController.class).list();
		}
	}
	
}
