package br.com.otgmobile.trackgoweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.PontoDeInteresse;
import br.com.otgmobile.trackgoweb.service.GrupoService;
import br.com.otgmobile.trackgoweb.service.PontoDeInteresseService;

@Resource
public class PontoDeInteresseController {

	private static final Logger logger = Logger.getLogger(PontoDeInteresseController.class);
	
	private final Result result;
	private final PontoDeInteresseService pontoDeInteresseService;
	private final GrupoService grupoService;
	
	public PontoDeInteresseController(Result result, PontoDeInteresseService pontoDeInteresseService, GrupoService grupoService){
		this.result = result;
		this.pontoDeInteresseService = pontoDeInteresseService;
		this.grupoService = grupoService;
	}
	
	@Get("/pontoDeInteresse/add")
	public void add() {
		if(!pontoDeInteresseService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}
	
	@Post("/pontoDeInteresse/create")
	public void create(PontoDeInteresse pontoDeInteresse) {
		try {
			if(!pontoDeInteresseService.allowed()){
				result.forwardTo(IndexController.class).unauthorized();
				return;
			}
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(pontoDeInteresse.getGrupos() != null){
				for(Grupo idGrupo : pontoDeInteresse.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getPontosDeInteresse() == null)
						grupo.setPontosDeInteresse(new ArrayList<PontoDeInteresse>());
					grupos.add(grupo);
				}
			}
			pontoDeInteresse.setGrupos(grupos);
			pontoDeInteresseService.add(pontoDeInteresse);
			result.redirectTo(PontoDeInteresseController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao criar uma nova pontoDeInteresse", e);
			result.include("erros", e.getMessage());
			result.redirectTo(PontoDeInteresseController.class).list();
		}
	}
	
	@Get
	@Path("/pontoDeInteresse/update/{pontoDeInteresse.id}")
	public void edit(PontoDeInteresse pontoDeInteresse) {
		if(!pontoDeInteresseService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		pontoDeInteresse = pontoDeInteresseService.find(pontoDeInteresse);
		result.include("pontoDeInteresse", pontoDeInteresse);
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}

	@Post("/pontoDeInteresse/update")
	public void update(PontoDeInteresse pontoDeInteresse) {
		try {
			if(!pontoDeInteresseService.allowed()){
				result.forwardTo(IndexController.class).unauthorized();
				return;
			}
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(pontoDeInteresse.getGrupos() != null){
				for(Grupo idGrupo : pontoDeInteresse.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getPontosDeInteresse() == null)
						grupo.setPontosDeInteresse(new ArrayList<PontoDeInteresse>());
					grupos.add(grupo);
				}
			}
			pontoDeInteresse.setGrupos(grupos);
			pontoDeInteresseService.update(pontoDeInteresse);
			result.redirectTo(PontoDeInteresseController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao atualizar uma pontoDeInteresse", e);
			result.include("erros", e.getMessage());
			result.forwardTo(PontoDeInteresseController.class).list();
		}
	}
	
	@Get
	@Path("/pontoDeInteresses")
	public void list() {
		if(!pontoDeInteresseService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<PontoDeInteresse> pontoDeInteresses = pontoDeInteresseService.getAllPontoDeInteresses(pontoDeInteresseService.getCurentUser());
			result.include("pontoDeInteresses", pontoDeInteresses);
		} catch (Exception e) {
			logger.error("Erro ao listar as pontoDeInteresse", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Delete("/pontoDeInteresse/{pontoDeInteresse.id}")
	public void remove(PontoDeInteresse pontoDeInteresse) {
		if(!pontoDeInteresseService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			PontoDeInteresse toDelete = pontoDeInteresseService.find(pontoDeInteresse);
			pontoDeInteresseService.delete(pontoDeInteresseService.find(toDelete));
			result.forwardTo(PontoDeInteresseController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao excluir uma pontoDeInteresse", e);
			result.include("erros", e.getMessage());
			result.forwardTo(PontoDeInteresseController.class).list();
		}
	}
	
}
