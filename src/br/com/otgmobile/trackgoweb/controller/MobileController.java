package br.com.otgmobile.trackgoweb.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.Usuario;
import br.com.otgmobile.trackgoweb.model.events.EventHistoryDetail;
import br.com.otgmobile.trackgoweb.service.ComandoService;
import br.com.otgmobile.trackgoweb.service.DeviceService;
import br.com.otgmobile.trackgoweb.service.EventHistoryDetailService;
import br.com.otgmobile.trackgoweb.service.MapaService;
import br.com.otgmobile.trackgoweb.service.UsuarioService;

@Resource
public class MobileController {

	private static final Logger logger = Logger.getLogger(MobileController.class);

	@Autowired
	@Qualifier("authManager")
	protected AuthenticationManager authenticationManager;

	private final Result result;
	private final MapaService mapaService;
	private ComandoService comandoService;
	private DeviceService deviceService;
	private EventHistoryDetailService eventHistoryDetailService;
	private UsuarioService usuarioService; 


	public MobileController(Result result, MapaService mapaService, ComandoService comandoService, DeviceService deviceService, EventHistoryDetailService eventHistoryDetailService, UsuarioService usuarioService) {
		this.result = result;
		this.mapaService = mapaService;
		this.comandoService = comandoService;
		this.deviceService = deviceService;
		this.eventHistoryDetailService = eventHistoryDetailService;
		this.usuarioService = usuarioService;
	}



	@Post
	@Path("/mobile/session")
	public void login(String username, String password) {
		UsernamePasswordAuthenticationToken usernamePassAuthToken = new UsernamePasswordAuthenticationToken(username, password);
		try{
			Authentication auth = authenticationManager.authenticate(usernamePassAuthToken);
			if(auth.isAuthenticated()){
				SecurityContextHolder.getContext().setAuthentication(auth);
				Usuario usuario = usuarioService.loadByUserName(username);
				usuario.setMobileSessionId(RequestContextHolder.currentRequestAttributes().getSessionId().toString());
				usuarioService.update(usuario);
				result.use(Results.json()).from(RequestContextHolder.currentRequestAttributes().getSessionId().toString(), "token").serialize();

			} else {
				result.use(Results.http()).setStatusCode(401);
			}
		}catch(BadCredentialsException e){
			result.use(Results.http()).setStatusCode(401);

		}
	}

	@Get
	@Path("/mobile/vehicles/{token}")
	public void getAllEnabledDevices(String token) throws JSONException {
		Usuario usuario = null;
		if(token != null && token.lastIndexOf("{") == -1){
			usuario = usuarioService.loadByToken(token.toString());			
		}else if(token != null){
			JSONObject jsonObject = new JSONObject(token);
			usuario = usuarioService.loadByToken(jsonObject.get("token").toString());						
		}
		if(usuario == null){
			result.use(Results.http()).setStatusCode(401);			
			return;
		}
		List<Device> toReturn = mapaService.getAllEnabledDevicesWithLastPosition(usuario);
		result.use(Results.json()).from(toReturn, "devices").include("lastPosition").serialize();
	}

	@Get
	@Path("/mobile/events/{token}")
	public void getAllEventsHistory(String token) throws JSONException {
		Usuario usuario = null;
		if(token != null && token.lastIndexOf("{") == -1){
			usuario = usuarioService.loadByToken(token.toString());			
		}else if(token != null){
			JSONObject jsonObject = new JSONObject(token);
			usuario = usuarioService.loadByToken(jsonObject.get("token").toString());						
		}	
		if(usuario == null){
			result.use(Results.http()).setStatusCode(401);			
			return;
		}
		List<EventHistoryDetail> toReturn = eventHistoryDetailService.getAllEventHistoryDetailsForMobile(usuario);
		result.use(Results.json()).from(toReturn, "events").include("device").include("event").serialize();
	}


	@Post("/mobile/comando/request_position")
	public void requestPosition(String token,Long device) {
		try {
			Device obj = new Device();
			obj.setId(device);
			obj = deviceService.find(obj);    
			comandoService.requestPosition(obj);
			result.include("device", obj);
			result.use(Results.json()).from("").serialize();
		} catch (Exception e) {
			logger.error("Erro ao pedir posição", e);
			result.include("erros", e.getMessage());
		}
	}

	@Post("/mobile/comando/disable_panic")
	public void disablePanic(String token,Long device) {
		try {
			Device obj = new Device();
			obj.setId(device);
			obj = deviceService.find(obj);
			comandoService.requestDisablePanic(obj);
			result.include("device", obj);
			result.use(Results.json()).from("").serialize();
		} catch (Exception e) {
			logger.error("Erro ao desativar o estado de pânico", e);
			result.include("erros", e.getMessage());
		}
	}

	@Post("/mobile/comando/anti_theft")
	public void antiTheft(String token,Long device, Boolean enabled) {
		try {
			Device obj = new Device();
			obj.setId(device);
			obj = deviceService.find(obj);
			comandoService.requestAntiTheft(obj, enabled);
			result.include("device", obj);
			result.use(Results.json()).from("").serialize();
		} catch (Exception e) {
			logger.error("Erro ao ativar/desativar anti roubo", e);
			result.include("erros", e.getMessage());
		}
	}	

	@Post("/mobile/comando/disable_anti_theft")
	public void disableAntiTheft(String token,Long device) {
		try {
			Device obj = new Device();
			obj.setId(device);
			obj = deviceService.find(obj);
			comandoService.requestDisableAntiTheft(obj);
			result.include("device", obj);
			result.use(Results.json()).from("").serialize();
		} catch (Exception e) {
			logger.error("Erro ao desarmar anti roubo", e);
			result.include("erros", e.getMessage());
		}
	}

	@Post("/mobile/comando/time_zone")
	public void timeZone(String token,Long device, Integer timeZone) {
		try {
			Device obj = new Device();
			obj.setId(device);
			obj = deviceService.find(obj);
			comandoService.changeTimezone(obj,timeZone);
			result.include("device", obj);
			result.use(Results.json()).from("").serialize();
		} catch (Exception e) {
			logger.error("Erro ao mudar o timezone", e);
			result.include("erros", e.getMessage());
		}
	}

	@Post("/mobile/comando/output")
	public void output(String token,Long device, Integer output1, Integer output2, Integer output3) {
		try {
			Device obj = new Device();
			obj.setId(device);
			obj = deviceService.find(obj);
			comandoService.changeOutput(obj,output1,output2,output3);
			result.include("device", obj);
			result.use(Results.json()).from("").serialize();
		} catch (Exception e) {
			logger.error("Erro ao modificar as saidas", e);
			result.include("erros", e.getMessage());
		}
	}

	@Post("/mobile/comando/apn")
	public void apn(String token,Long device, String url, String username, String password) {
		try {
			Device obj = new Device();
			obj.setId(device);
			obj = deviceService.find(obj);
			comandoService.changeApn(obj, url, username, password);
			result.include("device", obj);
			result.use(Results.json()).from("").serialize();
		} catch (Exception e) {
			logger.error("Erro ao modificar as configurações  de apn", e);
			result.include("erros", e.getMessage());
		}
	}

	@Post("/mobile/comando/server")
	public void server(String token,Long device, String primaryIp, String primaryPort, String seconderyIp, String seconderyPort) {
		try {
			Device obj = new Device();
			obj.setId(device);
			obj = deviceService.find(obj);
			comandoService.changeServer(obj, primaryIp, primaryPort, seconderyIp, seconderyPort);
			result.include("device", obj);
			result.use(Results.json()).from("").serialize();
		} catch (Exception e) {
			logger.error("Erro ao modificar as configurações  de servidor", e);
			result.include("erros", e.getMessage());
		}
	}

	@Post("mobile/comando/reset")
	public void reset(String token,Long device) {
		try {
			Device obj = new Device();
			obj.setId(device);
			obj = deviceService.find(obj);
			comandoService.requestReset(obj);
			result.include("device", obj);
			result.use(Results.json()).from("").serialize();
		} catch (Exception e) {
			logger.error("Erro ao resetar o modulo", e);
			result.include("erros", e.getMessage());
		}
	}

}

