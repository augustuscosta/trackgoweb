package br.com.otgmobile.trackgoweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Veiculo;
import br.com.otgmobile.trackgoweb.service.DeviceService;
import br.com.otgmobile.trackgoweb.service.GrupoService;
import br.com.otgmobile.trackgoweb.service.VeiculoService;

@Resource
public class DeviceController {

	private static final Logger logger = Logger.getLogger(DeviceController.class);
	
	private final Result result;
	private final DeviceService deviceService;
	private final VeiculoService veiculoService;
	private final GrupoService grupoService;
	
	public DeviceController(Result result, DeviceService deviceService, VeiculoService veiculoService, GrupoService grupoService){
		this.result = result;
		this.deviceService = deviceService;
		this.veiculoService = veiculoService;
		this.grupoService = grupoService;
	}
	
	@Get("/device/add")
	public void add() {
		if(!deviceService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		List<Veiculo> veiculos = veiculoService.getAllVeiculos();
		result.include("veiculos", veiculos);
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}
	
	@Post("/device/create")
	public void create(Device device) {
		if(!deviceService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			if(device.getEnabled() == null)
				device.setEnabled(false);
			if(device.getVeiculo() != null && device.getVeiculo().getId() == null){
				device.setVeiculo(null);
			}else{
				Veiculo veiculo = veiculoService.find(device.getVeiculo());
				device.setVeiculo(veiculo);
			}
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(device.getGrupos() != null){
				for(Grupo idGrupo : device.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getDevices() == null)
						grupo.setDevices(new ArrayList<Device>());
					grupos.add(grupo);
				}
			}
			device.setGrupos(grupos);
			deviceService.add(device);
			result.redirectTo(DeviceController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao criar um novo dispositivo", e);
			result.include("erros", e.getMessage());
			result.redirectTo(DeviceController.class).list();
		}
	}
	
	@Get
	@Path("/device/update/{device.id}")
	public void edit(Device device) {
		if(!deviceService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		device = deviceService.find(device);
		result.include("device", device).include("grupos");
		List<Veiculo> veiculos = veiculoService.getAllVeiculos();
		result.include("veiculos", veiculos);
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}

	@Post("/device/update")
	public void update(Device device) {
		if(!deviceService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(device.getGrupos() != null){
				for(Grupo idGrupo : device.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getDevices() == null)
						grupo.setDevices(new ArrayList<Device>());
					grupos.add(grupo);
				}
			}
			if(device.getVeiculo() != null && device.getVeiculo().getId() == null){
				device.setVeiculo(null);
			}else{
				Veiculo veiculo = veiculoService.find(device.getVeiculo());
				device.setVeiculo(veiculo);
			}
			device.setGrupos(grupos);
			deviceService.update(device);
			result.redirectTo(DeviceController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao atualizar um dispositivo", e);
			result.include("erros", e.getMessage());
			result.forwardTo(DeviceController.class).list();
		}
	}
	
	@Get
	@Path("/devices")
	public void list() {
		if(!deviceService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
			result.include("devices", devices);
		} catch (Exception e) {
			logger.error("Erro ao listar os dispositivos", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Get
	@Path("/devices/json")
	public void listTojson() {
		if(!deviceService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
			List<Long> ids = new ArrayList<Long>();
			for(Device device : devices){
				ids.add(device.getId());
			}
			result.use(Results.json()).from(ids, "devices").serialize();
		} catch (Exception e) {
			logger.error("Erro ao listar os dispositivos", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Delete("/device/{device.id}")
	public void remove(Device device) {
		if(!deviceService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			Device toDelete = deviceService.find(device);
			deviceService.delete(deviceService.find(toDelete));
			result.forwardTo(DeviceController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao excluir um dispositivo", e);
			result.include("erros", e.getMessage());
			result.forwardTo(DeviceController.class).list();
		}
	}
	
	
	
}
