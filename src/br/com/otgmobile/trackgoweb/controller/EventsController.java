package br.com.otgmobile.trackgoweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackgoweb.dao.DeviceDAO;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.events.Event;
import br.com.otgmobile.trackgoweb.service.EventService;
import br.com.otgmobile.trackgoweb.service.GrupoService;

@Resource
public class EventsController {

	private static final Logger logger = Logger.getLogger(EventsController.class);

	private final Result result;
	private final EventService eventService;
	private final DeviceDAO deviceDAO;
	private final GrupoService grupoService;

	public EventsController(Result result, EventService eventService, DeviceDAO deviceDAO, GrupoService grupoService){
		this.result = result;
		this.eventService = eventService;
		this.deviceDAO = deviceDAO;
		this.grupoService = grupoService;
	}

	@Get("/event/add")
	public void add() {
		if(!eventService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Device> devices = deviceDAO.getAllEnabledDevices();
			result.include("devices", devices);
			List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
			result.include("grupos", grupos);
		} catch (Exception e) {
			logger.error("Erro ao criar um novo evnto", e);
			result.include("erros", e.getMessage());
			result.redirectTo(EventsController.class).list();
		}
	}

	@Post("/event/create")
	public void create(Event event) {
		if(!eventService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			if(event.getEnable() == null)
				event.setEnable(false);
			List<Device> devices = new ArrayList<Device>();
			if(event.getDevices() != null){
				for(Device idDevice : event.getDevices()){
					Device device = deviceDAO.find(idDevice);
					if(device.getEvents() == null)
						device.setEvents(new ArrayList<Event>());
					devices.add(device);
				}
			}
			event.setDevices(devices);

			List<Grupo> grupos = new ArrayList<Grupo>();
			if(event.getGrupos() != null){
				for(Grupo idGrupo : event.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getDevices() == null)
						grupo.setDevices(new ArrayList<Device>());
					grupos.add(grupo);
				}
			}
			event.setGrupos(grupos);

			eventService.add(event);
			result.redirectTo(EventsController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao criar um novo evnto", e);
			result.include("erros", e.getMessage());
			result.redirectTo(EventsController.class).list();
		}
	}

	@Get
	@Path("/event/update/{event.id}")
	public void edit(Event event) {
		if(!eventService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			event = eventService.find(event);
			result.include("event", event).include("devices").include("grupos");
			List<Device> devices = deviceDAO.getAllEnabledDevices();
			result.include("devices", devices);
			List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
			result.include("grupos", grupos);
		} catch (Exception e) {
			logger.error("Erro ao editar um evento", e);
			result.include("erros", e.getMessage());
			result.redirectTo(EventsController.class).list();
		}
	}

	@Post("/event/update")
	public void update(Event event) {
		if(!eventService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			if(event.getEnable() == null)
				event.setEnable(false);
			List<Device> devices = new ArrayList<Device>();
			if(event.getDevices() != null){
				for(Device idDevice : event.getDevices()){
					Device device = deviceDAO.find(idDevice);
					if(device.getEvents() == null)
						device.setEvents(new ArrayList<Event>());
					devices.add(device);
				}
			}

			List<Grupo> grupos = new ArrayList<Grupo>();
			if(event.getGrupos() != null){
				for(Grupo idGrupo : event.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getDevices() == null)
						grupo.setDevices(new ArrayList<Device>());
					grupos.add(grupo);
				}
			}

			event.setDevices(devices);
			event.setGrupos(grupos);
			eventService.update(event);
			result.redirectTo(EventsController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao atualizar um evento", e);
			result.include("erros", e.getMessage());
			result.forwardTo(EventsController.class).list();
		}
	}

	@Get
	@Path("/events")
	public void list() {
		if(!eventService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Event> events = eventService.getAllEvents(eventService.getCurentUser());
			result.include("events", events);
		} catch (Exception e) {
			logger.error("Erro ao listar os eventos", e);
			result.include("erros", e.getMessage());
		}
	}

	@Delete("/event/{event.id}")
	public void remove(Event event) {
		if(!eventService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			Event toDelete = eventService.find(event);
			eventService.delete(toDelete);
			result.forwardTo(EventsController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao excluir um evento", e);
			result.include("erros", e.getMessage());
			result.forwardTo(EventsController.class).list();
		}
	}

}
