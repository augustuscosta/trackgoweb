package br.com.otgmobile.trackgoweb.controller;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;

@Resource
public class IndexController {

	private final Result result;

	public IndexController(Result result) {
		this.result = result;
	}

	@Path("/")
	public void index() {
		result.forwardTo(MapaController.class).index();
	}
	
	@Path("/login")
	public void login() {
	}
	
	@Path("/unauthorized")
	public void unauthorized() {
	}

}
