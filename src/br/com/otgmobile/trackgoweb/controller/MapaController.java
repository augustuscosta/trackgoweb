package br.com.otgmobile.trackgoweb.controller;

import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.service.MapaService;

@Resource
public class MapaController {

	private final Result result;
	private final MapaService mapaService;

	public MapaController(Result result, MapaService mapaService) {
		this.result = result;
		this.mapaService = mapaService;
	}

	@Get
	@Path("/mapa")
	public void index() {
	}
	
	@Get
	@Path("/mapa/devices")
	public void getAllEnabledDevices() {
		List<Device> toReturn = mapaService.getAllEnabledDevicesWithLastPosition();
		result.use(Results.json()).from(toReturn, "devices").include("lastPosition").serialize();
	}

}
