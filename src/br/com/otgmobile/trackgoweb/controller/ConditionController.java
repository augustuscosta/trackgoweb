package br.com.otgmobile.trackgoweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackgoweb.model.Cerca;
import br.com.otgmobile.trackgoweb.model.Rota;
import br.com.otgmobile.trackgoweb.model.events.Event;
import br.com.otgmobile.trackgoweb.model.events.EventCondition;
import br.com.otgmobile.trackgoweb.model.events.EventType;
import br.com.otgmobile.trackgoweb.service.CercaService;
import br.com.otgmobile.trackgoweb.service.EventConditionService;
import br.com.otgmobile.trackgoweb.service.EventService;
import br.com.otgmobile.trackgoweb.service.RotaService;

@Resource
public class ConditionController {

	private static final Logger logger = Logger.getLogger(ConditionController.class);
	
	private final Result result;
	private CercaService cercaService;
	private EventService eventService;
	private EventConditionService eventConditionService;
	private RotaService rotaService;
	
	public ConditionController(Result result, CercaService cercaService, EventService eventService, EventConditionService eventConditionService,RotaService rotaService){
		this.result = result;
		this.cercaService = cercaService;
		this.eventService = eventService;
		this.eventConditionService = eventConditionService;
		this.rotaService = rotaService;
	}
	
	@Get
	@Path("/conditions/{event.id}")
	public void list(Event event) {
		try {
			event = eventService.find(event);
			List<EventCondition> conditions = event.getEventConditions();
			result.include("conditions", conditions);
			result.include("event", event);
		} catch (Exception e) {
			logger.error("Erro ao listar os condições", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Get("/condition/add/{event.id}")
	public void add(Event event) {
		try {
			event = eventService.find(event);
			result.include("event", event);
			List<Cerca> cercas = cercaService.getAllCercas();
			result.include("cercas", cercas);
			List<Rota> rotas = rotaService.getAllRotas();
			result.include("rotas", rotas);
		} catch (Exception e) {
			logger.error("Erro ao criar uma nova condição", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Get
	@Path("/condition/update/{condition.id}")
	public void edit(EventCondition condition) {
		condition = eventConditionService.find(condition);
		result.include("condition", condition);
		Event event = condition.getEvent();
		result.include("event", event);
		List<Cerca> cercas = cercaService.getAllCercas();
		result.include("cercas", cercas);
		List<Rota> rotas = rotaService.getAllRotas();
		result.include("rotas", rotas);
	}
	
	@Delete("/condition/{condition.id}")
	public void remove(EventCondition condition) {
		try {
			condition = eventConditionService.find(condition);
			Event event = condition.getEvent();
			eventConditionService.delete(condition);
			result.forwardTo(ConditionController.class).list(event);
		} catch (Exception e) {
			logger.error("Erro ao excluir um dispositivo", e);
			result.include("erros", e.getMessage());
			Event event = condition.getEvent();
			result.forwardTo(ConditionController.class).list(event);
		}
	}
	
	@Post("/condition/create")
	public void create(EventCondition condition) {
		try {
			setEventDataByType(condition);
			if(condition.getEnable() == null)
				condition.setEnable(false);
			
			List<Cerca> cercas = new ArrayList<Cerca>();
			if(condition.getCercas() != null){
				for(Cerca idCerca : condition.getCercas()){
					Cerca cerca = cercaService.find(idCerca);
					cercas.add(cerca);
				}
				condition.setCercas(cercas);
			}
			
			eventConditionService.add(condition);
			condition = eventConditionService.find(condition);
			Event event = condition.getEvent();
			result.redirectTo(ConditionController.class).list(event);
		} catch (Exception e) {
			logger.error("Erro ao criar uma nova condição", e);
			result.include("erros", e.getMessage());
			Event event = condition.getEvent();
			result.forwardTo(ConditionController.class).list(event);
		}
	}
	
	@Post("/condition/update")
	public void update(EventCondition condition) {
		try {
			setEventDataByType(condition);
			if(condition.getEnable() == null)
				condition.setEnable(false);
			
			List<Cerca> cercas = new ArrayList<Cerca>();
			if(condition.getCercas() != null){
				for(Cerca idCerca : condition.getCercas()){
					Cerca cerca = cercaService.find(idCerca);
					cercas.add(cerca);
				}
				condition.setCercas(cercas);
			}
			
			EventCondition toUpdate = eventConditionService.find(condition);
			toUpdate.setName(condition.getName());
			toUpdate.setDescription(condition.getDescription());
			toUpdate.setEnable(condition.getEnable());
			toUpdate.setEventType(condition.getEventType());
			switch (condition.getEventType()) {
			case OGNL_EVENT:
				toUpdate.setScriptCondition(condition.getScriptCondition());				
				toUpdate.setCercas(null);
				break;

			default:
				toUpdate.setScriptCondition(null);				
				toUpdate.setCercas(condition.getCercas());
				break;
			}
			Event event = condition.getEvent();
			eventConditionService.update(toUpdate);
			result.redirectTo(ConditionController.class).list(event);
		} catch (Exception e) {
			logger.error("Erro ao editar uma condição", e);
			result.include("erros", e.getMessage());
			Event event = condition.getEvent();
			result.forwardTo(ConditionController.class).list(event);
		}
	}

	private void setEventDataByType(EventCondition condition) {
		if(condition.getEventType().equals(EventType.OGNL_EVENT)){
			condition.setCercas(null);
		}else{
			condition.setScriptCondition(null);
		}
	}
	
	
}
