package br.com.otgmobile.trackgoweb.controller;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackgoweb.model.ComandoWrapper;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.service.ComandoService;
import br.com.otgmobile.trackgoweb.service.DeviceService;

@Resource
public class ComandoController {

	private static final Logger logger = Logger.getLogger(ComandoController.class);
	
	private final Result result;
	private ComandoService comandoService;
	private DeviceService deviceService;
	
	public ComandoController(Result result, ComandoService comandoService, DeviceService deviceService){
		this.result = result;
		this.comandoService = comandoService;
		this.deviceService = deviceService;
	}
	
	@Get
	@Path("/comandos/{device.id}")
	public void list(Device device) {
		try {
			device = deviceService.find(device);
			List<ComandoWrapper> comandos = comandoService.getCommands(device);
			result.include("comandos", comandos);
			result.include("device", device);
		} catch (Exception e) {
			logger.error("Erro ao listar os comandos", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Get("/comando/add/{device.id}")
	public void add(Device device) {
		try {
			device = deviceService.find(device);
			result.include("device", device);
		} catch (Exception e) {
			logger.error("Erro ao criar um novo comando", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Post("/comando/requet_position")
	public void requestPosition(Device device) {
		try {
			device = deviceService.find(device);
			comandoService.requestPosition(device);
			result.include("device", device);
			result.forwardTo(ComandoController.class).list(device);
		} catch (Exception e) {
			logger.error("Erro ao pedir posição", e);
			result.include("erros", e.getMessage());
			result.forwardTo(ComandoController.class).list(device);
		}
	}
	
	@Post("/comando/disable_panic")
	public void disablePanic(Device device) {
		try {
			device = deviceService.find(device);
			comandoService.requestDisablePanic(device);
			result.include("device", device);
			result.forwardTo(ComandoController.class).list(device);
		} catch (Exception e) {
			logger.error("Erro ao desativar o estado de pânico", e);
			result.include("erros", e.getMessage());
			result.forwardTo(ComandoController.class).list(device);
		}
	}
	
	@Post("/comando/anti_theft")
	public void antiTheft(Device device, Boolean enabled) {
		try {
			device = deviceService.find(device);
			comandoService.requestAntiTheft(device, enabled);
			result.include("device", device);
			result.forwardTo(ComandoController.class).list(device);
		} catch (Exception e) {
			logger.error("Erro ao ativar/desativar anti roubo", e);
			result.include("erros", e.getMessage());
			result.forwardTo(ComandoController.class).list(device);
		}
	}	
	
	@Post("/comando/disable_anti_theft")
	public void disableAntiTheft(Device device) {
		try {
			device = deviceService.find(device);
			comandoService.requestDisableAntiTheft(device);
			result.include("device", device);
			result.forwardTo(ComandoController.class).list(device);
		} catch (Exception e) {
			logger.error("Erro ao desarmar anti roubo", e);
			result.include("erros", e.getMessage());
			result.forwardTo(ComandoController.class).list(device);
		}
	}
	
	@Post("/comando/time_zone")
	public void timeZone(Device device, Integer timeZone) {
		try {
			device = deviceService.find(device);
			comandoService.changeTimezone(device,timeZone);
			result.include("device", device);
			result.forwardTo(ComandoController.class).list(device);
		} catch (Exception e) {
			logger.error("Erro ao mudar o timezone", e);
			result.include("erros", e.getMessage());
			result.forwardTo(ComandoController.class).list(device);
		}
	}
	
	@Post("/comando/output")
	public void output(Device device, Integer output1, Integer output2, Integer output3) {
		try {
			device = deviceService.find(device);
			comandoService.changeOutput(device,output1,output2,output3);
			result.include("device", device);
			result.forwardTo(ComandoController.class).list(device);
		} catch (Exception e) {
			logger.error("Erro ao modificar as saídas", e);
			result.include("erros", e.getMessage());
			result.forwardTo(ComandoController.class).list(device);
		}
	}
	
	@Post("/comando/apn")
	public void apn(Device device, String url, String username, String password) {
		try {
			device = deviceService.find(device);
			comandoService.changeApn(device, url, username, password);
			result.include("device", device);
			result.forwardTo(ComandoController.class).list(device);
		} catch (Exception e) {
			logger.error("Erro ao modificar as configurações  de apn", e);
			result.include("erros", e.getMessage());
			result.forwardTo(ComandoController.class).list(device);
		}
	}
	
	@Post("/comando/server")
	public void server(Device device, String primaryIp, String primaryPort, String seconderyIp, String seconderyPort) {
		try {
			device = deviceService.find(device);
			comandoService.changeServer(device, primaryIp, primaryPort, seconderyIp, seconderyPort);
			result.include("device", device);
			result.forwardTo(ComandoController.class).list(device);
		} catch (Exception e) {
			logger.error("Erro ao modificar as configurações  de servidor", e);
			result.include("erros", e.getMessage());
			result.forwardTo(ComandoController.class).list(device);
		}
	}
	
	@Post("/comando/reset")
	public void reset(Device device) {
		try {
			device = deviceService.find(device);
			comandoService.requestReset(device);
			result.include("device", device);
			result.forwardTo(ComandoController.class).list(device);
		} catch (Exception e) {
			logger.error("Erro ao resetar o modulo", e);
			result.include("erros", e.getMessage());
			result.forwardTo(ComandoController.class).list(device);
		}
	}
	
}
