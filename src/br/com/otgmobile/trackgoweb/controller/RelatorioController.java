package br.com.otgmobile.trackgoweb.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.otgmobile.trackgoweb.model.Cerca;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.DevicePathCriteria;
import br.com.otgmobile.trackgoweb.model.GeoPontoCerca;
import br.com.otgmobile.trackgoweb.model.MarchaLenta;
import br.com.otgmobile.trackgoweb.model.PassagemPontoDeInteresse;
import br.com.otgmobile.trackgoweb.model.PermanenciaCerca;
import br.com.otgmobile.trackgoweb.model.PermanenciaForaCerca;
import br.com.otgmobile.trackgoweb.model.PermanenciaRota;
import br.com.otgmobile.trackgoweb.model.PontoDeInteresse;
import br.com.otgmobile.trackgoweb.model.Rota;
import br.com.otgmobile.trackgoweb.model.TempoLigado;
import br.com.otgmobile.trackgoweb.model.TrackerData;
import br.com.otgmobile.trackgoweb.model.events.Event;
import br.com.otgmobile.trackgoweb.model.events.EventHistoryDetail;
import br.com.otgmobile.trackgoweb.service.CercaService;
import br.com.otgmobile.trackgoweb.service.DeviceService;
import br.com.otgmobile.trackgoweb.service.MapaPesquisaService;
import br.com.otgmobile.trackgoweb.service.PontoDeInteresseService;
import br.com.otgmobile.trackgoweb.service.RelatorioService;
import br.com.otgmobile.trackgoweb.service.RotaService;
import br.com.otgmobile.trackgoweb.util.CercaUtil;
import br.com.otgmobile.trackgoweb.util.DateUtil;
import br.com.otgmobile.trackgoweb.util.LocatorUtil;

@Resource
public class RelatorioController {

	private final Result result;
	private final RelatorioService relatorioService;
	private final MapaPesquisaService mapaPesquisaService;
	private final DeviceService deviceService;
	private final CercaService cercaService;
	private final PontoDeInteresseService pontoService;
	private final RotaService rotaService;
	private static final int HOUR_IN_MILLS = 3600000;

	public RelatorioController(Result result, RelatorioService relatorioService, MapaPesquisaService mapaPesquisaService, DeviceService deviceService,CercaService cercaService,PontoDeInteresseService pontoService,RotaService rotaService) {
		this.result = result;
		this.relatorioService = relatorioService;
		this.mapaPesquisaService = mapaPesquisaService;
		this.deviceService = deviceService;
		this.cercaService = cercaService;
		this.pontoService = pontoService;
		this.rotaService = rotaService;
	}
	
	@Get
	@Path("/relatorio_eventos")
	public void eventos() {
	}
	
	@Get
	@Path("/relatorios/eventos")
	public void getEvents(String start, String end) {
		Date startDate = DateUtil.parseDateFromString(start);
		Date endDate = DateUtil.parseDateFromString(end);
		List<Event> events = relatorioService.getEvents(startDate, endDate);
		cleanCircularReferences(events);
		result.use(Results.json()).from(events, "events").recursive().serialize();
	}
	
	private void cleanCircularReferences(List<Event> events){
		if(events == null)
			return;
		
		for(Event event:events){
			event.setEventCondition(null);
			event.setGrupos(null);
			event.setDevices(null);
			if(event.getEventHistoryDetails() != null){
				for(EventHistoryDetail detail : event.getEventHistoryDetails()){
					detail.setEvent(null);
					detail.setEventHistory(null);
					if(detail.getDevice() != null){
						detail.getDevice().setEventHistoryDetails(null);
						detail.getDevice().setGrupos(null);
						detail.getDevice().setEvents(null);
						detail.getDevice().setVeiculo(null);
					}
				}
			}
		}
	}
	
	@Get
	@Path("/relatorio_path")
	public void path() {
		try {
			List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
			result.include("devices", devices);
		} catch (Exception e) {
			result.include("erros", e.getMessage());
		}
	}
	
	@Get
	@Path("/relatorios/path")
	public void getAllEnabledDevicesWithPath(Integer deviceId, String start, String end) {
		Date startDate = DateUtil.parseDateFromString(start);
		Date endDate = DateUtil.parseDateFromString(end);
		DevicePathCriteria criteria = getCriteria(deviceId, startDate, endDate);
		Device toReturn = mapaPesquisaService.getDevicePath(criteria);
		result.use(Results.json()).from(toReturn, "device").include("positions").serialize();
	}

	private DevicePathCriteria getCriteria(Integer deviceId, Date start,
			Date end) {
		DevicePathCriteria criteria = new DevicePathCriteria();
		Device device = new Device();
		device.setId(deviceId.longValue());
		criteria.setDevice(device);
		criteria.setStart(start);
		criteria.setEnd(end);
		return criteria;
	}
	
	@Get
	@Path("/relatorio_permanecia_cerca")
	public void permanenciaCerca(){
		List<Cerca> cercas = cercaService.getAllCercas();
		result.include("cercas",cercas);
		List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
		result.include("devices",devices);
	}
	
	
	@Get
	@Path("/relatorios/permanecia_cerca")
	public void getDataTempoPermanenciaCerca(String start, String end,Integer cercaId,Integer deviceId){
		Cerca cerca = new Cerca();
		cerca.setId(cercaId);
		cerca = cercaService.find(cerca);
		Date startDate = DateUtil.parseDateFromString(start);
		Date endDate = DateUtil.parseDateFromString(end);
		DevicePathCriteria criteria = getCriteria(deviceId, startDate, endDate);
		Device toReturn = mapaPesquisaService.getDevicePath(criteria);
		List<PermanenciaCerca> permanencias = new ArrayList<PermanenciaCerca>();
		if(toReturn.getPositions() == null || toReturn.getPositions().isEmpty()){
			result.include("permanencias",permanencias);
			return;
		}
		
		List<TrackerData> positions = toReturn.getPositions();
		int runedIndex = 0;
		while(runedIndex < positions.size()){
			runedIndex = getPermanenciaCerca(cerca, positions,runedIndex,permanencias,toReturn);			
		}		
		result.use(Results.json()).from(permanencias,"permanencias").include("device").include("cerca").include("firstPosition").include("lastPosition").serialize();
		
	}

	private int getPermanenciaCerca(Cerca cerca, List<TrackerData> positions,int runedIndex, List<PermanenciaCerca> permanencias,Device device) {
		
		PermanenciaCerca permanencia = new PermanenciaCerca();
		permanencia.setCerca(cerca);
		permanencia.setDevice(device);
		for (int i = runedIndex; i < positions.size(); i++) {
			runedIndex = i;
			GeoPontoCerca ponto = new GeoPontoCerca();
			ponto.setLatitude(positions.get(i).getLatitude());
			ponto.setLongitude(positions.get(i).getLongitude());
			if(CercaUtil.isInsideCercaWithGeometry(cerca, ponto) && permanencia.getFirstPosition() == null){
				permanencia.setFirstPosition((positions.get(i)));
			}else if(!CercaUtil.isInsideCercaWithGeometry(cerca, ponto) && permanencia.getFirstPosition() != null){
				permanencia.setLastPosition(positions.get(i));
				permanencias.add(permanencia);
				return runedIndex+1;
			}
		}
		if(runedIndex == positions.size() -1){
			runedIndex +=1;
		}
		if(permanencia.getFirstPosition()!= null)	permanencias.add(permanencia);	
		if(permanencia.getLastPosition() == null)  permanencia.setLastPosition(positions.get(positions.size() - 1));
		return runedIndex;
	}
	
	
	
	
	@Get
	@Path("/relatorio_permanecia_fora_cerca")
	public void permanenciaForaCerca(){
		List<Cerca> cercas = cercaService.getAllCercas();
		result.include("cercas",cercas);
		List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
		result.include("devices",devices);
	}
	
	
	
	@Get
	@Path("/relatorios/permanecia_fora_cerca")
	public void getDataTempoPermanenciaForaCerca(String start, String end,Integer cercaId,Integer deviceId){
		Cerca cerca = new Cerca();
		cerca.setId(cercaId);
		cerca = cercaService.find(cerca);
		Date startDate = DateUtil.parseDateFromString(start);
		Date endDate = DateUtil.parseDateFromString(end);
		DevicePathCriteria criteria = getCriteria(deviceId, startDate, endDate);
		Device toReturn = mapaPesquisaService.getDevicePath(criteria);
		List<PermanenciaForaCerca> permanencias = new ArrayList<PermanenciaForaCerca>();
		if(toReturn.getPositions() == null || toReturn.getPositions().isEmpty()){
			result.include("permanencias",permanencias);
			return;
		}
		
		List<TrackerData> positions = toReturn.getPositions();
		int runedIndex = 0;
		while(runedIndex < positions.size()){
			runedIndex = getPermanenciaForaCerca(cerca, positions,runedIndex,permanencias,toReturn);			
		}		
		result.use(Results.json()).from(permanencias,"permanencias").include("device").include("cerca").include("firstPosition").include("lastPosition").serialize();
		
	}

	private int getPermanenciaForaCerca(Cerca cerca, List<TrackerData> positions,int runedIndex, List<PermanenciaForaCerca> permanencias,Device device) {
		
		PermanenciaForaCerca permanencia = new PermanenciaForaCerca();
		permanencia.setCerca(cerca);
		permanencia.setDevice(device);
		for (int i = runedIndex; i < positions.size(); i++) {
			runedIndex = i;
			GeoPontoCerca ponto = new GeoPontoCerca();
			ponto.setLatitude(positions.get(i).getLatitude());
			ponto.setLongitude(positions.get(i).getLongitude());
			if(!CercaUtil.isInsideCercaWithGeometry(cerca, ponto) && permanencia.getFirstPosition() == null){
				permanencia.setFirstPosition((positions.get(i)));
			}else if(CercaUtil.isInsideCercaWithGeometry(cerca, ponto) && permanencia.getFirstPosition() != null){
				permanencia.setLastPosition(positions.get(i));
				permanencias.add(permanencia);
				return runedIndex+1;
			}
		}
		if(runedIndex == positions.size() -1){
			runedIndex +=1;
		}
		if(permanencia.getFirstPosition()!= null)	permanencias.add(permanencia);
		if(permanencia.getLastPosition() == null)   permanencia.setLastPosition(positions.get(positions.size() - 1));
		return runedIndex;
	}
	
	
	@Get
	@Path("/relatorio_permanecia_rota")
	public void permanenciaRota(){
		List<Rota> rotas = rotaService.getAllRotas();
		result.include("rotas",rotas);
		List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
		result.include("devices",devices);
	}
	
	@Get
	@Path("/relatorios/permanecia_rota")
	public void getDataTempoPermanenciaRota(String start, String end,Integer rotaId,Integer deviceId){
		Rota rota = new Rota();
		rota.setId(rotaId);
		rota = rotaService.find(rota);
		Date startDate = DateUtil.parseDateFromString(start);
		Date endDate = DateUtil.parseDateFromString(end);
		DevicePathCriteria criteria = getCriteria(deviceId, startDate, endDate);
		Device toReturn = mapaPesquisaService.getDevicePath(criteria);
		List<PermanenciaRota> permanencias = new ArrayList<PermanenciaRota>();
		if(toReturn.getPositions() == null || toReturn.getPositions().isEmpty()){
			result.include("permanencias",permanencias);
			return;
		}
		
		List<TrackerData> positions = toReturn.getPositions();
		int runedIndex = 0;
		while(runedIndex < positions.size()){
			runedIndex = getPermanenciaRota(rota, positions,runedIndex,permanencias,toReturn);			
		}		
		result.use(Results.json()).from(permanencias,"permanencias").include("device").include("rota").include("firstPosition").include("lastPosition").serialize();
		
	}

	private int getPermanenciaRota(Rota rota, List<TrackerData> positions,int runedIndex, List<PermanenciaRota> permanencias,Device device) {
		
		PermanenciaRota permanencia = new PermanenciaRota();
		permanencia.setRota(rota);
		permanencia.setDevice(device);
		for (int i = runedIndex; i < positions.size(); i++) {
			runedIndex = i;
			if(LocatorUtil.isInRoute(positions.get(i), rota) && permanencia.getFirstPosition() == null){
				permanencia.setFirstPosition((positions.get(i)));
			}else if(!LocatorUtil.isInRoute(positions.get(i), rota) && permanencia.getFirstPosition() != null){
				permanencia.setLastPosition(positions.get(i));
				permanencias.add(permanencia);
				return runedIndex+1;
			}
		}
		if(runedIndex == positions.size() -1){
			runedIndex +=1;
		}
		if(permanencia.getFirstPosition()!= null)	permanencias.add(permanencia);	
		if(permanencia.getLastPosition() == null)  permanencia.setLastPosition(positions.get(positions.size() - 1));
		return runedIndex;
	}
	
	@Get
	@Path("/relatiorios_relatorio_permanecia_fora_rota")
	public void permanenciaForaRota(){
		List<Rota> rotas = rotaService.getAllRotas();
		result.include("rotas",rotas);
		List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
		result.include("devices",devices);
	}
	
	
	@Get
	@Path("/relatorios/permanecia_fora_rota")
	public void getDataTempoPermanenciaForaRota(String start, String end,Integer rotaId,Integer deviceId){
		Rota rota = new Rota();
		rota.setId(rotaId);
		rota = rotaService.find(rota);
		Date startDate = DateUtil.parseDateFromString(start);
		Date endDate = DateUtil.parseDateFromString(end);
		DevicePathCriteria criteria = getCriteria(deviceId, startDate, endDate);
		Device toReturn = mapaPesquisaService.getDevicePath(criteria);
		List<PermanenciaRota> permanencias = new ArrayList<PermanenciaRota>();
		if(toReturn.getPositions() == null || toReturn.getPositions().isEmpty()){
			result.include("permanencias",permanencias);
			return;
		}
		
		List<TrackerData> positions = toReturn.getPositions();
		int runedIndex = 0;
		while(runedIndex < positions.size()){
			runedIndex = getPermanenciaForaRota(rota, positions,runedIndex,permanencias,toReturn);			
		}		
		result.use(Results.json()).from(permanencias,"permanencias").include("device").include("rota").include("firstPosition").include("lastPosition").serialize();
		
	}

	private int getPermanenciaForaRota(Rota rota, List<TrackerData> positions,int runedIndex, List<PermanenciaRota> permanencias,Device device) {
		
		PermanenciaRota permanencia = new PermanenciaRota();
		permanencia.setRota(rota);
		permanencia.setDevice(device);
		for (int i = runedIndex; i < positions.size(); i++) {
			runedIndex = i;
			if(!LocatorUtil.isInRoute(positions.get(i), rota) && permanencia.getFirstPosition() == null){
				permanencia.setFirstPosition((positions.get(i)));
			}else if(LocatorUtil.isInRoute(positions.get(i), rota) && permanencia.getFirstPosition() != null){
				permanencia.setLastPosition(positions.get(i));
				permanencias.add(permanencia);
				return runedIndex+1;
			}
		}
		if(runedIndex == positions.size() -1){
			runedIndex +=1;
		}
		if(permanencia.getFirstPosition()!= null)	permanencias.add(permanencia);
		if(permanencia.getLastPosition() == null)  permanencia.setLastPosition(positions.get(positions.size() - 1));
		return runedIndex;
	}
	
	@Get
	@Path("/relatorio_passagem_ponto")
	public void passagemPonto(){
		List<PontoDeInteresse> pontos = pontoService.getAllPontoDeInteresses(pontoService.getCurentUser());
		result.include("pontos",pontos);
		List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
		result.include("devices",devices);
	}
	
	
	@Get
	@Path("/relatorios/passagem_ponto")
	public void getDataTempoPassagemPonto(String start, String end,Integer pontoId,Integer deviceId){
		Date startDate = DateUtil.parseDateFromString(start);
		Date endDate = DateUtil.parseDateFromString(end);
		DevicePathCriteria criteria = getCriteria(deviceId, startDate, endDate);
		PontoDeInteresse pontoDeInteresse = new PontoDeInteresse();
		pontoDeInteresse.setId(pontoId.longValue());
		pontoDeInteresse = pontoService.find(pontoDeInteresse);
		Device toReturn = mapaPesquisaService.getDevicePath(criteria);
		List<PassagemPontoDeInteresse> permanencias = new ArrayList<PassagemPontoDeInteresse>();
		if(toReturn.getPositions() == null || toReturn.getPositions().isEmpty()){
			result.include("permanencias",permanencias);
			return;
		}
		
		List<TrackerData> positions = toReturn.getPositions();
		int runedIndex = 0;
		while(runedIndex < positions.size()){
			runedIndex = getPassagemPonto(pontoDeInteresse, positions,runedIndex,permanencias,toReturn);			
		}		
		result.use(Results.json()).from(permanencias,"permanencias").include("device").include("pontoDeInteresse").include("firstPosition").include("lastPosition").serialize();
		
	}

	private int getPassagemPonto(PontoDeInteresse pontoDeInteresse, List<TrackerData> positions,int runedIndex, List<PassagemPontoDeInteresse> permanencias,Device device) {
		
		PassagemPontoDeInteresse permanencia = new PassagemPontoDeInteresse();
		permanencia.setPontoDeInteresse(pontoDeInteresse);
		permanencia.setDevice(device);
		for (int i = runedIndex; i < positions.size(); i++) {
			runedIndex = i;
			if(LocatorUtil.isInPonto(positions.get(i), pontoDeInteresse) && permanencia.getFirstPosition() == null){
				permanencia.setFirstPosition((positions.get(i)));
			}else if(!LocatorUtil.isInPonto(positions.get(i), pontoDeInteresse) && permanencia.getFirstPosition() != null){
				permanencia.setLastPosition(positions.get(i));
				permanencias.add(permanencia);
				return runedIndex+1;
			}
		}
		if(runedIndex == positions.size() -1){
			runedIndex +=1;
		}
		if(permanencia.getFirstPosition()!= null)	permanencias.add(permanencia);	
		if(permanencia.getLastPosition() == null)   permanencia.setLastPosition(positions.get(positions.size() - 1));
		return runedIndex;
	}
	
	@Get
	@Path("/relatorio_sem_posicionar")
	public void semPosicionar(){
		// Notinhg to fill form
	}
	
	@Get
	@Path("relatorios/sem_posicionar")
	public void getAllVehiclesSemPosicionar(Integer horasSenPosicionar){
		if(horasSenPosicionar== null){
			return;
		}
		Date date = getDateSet(horasSenPosicionar);
		 List<Device> toReturn = mapaPesquisaService.getDevicesNotTransmiting(date);
		 result.use(Results.json()).from(toReturn, "device").include("lastPosition").serialize();
	}

	private Date getDateSet(Integer horasSenPosicionar) {
		long inMillsnoPosition = new Date().getTime() -(horasSenPosicionar * HOUR_IN_MILLS);
		Date d = new Date(inMillsnoPosition);
		return d;
	}
	
	
	@Get
	@Path("/relatorio_tempo_inicao")
	public void tempoIgnicao(){
		List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
		result.include("devices", devices);
	}
	
	@Get
	@Path("relatorios/tempo_ignicao")
	public void getTempoDeIgnicao(Integer deviceId,String start, String end){
		Date startDate = DateUtil.parseDateFromString(start);
		Date endDate = DateUtil.parseDateFromString(end);
		DevicePathCriteria criteria = getCriteria(deviceId, startDate, endDate);
		Device toReturn = mapaPesquisaService.getDevicePath(criteria);
		if(toReturn!= null && toReturn.getPositions() != null && !toReturn.getPositions().isEmpty()){
			List<TempoLigado> permanencias = getTemposLigado(toReturn.getPositions(),toReturn);
			result.use(Results.json()).from(permanencias,"permanencias").include("device").include("firstPosition").include("lastPosition").serialize();
		}
		
	}

	private List<TempoLigado> getTemposLigado(List<TrackerData> positions, Device device) {
		int runedIndex = 0;
		List<TempoLigado> permanencias = new ArrayList<TempoLigado>();
		while(runedIndex < positions.size()){
			runedIndex = getTempoLigado(positions,runedIndex,permanencias,device);			
		}	
		return permanencias;
	}

	private int getTempoLigado(List<TrackerData> positions, int runedIndex,List<TempoLigado> permanencias, Device device) {
		TempoLigado permanencia = new TempoLigado();
		permanencia.setDevice(device);
		for (int i = runedIndex; i < positions.size(); i++) {
			runedIndex = i;
			if(positions.get(i).getIgnition() == true && permanencia.getFirstPosition() == null){
				permanencia.setFirstPosition((positions.get(i)));
			}else if(positions.get(i).getIgnition() == false && permanencia.getFirstPosition() != null){
				permanencia.setLastPosition(positions.get(i));
				permanencias.add(permanencia);
				return runedIndex+1;
			}
		}
		
		if(runedIndex == positions.size() -1){
			runedIndex +=1;
			if(permanencia.getFirstPosition()!= null){
				permanencia.setLastPosition(positions.get(positions.size()-1));
				permanencias.add(permanencia);		
			}
		}
		
		return runedIndex;
	}
	
	@Get
	@Path("/relatorio_marcha_lenta")
	public void marchaLenta(){
		List<Device> devices = deviceService.getAllDevices(deviceService.getCurentUser());
		result.include("devices", devices);
	}
	
	
	@Get
	@Path("relatorios/marcha_lenta")
	public void getMarchaLenta(Integer deviceId,String start, String end, int minutos){
		Date startDate = DateUtil.parseDateFromString(start);
		Date endDate = DateUtil.parseDateFromString(end);
		DevicePathCriteria criteria = getCriteria(deviceId, startDate, endDate);
		Device toReturn = mapaPesquisaService.getDevicePath(criteria);
		if(toReturn!= null && toReturn.getPositions() != null && !toReturn.getPositions().isEmpty()){
			List<MarchaLenta> permanencias = getMarchasLentas(toReturn.getPositions(),toReturn, minutos);
			result.use(Results.json()).from(permanencias,"permanencias").include("device").include("firstPosition").include("lastPosition").serialize();
		}	
		
	}
	
	private List<MarchaLenta> getMarchasLentas(List<TrackerData> positions, Device device, int minutos) {
		int runedIndex = 0;
		List<MarchaLenta> permanencias = new ArrayList<MarchaLenta>();
		while(runedIndex < positions.size()){
			runedIndex = getMarchLenta(positions,runedIndex,permanencias,device, minutos);			
		}	
		return permanencias;
	}
	
	private int getMarchLenta(List<TrackerData> positions, int runedIndex,List<MarchaLenta> permanencias, Device device,int minutos) {
		Integer minuteCalculater = minutos *(HOUR_IN_MILLS/60);
		MarchaLenta permanencia = new MarchaLenta();
		permanencia.setDevice(device);
		for (int i = runedIndex; i < positions.size(); i++) {
			runedIndex = i;
			if(isMarchalenta(positions.get(i)) == true && permanencia.getFirstPosition() == null){
				permanencia.setFirstPosition((positions.get(i)));
			}else if(isMarchalenta(positions.get(i)) == false && permanencia.getFirstPosition() != null){
				permanencia.setLastPosition(positions.get(i));
				if(calculateMarchalentaTime(permanencia,minuteCalculater))
				permanencias.add(permanencia);
				return runedIndex+1;
			}
		}
		
		if(runedIndex == positions.size() -1){
			runedIndex +=1;
			if(permanencia.getFirstPosition()!= null){
				permanencia.setLastPosition(positions.get(positions.size()-1));
				permanencias.add(permanencia);		
			}
		}
		
		return runedIndex;
	}
	

	private boolean isMarchalenta(TrackerData position){
		if(position != null && position.getIgnition() == true && position.getSpeed() <1){
			return true;
		}
		return false;
	}
	
	private boolean calculateMarchalentaTime(MarchaLenta permanencia,Integer minuteCalculater) {
		if((permanencia.getLastPosition().getDate().getTime()-permanencia.getFirstPosition().getDate().getTime() ) > minuteCalculater.longValue()){
			return true;
		}
		return false;
	}
}
