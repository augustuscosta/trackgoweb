package br.com.otgmobile.trackgoweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackgoweb.model.Cerca;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.service.CercaService;
import br.com.otgmobile.trackgoweb.service.GrupoService;

@Resource
public class CercaController {

	private static final Logger logger = Logger.getLogger(CercaController.class);
	
	private final Result result;
	private final CercaService cercaService;
	private final GrupoService grupoService;
	
	public CercaController(Result result, CercaService cercaService, GrupoService grupoService){
		this.result = result;
		this.cercaService = cercaService;
		this.grupoService = grupoService;
	}
	
	@Get("/cerca/add")
	public void add() {
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}
	
	@Post("/cerca/create")
	public void create(Cerca cerca) {
		try {
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(cerca.getGrupos() != null){
				for(Grupo idGrupo : cerca.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getCercas() == null)
						grupo.setCercas(new ArrayList<Cerca>());
					grupos.add(grupo);
				}
			}
			cerca.setGrupos(grupos);
			cercaService.add(cerca);
			result.redirectTo(CercaController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao criar uma nova cerca", e);
			result.include("erros", e.getMessage());
			result.redirectTo(CercaController.class).list();
		}
	}
	
	@Get
	@Path("/cerca/update/{cerca.id}")
	public void edit(Cerca cerca) {
		cerca = cercaService.find(cerca);
		result.include("cerca", cerca).include("grupos");
		List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
		result.include("grupos", grupos);
	}

	@Post("/cerca/update")
	public void update(Cerca cerca) {
		try {
			List<Grupo> grupos = new ArrayList<Grupo>();
			if(cerca.getGrupos() != null){
				for(Grupo idGrupo : cerca.getGrupos()){
					Grupo grupo = grupoService.find(idGrupo);
					if(grupo.getCercas() == null)
						grupo.setCercas(new ArrayList<Cerca>());
					grupos.add(grupo);
				}
			}
			cerca.setGrupos(grupos);
			cercaService.update(cerca);
			result.redirectTo(CercaController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao atualizar uma cerca", e);
			result.include("erros", e.getMessage());
			result.forwardTo(CercaController.class).list();
		}
	}
	
	@Get
	@Path("/cercas")
	public void list() {
		try {
			List<Cerca> cercas = cercaService.getAllCercas();
			result.include("cercas", cercas);
		} catch (Exception e) {
			logger.error("Erro ao listar as cercas", e);
			result.include("erros", e.getMessage());
		}
	}
	
	@Delete("/cerca/{cerca.id}")
	public void remove(Cerca cerca) {
		try {
			Cerca toDelete = cercaService.find(cerca);
			cercaService.delete(cercaService.find(toDelete));
			result.forwardTo(CercaController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao excluir uma cerca", e);
			result.include("erros", e.getMessage());
			result.forwardTo(CercaController.class).list();
		}
	}
	
}
