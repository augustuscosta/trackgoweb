package br.com.otgmobile.trackgoweb.controller;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.otgmobile.trackgoweb.model.Grupo;
import br.com.otgmobile.trackgoweb.model.Permissao;
import br.com.otgmobile.trackgoweb.service.GrupoService;

@Resource
public class GrupoController {

	private static final Logger logger = Logger.getLogger(GrupoController.class);
	
	private final Result result;
	private final GrupoService grupoService;
	
	public GrupoController(Result result, GrupoService grupoService){
		this.result = result;
		this.grupoService = grupoService;
	}
	
	@Get("/grupo/add")
	public void add() {
		if(!grupoService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
	}
	
	@Post("/grupo/create")
	public void create(Grupo grupo) {
		if(!grupoService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			if(grupo.getEnabled() == null)
				grupo.setEnabled(false);
			setBooleanCheckBoxesOnGrupo(grupo);
			grupoService.add(grupo);
			result.redirectTo(GrupoController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao criar uma nova grupo", e);
			result.include("erros", e.getMessage());
			result.redirectTo(GrupoController.class).list();
		}
	}
	
	private void setBooleanCheckBoxesOnGrupo(Grupo grupo) {
		if(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR)){
			grupo.setCercaAddEdit(true);
			grupo.setDeviceAddEdit(true);
			grupo.setGruposAddEdit(true);
			grupo.setPontoDeInteresseAddEdit(true);
			grupo.setEventosAddEdit(true);
			grupo.setRotaAddEdit(true);
			grupo.setUsuariosAddEdit(true);
			grupo.setVeiculoAddEdit(true);
		}else{
			if(grupo.getCercaAddEdit() == null) grupo.setCercaAddEdit(false);
			if(grupo.getDeviceAddEdit() == null) grupo.setDeviceAddEdit(false);
			if(grupo.getGruposAddEdit() == null) grupo.setGruposAddEdit(false);
			if(grupo.getPontoDeInteresseAddEdit() == null) grupo.setPontoDeInteresseAddEdit(false);
			if(grupo.getRotaAddEdit() == null) grupo.setRotaAddEdit(false);
			if(grupo.getUsuariosAddEdit() == null) grupo.setUsuariosAddEdit(false);
			if(grupo.getVeiculoAddEdit() == null)grupo.setVeiculoAddEdit(false);
			if(grupo.getEventosAddEdit() == null)grupo.setEventosAddEdit(false);
		}
		
	}

	@Get
	@Path("/grupo/update/{grupo.id}")
	public void edit(Grupo grupo) {
		if(!grupoService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		grupo = grupoService.find(grupo);
		result.include("grupo", grupo);
	}

	@Post("/grupo/update")
	public void update(Grupo grupo) {
		if(!grupoService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			Grupo grupoToUptade = grupoService.find(grupo);
			setBooleanCheckBoxesOnGrupo(grupo);
			grupoToUptade.setName(grupo.getName());
			grupoToUptade.setDescription(grupo.getDescription());
			grupoToUptade.setEnabled(grupo.getEnabled());
			grupoToUptade.setDeviceAddEdit(grupo.getDeviceAddEdit());
			grupoToUptade.setCercaAddEdit(grupo.getCercaAddEdit());
			grupoToUptade.setRotaAddEdit(grupo.getRotaAddEdit());
			grupoToUptade.setEventosAddEdit(grupo.getEventosAddEdit());
			grupoToUptade.setPontoDeInteresseAddEdit(grupo.getPontoDeInteresseAddEdit());
			grupoToUptade.setVeiculoAddEdit(grupo.getVeiculoAddEdit());
			grupoToUptade.setUsuariosAddEdit(grupo.getUsuariosAddEdit());
			grupoToUptade.setGruposAddEdit(grupo.getVeiculoAddEdit());
			grupoService.update(grupoToUptade);
			result.redirectTo(GrupoController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao atualizar uma grupo", e);
			result.include("erros", e.getMessage());
			result.forwardTo(GrupoController.class).list();
		}
	}
	
	@Get
	@Path("/grupos")
	public void list() {
		if(!grupoService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			List<Grupo> grupos = grupoService.getAllGrupos(grupoService.getCurentUser());
			result.include("grupos", grupos);
		} catch (Exception e) {
			logger.error("Erro ao listar as grupos", e);
			result.include("erros", e.getMessage());
		}
	}
	@Delete("/grupo/{grupo.id}")
	public void remove(Grupo grupo) {
		if(!grupoService.allowed()){
			result.forwardTo(IndexController.class).unauthorized();
			return;
		}
		try {
			Grupo toDelete = grupoService.find(grupo);
			grupoService.delete(grupoService.find(toDelete));
			result.forwardTo(GrupoController.class).list();
		} catch (Exception e) {
			logger.error("Erro ao excluir uma grupo", e);
			result.include("erros", e.getMessage());
			result.forwardTo(GrupoController.class).list();
		}
	}
	
	
}
