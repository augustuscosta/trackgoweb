package br.com.otgmobile.trackgoweb.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.Converter;
  
@Convert(Date.class)  
public class DateConverter implements Converter<Date> {  
  
	private static final Logger logger = Logger.getLogger(DateConverter.class);
    @Override  
    public Date convert(String value, Class<? extends Date> arg1, ResourceBundle arg2) { 
        try {  
            return new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(value);  
        } catch (ParseException e) {
            logger.error("Erro convertendo datas", e);
            return null;  
        }  
    }  
  
}  
