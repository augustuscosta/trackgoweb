package br.com.otgmobile.trackgoweb.util;

import br.com.otgmobile.trackgoweb.model.GeoPontoRota;
import br.com.otgmobile.trackgoweb.model.PontoDeInteresse;
import br.com.otgmobile.trackgoweb.model.Rota;
import br.com.otgmobile.trackgoweb.model.TrackerData;

public class LocatorUtil {

	private static final double TOLERANCE_IN_KM = 0.30;
	private static final double TOLERANCE_IN_KM_FOR_PONTO = 0.15;
	private static Double EARTH_RADIUS = 6371.00; // Raio da terra em kilometros

	public static Double calculateDistance(Double lat1, Double lon1, Double lat2, Double lon2) {
		Double Radius = LocatorUtil.EARTH_RADIUS; // 6371.00;
		Double dLat = LocatorUtil.toRadians(lat2 - lat1);
		Double dLon = LocatorUtil.toRadians(lon2 - lon1);
		Double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(LocatorUtil.toRadians(lat1))
				* Math.cos(LocatorUtil.toRadians(lat2)) * Math.sin(dLon / 2)
				* Math.sin(dLon / 2);
		Double c = 2 * Math.asin(Math.sqrt(a));
		return Radius * c;
	}
	
	public static boolean isInRoute(TrackerData position, Rota rota){
		for(GeoPontoRota ponto : rota.getGeoPonto()){
			if(isInRoute(position, ponto)){
				return true;
			}
		}
		return false;
		
	}

	private static boolean isInRoute(TrackerData position, GeoPontoRota ponto) {
		return LocatorUtil.calculateDistance(position.getLatitude(),
				position.getLongitude(), ponto.getLatitude(),
				ponto.getLongitude()) < TOLERANCE_IN_KM;
	}
	public static boolean isInPonto(TrackerData position, PontoDeInteresse ponto) {
		return LocatorUtil.calculateDistance(position.getLatitude(),
				position.getLongitude(), ponto.getLatitude(),
				ponto.getLongitude()) < TOLERANCE_IN_KM_FOR_PONTO;
	}

	public static Double toRadians(Double degree) {
		return degree * 3.1415926 / 180;
	}
}
