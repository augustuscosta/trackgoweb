package br.com.otgmobile.trackgoweb.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private static final String FORMAT ="dd/MM/yyyy HH:mm:ss";
	
	public static Date parseDateFromString(String dateString){
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		try {
			return sdf.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
}
