package br.com.otgmobile.trackgoweb.util;


import br.com.caelum.vraptor.interceptor.TypeNameExtractor;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.PrototypeScoped;
import br.com.caelum.vraptor.serialization.xstream.XStreamBuilderImpl;
import br.com.caelum.vraptor.serialization.xstream.XStreamConverters;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;

@PrototypeScoped
@Component
public class CustomXStreamBuilder extends XStreamBuilderImpl {


	public CustomXStreamBuilder(XStreamConverters converters,
			TypeNameExtractor extractor) {
		super(converters, extractor);
	}
	
	@Override
	public XStream jsonInstance() {
	    XStream xstream = super.jsonInstance();
	    xstream.registerConverter(new DateConverter("dd/MM/yyyy hh:mm:ss", new String[0]));
	    xstream.aliasSystemAttribute(null, "class");
	    xstream.aliasSystemAttribute(null, "resolves-to");
	    return xstream;
	}
}