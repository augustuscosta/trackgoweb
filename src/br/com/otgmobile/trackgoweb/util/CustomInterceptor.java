package br.com.otgmobile.trackgoweb.util;


import org.apache.log4j.Logger;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;

@Intercepts  
public class CustomInterceptor implements Interceptor {  
	
	private static final Logger logger = Logger.getLogger(CustomInterceptor.class);
	
    public boolean accepts(ResourceMethod method) {  
        return true;  
    }  
  
    public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance)  
            throws InterceptionException {  
    	try {  
    	     stack.next(method, resourceInstance);  
    	} catch (Exception e) {  
    	    try {  
    	        logger.error("Erro no intercept", e);
    	    } catch (Exception ex) {  
    	        throw new InterceptionException(ex);  
    	    }  
    	}    
    }  
  
}  
