package br.com.otgmobile.trackgoweb.util;

import br.com.otgmobile.trackgoweb.model.Cerca;
import br.com.otgmobile.trackgoweb.model.GeoPontoCerca;

import com.vividsolutions.jts.geom.Coordinate;

public class CercaUtil {

	public static boolean isInsideCercaWithGeometry(Cerca region, GeoPontoCerca coord){
		 Coordinate coordinate = new Coordinate(coord.getLongitude(), coord.getLatitude());
		
		if((coordinate.x > region.getGeometry().getEnvelopeInternal().getMinX()) 
				&& (coordinate.x < region.getGeometry().getEnvelopeInternal().getMaxX())
				&& (coordinate.y > region.getGeometry().getEnvelopeInternal().getMinY())
				&& (coordinate.x < region.getGeometry().getEnvelopeInternal().getMaxY())
				){
			return true;
		}
		
		return false;
	}
	
}
