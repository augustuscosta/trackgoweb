package br.com.otgmobile.trackgoweb.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import br.com.otgmobile.trackgoweb.model.ComandoWrapper;
import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.maxtrack.Comando;
import br.com.otgmobile.trackgoweb.model.maxtrack.ComandoEstadoTipo;
import br.com.otgmobile.trackgoweb.model.maxtrack.ComandoTipo;
import br.com.otgmobile.trackgoweb.model.maxtrack.Parametro;
import br.com.otgmobile.trackgoweb.model.niagarahw6.ComandoNiagaraHw06;
import br.com.otgmobile.trackgoweb.model.niagarahw6.NiagaraHW06ServerMessageType;
import br.com.otgmobile.trackgoweb.model.niagarahw6.NiagaraServerConfiguration;
import br.com.otgmobile.trackgoweb.model.niagarahw6.NiagraHw06APNConfiguration;

public class ComandoUtil {

	public static Comando createTimeZoneCommand(Device device, Integer timeZone) {
		Comando comando = getDefaultMaxtrackCommand(device);
		comando.setType(ComandoTipo.getType(ComandoTipo.SET_TIME_ZONE));
		comando.setParameters(new ArrayList<Parametro>());
		Parametro param = new Parametro();
		param.setId(ComandoTipo.SET_TIME_ZONE.toString());
		param.setValue("1");
		param.setComando(comando);
		comando.getParameters().add(param);
		param = new Parametro();
		param.setId("TIME ZONE");
		param.setValue(timeZone.toString());
		param.setComando(comando);
		comando.getParameters().add(param);
		return comando;
	}

	public static Comando createDisableAntiTheftCommand(Device device) {
		Comando comando = getDefaultMaxtrackCommand(device);
		comando.setType(ComandoTipo.getType(ComandoTipo.SET_DISARMED_MODE));
		comando.setParameters(new ArrayList<Parametro>());
		Parametro param = new Parametro();
		param.setId(ComandoTipo.SET_DISARMED_MODE.toString());
		param.setValue("1");
		param.setComando(comando);
		comando.getParameters().add(param);
		return comando;
	}

	public static Comando createAntiTheftCommand(Device device, Boolean enabled) {
		int value = 0;
		if (enabled)
			value = 1;
		Comando comando = getDefaultMaxtrackCommand(device);
		comando.setType(ComandoTipo.getType(ComandoTipo.SET_ANTI_THEFT));
		comando.setParameters(new ArrayList<Parametro>());
		Parametro param = new Parametro();
		param.setId(ComandoTipo.SET_ANTI_THEFT.toString());
		param.setValue(value + "");
		param.setComando(comando);
		comando.getParameters().add(param);
		param = new Parametro();
		param.setId(ComandoTipo.SET_ANTI_THEFT.toString().replace("SET_", ""));
		param.setValue(value + "");
		param.setComando(comando);
		comando.getParameters().add(param);
		return comando;
	}

	public static List<ComandoWrapper> getCommandsWrapperFromMaxtrackCommands(
			List<Comando> allMaxtrackCommands) {
		if (allMaxtrackCommands == null)
			return Collections.emptyList();
		List<ComandoWrapper> toReturn = new ArrayList<ComandoWrapper>();
		ComandoWrapper comandoWrapper;
		for (Comando comando : allMaxtrackCommands) {
			comandoWrapper = new ComandoWrapper();
			comandoWrapper.setComando(comando.getParameters().get(0).getId());
			comandoWrapper.setEnviado(comando.getSent());
			comandoWrapper.setEstado(getMaxtrackEstado(comando));
			toReturn.add(comandoWrapper);
		}
		return toReturn;
	}

	public static String getMaxtrackEstado(Comando comando) {
		if (comando.getEstado() == null
				|| comando.getEstado().getStsId() == null)
			return null;
		return comando.getEstado().getStsId().toString();
	}

	public static List<ComandoWrapper> getCommandsWrapperFromNiagarahw6Commands(
			List<ComandoNiagaraHw06> allNiagarahw6Commands) {
		if (allNiagarahw6Commands == null)
			return Collections.emptyList();

		List<ComandoWrapper> toReturn = new ArrayList<ComandoWrapper>();
		ComandoWrapper comandoWrapper;
		for (ComandoNiagaraHw06 comando : allNiagarahw6Commands) {
			comandoWrapper = new ComandoWrapper();
			comandoWrapper
					.setComando(comando.getServerMessageType().toString());
			comandoWrapper.setEnviado(comando.isSent());
			if (comando.isSent())
				comandoWrapper.setEstado(ComandoEstadoTipo.COMANDO_ENVIADO
						.toString());
			toReturn.add(comandoWrapper);
		}
		return toReturn;
	}

	public static Comando createRequestPositionCommandMaxtrack(Device device) {
		Comando comando = getDefaultMaxtrackCommand(device);
		comando.setType(ComandoTipo.getType(ComandoTipo.MXT_REQUEST_POS));
		comando.setParameters(new ArrayList<Parametro>());
		Parametro param = new Parametro();
		param.setId(ComandoTipo.MXT_REQUEST_POS.toString());
		param.setValue("1");
		param.setComando(comando);
		comando.getParameters().add(param);
		return comando;
	}

	public static Comando createDisablePanicCommand(Device device) {
		Comando comando = getDefaultMaxtrackCommand(device);
		comando.setType(ComandoTipo.getType(ComandoTipo.SET_DEACTIVATE_PANIC));
		comando.setParameters(new ArrayList<Parametro>());
		Parametro param = new Parametro();
		param.setId(ComandoTipo.SET_DEACTIVATE_PANIC.toString());
		param.setValue("1");
		param.setComando(comando);
		comando.getParameters().add(param);
		return comando;
	}

	public static Comando getDefaultMaxtrackCommand(Device device) {
		Comando comando = new Comando();
		comando.setSerial(device.getCode());
		comando.setAttempts(5);
		comando.setCommandTimeout(getCommandTimeOut());
		comando.setProtocol(device.getProtocol());
		comando.setSent(false);
		return comando;
	}

	public static Date getCommandTimeOut() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 5);
		return calendar.getTime();
	}

	public static ComandoNiagaraHw06 createRequestPositionCommandNiagaraHw6(
			Device device) {
		ComandoNiagaraHw06 comando = new ComandoNiagaraHw06();
		comando.setServerMessageType(NiagaraHW06ServerMessageType.REQUEST_IMMEDIATE_POSITION
				.getValue());
		comando.setProductserialnumber(device.getCode());
		return comando;
	}

	public static Comando createChangeOutputCommand(Device device,
			Integer output1, Integer output2, Integer output3) {
		Comando comando = getDefaultMaxtrackCommand(device);
		comando.setType(ComandoTipo.getType(ComandoTipo.SET_OUTPUT));
		comando.setParameters(new ArrayList<Parametro>());
		// header
		Parametro parametro = new Parametro();
		parametro.setId(ComandoTipo.SET_OUTPUT.toString());
		parametro.setValue("1");
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		// output1
		parametro = new Parametro();
		parametro.setId("SET OUTPUT 1");
		parametro.setValue(output1.toString());
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		// output2
		parametro = new Parametro();
		parametro.setId("SET OUTPUT 2");
		parametro.setValue(output2.toString());
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		// output3
		parametro = new Parametro();
		parametro.setId("SET OUTPUT 3");
		parametro.setValue(output3.toString());
		parametro.setComando(comando);
		comando.getParameters().add(parametro);

		return comando;
	}

	public static Comando createChangeApnCommand(Device device, String url,
			String username, String password) {
		Comando comando = getDefaultMaxtrackCommand(device);
		comando.setType(ComandoTipo.getType(ComandoTipo.SET_APN_USER_PASS));
		comando.setParameters(new ArrayList<Parametro>());
		// header
		Parametro parametro = new Parametro();
		parametro.setId(ComandoTipo.SET_APN_USER_PASS.toString());
		parametro.setValue("1");
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		// apn
		parametro = new Parametro();
		parametro.setId("APN");
		parametro.setValue(url);
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		// user
		parametro = new Parametro();
		parametro.setId("USER");
		parametro.setValue(username);
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		// pass
		parametro = new Parametro();
		parametro.setId("PASS");
		parametro.setValue(username);
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		return comando;
	}

	public static ComandoNiagaraHw06 createChangeApnNiagarahw6Command(
			Device device, String url, String username, String password) {
		ComandoNiagaraHw06 comando = new ComandoNiagaraHw06();
		comando.setServerMessageType(NiagaraHW06ServerMessageType.ISP_CONFIGURATION_SETTINGS
				.getValue());
		comando.setProductserialnumber(device.getCode());
		NiagraHw06APNConfiguration apnConfiguration = new NiagraHw06APNConfiguration();
		apnConfiguration.setApn(url);
		apnConfiguration.setUserName(username);
		apnConfiguration.setPassword(password);
		apnConfiguration.setComando(comando);
		comando.setApnConfiguration(apnConfiguration);
		return comando;
	}

	public static ComandoNiagaraHw06 createActuatorCommand(Device device,
			Integer output1) {
		ComandoNiagaraHw06 comando = new ComandoNiagaraHw06();
		comando.setServerMessageType(NiagaraHW06ServerMessageType.ACTUATOR_TURN_ON_AND_OFF
				.getValue());
		comando.setProductserialnumber(device.getCode());
		if (output1 == 1)
			comando.setActivate(true);
		else
			comando.setActivate(false);
		return comando;
	}

	public static Comando createChangeServerCommand(Device device,
			String primaryIp, String primaryPort, String seconderyIp,
			String seconderyPort) {
		Comando comando = getDefaultMaxtrackCommand(device);
		comando.setType(ComandoTipo.getType(ComandoTipo.SET_IP_PORT));
		comando.setParameters(new ArrayList<Parametro>());
		// header
		Parametro parametro = new Parametro();
		parametro.setId(ComandoTipo.SET_IP_PORT.toString());
		parametro.setValue("1");
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		// PRIMARY IP
		parametro = new Parametro();
		parametro.setId("PRIMARY IP");
		parametro.setValue(primaryIp);
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		// PRIMARY PORT
		parametro = new Parametro();
		parametro.setId("PRIMARY PORT");
		parametro.setValue(primaryPort);
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		// SECONDARY IP
		parametro = new Parametro();
		parametro.setId("SECONDARY IP");
		parametro.setValue(seconderyIp);
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		// SECONDARY PORT
		parametro = new Parametro();
		parametro.setId("SECONDARY PORT");
		parametro.setValue(seconderyPort);
		parametro.setComando(comando);
		comando.getParameters().add(parametro);
		return comando;
	}

	public static ComandoNiagaraHw06 createChangeServerNiagarahw6Command(
			Device device, String primaryIp, String primaryPort,
			String seconderyIp, String seconderyPort) {
		ComandoNiagaraHw06 comando = new ComandoNiagaraHw06();
		comando.setServerMessageType(NiagaraHW06ServerMessageType.SERVER_IP_CONFIGURATION.getValue());
		comando.setProductserialnumber(device.getCode());
		NiagaraServerConfiguration serverConfiguration = new NiagaraServerConfiguration();
		serverConfiguration.setBackupServerIP(primaryIp);
		serverConfiguration.setBackupServerPort(primaryPort);
		serverConfiguration.setBackupServer2IP(seconderyIp);
		serverConfiguration.setBackupServer2Port(seconderyPort);
		serverConfiguration.setBackupServer3IP(seconderyIp);
		serverConfiguration.setBackupServer3Port(seconderyPort);
		serverConfiguration.setComando(comando);
		comando.setServerConfiguration(serverConfiguration);
		return comando;
	}

	public static Comando createResetCommand(Device device) {
		Comando comando = getDefaultMaxtrackCommand(device);
		comando.setType(ComandoTipo.getType(ComandoTipo.DELAY));
		comando.setParameters(new ArrayList<Parametro>());
		Parametro param = new Parametro();
		param.setId(ComandoTipo.DELAY.toString());
		param.setValue("1");
		param.setComando(comando);
		comando.getParameters().add(param);
		return comando;
	}

}
