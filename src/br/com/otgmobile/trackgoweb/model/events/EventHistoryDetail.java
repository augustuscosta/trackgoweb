package br.com.otgmobile.trackgoweb.model.events;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import br.com.otgmobile.trackgoweb.model.Device;

@Entity
@Table(name = "event_history_detail")
public class EventHistoryDetail implements Serializable{
	
	private static final long serialVersionUID = -7183666169160835142L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "event_hitory_detail_sequence", sequenceName = "event_hitory_detail_sequence")
	private Long id;
	
	@Index(name = "eventhistory_eventid_idx")
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	private Event event;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	private EventHistory eventHistory;
	
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	private Device device;
	
	@Column(name = "event_date")
	private Date metricDate;
	
	private Float latitude;
	
	private Float longitude;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public EventHistory getEventHistory() {
		return eventHistory;
	}

	public void setEventHistory(EventHistory eventHistory) {
		this.eventHistory = eventHistory;
	}


	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Date getMetricDate() {
		return metricDate;
	}

	public void setMetricDate(Date metricDate) {
		this.metricDate = metricDate;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

}
