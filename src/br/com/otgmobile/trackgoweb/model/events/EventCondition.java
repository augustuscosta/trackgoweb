package br.com.otgmobile.trackgoweb.model.events;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import br.com.otgmobile.trackgoweb.model.Cerca;
import br.com.otgmobile.trackgoweb.model.Rota;

@Entity
@Table(name = "event_condition")
public class EventCondition implements Serializable{
	
	private static final long serialVersionUID = -4733700788560647735L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "event_condition_sequence", sequenceName = "event_condition_sequence")
	private Long id;
	
	private String name;
	
	private String description;
	
	private String scriptCondition;
	
	@Enumerated(EnumType.STRING)
	private EventType eventType;
	
	@JoinColumn(name = "event_id", referencedColumnName = "id")
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	private Event event;
	
	@Index(name = "eventcondition_enable_idx")
	@Column(name = "enable",nullable = false, columnDefinition = "boolean default true")
	private Boolean enable = true;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "eventcondition_cerca", joinColumns = @JoinColumn(name = "eventcondition_id"), inverseJoinColumns = @JoinColumn(name = "cerca_id"))
	private List<Cerca> cercas;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "eventcondition_rota", joinColumns = @JoinColumn(name = "eventcondition_id"), inverseJoinColumns = @JoinColumn(name = "rota_id"))
	private List<Rota> rotas;
	
	public EventCondition(){}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getScriptCondition() {
		return scriptCondition;
	}

	public void setScriptCondition(String scriptCondition) {
		this.scriptCondition = scriptCondition;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EventCondition)) {
			return false;
		}
		EventCondition other = (EventCondition) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	
	public Boolean isEnable(){
		return getEnable();
	}

	public List<Cerca> getCercas() {
		return cercas;
	}

	public void setCercas(List<Cerca> cercas) {
		this.cercas = cercas;
	}

	public List<Rota> getRotas() {
		return rotas;
	}

	public void setRotas(List<Rota> rotas) {
		this.rotas = rotas;
	}
	
}
