package br.com.otgmobile.trackgoweb.model.events;

public enum EventType {
	
	OGNL_EVENT(0),
	ENTERED_CERCA(1),
	GOT_OUT_OF_CERCA(2),
	OUT_OF_ROUTE(3),
	ENTERED_ROUTE(4);
	
	
	private int value;
	
	EventType(int value) {
		this.setValue(value);
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public static EventType getValue(int value) {
		for (EventType eventType : EventType.values()) {
			
			if(value == eventType.getValue()) {
				return eventType;
			}
		}
 		return null;
	}

}
