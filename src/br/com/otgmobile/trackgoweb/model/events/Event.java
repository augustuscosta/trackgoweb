package br.com.otgmobile.trackgoweb.model.events;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.otgmobile.trackgoweb.model.Device;
import br.com.otgmobile.trackgoweb.model.Grupo;

@Entity
@Table(name = "event")
public class Event implements Serializable{

	private static final long serialVersionUID = 6434660864129632132L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "event_sequence", sequenceName = "event_sequence")
	private Long id;
	
	private String name;
	
	private String description;
	
	@OneToMany(mappedBy = "event",cascade={CascadeType.PERSIST, CascadeType.ALL})
	private List<EventHistoryDetail> eventHistoryDetails;
	
	@OneToMany(mappedBy = "event",cascade={CascadeType.PERSIST, CascadeType.ALL})
	private List<EventCondition> eventCondition;
	
	@Column(name = "enable",nullable = false, columnDefinition = "boolean default true")
	private Boolean enable;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "event_device", joinColumns = @JoinColumn(name = "event_id"), inverseJoinColumns = @JoinColumn(name = "device_id"))
	private List<Device> devices;
	
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "grupo_event", joinColumns = @JoinColumn(name = "event_id"), inverseJoinColumns = @JoinColumn(name = "grupo_id"))
	private List<Grupo> grupos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<EventHistoryDetail> getEventHistoryDetails() {
		return eventHistoryDetails;
	}

	public void setEventHistoryDetails(List<EventHistoryDetail> eventHistoryDetails) {
		this.eventHistoryDetails = eventHistoryDetails;
	}

	public List<EventCondition> getEventConditions() {
		return eventCondition;
	}

	public void setEventConditions(List<EventCondition> eventConditions) {
		this.eventCondition = eventConditions;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	public List<EventCondition> getEventCondition() {
		return eventCondition;
	}

	public void setEventCondition(List<EventCondition> eventCondition) {
		this.eventCondition = eventCondition;
	}

	public List<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}

}
