package br.com.otgmobile.trackgoweb.model.events;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import br.com.otgmobile.trackgoweb.model.Device;

@Entity
@Table(name = "event_history")
public class EventHistory implements Serializable{

	private static final long serialVersionUID = -9034163044427150788L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "event_history_sequence", sequenceName = "event_history_sequence")
	private Long id;
	
	private Date initialDate;

	@Index(name = "eventhistory_finaldate_idx")
	private Date finalDate;
	
	@OneToMany(mappedBy = "eventHistory",cascade={CascadeType.PERSIST, CascadeType.MERGE})
	private List<EventHistoryDetail> eventHistoryDetails;
	
	private boolean sent;
	
	@JoinColumn(name = "device_event_history_id", referencedColumnName = "id")
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	private Device device;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}


	public List<EventHistoryDetail> getEventHistoryDetails() {
		return eventHistoryDetails;
	}

	public void setEventHistoryDetails(List<EventHistoryDetail> eventHistoryDetails) {
		this.eventHistoryDetails = eventHistoryDetails;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}
}
