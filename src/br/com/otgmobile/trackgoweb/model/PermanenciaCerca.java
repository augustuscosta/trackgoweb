package br.com.otgmobile.trackgoweb.model;

import java.io.Serializable;


public class PermanenciaCerca implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1666023677017339667L;

	private Cerca cerca;
	
	private Device device;
	
	private TrackerData firstPosition;
	
	private TrackerData lastPosition;

	public Cerca getCerca() {
		return cerca;
	}

	public void setCerca(Cerca cerca) {
		this.cerca = cerca;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public TrackerData getFirstPosition() {
		return firstPosition;
	}

	public void setFirstPosition(TrackerData firstPosition) {
		this.firstPosition = firstPosition;
	}

	public TrackerData getLastPosition() {
		return lastPosition;
	}

	public void setLastPosition(TrackerData lastPosition) {
		this.lastPosition = lastPosition;
	}
	
}
