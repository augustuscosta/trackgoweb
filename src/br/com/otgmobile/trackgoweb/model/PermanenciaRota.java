package br.com.otgmobile.trackgoweb.model;

import java.io.Serializable;


public class PermanenciaRota implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7142043174819329660L;

	private Rota rota;
	
	private Device device;
	
	private TrackerData firstPosition;
	
	private TrackerData lastPosition;

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}


	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public TrackerData getFirstPosition() {
		return firstPosition;
	}

	public void setFirstPosition(TrackerData firstPosition) {
		this.firstPosition = firstPosition;
	}

	public TrackerData getLastPosition() {
		return lastPosition;
	}

	public void setLastPosition(TrackerData lastPosition) {
		this.lastPosition = lastPosition;
	}
	
}
