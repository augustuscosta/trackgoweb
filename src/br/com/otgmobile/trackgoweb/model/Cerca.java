package br.com.otgmobile.trackgoweb.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.otgmobile.trackgoweb.model.events.EventCondition;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.PrecisionModel;

@Entity
@Table(name="cerca")
public class Cerca {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="cerca_sequence", sequenceName="cerca_sequence")
	private Integer id;
	
	private String name;
	
	@OneToMany(mappedBy="cerca", cascade=CascadeType.ALL,targetEntity=GeoPontoCerca.class)   
	private List<GeoPontoCerca> geoPonto;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "eventcondition_cerca", joinColumns = @JoinColumn(name = "cerca_id"), inverseJoinColumns = @JoinColumn(name = "eventcondition_id"))
	private List<EventCondition> eventConditions;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "grupo_cerca", joinColumns = @JoinColumn(name = "cerca_id"), inverseJoinColumns = @JoinColumn(name = "grupo_id"))
	private List<Grupo> grupos;
	
	private Geometry geometry;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<GeoPontoCerca> getGeoPonto() {
		return geoPonto;
	}

	public void setGeoPonto(List<GeoPontoCerca> geoPonto) {
		this.geoPonto = geoPonto;
	}
	

	public List<EventCondition> getEventConstions() {
		return eventConditions;
	}

	public void setEventConditions(List<EventCondition> events) {
		this.eventConditions = events;
	}

	public List<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}

	public Geometry getGeometry() {
		if(geometry == null){
			populateGeometry(getGeoPonto());
		}
		return geometry;
	}

	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}
	
	private void populateGeometry(List<GeoPontoCerca> geoPontos) {
		if (geoPontos == null || geoPontos.isEmpty())
			return;

		Coordinate[] coordinates = new Coordinate[geoPontos.size() + 1];
		Coordinate coordinate = null;
		Coordinate firstcoordinate = null;

		int i = 0;
		for (GeoPontoCerca geoponto : geoPontos) {
			if (firstcoordinate == null)
				firstcoordinate = getCoordinatefromGeoponto(geoponto);
			coordinate = getCoordinatefromGeoponto(geoponto);
			coordinates[i] = coordinate;
			i++;
		}
		coordinates[coordinates.length - 1] = firstcoordinate;

		GeometryFactory geometryFactory = new GeometryFactory(
				new PrecisionModel(PrecisionModel.FIXED), 4326);
		LinearRing polygonBoundary = geometryFactory
				.createLinearRing(coordinates);
		Polygon polygon = geometryFactory.createPolygon(polygonBoundary, null);
		setGeometry(polygon);
	}
	
	private Coordinate getCoordinatefromGeoponto(GeoPontoCerca geoponto) {
		Coordinate coordinate = new Coordinate();
		coordinate.x = geoponto.getLongitude();
		coordinate.y = geoponto.getLatitude();
		return coordinate;
	}
}
