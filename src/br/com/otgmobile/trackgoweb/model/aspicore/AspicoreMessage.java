package br.com.otgmobile.trackgoweb.model.aspicore;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "aspicore")
public class AspicoreMessage {
	
	@Id
	private Long id;
	private Float postlat;
	private Float postlong;
	private Date postime;
	private Float speed;
	private Float course;
	private String imei;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Float getPostlat() {
		return postlat;
	}
	public void setPostlat(Float postlat) {
		this.postlat = postlat;
	}
	public Float getPostlong() {
		return postlong;
	}
	public void setPostlong(Float postlong) {
		this.postlong = postlong;
	}
	public Date getPostime() {
		return postime;
	}
	public void setPostime(Date postime) {
		this.postime = postime;
	}
	public Float getSpeed() {
		return speed;
	}
	public void setSpeed(Float speed) {
		this.speed = speed;
	}
	public Float getCourse() {
		return course;
	}
	public void setCourse(Float course) {
		this.course = course;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
}
