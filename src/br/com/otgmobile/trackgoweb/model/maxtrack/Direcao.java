package br.com.otgmobile.trackgoweb.model.maxtrack;

public enum Direcao {
	
	NORTE,
	NORDESTE,
	LESTE,
	SUDESTE,
	SUL,
	SUDOESTE,
	OESTE,
	NOROESTE,
	QUALQUER_DIRECAO
}
