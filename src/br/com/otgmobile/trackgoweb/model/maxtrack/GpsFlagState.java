package br.com.otgmobile.trackgoweb.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_gps_flag_state")
public class GpsFlagState {

	@Id
	private Long id;	
	private Integer validGps;
	private Integer gpsAntenaFailure;
	private Integer usingDr; // Usando DR para calculo de posições
	private Integer excessSpeed;
	private Integer excessStoppedTime;
	private Integer voiceCall;
	private Integer forceGps;
	private Integer gprsConnection;
	private Integer gpsSignal; //0 = memoria 1 = atual
	private Integer gpsAntennaDisconnected;
	private Integer gsmJamming; // 0 - transmissão realizada no estado normal
	private Integer gpsSleep; //receptor gps em modo sleep no momento da tranmissao
	private Boolean moving; // 0 = parado, 1 = movimento
	private Boolean cellPresent; // presença do MXT dewntro de uma área de uma ERB (célula GSM) 0 = fora, 1 = dentro
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getValidGps() {
		return validGps;
	}
	public void setValidGps(Integer validGps) {
		this.validGps = validGps;
	}
	public Integer getGpsAntenaFailure() {
		return gpsAntenaFailure;
	}
	public void setGpsAntenaFailure(Integer gpsAntenaFailure) {
		this.gpsAntenaFailure = gpsAntenaFailure;
	}
	public Integer getUsingDr() {
		return usingDr;
	}
	public void setUsingDr(Integer usingDr) {
		this.usingDr = usingDr;
	}
	public Integer getExcessSpeed() {
		return excessSpeed;
	}
	public void setExcessSpeed(Integer excessSpeed) {
		this.excessSpeed = excessSpeed;
	}
	public Integer getExcessStoppedTime() {
		return excessStoppedTime;
	}
	public void setExcessStoppedTime(Integer excessStoppedTime) {
		this.excessStoppedTime = excessStoppedTime;
	}
	public Integer getVoiceCall() {
		return voiceCall;
	}
	public void setVoiceCall(Integer voiceCall) {
		this.voiceCall = voiceCall;
	}
	public Integer getForceGps() {
		return forceGps;
	}
	public void setForceGps(Integer forceGps) {
		this.forceGps = forceGps;
	}
	public Integer getGprsConnection() {
		return gprsConnection;
	}
	public void setGprsConnection(Integer gprsConnection) {
		this.gprsConnection = gprsConnection;
	}
	public Integer getGpsSignal() {
		return gpsSignal;
	}
	public void setGpsSignal(Integer gpsSignal) {
		this.gpsSignal = gpsSignal;
	}
	public Integer getGpsAntennaDisconnected() {
		return gpsAntennaDisconnected;
	}
	public void setGpsAntennaDisconnected(Integer gpsAntennaDisconnected) {
		this.gpsAntennaDisconnected = gpsAntennaDisconnected;
	}
	public Integer getGsmJamming() {
		return gsmJamming;
	}
	public void setGsmJamming(Integer gsmJamming) {
		this.gsmJamming = gsmJamming;
	}
	public Integer getGpsSleep() {
		return gpsSleep;
	}
	public void setGpsSleep(Integer gpsSleep) {
		this.gpsSleep = gpsSleep;
	}
	public Boolean getMoving() {
		return moving;
	}
	public void setMoving(Boolean moving) {
		this.moving = moving;
	}
	public Boolean getCellPresent() {
		return cellPresent;
	}
	public void setCellPresent(Boolean cellPresent) {
		this.cellPresent = cellPresent;
	}
	
	
}
