package br.com.otgmobile.trackgoweb.model.maxtrack;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_parametro")
public class Parametro {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_parameter_sequence", sequenceName="maxtrack_parameter_sequence")
	private Long idParameter;
	private String id;
	private String value;
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	private Comando comando;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Comando getComando() {
		return comando;
	}
	public void setComando(Comando comando) {
		this.comando = comando;
	}
	public Long getIdParameter() {
		return idParameter;
	}
	public void setIdParameter(Long idParameter) {
		this.idParameter = idParameter;
	}
}
