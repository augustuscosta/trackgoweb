package br.com.otgmobile.trackgoweb.model.maxtrack;

public enum AntiTheftStatus {
	
	NORMAL,
	ARMADO,
	SUSPENSO,
	DISPARADO

}
