package br.com.otgmobile.trackgoweb.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_inputs")
public class Inputs {
	
	@Id
	private Long id;
	private Boolean ignition;
	private Integer input1;
	private Integer input2;
	private Integer input3;
	private Integer input4;
	private Integer input5;
	private Integer input6;
	private Integer input7;
	private Integer input8;
	private Boolean panic;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getIgnition() {
		return ignition;
	}
	public void setIgnition(Boolean ignition) {
		this.ignition = ignition;
	}
	public Integer getInput1() {
		return input1;
	}
	public void setInput1(Integer input1) {
		this.input1 = input1;
	}
	public Integer getInput2() {
		return input2;
	}
	public void setInput2(Integer input2) {
		this.input2 = input2;
	}
	public Integer getInput3() {
		return input3;
	}
	public void setInput3(Integer input3) {
		this.input3 = input3;
	}
	public Integer getInput4() {
		return input4;
	}
	public void setInput4(Integer input4) {
		this.input4 = input4;
	}
	public Integer getInput5() {
		return input5;
	}
	public void setInput5(Integer input5) {
		this.input5 = input5;
	}
	public Integer getInput6() {
		return input6;
	}
	public void setInput6(Integer input6) {
		this.input6 = input6;
	}
	public Integer getInput7() {
		return input7;
	}
	public void setInput7(Integer input7) {
		this.input7 = input7;
	}
	public Integer getInput8() {
		return input8;
	}
	public void setInput8(Integer input8) {
		this.input8 = input8;
	}
	public Boolean getPanic() {
		return panic;
	}
	public void setPanic(Boolean panic) {
		this.panic = panic;
	}
	
	
}
