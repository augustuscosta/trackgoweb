package br.com.otgmobile.trackgoweb.model.maxtrack;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_position")
public class Position{

	@Id
	private Long id;
	private String serial;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	private Firmware firmware;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	private Gps gps;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	private HardwareMonitor hardwareMonitor;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public Firmware getFirmware() {
		return firmware;
	}
	public void setFirmware(Firmware firmware) {
		this.firmware = firmware;
	}
	public Gps getGps() {
		return gps;
	}
	public void setGps(Gps gps) {
		this.gps = gps;
	}
	public HardwareMonitor getHardwareMonitor() {
		return hardwareMonitor;
	}
	public void setHardwareMonitor(HardwareMonitor hardwareMonitor) {
		this.hardwareMonitor = hardwareMonitor;
	}
}
