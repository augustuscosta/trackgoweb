package br.com.otgmobile.trackgoweb.model.maxtrack;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_firmware")
public class Firmware {
	
	@Id
	private Long id;
	private String serial;
	private Integer protocol;  				// código de protocolo do equipamento
	private Long memoryIndex; 			// contador de posições AVL
	private Long lifeTime; 					// Tempo decorrido desde o último boot MXT
	@Enumerated(EnumType.STRING)
	private MotivoTransmissao transmissionReason;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	private FirmwareFlagState flagState;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public Integer getProtocol() {
		return protocol;
	}
	public void setProtocol(Integer protocol) {
		this.protocol = protocol;
	}
	public Long getMemoryIndex() {
		return memoryIndex;
	}
	public void setMemoryIndex(Long memoryIndex) {
		this.memoryIndex = memoryIndex;
	}
	public Long getLifeTime() {
		return lifeTime;
	}
	public void setLifeTime(Long lifeTime) {
		this.lifeTime = lifeTime;
	}
	public MotivoTransmissao getTransmissionReason() {
		return transmissionReason;
	}
	public void setTransmissionReason(MotivoTransmissao transmissionReason) {
		this.transmissionReason = transmissionReason;
	}
	public FirmwareFlagState getFlagState() {
		return flagState;
	}
	public void setFlagState(FirmwareFlagState flagState) {
		this.flagState = flagState;
	}
	
}
