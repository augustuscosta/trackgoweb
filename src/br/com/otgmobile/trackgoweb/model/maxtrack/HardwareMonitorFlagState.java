package br.com.otgmobile.trackgoweb.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_hardware_monitor_flag_state")
public class HardwareMonitorFlagState {
	
	@Id
	private Long id;
	private Boolean lowPower;
	private Boolean cfc;
	private Boolean satelital;
	private Boolean sleepMode;
	private Boolean mainSerial;
	private Boolean additionalSerial;
	private Boolean internalAlarm;
	private Boolean openedDoor;
	private Boolean moneyMachine1Failure;
	private Boolean moneyMachine2Failure;
	private Boolean coinMachineFailure;
	private Boolean passengerCountFailure;
	private Boolean drvFailure;
	private Boolean keyboardFailure;
	private Boolean panelFailure;
	private Boolean cameraFailure;
	private Boolean coinMachineFull;
	private Boolean scheduleBlock;
	private Boolean batteryFailure;
	private Boolean batteryCharging;
	private Boolean telenodoFailure;
	private Boolean streadSpectrumFailure;
	private Boolean acessoryMissing;
	private AntiTheftStatus antiTheftStatus;
	private Integer ad2Filter;
	private Integer ad3Filter;
	private Integer ad4Filter;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getLowPower() {
		return lowPower;
	}
	public void setLowPower(Boolean lowPower) {
		this.lowPower = lowPower;
	}
	public Boolean getCfc() {
		return cfc;
	}
	public void setCfc(Boolean cfc) {
		this.cfc = cfc;
	}
	public Boolean getSatelital() {
		return satelital;
	}
	public void setSatelital(Boolean satelital) {
		this.satelital = satelital;
	}
	public Boolean getSleepMode() {
		return sleepMode;
	}
	public void setSleepMode(Boolean sleepMode) {
		this.sleepMode = sleepMode;
	}
	public Boolean getMainSerial() {
		return mainSerial;
	}
	public void setMainSerial(Boolean mainSerial) {
		this.mainSerial = mainSerial;
	}
	public Boolean getAdditionalSerial() {
		return additionalSerial;
	}
	public void setAdditionalSerial(Boolean additionalSerial) {
		this.additionalSerial = additionalSerial;
	}
	public Boolean getInternalAlarm() {
		return internalAlarm;
	}
	public void setInternalAlarm(Boolean internalAlarm) {
		this.internalAlarm = internalAlarm;
	}
	public Boolean getOpenedDoor() {
		return openedDoor;
	}
	public void setOpenedDoor(Boolean openedDoor) {
		this.openedDoor = openedDoor;
	}
	public Boolean getMoneyMachine1Failure() {
		return moneyMachine1Failure;
	}
	public void setMoneyMachine1Failure(Boolean moneyMachine1Failure) {
		this.moneyMachine1Failure = moneyMachine1Failure;
	}
	public Boolean getMoneyMachine2Failure() {
		return moneyMachine2Failure;
	}
	public void setMoneyMachine2Failure(Boolean moneyMachine2Failure) {
		this.moneyMachine2Failure = moneyMachine2Failure;
	}
	public Boolean getCoinMachineFailure() {
		return coinMachineFailure;
	}
	public void setCoinMachineFailure(Boolean coinMachineFailure) {
		this.coinMachineFailure = coinMachineFailure;
	}
	public Boolean getPassengerCountFailure() {
		return passengerCountFailure;
	}
	public void setPassengerCountFailure(Boolean passengerCountFailure) {
		this.passengerCountFailure = passengerCountFailure;
	}
	public Boolean getDrvFailure() {
		return drvFailure;
	}
	public void setDrvFailure(Boolean drvFailure) {
		this.drvFailure = drvFailure;
	}
	public Boolean getKeyboardFailure() {
		return keyboardFailure;
	}
	public void setKeyboardFailure(Boolean keyboardFailure) {
		this.keyboardFailure = keyboardFailure;
	}
	public Boolean getPanelFailure() {
		return panelFailure;
	}
	public void setPanelFailure(Boolean panelFailure) {
		this.panelFailure = panelFailure;
	}
	public Boolean getCameraFailure() {
		return cameraFailure;
	}
	public void setCameraFailure(Boolean cameraFailure) {
		this.cameraFailure = cameraFailure;
	}
	public Boolean getCoinMachineFull() {
		return coinMachineFull;
	}
	public void setCoinMachineFull(Boolean coinMachineFull) {
		this.coinMachineFull = coinMachineFull;
	}
	public Boolean getScheduleBlock() {
		return scheduleBlock;
	}
	public void setScheduleBlock(Boolean scheduleBlock) {
		this.scheduleBlock = scheduleBlock;
	}
	public Boolean getBatteryFailure() {
		return batteryFailure;
	}
	public void setBatteryFailure(Boolean batteryFailure) {
		this.batteryFailure = batteryFailure;
	}
	public Boolean getBatteryCharging() {
		return batteryCharging;
	}
	public void setBatteryCharging(Boolean batteryCharging) {
		this.batteryCharging = batteryCharging;
	}
	public Boolean getTelenodoFailure() {
		return telenodoFailure;
	}
	public void setTelenodoFailure(Boolean telenodoFailure) {
		this.telenodoFailure = telenodoFailure;
	}
	public Boolean getStreadSpectrumFailure() {
		return streadSpectrumFailure;
	}
	public void setStreadSpectrumFailure(Boolean streadSpectrumFailure) {
		this.streadSpectrumFailure = streadSpectrumFailure;
	}
	public Boolean getAcessoryMissing() {
		return acessoryMissing;
	}
	public void setAcessoryMissing(Boolean acessoryMissing) {
		this.acessoryMissing = acessoryMissing;
	}
	public AntiTheftStatus getAntiTheftStatus() {
		return antiTheftStatus;
	}
	public void setAntiTheftStatus(AntiTheftStatus antiTheftStatus) {
		this.antiTheftStatus = antiTheftStatus;
	}
	public Integer getAd2Filter() {
		return ad2Filter;
	}
	public void setAd2Filter(Integer ad2Filter) {
		this.ad2Filter = ad2Filter;
	}
	public Integer getAd3Filter() {
		return ad3Filter;
	}
	public void setAd3Filter(Integer ad3Filter) {
		this.ad3Filter = ad3Filter;
	}
	public Integer getAd4Filter() {
		return ad4Filter;
	}
	public void setAd4Filter(Integer ad4Filter) {
		this.ad4Filter = ad4Filter;
	}
	
}
