package br.com.otgmobile.trackgoweb.model.maxtrack;

public enum ComandoEstadoTipo {

	COMANDO_ENVIADO,
	TENTATIVAS_EXCEDIDAS,
	EXPIROU_POR_TEMPO,
	CANCELADO,
	CONFIRMADO,
	XML_INVALIDO
}
