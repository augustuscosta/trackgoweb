package br.com.otgmobile.trackgoweb.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_firmware_flag_state")
public class FirmwareFlagState {
	
	@Id
	private Long id;
	private Boolean setupModified;			// alteração no setup
	private Boolean firmwareStored;         // firmware armazenado
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getSetupModified() {
		return setupModified;
	}
	public void setSetupModified(Boolean setupModified) {
		this.setupModified = setupModified;
	}
	public Boolean getFirmwareStored() {
		return firmwareStored;
	}
	public void setFirmwareStored(Boolean firmwareStored) {
		this.firmwareStored = firmwareStored;
	}
}
