package br.com.otgmobile.trackgoweb.model.maxtrack;

public enum ProtocoloComando {
	
	MTC400_ID_MENOR_QUE_65535(42),
	MTC400_ID_MAIOR_QUE_65535(44),
	MTC500(45),
	MTC600(4),
	MXT100(160),
	MXT101(161),
	MXT140(166),
	MXT150(162),
	MXT151(163),
	MXT120(164);

	private int value;
	
	ProtocoloComando(int value){
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
}
