package br.com.otgmobile.trackgoweb.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_secure_output_mode")
public class SecureOutputMode {
	
	@Id
	private Long id;
	
	private Integer output1;
	private Integer output2;
	private Integer output3;
	private Integer output4;
	private Integer output5;
	private Integer output6;
	private Integer output7;
	private Integer output8;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getOutput1() {
		return output1;
	}
	public void setOutput1(Integer output1) {
		this.output1 = output1;
	}
	public Integer getOutput2() {
		return output2;
	}
	public void setOutput2(Integer output2) {
		this.output2 = output2;
	}
	public Integer getOutput3() {
		return output3;
	}
	public void setOutput3(Integer output3) {
		this.output3 = output3;
	}
	public Integer getOutput4() {
		return output4;
	}
	public void setOutput4(Integer output4) {
		this.output4 = output4;
	}
	public Integer getOutput5() {
		return output5;
	}
	public void setOutput5(Integer output5) {
		this.output5 = output5;
	}
	public Integer getOutput6() {
		return output6;
	}
	public void setOutput6(Integer output6) {
		this.output6 = output6;
	}
	public Integer getOutput7() {
		return output7;
	}
	public void setOutput7(Integer output7) {
		this.output7 = output7;
	}
	public Integer getOutput8() {
		return output8;
	}
	public void setOutput8(Integer output8) {
		this.output8 = output8;
	}

	
	
}
