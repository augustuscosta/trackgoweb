package br.com.otgmobile.trackgoweb.model.maxtrack;

public enum ConfiguracaoIntervalo {

	NAO_CONFIGURADO,
	BAIXO,
	MEDIO,
	ALTO
}
