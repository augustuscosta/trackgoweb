package br.com.otgmobile.trackgoweb.model.maxtrack;


public enum EstadoIntervalo {

	NORMAL,
	EM_USO,
	CARREGANDO,
	FALHA
}
