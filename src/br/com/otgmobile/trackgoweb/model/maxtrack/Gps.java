package br.com.otgmobile.trackgoweb.model.maxtrack;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_gps")
public class Gps {
	
	@Id
	private Long id;
	private Date date;
	private Float latitude;
	private Float longitude;
	private Integer altitude;
	private Direcao course;  
	private Double speed;
	private Integer csq;
	private Double hdop;
	private Integer svn;
	private Integer hodometer;
	private Long lastValidPositionTime;
	private String acelerometerEvent;
	private String acelerometerValue;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	private GpsFlagState flagState;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Integer getAltitude() {
		return altitude;
	}

	public void setAltitude(Integer altitude) {
		this.altitude = altitude;
	}

	public Direcao getCourse() {
		return course;
	}

	public void setCourse(Direcao course) {
		this.course = course;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Integer getCsq() {
		return csq;
	}

	public void setCsq(Integer csq) {
		this.csq = csq;
	}

	public Double getHdop() {
		return hdop;
	}

	public void setHdop(Double hdop) {
		this.hdop = hdop;
	}

	public Integer getSvn() {
		return svn;
	}

	public void setSvn(Integer svn) {
		this.svn = svn;
	}

	public Integer getHodometer() {
		return hodometer;
	}

	public void setHodometer(Integer hodometer) {
		this.hodometer = hodometer;
	}

	public Long getLastValidPositionTime() {
		return lastValidPositionTime;
	}

	public void setLastValidPositionTime(Long lastValidPositionTime) {
		this.lastValidPositionTime = lastValidPositionTime;
	}

	public String getAcelerometerEvent() {
		return acelerometerEvent;
	}

	public void setAcelerometerEvent(String acelerometerEvent) {
		this.acelerometerEvent = acelerometerEvent;
	}

	public String getAcelerometerValue() {
		return acelerometerValue;
	}

	public void setAcelerometerValue(String acelerometerValue) {
		this.acelerometerValue = acelerometerValue;
	}

	public GpsFlagState getFlagState() {
		return flagState;
	}

	public void setFlagState(GpsFlagState flagState) {
		this.flagState = flagState;
	}

}
