package br.com.otgmobile.trackgoweb.model.maxtrack;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_command")
public class Comando {

	@Enumerated(EnumType.STRING)
	private ProtocoloComando protocol;
	private String serial;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_command_sequence", sequenceName="maxtrack_command_sequence")
	private Long idCommand;
	@Enumerated(EnumType.STRING)
	private Integer type;
	private Integer attempts;
	private Date commandTimeout; // yyyy-mm-dd hh:mm:ss
	@OneToMany(mappedBy="comando", cascade=CascadeType.ALL,targetEntity=Parametro.class)
	private List<Parametro> parameters;
	@OneToOne(cascade = CascadeType.ALL)
	private ComandoEstado estado;
	@Column(name = "sent",nullable=false, columnDefinition="boolean default false")
	private Boolean sent;
	
	public ProtocoloComando getProtocol() {
		return protocol;
	}
	public void setProtocol(ProtocoloComando protocol) {
		this.protocol = protocol;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public Long getIdCommand() {
		return idCommand;
	}
	public void setIdCommand(Long idCommand) {
		this.idCommand = idCommand;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getAttempts() {
		return attempts;
	}
	public void setAttempts(Integer attempts) {
		this.attempts = attempts;
	}
	public Date getCommandTimeout() {
		return commandTimeout;
	}
	public void setCommandTimeout(Date commandTimeout) {
		this.commandTimeout = commandTimeout;
	}
	public List<Parametro> getParameters() {
		return parameters;
	}
	public void setParameters(List<Parametro> parameters) {
		this.parameters = parameters;
	}
	public ComandoEstado getEstado() {
		return estado;
	}
	public void setEstado(ComandoEstado estado) {
		this.estado = estado;
	}
	public Boolean getSent() {
		return sent;
	}
	public void setSent(Boolean sent) {
		this.sent = sent;
	}
}
