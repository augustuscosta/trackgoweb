package br.com.otgmobile.trackgoweb.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_range_state")
public class RangeState {
	
	@Id
	private Long id;
	private EstadoIntervalo internalBattery;
	private EstadoIntervalo externalBattery;
	private Integer powerSupply; 
	private ConfiguracaoIntervalo ad1Range;
	private ConfiguracaoIntervalo ad2Range;
	private ConfiguracaoIntervalo ad3Range;
	private ConfiguracaoIntervalo ad4Range;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public EstadoIntervalo getInternalBattery() {
		return internalBattery;
	}
	public void setInternalBattery(EstadoIntervalo internalBattery) {
		this.internalBattery = internalBattery;
	}
	public EstadoIntervalo getExternalBattery() {
		return externalBattery;
	}
	public void setExternalBattery(EstadoIntervalo externalBattery) {
		this.externalBattery = externalBattery;
	}
	public Integer getPowerSupply() {
		return powerSupply;
	}
	public void setPowerSupply(Integer powerSupply) {
		this.powerSupply = powerSupply;
	}
	public ConfiguracaoIntervalo getAd1Range() {
		return ad1Range;
	}
	public void setAd1Range(ConfiguracaoIntervalo ad1Range) {
		this.ad1Range = ad1Range;
	}
	public ConfiguracaoIntervalo getAd2Range() {
		return ad2Range;
	}
	public void setAd2Range(ConfiguracaoIntervalo ad2Range) {
		this.ad2Range = ad2Range;
	}
	public ConfiguracaoIntervalo getAd3Range() {
		return ad3Range;
	}
	public void setAd3Range(ConfiguracaoIntervalo ad3Range) {
		this.ad3Range = ad3Range;
	}
	public ConfiguracaoIntervalo getAd4Range() {
		return ad4Range;
	}
	public void setAd4Range(ConfiguracaoIntervalo ad4Range) {
		this.ad4Range = ad4Range;
	}
}
