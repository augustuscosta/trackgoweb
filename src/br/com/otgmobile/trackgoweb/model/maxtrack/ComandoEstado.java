package br.com.otgmobile.trackgoweb.model.maxtrack;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="maxtrack_command_report")
public class ComandoEstado {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_command_report_sequence", sequenceName="maxtrack_command_report_sequence")
	private Long id;
	private String serial;
	private Long idCommand;
	private Integer attempt;
	@Enumerated(EnumType.STRING)
	private ComandoEstadoTipo stsId;
	private Date stsTimeStamp;
	@OneToOne(cascade = CascadeType.ALL)
	private Comando comando;
	
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public Long getIdCommand() {
		return idCommand;
	}
	public void setIdCommand(Long idCommand) {
		this.idCommand = idCommand;
	}
	public Integer getAttempt() {
		return attempt;
	}
	public void setAttempt(Integer attempt) {
		this.attempt = attempt;
	}
	public ComandoEstadoTipo getStsId() {
		return stsId;
	}
	public void setStsId(ComandoEstadoTipo stsId) {
		this.stsId = stsId;
	}
	public Date getStsTimeStamp() {
		return stsTimeStamp;
	}
	public void setStsTimeStamp(Date stsTimeStamp) {
		this.stsTimeStamp = stsTimeStamp;
	}
	public Comando getComando() {
		return comando;
	}
	public void setComando(Comando comando) {
		this.comando = comando;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
