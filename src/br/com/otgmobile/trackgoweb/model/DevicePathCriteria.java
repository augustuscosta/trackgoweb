package br.com.otgmobile.trackgoweb.model;

import java.util.Date;

public class DevicePathCriteria {

	private Device device;
	private Date start;
	private Date end;
	
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
}
