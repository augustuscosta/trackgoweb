package br.com.otgmobile.trackgoweb.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.otgmobile.trackgoweb.model.events.Event;

@Entity
@Table(name = "grupo")
public class Grupo {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="device_sequence", sequenceName="device_sequence")
	private Long id;
	
	private String name;
	
	private String description;
	
	private Boolean enabled;
	
	private Boolean deviceAddEdit;
	
	private Boolean cercaAddEdit;
	
	private Boolean rotaAddEdit;
	
	private Boolean eventosAddEdit;
	
	private Boolean pontoDeInteresseAddEdit;
	
	private Boolean veiculoAddEdit;
	
	private Boolean usuariosAddEdit;
	
	private Boolean gruposAddEdit;
	
	
	@Enumerated(EnumType.STRING)
	private Permissao permissao;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "grupo_usuario", joinColumns = @JoinColumn(name = "grupo_id"), inverseJoinColumns = @JoinColumn(name = "usuario_id"))
	private List<Usuario> usuarios;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "grupo_device", joinColumns = @JoinColumn(name = "grupo_id"), inverseJoinColumns = @JoinColumn(name = "device_id"))
	private List<Device> devices;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "grupo_cerca", joinColumns = @JoinColumn(name = "grupo_id"), inverseJoinColumns = @JoinColumn(name = "cerca_id"))
	private List<Cerca> cercas;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "grupo_rota", joinColumns = @JoinColumn(name = "grupo_id"), inverseJoinColumns = @JoinColumn(name = "rota_id"))
	private List<Rota> rotas;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "grupo_veiculo", joinColumns = @JoinColumn(name = "grupo_id"), inverseJoinColumns = @JoinColumn(name = "veiculo_id"))
	private List<Veiculo> veiculos;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "grupo_event", joinColumns = @JoinColumn(name = "grupo_id"), inverseJoinColumns = @JoinColumn(name = "event_id"))
	private List<Event> events;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name = "grupo_pontos_de_interesse", joinColumns = @JoinColumn(name = "grupo_id"), inverseJoinColumns = @JoinColumn(name = "ponto_de_interesse_id"))
	private List<PontoDeInteresse> pontosDeInteresse;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}
	
	

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getDeviceAddEdit() {
		return deviceAddEdit;
	}

	public void setDeviceAddEdit(Boolean deviceAddEdit) {
		this.deviceAddEdit = deviceAddEdit;
	}

	public Boolean getCercaAddEdit() {
		return cercaAddEdit;
	}

	public void setCercaAddEdit(Boolean cercaAddEdit) {
		this.cercaAddEdit = cercaAddEdit;
	}

	public Boolean getRotaAddEdit() {
		return rotaAddEdit;
	}

	public void setRotaAddEdit(Boolean rotaAddEdit) {
		this.rotaAddEdit = rotaAddEdit;
	}

	public Boolean getEventosAddEdit() {
		return eventosAddEdit;
	}

	public void setEventosAddEdit(Boolean eventosAddEdit) {
		this.eventosAddEdit = eventosAddEdit;
	}

	public Boolean getPontoDeInteresseAddEdit() {
		return pontoDeInteresseAddEdit;
	}

	public void setPontoDeInteresseAddEdit(Boolean pontoDeInteresseAddEdit) {
		this.pontoDeInteresseAddEdit = pontoDeInteresseAddEdit;
	}

	public Boolean getVeiculoAddEdit() {
		return veiculoAddEdit;
	}

	public void setVeiculoAddEdit(Boolean veiculoAddEdit) {
		this.veiculoAddEdit = veiculoAddEdit;
	}

	public Boolean getUsuariosAddEdit() {
		return usuariosAddEdit;
	}

	public void setUsuariosAddEdit(Boolean usuariosAddEdit) {
		this.usuariosAddEdit = usuariosAddEdit;
	}

	public Boolean getGruposAddEdit() {
		return gruposAddEdit;
	}

	public void setGruposAddEdit(Boolean gruposAddEdit) {
		this.gruposAddEdit = gruposAddEdit;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	public List<Cerca> getCercas() {
		return cercas;
	}

	public void setCercas(List<Cerca> cercas) {
		this.cercas = cercas;
	}

	public List<Rota> getRotas() {
		return rotas;
	}

	public void setRotas(List<Rota> rotas) {
		this.rotas = rotas;
	}

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public List<PontoDeInteresse> getPontosDeInteresse() {
		return pontosDeInteresse;
	}

	public void setPontosDeInteresse(List<PontoDeInteresse> pontosDeInteresse) {
		this.pontosDeInteresse = pontosDeInteresse;
	}
	
	
}
