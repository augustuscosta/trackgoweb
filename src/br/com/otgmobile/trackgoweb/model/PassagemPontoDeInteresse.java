package br.com.otgmobile.trackgoweb.model;

import java.io.Serializable;


public class PassagemPontoDeInteresse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1496286907633069849L;

	private PontoDeInteresse pontoDeInteresse;
	
	private Device device;
	
	private TrackerData firstPosition;
	
	private TrackerData lastPosition;


	public PontoDeInteresse getPontoDeInteresse() {
		return pontoDeInteresse;
	}

	public void setPontoDeInteresse(PontoDeInteresse pontoDeInteresse) {
		this.pontoDeInteresse = pontoDeInteresse;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public TrackerData getFirstPosition() {
		return firstPosition;
	}

	public void setFirstPosition(TrackerData firstPosition) {
		this.firstPosition = firstPosition;
	}

	public TrackerData getLastPosition() {
		return lastPosition;
	}

	public void setLastPosition(TrackerData lastPosition) {
		this.lastPosition = lastPosition;
	}
	
}
