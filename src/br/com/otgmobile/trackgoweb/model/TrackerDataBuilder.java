package br.com.otgmobile.trackgoweb.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.otgmobile.trackgoweb.model.aspicore.AspicoreMessage;
import br.com.otgmobile.trackgoweb.model.maxtrack.Position;
import br.com.otgmobile.trackgoweb.model.niagarahw6.NiagaraHW06message;
import br.com.otgmobile.trackgoweb.model.skypatrol.SkypatrolMessage;
import br.com.otgmobile.trackgoweb.model.tk10x.TK10XMessage;

public class TrackerDataBuilder {

	public static List<TrackerData> parseMaxtrackData(List<Position> positions) {
		if (positions == null)
			return Collections.emptyList();
		List<TrackerData> toReturn = new ArrayList<TrackerData>();
		for (Position position : positions) {
			toReturn.add(parseMaxtrackData(position));
		}
		return toReturn;
	}

	public static TrackerData parseMaxtrackData(Position position) {
		TrackerData toReturn = new TrackerData();
		if (position != null) {
			
			if(position.getGps() != null){
				if (position.getGps().getLatitude() != null)
					toReturn.setLatitude(position.getGps().getLatitude()
							.doubleValue());

				if (position.getGps().getLongitude() != null)
					toReturn.setLongitude(position.getGps().getLongitude()
							.doubleValue());

				toReturn.setAltitude(position.getGps().getAltitude());

				if (position.getGps().getCourse() != null)
					toReturn.setCourse(position.getGps().getCourse().ordinal());
				toReturn.setSpeed(position.getGps().getSpeed());

				if (position.getGps().getFlagState() != null)
					toReturn.setMoving(position.getGps().getFlagState().getMoving());
				
				toReturn.setDate(position.getGps().getDate());
			}

			if(position.getHardwareMonitor() != null && position.getHardwareMonitor().getInputs() != null){
				toReturn.setIgnition(position.getHardwareMonitor().getInputs().getIgnition());
				toReturn.setPanic(position.getHardwareMonitor().getInputs().getPanic());
			}
			
		}
		return toReturn;
	}

	public static List<TrackerData> parseTk10xkData(List<TK10XMessage> messages) {
		if (messages == null)
			return Collections.emptyList();
		List<TrackerData> toReturn = new ArrayList<TrackerData>();
		for (TK10XMessage messsage : messages) {
			toReturn.add(parseTk10xkData(messsage));
		}
		return toReturn;
	}

	public static TrackerData parseTk10xkData(TK10XMessage message) {
		TrackerData toReturn = new TrackerData();
		toReturn.setLatitude(message.getLatitude());
		toReturn.setLongitude(message.getLongitude());
		toReturn.setSpeed(message.getSpeed());
		toReturn.setDate(message.getTime());
		return toReturn;
	}
	
	public static List<TrackerData> parseAspicoreData(List<AspicoreMessage> messages) {
		if (messages == null)
			return Collections.emptyList();
		List<TrackerData> toReturn = new ArrayList<TrackerData>();
		for (AspicoreMessage messsage : messages) {
			toReturn.add(parseAspicoreData(messsage));
		}
		return toReturn;
	}

	public static TrackerData parseAspicoreData(AspicoreMessage message) {
		TrackerData toReturn = new TrackerData();
		
		if(message.getPostlat() != null)
			toReturn.setLatitude(message.getPostlat().doubleValue());
		
		if(message.getPostlong() != null)
			toReturn.setLongitude(message.getPostlong().doubleValue());
		
		if(message.getSpeed() != null)
			toReturn.setSpeed(message.getSpeed().doubleValue());
		
		toReturn.setDate(message.getPostime());
		return toReturn;
	}
	
	public static TrackerData parseSkypatrolData(SkypatrolMessage message) {
		TrackerData toReturn = new TrackerData();
		
		if(message.getLatitude() != null)
			toReturn.setLatitude(message.getLatitude().doubleValue());
		
		if(message.getLongitude() != null)
			toReturn.setLongitude(message.getLongitude().doubleValue());
		
		if(message.getSpeed() != null)
			toReturn.setSpeed(message.getSpeed().doubleValue());
		
		if(message.getAltitude() != null)
			toReturn.setAltitude(message.getAltitude().intValue());
		
		toReturn.setDate(message.getGpsUtcTime());
		return toReturn;
	}
	
	public static List<TrackerData> parseSkypatrolData(List<SkypatrolMessage> messages) {
		if (messages == null)
			return Collections.emptyList();
		List<TrackerData> toReturn = new ArrayList<TrackerData>();
		for (SkypatrolMessage messsage : messages) {
			toReturn.add(parseSkypatrolData(messsage));
		}
		return toReturn;
	}
	
	public static List<TrackerData> parseNiagaraHW6Data(List<NiagaraHW06message> messages) {
		if (messages == null)
			return Collections.emptyList();
		List<TrackerData> toReturn = new ArrayList<TrackerData>();
		for (NiagaraHW06message message : messages) {
			toReturn.add(parseNiagaraHW6Data(message));
		}
		return toReturn;
	}

	public static TrackerData parseNiagaraHW6Data(NiagaraHW06message message) {
		TrackerData toReturn = new TrackerData();
		if (message != null) {
			
			toReturn.setLatitude(message.getLatitude());
			toReturn.setLongitude(message.getLongitude());

			if (message.getAltitude() != null)
				toReturn.setAltitude(message.getAltitude().intValue());

			if (message.getSpeed() != null)
				toReturn.setSpeed(message.getSpeed().doubleValue());

			toReturn.setDate(message.getMessageDate());
		}
		return toReturn;
	}

}
