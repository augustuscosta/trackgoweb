package br.com.otgmobile.trackgoweb.model;

import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "usuario")
public class Usuario implements UserDetails{


	/**
	 * 
	 */
	private static final long serialVersionUID = -1696455750924345128L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="usuario_sequence", sequenceName="usuario_sequence")
	private Long id;
	
	private String name;
	@Column(unique=true)
	private String username;
	@Column(unique=true)
	private String email;
	private String password;
	private String mobileSessionId;
	private Boolean enabled;
	private Boolean mailNotifications;
	private Boolean smsNotifications;
	private String mobilephoneNumber;
	
	@Transient
	private Boolean deviceAddEdit;
	@Transient
	private Boolean cercaAddEdit;
	@Transient
	private Boolean rotaAddEdit;
	@Transient
	private Boolean eventosAddEdit;
	@Transient
	private Boolean pontoDeInteresseAddEdit;
	@Transient
	private Boolean veiculoAddEdit;
	@Transient
	private Boolean usuariosAddEdit;
	@Transient
	private Boolean gruposAddEdit;
	
	
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "grupo_usuario", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "grupo_id"))
	private List<Grupo> grupos;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	public String getUserRole(){
		String role = "";
		for(Grupo grupo : getGrupos()){
			role = grupo.getPermissao().toString();
			if(grupo.getPermissao().equals(Permissao.ROLE_ADMINISTRATOR)){
				return role;
			}
		}
		return role;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		  
	   return  null;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	@Override
	public boolean isEnabled() {
		return enabled;
	}
	public List<Grupo> getGrupos() {
		return grupos;
	}
	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}
	public String getMobileSessionId() {
		return mobileSessionId;
	}
	public void setMobileSessionId(String mobileSessionId) {
		this.mobileSessionId = mobileSessionId;
	}
	public Boolean getMailNotifications() {
		return mailNotifications;
	}
	public void setMailNotifications(Boolean mailNotifications) {
		this.mailNotifications = mailNotifications;
	}
	public Boolean getSmsNotifications() {
		return smsNotifications;
	}
	public void setSmsNotifications(Boolean smsNotifications) {
		this.smsNotifications = smsNotifications;
	}
	public String getMobilephoneNumber() {
		return mobilephoneNumber;
	}
	public void setMobilephoneNumber(String mobilephoneNumber) {
		this.mobilephoneNumber = mobilephoneNumber;
	}
	

	public boolean getDeviceAddEditFromGroups(){
		if(grupos == null)
			return false;
		
		for(Grupo grupo: grupos){
			if(Permissao.ROLE_ADMINISTRATOR.equals(grupo.getPermissao()))
				return true;
			if(grupo.getDeviceAddEdit())
				return true;
		}
		
		return false;
	}
	
	public boolean getCercaAddEditFromGroups(){
		if(grupos == null)
			return false;
		
		for(Grupo grupo: grupos){
			if(Permissao.ROLE_ADMINISTRATOR.equals(grupo.getPermissao()))
				return true;
			if(grupo.getCercaAddEdit())
				return true;
		}
		
		return false;
	}
	
	public boolean getRotaAddEditFromGroups(){
		if(grupos == null)
			return false;
		
		for(Grupo grupo: grupos){
			if(Permissao.ROLE_ADMINISTRATOR.equals(grupo.getPermissao()))
				return true;
			if(grupo.getRotaAddEdit())
				return true;
		}
		
		return false;
	}
	
	public boolean getEventosAddEditFromGroups(){
		if(grupos == null)
			return false;
		
		for(Grupo grupo: grupos){
			if(Permissao.ROLE_ADMINISTRATOR.equals(grupo.getPermissao()))
				return true;
			if(grupo.getEventosAddEdit())
				return true;
		}
		
		return false;
	}
	
	public boolean getPontoDeInteresseAddEditFromGroups(){
		if(grupos == null)
			return false;
		
		for(Grupo grupo: grupos){
			if(Permissao.ROLE_ADMINISTRATOR.equals(grupo.getPermissao()))
				return true;
			if(grupo.getPontoDeInteresseAddEdit())
				return true;
		}
		
		return false;
	}
	
	public boolean getVeiculoAddEditFromGroups(){
		if(grupos == null)
			return false;
		
		for(Grupo grupo: grupos){
			if(Permissao.ROLE_ADMINISTRATOR.equals(grupo.getPermissao()))
				return true;
			if(grupo.getVeiculoAddEdit())
				return true;
		}
		
		return false;
	}
	
	public boolean getGruposAddEditFromGroups(){
		if(grupos == null)
			return false;
		
		for(Grupo grupo: grupos){
			if(Permissao.ROLE_ADMINISTRATOR.equals(grupo.getPermissao()))
				return true;
			if(grupo.getGruposAddEdit())
				return true;
		}
		
		return false;
	}
	
	public boolean getUsuariosAddEditFromGroups(){
		if(grupos == null)
			return false;
		
		for(Grupo grupo: grupos){
			if(Permissao.ROLE_ADMINISTRATOR.equals(grupo.getPermissao()))
				return true;
			if(grupo.getUsuariosAddEdit())
				return true;
		}
		
		return false;
	}
	public Boolean getDeviceAddEdit() {
		return deviceAddEdit;
	}
	public void setDeviceAddEdit(Boolean deviceAddEdit) {
		this.deviceAddEdit = deviceAddEdit;
	}
	public Boolean getCercaAddEdit() {
		return cercaAddEdit;
	}
	public void setCercaAddEdit(Boolean cercaAddEdit) {
		this.cercaAddEdit = cercaAddEdit;
	}
	public Boolean getRotaAddEdit() {
		return rotaAddEdit;
	}
	public void setRotaAddEdit(Boolean rotaAddEdit) {
		this.rotaAddEdit = rotaAddEdit;
	}
	public Boolean getEventosAddEdit() {
		return eventosAddEdit;
	}
	public void setEventosAddEdit(Boolean eventosAddEdit) {
		this.eventosAddEdit = eventosAddEdit;
	}
	public Boolean getPontoDeInteresseAddEdit() {
		return pontoDeInteresseAddEdit;
	}
	public void setPontoDeInteresseAddEdit(Boolean pontoDeInteresseAddEdit) {
		this.pontoDeInteresseAddEdit = pontoDeInteresseAddEdit;
	}
	public Boolean getVeiculoAddEdit() {
		return veiculoAddEdit;
	}
	public void setVeiculoAddEdit(Boolean veiculoAddEdit) {
		this.veiculoAddEdit = veiculoAddEdit;
	}
	public Boolean getUsuariosAddEdit() {
		return usuariosAddEdit;
	}
	public void setUsuariosAddEdit(Boolean usuariosAddEdit) {
		this.usuariosAddEdit = usuariosAddEdit;
	}
	public Boolean getGruposAddEdit() {
		return gruposAddEdit;
	}
	public void setGruposAddEdit(Boolean gruposAddEdit) {
		this.gruposAddEdit = gruposAddEdit;
	}
}
