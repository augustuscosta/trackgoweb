package br.com.otgmobile.trackgoweb.model;

public enum DeviceType {

	TK10X,
	ASPICORE,
	MAXTRACK,
	SKYPATROL,
	NIAGARA2,
	NIAGARAHW6
}
