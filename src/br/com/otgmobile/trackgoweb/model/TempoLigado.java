package br.com.otgmobile.trackgoweb.model;

public class TempoLigado {
	
	private Device device;
	
	private TrackerData firstPosition;
	
	private TrackerData lastPosition;

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}


	public TrackerData getFirstPosition() {
		return firstPosition;
	}

	public void setFirstPosition(TrackerData firstPosition) {
		this.firstPosition = firstPosition;
	}

	public TrackerData getLastPosition() {
		return lastPosition;
	}

	public void setLastPosition(TrackerData lastPosition) {
		this.lastPosition = lastPosition;
	}
	

}
