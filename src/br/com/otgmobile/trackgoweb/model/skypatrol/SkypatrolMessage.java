package br.com.otgmobile.trackgoweb.model.skypatrol;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "skypatrol")
public class SkypatrolMessage{

	@Id
	private Long id;
	private String reportItemMask;
	private Boolean sackEnabled;
	private SkypatrolMessageType messageType;
	private Integer protocolVersion;
	private String uniqueID;
	private String deviceName;
	private Integer reportId;
	private Integer reportType;
	private Integer number;
	private Integer gpsAccuracy;
	private Double speed;
	private Integer azimuth;
	private Double altitude;
	private Double longitude;
	private Double latitude;
	private Date gpsUtcTime;
	private Integer mobileCountryCode;
	private Integer mobileNetworkCode;
	private Integer locationAreaCode;
	private Integer cellId;
	private Integer batteryPercentage;
	private Date sendTime;
	private Integer countNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReportItemMask() {
		return reportItemMask;
	}

	public void setReportItemMask(String reportItemMask) {
		this.reportItemMask = reportItemMask;
	}

	public Boolean getSackEnabled() {
		return sackEnabled;
	}

	public void setSackEnabled(Boolean sackEnabled) {
		this.sackEnabled = sackEnabled;
	}

	public SkypatrolMessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(SkypatrolMessageType messageType) {
		this.messageType = messageType;
	}

	public Integer getReportId() {
		return reportId;
	}

	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}

	public Integer getReportType() {
		return reportType;
	}

	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getGpsAccuracy() {
		return gpsAccuracy;
	}

	public void setGpsAccuracy(Integer gpsAccuracy) {
		this.gpsAccuracy = gpsAccuracy;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Integer getAzimuth() {
		return azimuth;
	}

	public void setAzimuth(Integer azimuth) {
		this.azimuth = azimuth;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Date getGpsUtcTime() {
		return gpsUtcTime;
	}

	public void setGpsUtcTime(Date gpsUtcTime) {
		this.gpsUtcTime = gpsUtcTime;
	}

	public Integer getMobileCountryCode() {
		return mobileCountryCode;
	}

	public void setMobileCountryCode(Integer mobileCountryCode) {
		this.mobileCountryCode = mobileCountryCode;
	}

	public Integer getMobileNetworkCode() {
		return mobileNetworkCode;
	}

	public void setMobileNetworkCode(Integer mobileNetworkCode) {
		this.mobileNetworkCode = mobileNetworkCode;
	}

	public Integer getLocationAreaCode() {
		return locationAreaCode;
	}

	public void setLocationAreaCode(Integer locationAreaCode) {
		this.locationAreaCode = locationAreaCode;
	}

	public Integer getCellId() {
		return cellId;
	}

	public void setCellId(Integer cellId) {
		this.cellId = cellId;
	}

	public Integer getBatteryPercentage() {
		return batteryPercentage;
	}

	public void setBatteryPercentage(Integer batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}
	
	public Integer getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(Integer protocolVersion) {
		this.protocolVersion = protocolVersion;
	}
	
	public String getUniqueID() {
		return uniqueID;
	}

	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public Integer getCountNumber() {
		return countNumber;
	}

	public void setCountNumber(Integer countNumber) {
		this.countNumber = countNumber;
	}

	public SkypatrolReportType getReportTypeFormated() {
		if (reportType == null && messageType == null)
			return SkypatrolReportType.getValue(reportType, messageType);
		return SkypatrolReportType.OUTRAS_MENSAGENS;
	}

}
