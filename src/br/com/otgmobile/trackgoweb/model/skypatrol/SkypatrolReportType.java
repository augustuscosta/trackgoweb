package br.com.otgmobile.trackgoweb.model.skypatrol;

public enum SkypatrolReportType {

	//For GTGEO
	SAINDO_DA_GEO_REFERENCIA(0),
	ENTRANDO_NA_GEO_REFERENCIA(1),
	//For GTSPD
	FORA_DO_INTERVALO_DE_VELOCIDADE(0),
	DENTRO_DO_INTERVALO_DE_VELOCIDADE(1),
	//For GTNMR
	MUDOU_DE_MOVIMENTO_PARA_PARADO(0),
	MUDOU_DE_PARADO_PARA_MOVIMENTO(1),
	//For GTGCR
	DESABILATOU_GEO_REFERENCIA_0(0),
	ENTROU_GEO_REFERENCIA_0(1),
	SAIU_GEO_REFERENCIA_0(2),
	ENTROU_OU_SAIU_GEO_REFERENCIA_0(3),
	//For GTSTT
	IGNICAO_LIGADA_PARADO(21),
	IGNICAO_LIGADA_MOVIMENTO(22),
	IGNICAO_DESLIGADA_PARADO(41),
	IGNICAO_DESLIGADA_MOVIMENTO(42),
	//For GTSWG
	DESABILATOU_GEO_REFERENCIA_0_2(0),
	HABILITOU_GEO_REFERENCIA_0_2(1),
	// Others
	OUTRAS_MENSAGENS(0);
	
	private int value;
	
	private SkypatrolReportType(int value){
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
	
	public static SkypatrolReportType getValue(int value,SkypatrolMessageType messageType){
		if(messageType == SkypatrolMessageType.GTGEO){
			return getValueFromGTGEO(value);
		}
		if(messageType == SkypatrolMessageType.GTSPD){
			return getValueFromGTSPD(value);
		}
		if(messageType == SkypatrolMessageType.GTNMR){
			return getValueFromGTNMR(value);
		}
		if(messageType == SkypatrolMessageType.GTGCR){
			return getValueFromGTGCR(value);
		}
		if(messageType == SkypatrolMessageType.GTSTT){
			return getValueFromGTSTT(value);
		}
		if(messageType == SkypatrolMessageType.GTSWG){
			return getValueFromGTSWG(value);
		}
		return OUTRAS_MENSAGENS;
	}

	private static SkypatrolReportType getValueFromGTSWG(int value) {
		if(value == 0)
			return DESABILATOU_GEO_REFERENCIA_0_2;
		if(value == 1)
			return HABILITOU_GEO_REFERENCIA_0_2;
		return OUTRAS_MENSAGENS;
	}

	private static SkypatrolReportType getValueFromGTSTT(int value) {
		if(value == 21)
			return IGNICAO_LIGADA_PARADO;
		if(value == 22)
			return IGNICAO_LIGADA_MOVIMENTO;
		if(value == 41)
			return IGNICAO_DESLIGADA_PARADO;
		if(value == 42)
			return IGNICAO_DESLIGADA_MOVIMENTO;
		return OUTRAS_MENSAGENS;
	}

	private static SkypatrolReportType getValueFromGTGCR(int value) {
		if(value == 0)
			return DESABILATOU_GEO_REFERENCIA_0;
		if(value == 1)
			return ENTROU_GEO_REFERENCIA_0;
		if(value == 2)
			return SAIU_GEO_REFERENCIA_0;
		if(value == 3)
			return ENTROU_OU_SAIU_GEO_REFERENCIA_0;
		return OUTRAS_MENSAGENS;
	}

	private static SkypatrolReportType getValueFromGTNMR(int value) {
		if(value == 0)
			return MUDOU_DE_MOVIMENTO_PARA_PARADO;
		if(value == 1)
			return MUDOU_DE_PARADO_PARA_MOVIMENTO;
		return OUTRAS_MENSAGENS;
	}

	private static SkypatrolReportType getValueFromGTSPD(int value) {
		if(value == 0)
			return FORA_DO_INTERVALO_DE_VELOCIDADE;
		if(value == 1)
			return DENTRO_DO_INTERVALO_DE_VELOCIDADE;
		return OUTRAS_MENSAGENS;
	}

	private static SkypatrolReportType getValueFromGTGEO(int value) {
		if(value == 0)
			return SAINDO_DA_GEO_REFERENCIA;
		if(value == 1)
			return ENTRANDO_NA_GEO_REFERENCIA;
		return OUTRAS_MENSAGENS;
	}
	
}
