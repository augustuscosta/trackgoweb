package br.com.otgmobile.trackgoweb.model;

import java.util.Date;

public class TrackerData {
	
	private Double latitude;
	private Double longitude;
	private Integer altitude;
	private Integer course;
	private Double speed;
	private Boolean moving;
	private Date date;
	private Boolean ignition;
	private Boolean panic;
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Integer getAltitude() {
		return altitude;
	}
	public void setAltitude(Integer altitude) {
		this.altitude = altitude;
	}
	public Integer getCourse() {
		return course;
	}
	public void setCourse(Integer course) {
		this.course = course;
	}
	public Double getSpeed() {
		return speed;
	}
	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	public Boolean getMoving() {
		return moving;
	}
	public void setMoving(Boolean moving) {
		this.moving = moving;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Boolean getIgnition() {
		return ignition;
	}
	public void setIgnition(Boolean ignition) {
		this.ignition = ignition;
	}
	public Boolean getPanic() {
		return panic;
	}
	public void setPanic(Boolean panic) {
		this.panic = panic;
	}
	
}
