package br.com.otgmobile.trackgoweb.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.otgmobile.trackgoweb.model.events.Event;
import br.com.otgmobile.trackgoweb.model.events.EventHistoryDetail;
import br.com.otgmobile.trackgoweb.model.maxtrack.ProtocoloComando;

@Entity
@Table(name = "device")
public class Device {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="device_sequence", sequenceName="device_sequence")
	private Long id;
	
	@Column(unique = true)
	private String code;
	private String name;
	private String description;
	private Boolean enabled;
	private DeviceType deviceType;
	
	@Column(unique = true)
	private String chip;
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "event_device", joinColumns = @JoinColumn(name = "device_id"), inverseJoinColumns = @JoinColumn(name = "event_id"))
	private List<Event> events;
	@OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE})
	private List<EventHistoryDetail> eventHistoryDetails;
	@Enumerated(EnumType.STRING)
	private ProtocoloComando protocol;//for maxtrack devices
	@ManyToOne(fetch=FetchType.LAZY,cascade=CascadeType.MERGE)
    @JoinColumn(name="veiculo_id")
    private Veiculo veiculo;
	@Transient
	private TrackerData lastPosition;
	@Transient
	private List<TrackerData> positions;
	
	@ManyToMany(cascade=CascadeType.MERGE,fetch=FetchType.EAGER)
	@JoinTable(name = "grupo_device", joinColumns = @JoinColumn(name = "device_id"), inverseJoinColumns = @JoinColumn(name = "grupo_id"))
	private List<Grupo> grupos;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public DeviceType getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}
	public String getChip() {
		return chip;
	}
	public void setChip(String chip) {
		this.chip = chip;
	}
	public List<EventHistoryDetail> getEventHistoryDetails() {
		return eventHistoryDetails;
	}
	public void setEventHistoryDetails(List<EventHistoryDetail> eventHistoryDetails) {
		this.eventHistoryDetails = eventHistoryDetails;
	}
	public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	public ProtocoloComando getProtocol() {
		return protocol;
	}
	public void setProtocol(ProtocoloComando protocol) {
		this.protocol = protocol;
	}
	public TrackerData getLastPosition() {
		return lastPosition;
	}
	public void setLastPosition(TrackerData lastPosition) {
		this.lastPosition = lastPosition;
	}
	public List<TrackerData> getPositions() {
		return positions;
	}
	public void setPositions(List<TrackerData> positions) {
		this.positions = positions;
	}
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public List<Grupo> getGrupos() {
		return grupos;
	}
	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}
	
}
