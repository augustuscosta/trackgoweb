package br.com.otgmobile.trackgoweb.model.niagarahw6;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="niagarahw06_server")
public class ComandoNiagaraHw06 {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="nigrahw06_server_sequence", sequenceName="nigrahw06_server_sequence")
	private Long id;
	
	private String productIdentification = "55";
	private String firmWareVersion = "21";
	
	@Enumerated(EnumType.STRING)
	private NiagaraHW06ServerMessageType serverMessageType;
	
	@Column(name="sequencecontrolnumber",unique=false, nullable=false)
	private Integer sequenceControlNumber = 0;
	
	private String productserialnumber;
	
	@OneToOne(cascade = CascadeType.ALL)
	private NiagraHw06APNConfiguration apnConfiguration;
	
	@OneToOne(cascade = CascadeType.ALL)
	private NiagaraServerConfiguration serverConfiguration;
	private Boolean activate;
	private boolean sent;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getProductIdentification() {
		return productIdentification;
	}
	
	public void setProductIdentification(String productIdentification) {
		this.productIdentification = productIdentification;
	}
	
	public NiagraHw06APNConfiguration getApnConfiguration() {
		return apnConfiguration;
	}

	public void setApnConfiguration(NiagraHw06APNConfiguration apnConfiguration) {
		this.apnConfiguration = apnConfiguration;
	}

	public NiagaraServerConfiguration getServerConfiguration() {
		return serverConfiguration;
	}

	public void setServerConfiguration(
			NiagaraServerConfiguration serverConfiguration) {
		this.serverConfiguration = serverConfiguration;
	}

	public String getFirmWareVersion() {
		return firmWareVersion;
	}
	
	public void setFirmWareVersion(String firmWareVersion) {
		this.firmWareVersion = firmWareVersion;
	}
	public String getProductserialnumber() {
		return productserialnumber;
	}
	
	public void setProductserialnumber(String productserialnumber) {
		this.productserialnumber = productserialnumber;
	}
	public Integer getSequenceControlNumber() {
		return sequenceControlNumber;
	}
	
	public void setSequenceControlNumber(Integer sequenceControlNumber) {
		this.sequenceControlNumber = sequenceControlNumber;
	}
	
	public NiagaraHW06ServerMessageType getServerMessageType() {
		return serverMessageType;
	}
	
	public void setServerMessageType(int serverMessageTypeValue) {
		this.serverMessageType = NiagaraHW06ServerMessageType.getValue(serverMessageTypeValue);
	}

	
	public boolean isActivate() {
		return activate;
	}

	public void setActivate(Boolean activate) {
		this.activate = activate;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(Boolean sent) {
		this.sent = sent;
	}

}
