package br.com.otgmobile.trackgoweb.model.niagarahw6;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="niagarahw06")
public class NiagaraHW06message{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="nigrahw06_ser_sequence", sequenceName="nigrahw06_sequence")
	private Long id;
	
	// Message Header
	private String productIdentification;
	private String firmWareVersion;
	private String productserialnumber;
	
	@Enumerated(EnumType.STRING)
	private NiagaraHw06MessageType messageType;
	private Integer sequenceControlNumber;
	
	//Message Body
	private Date messageDate;
	private Date gpsDataDate;
	private Double latitude;
	private Double longitude;
	private Integer speed;
	private Integer direction;
	private Double altitude;
	private Integer gpsStatus;
	private Integer numberOfSatelites;
	
	@Enumerated(EnumType.STRING)
	private NiagaraHw06SensorStatus sensorStatus;
	
	@Enumerated(EnumType.STRING)
	private NiagaraHw06ActuatorStatus actuatorStatus;
	private Double odometer;
	
	@Enumerated(EnumType.STRING)
	private NiagaraHw06WarningMessageType warning;
	
 	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getProductIdentification() {
		return productIdentification;
	}
	
	public void setProductIdentification(String productIdentification) {
		this.productIdentification = productIdentification;
	}
	
	public String getFirmWareVersion() {
		return firmWareVersion;
	}
	
	public void setFirmWareVersion(String firmWareVersion) {
		this.firmWareVersion = firmWareVersion;
	}
	
	public String getProductserialnumber() {
		return productserialnumber;
	}

	public void setProductserialnumber(String productserialnumber) {
		this.productserialnumber = productserialnumber;
	}

	public NiagaraHw06MessageType getMessageType() {
		return messageType;
	}
	
	public void setMessageType(Integer messageType) {
		this.messageType = NiagaraHw06MessageType.getValue(messageType);
	}
	
	public Integer getSequenceControlNumber() {
		return sequenceControlNumber;
	}
	
	public void setSequenceControlNumber(Integer sequenceControlNumber) {
		this.sequenceControlNumber = sequenceControlNumber;
	}
	
	public Date getMessageDate() {
		return messageDate;
	}
	
	public void setMessageDate(Date messageDate) {
		this.messageDate = messageDate;
	}
	
	public Date getGpsDataDate() {
		return gpsDataDate;
	}
	
	public void setGpsDataDate(Date gpsDataDate) {
		this.gpsDataDate = gpsDataDate;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Integer getSpeed1() {
		return speed;
	}
	
	public void setSpeed(Integer speed) {
		this.speed = speed;
	}
	
	public Integer getDirection() {
		return direction;
	}
	
	public void setDirection(Integer direction) {
		this.direction = direction;
	}
	
	public Double getAltitude1() {
		return altitude;
	}
	
	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}
	
	public Integer getGpsStatus() {
		return gpsStatus;
	}
	
	public void setGpsStatus(Integer gpsStatus) {
		this.gpsStatus = gpsStatus;
	}
	
	public Integer getNumberOfSatelites() {
		return numberOfSatelites;
	}
	
	public void setNumberOfSatelites(Integer numberOfSatelites) {
		this.numberOfSatelites = numberOfSatelites;
	}
	
	public NiagaraHw06SensorStatus getSensorStatus() {
		return sensorStatus;
	}
	
	public void setSensorStatus(NiagaraHw06SensorStatus sensorStatus) {
		this.sensorStatus = sensorStatus;
	}
	
	public NiagaraHw06ActuatorStatus getActuatorStatus() {
		return actuatorStatus;
	}
	
	public void setActuatorStatus(NiagaraHw06ActuatorStatus actuatorStatus) {
		this.actuatorStatus = actuatorStatus;
	}
	
	public Double getOdometer() {
		return odometer;
	}
	
	public void setOdometer(Double odometer) {
		this.odometer = odometer;
	}

	public NiagaraHw06WarningMessageType getWarning() {
		return warning;
	}

	public void setWarning(int value1, int value2) {
		this.warning  = NiagaraHw06WarningMessageType.getValue(value1,value2);
	}

	public Double getCourse() {
		 
		return null;
	}

	public Boolean getMoving() {
		return getSpeed() > 3;
	}

	public Date getDate() {
		return getGpsDataDate();
	}

	public Boolean getIgnition() {
		return null;
	}

	public Boolean getPanic() {
		if(getWarning() == NiagaraHw06WarningMessageType.PANIC_BUTTON_STATE_ON){
			return true;
		}
	
		return false;
	}

	public Integer getRpm() {
		return null;
	}

	public Boolean getOpenedDoor() {
		return null;
	}

	public Boolean getEngineBlocked() {
		if(getWarning() == NiagaraHw06WarningMessageType.ENGINE_BLOCKED_STATE_ON){
			return true;
		}
		
		return false;
	}

	public String getCode() {
		return getProductIdentification();
	}
	
	public Double getSpeed(){
		if(getSpeed1() == null) return null;
		int temp = getSpeed1();
		return (double) temp;
	}
	
	public Integer getAltitude() {
		if(altitude == null){
			return null;
		}
		
		double a = getAltitude1();
		return (int) a;
	}
}
