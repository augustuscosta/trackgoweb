package br.com.otgmobile.trackgoweb.model.niagarahw6;

public enum NiagaraHW06ServerMessageType {
	
	CONFIGURATE_SLEEP_MODE(118),
	ENABLE_WARNING_MESSAGES(119),
	CONFIGURE_KEEP_ALIVE_MESSAGE(120),
	DOP_CONFIGURATION(121),
	ISP_CONFIGURATION_SETTINGS(122),
	SERVER_IP_CONFIGURATION(123),
	TRACKER_TIME_CONFIGURATION(124),
	DEEP_SLEEP_TRANSTION_TIME_CONFIGURATION(114),
	START_EMERGENCY_TRACKING_PERIOD(113),
	REQUEST_IMMEDIATE_POSITION(117),
	WAKE_UP_SOFT_SLEEP_MODE(116),
	RETURN_SOFT_SLEEP_MODE(115),
	ACTUATOR_TURN_ON_AND_OFF(131),
	RESET_ODOMETER(132),
	TELEMETRY_TRANSPARENT_COMMAND(135),
	SERVER_ACK(133),
	SERVER_NACK(134),
	START_A_NEW_FIRMWARE_UPDATE_DOWNLOAD(64),
	ABORT_FIRMWARE_DOWNLOAD(65),
	NEW_DATA_PACKAGE(66),
	FIRMWARE_SWITCH(67),
	QUERY_FIRMWARE_VERSION(68);
	
	private int value;
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	private NiagaraHW06ServerMessageType(int value) {
		this.value = value;
	}
	
	public static NiagaraHW06ServerMessageType getValue(int value){
		for (NiagaraHW06ServerMessageType serverMessageType : NiagaraHW06ServerMessageType.values()) {
			if(serverMessageType.getValue() == value){
				return serverMessageType;
			}
		}
		
		return null;
	}
	

}
