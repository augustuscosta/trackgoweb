package br.com.otgmobile.trackgoweb.model.niagarahw6;


public enum NiagaraHw06MessageType {
	
	RESET(0),
	IGNITION_ON(1),
	IGNITION_OFF(16),
	POWER_SAVE_MODE(2),
	NORMAL_TRACKING_MESSAGE(3),
	SERVER_REQUESTED_TRACKING(4),
	WARNING_MESSAGE(102),
	DEEP_SLEEP_MODE(8),
	POSITION_WITH_TELEMETRY_MESSAGE(9),
	TELEMETRY_TRANSPARENT_DATA(10),
	ACK(5),
	NACK(6),
	KEEP_ALIVE(7),
	NEW_DATA_PACKAGE_REQUEST(34),
	DOWNLOAD_COMPLETE(35),
	DOWNLOAD_ABORTED(36),
	FIRMWARE_STATUS_INFORMATION(37);
	
	private int value;

	NiagaraHw06MessageType(int value){
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public static NiagaraHw06MessageType getValue(int value){
		for (NiagaraHw06MessageType messageType : NiagaraHw06MessageType.values()) {
			if (value == messageType.getValue()) {
				return messageType;
			}
		}
		
		return null;
	}

}
