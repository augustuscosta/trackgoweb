package br.com.otgmobile.trackgoweb.model.niagarahw6;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="niagarahw06_server_config")
public class NiagaraServerConfiguration {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="nigrahw06_apn_sequence", sequenceName="nigrahw06_apn_sequence")
	private Long id;
	
	private String serverIP;
	
	private String serverPort;
	
	private String backupServerIP;
	
	private String backupServerPort;
	
	private String backupServer2IP;
	
	private String backupServer2Port;
	
	private String backupServer3IP;
	
	private String backupServer3Port;
	
	@OneToOne(cascade = CascadeType.ALL)
	private ComandoNiagaraHw06 comando;
	
	public String getServerIP() {
		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	public String getBackupServerIP() {
		return backupServerIP;
	}

	public void setBackupServerIP(String backupServerIP) {
		this.backupServerIP = backupServerIP;
	}

	public String getBackupServerPort() {
		return backupServerPort;
	}

	public void setBackupServerPort(String backupServerPort) {
		this.backupServerPort = backupServerPort;
	}

	public String getBackupServer2IP() {
		return backupServer2IP;
	}

	public void setBackupServer2IP(String backupServer2IP) {
		this.backupServer2IP = backupServer2IP;
	}

	public String getBackupServer2Port() {
		return backupServer2Port;
	}

	public void setBackupServer2Port(String backupServer2Port) {
		this.backupServer2Port = backupServer2Port;
	}

	public String getBackupServer3IP() {
		return backupServer3IP;
	}

	public void setBackupServer3IP(String backupServer3IP) {
		this.backupServer3IP = backupServer3IP;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBackupServer3Port() {
		return backupServer3Port;
	}

	public void setBackupServer3Port(String backupServer3Port) {
		this.backupServer3Port = backupServer3Port;
	}
	public ComandoNiagaraHw06 getComando() {
		return comando;
	}
	public void setComando(ComandoNiagaraHw06 comando) {
		this.comando = comando;
	}

}
