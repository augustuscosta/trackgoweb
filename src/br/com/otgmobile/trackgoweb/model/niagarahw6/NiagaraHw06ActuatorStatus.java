package br.com.otgmobile.trackgoweb.model.niagarahw6;

public enum NiagaraHw06ActuatorStatus {
	
	ENGINE_BLOCK_OFF(0),
	ENGINE_BLOCK_ON(1);
	
	private int engineBit;
	
	 public int getEngineBit() {
		return engineBit;
	}

	public void setEngineBit(int engineBit) {
		this.engineBit = engineBit;
	}

	NiagaraHw06ActuatorStatus(int engineBit) {
		 this.engineBit = engineBit;
	}
	
	public static NiagaraHw06ActuatorStatus getValue(int engineBit){
		for (NiagaraHw06ActuatorStatus status : NiagaraHw06ActuatorStatus.values()) {
			if (status.getEngineBit() == engineBit) {
				return status;
			}
		}
		
		return null;
	}

}
