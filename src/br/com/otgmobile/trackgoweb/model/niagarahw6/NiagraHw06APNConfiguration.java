package br.com.otgmobile.trackgoweb.model.niagarahw6;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="niagarahw06_apn_config")
public class NiagraHw06APNConfiguration {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="nigrahw06_apn_sequence", sequenceName="nigrahw06_apn_sequence")
	private Long id;
	
	private String apn;

	private String userName;
	
	private String password;
	
	@OneToOne(cascade = CascadeType.ALL)
	private ComandoNiagaraHw06 comando;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApn() {
		return apn;
	}

	public void setApn(String apn) {
		this.apn = apn;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ComandoNiagaraHw06 getComando() {
		return comando;
	}

	public void setComando(ComandoNiagaraHw06 comando) {
		this.comando = comando;
	}
}
