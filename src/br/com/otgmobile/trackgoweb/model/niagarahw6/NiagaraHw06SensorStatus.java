package br.com.otgmobile.trackgoweb.model.niagarahw6;

public enum NiagaraHw06SensorStatus {
	
	IGNITION_OFF_PANIC_OFF(0,0),
	IGNITION_ON_PANIC_OFF(1,0),
	IGNITION_ON_PANIC_ON(1,1),
	IGNITION_OFF_PANIC_ON(0,1);
	
	private int ignitionBit;
	private int panicBit;
	
	public int getIgnitionBit() {
		return ignitionBit;
	}

	public void setIgnitionBit(int ignitionBit) {
		this.ignitionBit = ignitionBit;
	}

	public int getPanicBit() {
		return panicBit;
	}

	public void setPanicBit(int panicBit) {
		this.panicBit = panicBit;
	}

	NiagaraHw06SensorStatus(int ignitionBit,int panicBit) {
		this.ignitionBit = ignitionBit;
		this.panicBit = panicBit;
	}
	
	public static NiagaraHw06SensorStatus getValue(int ignitionBit, int panicBit){
		for (NiagaraHw06SensorStatus status : NiagaraHw06SensorStatus.values()) {
			if(status.getPanicBit() == panicBit && status.getIgnitionBit() == ignitionBit ){
				return status;
			}
		}
		
		return null;
	}

}
