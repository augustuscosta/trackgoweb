package br.com.otgmobile.trackgoweb.model.niagarahw6;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.engine.SessionImplementor;
import org.hibernate.exception.JDBCExceptionHelper;
import org.hibernate.id.SequenceGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SequenceControlNumber extends SequenceGenerator {
	
	private static final Logger log = LoggerFactory.getLogger(SequenceGenerator.class);
	
	@Override
	public Serializable generate(SessionImplementor session, Object obj) {
		Connection connection = session.connection();
		try {
			     
			     			PreparedStatement st = connection
			                        .prepareStatement("SELECT nextval ('sequencecontrolnumber') as nextval");
			     			try {
			     				ResultSet rs = st.executeQuery();
			     				try {
			     					rs.next();
			     					int currentVall = rs.getInt("sequencecontrolnumber");
			     					int result = 0;
			     					if(currentVall <255){
			     						result = currentVall +1;
			     					}
			     					if ( log.isDebugEnabled() ) {
			     						log.debug("Sequence identifier generated: " + result);
			     					}
			     					return result;
			     				}
			     				finally {
			     					rs.close();
			     				}
			     			}
			     			finally {
			     				session.getBatcher().closeStatement(st);
			     			}
			     			
			     		}
			     		catch (SQLException sqle) {
			     			throw JDBCExceptionHelper.convert(	session.getFactory().getSQLExceptionConverter(),
			     					sqle,"could not get next sequence value");
			     		}

	}

	

}
