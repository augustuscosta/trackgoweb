package br.com.otgmobile.trackgoweb.model.niagarahw6;

public enum NiagaraHw06WarningMessageType {
	
	PANIC_BUTTON_STATE_ON(1, 1),
	PANIC_BUTTON_STATE_OFF(1, 0),
	VIOLATED_BATTERY_STATE_ON(2, 1),
	VIOLATED_BATTERY_STATE_OFF(2, 0),
	ENGINE_BLOCKED_STATE_ON(3, 1),
	ENGINE_BLOCKED_STATE_OFF(3, 0),
	EMERGENCY_STATE_ON(9, 1),
	EMERGENCY_STATE_OFF(9, 0);
	
	private int value1;
	private int value2;
	
	public int getValue1() {
		return value1;
	}

	public int getValue2() {
		return value2;
	}
	
	 NiagaraHw06WarningMessageType(int value1, int value2) {
		 this.value1 = value1;
		 this.value2 = value2;
	}
	 
	 public static NiagaraHw06WarningMessageType getValue(int value1, int value2) {
		 for (NiagaraHw06WarningMessageType type : NiagaraHw06WarningMessageType.values()) {
			if(type.getValue1() == value1 && type.getValue2() == value2){
				return type;
			}
		}
		 
		 return null;
	 }

}
