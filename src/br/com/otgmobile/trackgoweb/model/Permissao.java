package br.com.otgmobile.trackgoweb.model;

public enum Permissao {

	ROLE_ADMINISTRATOR,
	ROLE_USER
}
